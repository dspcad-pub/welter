/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <iostream>
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"

bool welt_cpp_util_guarded_execution(welt_cpp_actor *context,
                                      char *descriptor) {

    if (context->enable()) {
        context->invoke();
        //printf("%s visit complete.\n", descriptor);
        return true;
    } else {
        return false;
    }
}

void welt_cpp_util_simple_scheduler(vector<welt_cpp_actor*>& actors, int
actor_start, int actor_count, vector<char*>& descriptors){

    bool progress = false;
    int i = actor_start;

    do {
        progress = 0;
        for (i = actor_start; i < actor_count+actor_start; i++) {
            progress |=
                    welt_cpp_util_guarded_execution(actors[i], descriptors[i]);


        }
    } while (progress);
}
