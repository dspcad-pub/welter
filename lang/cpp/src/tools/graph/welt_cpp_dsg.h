#ifndef _welt_cpp_dsg_h
#define _welt_cpp_dsg_h

#include <stdarg.h>
#include "welt_cpp_graph.h"
#include "welt_cpp_actor.h"

extern "C"{
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_ref_actor.h"
#include "welt_cpp_sca_sch_loop.h"
#include "welt_cpp_sca_static_loop.h"
#include "welt_cpp_rec_actor.h"
#include "welt_cpp_snd_actor.h"

class welt_cpp_dsg:public welt_cpp_graph{
    /* Common elements of dsg graph. */
public:
    /* current index*/
    int ind;
    /* method for adding an actor to dsg */
    void dsg_add_actor(welt_cpp_graph* dsg_graph, welt_cpp_actor* actor ,
            int index);
    /* method for storing the first actor of each SDSGs */
    void set_first_actor( welt_cpp_actor* actor , int actor_index );
    /* deconstruct */
    virtual ~welt_cpp_dsg() = default;

    /* the number of dsg actors*/
    int dsg_count;

    /* the number of threads*/
    int thread_count;

    /* indices of first actors */
    vector<int> first_actors_index;

    /* the number of fifos */
    int dsg_fifo_count;

	virtual void dsg_scheduler(int thread_ind, int thread_num)= 0 ;
};
#endif
