ADD_LIBRARY(welt_cpp_graph
        welt_cpp_graph.cpp
        welt_cpp_dsg.cpp)

INCLUDE_DIRECTORIES(
$ENV{UXWELTER}/lang/c/src/gems/actors/basic
$ENV{UXWELTER}/lang/c/src/gems/actors/common
$ENV{UXWELTER}/lang/c/src/gems/edges
$ENV{UXWELTER}/lang/c/src/tools/runtime
$ENV{UXWELTER}/lang/c/src/tools/graph
$ENV{UXWELTER}/lang/cpp/src/tools/graph
$ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/sca_actors
$ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/cdsg_actors
$ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/ref_actors
$ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/common
$ENV{UXWELTER}/lang/cpp/src/gems/actors/common
$ENV{UXWELTER}/lang/cpp/src/gems/actors/basic
$ENV{UXWELTER}/lang/cpp/src/tools/graph
$ENV{UXWELTER}/lang/cpp/src/tools/runtime
$ENV{UXWELTER}/lang/cpp/src/apps/basic
$ENV{UXWELTER}/lang/cpp/src/utils
$ENV{UXDICELANG}/c/src/util
)
INSTALL(TARGETS welt_cpp_graph DESTINATION .)