#ifndef _welt_cpp_graph_h
#define _welt_cpp_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <vector>
//#include <queue>

extern "C"{
#include "welt_c_fifo.h"
}

#define GRAPH_IN_CONN_DIRECTION  0   /* input port of an actor*/
#define GRAPH_OUT_CONN_DIRECTION 1   /* output port of an actor*/
using namespace std;


class welt_cpp_actor;

class welt_cpp_graph{
public:
    /* Common field elements in the graph. */
    /* A collection of fifos */
    vector<welt_c_fifo_pointer> fifos;
    /* A collection of actor pointers */
    vector<welt_cpp_actor*> actors;
    /* A collection of descriptors */
    vector<char*> descriptors;
    /* The number of actors */
    int actor_count;
    /* The number of fifos */
    int fifo_count;

    /* following two members are not tested */
    vector<welt_cpp_actor*> source_array;
    vector<welt_cpp_actor*> sink_array;

    /* The scheduler method of a welt-cpp graph corresponds to the scheduler of
     * the lightweight/welterweight dataflow API. Every actor must have a
     * scheduler function that executes the dataflow graph */
    virtual void scheduler() = 0;
    
    /* The destructor destroys all of the actors and edges that the graph
    contains */
    ~welt_cpp_graph();

    void add_actor();
    void add_connection(
            welt_cpp_actor *context, int port_index,
            int direction);
    void update_connection(
            welt_cpp_actor *context, int port_index,
            int fifo_index, int direction);
    void set_actor(int index, welt_cpp_actor *actor_context);
    void set_fifo(int index, welt_c_fifo_pointer fifo);
    welt_c_fifo_pointer get_fifo(int index);
    void set_actor_count(int count);
    void set_fifo_count(int count);
    int graph_actor_count();
    int graph_fifo_count();
    welt_cpp_actor* get_actor(int index);
    vector<welt_cpp_actor*>* get_sink_array();
    vector<welt_cpp_actor*>* get_source_array();
    void add_fifo();
    };
#endif // _welt_cpp_graph_h
