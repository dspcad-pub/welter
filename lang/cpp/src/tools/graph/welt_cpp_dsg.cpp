#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_util.h"
}
#include "welt_cpp_dsg.h"

void welt_cpp_dsg::dsg_add_actor(welt_cpp_graph* dsg_graph,
        welt_cpp_actor* dsg_actor , int dsg_ind){
    /* set index for the actor */
    dsg_actor->actor_set_index(dsg_ind);
    dsg_actor->connect((welt_cpp_graph*)dsg_graph);
}

void welt_cpp_dsg::set_first_actor( welt_cpp_actor* actor , int actor_index ){
    this->first_actors_index.push_back(actor_index);
    this->ind ++;
}


