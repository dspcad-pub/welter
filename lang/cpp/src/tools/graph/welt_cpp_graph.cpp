/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2016
Maryland DSPCAD Research Group, The University of Maryland at College Park

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

extern "C"{
#include "welt_c_util.h"
}
extern "C"{
#include "welt_c_fifo.h"
}
#include "welt_cpp_graph.h"
#include "welt_cpp_actor.h"
/*Update actor count*/
void welt_cpp_graph::add_actor(){
    this->actor_count++;
}

/*Initialize sink_array and source array*/
void welt_cpp_graph::add_connection(
        welt_cpp_actor *context, int port_index,
        int direction){
    welt_c_fifo_pointer *p;
    int fifo_index;

	p = (welt_c_fifo_pointer *)context->portrefs[port_index];
	fifo_index = (*p)->get_index(*p);


	if(direction == GRAPH_IN_CONN_DIRECTION){
		this->sink_array[fifo_index] = context;

	}else if(direction == GRAPH_OUT_CONN_DIRECTION){
		this->source_array[fifo_index] = context;

	}else{
		fprintf(stderr, "direction of the port is neither input nor output\n");
	}
	return;

}

/*Update port connection in the actor and then the graph*/
void welt_cpp_graph::update_connection(
        welt_cpp_actor *context, int port_index,
        int fifo_index, int direction){
    welt_c_fifo_pointer new_fifo;
    welt_c_fifo_pointer *p;
    int fifo_gr_index;

}

void welt_cpp_graph::set_actor(
            int index, welt_cpp_actor *actor_context){
    this->actors[index] = actor_context;
}


welt_cpp_actor *welt_cpp_graph::get_actor(int index){
    return this->actors[index];
}


void welt_cpp_graph::set_fifo(int index, welt_c_fifo_pointer fifo){
    this->fifos[index] = fifo;
}


welt_c_fifo_pointer welt_cpp_graph::get_fifo(int index){
    return this->fifos[index];
}

void welt_cpp_graph::set_actor_count(int count){
    this->actor_count = count;
}

void welt_cpp_graph::set_fifo_count(int count){
    this->fifo_count = count;
}

int welt_cpp_graph::graph_actor_count(){
    return this->actor_count;
}


int welt_cpp_graph::graph_fifo_count(){
    return this->fifo_count;
}

/*void welt_cpp_graph::welt_cpp_graph_set_scheduler(welt_cpp_graph *graph,
                                lide_c_graph_scheduler_ftype scheduler_ftype){
    graph->scheduler = scheduler_ftype;
    return;
} */


vector<welt_cpp_actor*>* welt_cpp_graph::get_sink_array(){
    return &this->sink_array;
}

vector<welt_cpp_actor*>* welt_cpp_graph::get_source_array(){
    return &this->source_array;
}

void welt_cpp_graph::add_fifo(){
    this->fifo_count++;
}
welt_cpp_graph::~welt_cpp_graph(){
    for (int i =0; i < (this->actors).size(); i++){
        delete this->actors[i];
    }
    for (int i =0; i < (this->fifos).size(); i++){
        welt_c_fifo_free(this->fifos[i]);
    }
}
