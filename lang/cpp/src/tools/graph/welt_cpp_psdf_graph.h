//
// Created by yaesop lee on 10/20/20.
//

#ifndef WELTER_WELT_CPP_PSDF_GRAPH_H
#define WELTER_WELT_CPP_PSDF_GRAPH_H
#define APPGRAPH 0
#define DSG 0

class welt_cpp_graph;
class welt_cpp_psdf_graph{

public:
    void setGraph(welt_cpp_graph graph, int graph_type);
    welt_cpp_graph setGraph(welt_cpp_graph graph, int graph_type);

private:
    vector<welt_cpp_graph *> app_graph;
    vector<welt_cpp_graph *> dsg;


};
#endif //WELTER_WELT_CPP_PSDF_SCHEDULER_H
