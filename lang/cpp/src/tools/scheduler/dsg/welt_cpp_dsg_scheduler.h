#ifndef _welt_cpp_dsg_schedule_h
#define _welt_cpp_dsg_schedule_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern "C"{
#include "welt_c_fifo.h"
}
#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_dsg_actor.h"


bool welt_cpp_util_dsg_dtt_scheduler(welt_cpp_graph* context,
        int start_idx, int end_idx, int actor_count, int *fifo_sink);

bool welt_cpp_util_dsg_optimized_scheduler(
        welt_cpp_graph* context, int start_index, int actor_count,
        vector<welt_cpp_actor*>& sink_array);

#endif
