#include "welt_cpp_dsg_scheduler.h"
#include <iostream>

bool welt_cpp_util_dsg_dtt_scheduler(welt_cpp_graph* dsg_graph,
        int fifo_delay_idx, int fifo_start_idx, int fifo_end_idx,
        int *actor_fifo_sink){

    int fifo_next_idx = fifo_delay_idx;
    welt_c_fifo_pointer fifo_next;
    welt_cpp_dsg_actor* nextDsg;

    while(1){

        /* 1. Find actor and Invoke nextDSG */
        nextDsg=(welt_cpp_dsg_actor*)dsg_graph->
                actors[actor_fifo_sink[fifo_next_idx]];
        nextDsg->invoke();
        
        /* 2. next_fifo = next_fifo + 1; */
        if (fifo_next_idx == fifo_end_idx) fifo_next_idx = fifo_start_idx;
        else fifo_next_idx++;

        /* 3. Check if the next fifo has a token or not. */
        fifo_next = (welt_c_fifo_pointer)dsg_graph->fifos[fifo_next_idx];
        if (welt_c_fifo_capacity(fifo_next)==0) break;
    }

    return true;

}


bool welt_cpp_util_dsg_optimized_scheduler(
        welt_cpp_graph* dsg_graph, int start_index, int actor_count,
        vector<welt_cpp_actor *> &sink_array_ptr){
    int fifo_index;
    welt_c_fifo_pointer last_fifo;
    welt_cpp_dsg_actor* nextDsg;
    vector<welt_cpp_dsg_actor*> dsg_arr;
    dsg_arr.reserve((sink_array_ptr).size());

    for (int i =0; i<(sink_array_ptr).size(); i++){
        (dsg_arr).push_back(0);
    }

    for(int i=0; i < (sink_array_ptr).size(); i++){
        dsg_arr[i] = (welt_cpp_dsg_actor*)((sink_array_ptr)[i]);
    }

    if(start_index >=  actor_count){
        return false;
    }
    nextDsg=(welt_cpp_dsg_actor *)
        (dsg_graph->actors[start_index]);
    
    while(1){
        nextDsg->invoke();
        if(((nextDsg)->last_fifo)==NULL){
            break;
        }
        
        last_fifo = (welt_c_fifo_pointer)(nextDsg->last_fifo);
        fifo_index = welt_c_fifo_unit_size_get_index(
                (welt_c_fifo_unit_size_pointer)last_fifo);

        nextDsg = (welt_cpp_dsg_actor*)((dsg_arr)[fifo_index]);
    }
    return true;
}
