/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_hand_detection_graph.h"

#define NAME_LENGTH 20

using namespace std;
// constructor
welt_cpp_hand_detection_graph::welt_cpp_hand_detection_graph(char *in_img_file,
                                                             char *in_range_file,
                                                             char *out_file) {
    /* token_type */
    int token_type = WELT_CPP_TOKEN_TYPE_PRIMITIVE;
    this->img_file = in_img_file;
    this->range_file = in_range_file;
    this->out_file = out_file;

    this->actor_count = ACTOR_COUNT;
    this->fifo_count = FIFO_COUNT;

    /* Initialize fifos. */
    int token_size;
    token_size = sizeof(void*);
    this->fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
            token_size,FIFO_IMREAD_HANDDETECTION)); 
    this->fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new
            (BUFFER_CAPACITY,sizeof(int),FIFO_RANGESOURCE_HANDDETECTION));
    this->fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new
            (BUFFER_CAPACITY,sizeof(int),FIFO_HANDDETECTION_SINK));            

    /* Create and connect the actors. */
    /* imread actor */
    this->actors.push_back(new welt_cpp_imread(this->fifos[FIFO_IMREAD_HANDDETECTION],
            this->img_file, ACTOR_IMREAD));
    this->descriptors.push_back((char*)"actor img read");
    /* range source actor */
    this->actors.push_back(new welt_cpp_file_source
          (this->fifos[FIFO_RANGESOURCE_HANDDETECTION], 
           this->range_file, ACTOR_RANGESOURCE, token_type));
    this->descriptors.push_back((char*)"actor range source");
    /* hand detection actor*/
    this->actors.push_back(new welt_cpp_hand_detection
                            (this->fifos[FIFO_IMREAD_HANDDETECTION],
                             this->fifos[FIFO_RANGESOURCE_HANDDETECTION],
                             this->fifos[FIFO_HANDDETECTION_SINK],
                             ACTOR_HANDDETECTION));
    this->descriptors.push_back((char*)"actor hand detection");
    /* sink actor */
    actors.push_back(new welt_cpp_file_sink
                             (fifos[FIFO_HANDDETECTION_SINK], this->out_file, ACTOR_SINK));
    descriptors.push_back((char*)"actor sink");

}

void welt_cpp_hand_detection_graph::scheduler() {
     /* static scheduler */
     //welt_cpp_util_simple_scheduler(actors,actor_count,descriptors);   
     actors[ACTOR_IMREAD]->invoke();
     actors[ACTOR_RANGESOURCE]->invoke();
     actors[ACTOR_HANDDETECTION]->invoke();
     actors[ACTOR_SINK]->invoke();
}

// destructor
welt_cpp_hand_detection_graph::~welt_cpp_hand_detection_graph() {
    cout << "delete hand detection graph" << endl;
}

void welt_cpp_hand_detection_graph_terminate(welt_cpp_hand_detection_graph *context){
//    int i;
//    /* Terminate FIFO*/ FIXME
//    for(i = 0; i<context->fifo_count; i++){
//        welt_c_fifo_free((welt_c_fifo_pointer)context->fifos[i]);
//    }

    /* Terminate Actors*/
//   welt_cpp_imread_terminate((welt_cpp_imread*)
//                              context->actors[ACTOR_IMREAD]);
//   welt_cpp_file_source_terminate((welt_cpp_file_source*)
//                                      context->actors[ACTOR_RANGESOURCE]);
//   welt_cpp_hand_detection_terminate((welt_cpp_hand_detection*)
//                                        context->actors[ACTOR_HANDDETECTION]);
//   welt_cpp_file_sink_terminate((welt_cpp_file_sink*)
//                                      context->actors[ACTOR_SINK]);
//    free(context->actors);
//    free(context);
//    delete context;
}
