/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_add_graph.h"

#define NAME_LENGTH 20

using namespace std;
// constructor
welt_cpp_add_graph::welt_cpp_add_graph(char *x_file,
                                       char *y_file,
                                       char *out_file) {
    /* token_type */
    //int token_type = WELT_CPP_TOKEN_TYPE_PRIMITIVE;
    this->x_file = x_file;
    this->y_file = y_file;
    this->out_file = out_file;

    this->actor_count = ACTOR_COUNT;
    this->fifo_count = FIFO_COUNT;

    /* Initialize fifos. */
    int token_size;
    for (int i = 0; i< this->fifo_count; i++){
        this->fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new
                (BUFFER_CAPACITY,sizeof(int),i));
    }
    /* Initialize source array and sink array */
    this->source_array.reserve(this->fifo_count);
    this->sink_array.reserve(this->fifo_count);

    /* Create and connect the actors. */
    /* x source actor */
    this->actors.push_back(new file_source_int
            (this->fifos[FIFO_XSRC_ADD], this->x_file));
    this->descriptors.push_back((char*)"actor x src");
    this->actors[ACTOR_XSOURCE]->connect((welt_cpp_graph*)this);
	this->actors[ACTOR_XSOURCE]->actor_set_index(ACTOR_XSOURCE);
    /* y source actor */
    this->actors.push_back(new file_source_int(this->fifos[FIFO_YSRC_ADD],
                                              y_file));
    descriptors.push_back((char*)"actor y source");
    this->actors[ACTOR_YSOURCE]->connect((welt_cpp_graph*)this);
	this->actors[ACTOR_YSOURCE]->actor_set_index(ACTOR_YSOURCE);

    /* add actor */
    this->actors.push_back(new welt_cpp_add(this->fifos[FIFO_XSRC_ADD],
                                            this->fifos[FIFO_YSRC_ADD],
                                            this->fifos[FIFO_ADD_SNK], ACTOR_ADD));

    descriptors.push_back((char*)"actor add");
    (this->actors[ACTOR_ADD])->connect((welt_cpp_graph*)this);
	this->actors[ACTOR_ADD]->actor_set_index(ACTOR_ADD);

	/* sink actor */
    this->actors.push_back(new file_sink_int
                             (fifos[FIFO_ADD_SNK], this->out_file));
    descriptors.push_back((char*)"actor sink");
    this->actors[ACTOR_SNK]->connect((welt_cpp_graph*)this);
	this->actors[ACTOR_SNK]->actor_set_index(ACTOR_SNK);

}

void welt_cpp_add_graph::scheduler() {
    //welt_cpp_util_simple_scheduler(actors,actor_count,descriptors);

    /* static scheduler */
    actors[ACTOR_XSOURCE]->invoke();
    actors[ACTOR_YSOURCE]->invoke();
    actors[ACTOR_ADD]->invoke();
    actors[ACTOR_SNK]->invoke();
}

// destructor
welt_cpp_add_graph::~welt_cpp_add_graph() {
    cout << "delete add graph" << endl;
   
    for(int i = 0; i< this->actor_count; i++){
        delete actors[i];
    }
    for(int i = 0; i< this->fifo_count; i++){
       welt_c_fifo_free((welt_c_fifo_pointer)this->fifos[i]);
    }
}

void welt_cpp_add_graph_terminate(welt_cpp_add_graph *context){
//    int i;
//    /* Terminate FIFO*/ FIXME
//    for(i = 0; i<context->fifo_count; i++){
//        welt_c_fifo_free((welt_c_fifo_pointer)context->fifos[i]);
//    }

    /* Terminate Actors*/
    //welt_cpp_file_source_terminate((welt_cpp_file_source*)
    //                                  context->actors[ACTOR_XSOURCE]);
    //welt_cpp_file_source_terminate((welt_cpp_file_source*)
    //                                       context->actors[ACTOR_YSOURCE]);
    welt_cpp_add_terminate((welt_cpp_add*)context->actors[ACTOR_ADD]);
    //welt_cpp_file_sink_terminate((welt_cpp_file_sink*)
    //                                  context->actors[ACTOR_SNK]);
//    free(context->actors);
//    free(context);
    delete context;
}
