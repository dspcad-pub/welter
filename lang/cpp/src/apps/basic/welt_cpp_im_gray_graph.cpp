/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_im_gray_graph.h"

#define NAME_LENGTH 20

using namespace std;


// constructor
welt_cpp_im_gray_graph::welt_cpp_im_gray_graph(char *in_img_file ,char *out_file ) {
    /* Initialize fifos. */
    int token_size;
    token_size = sizeof(void*);
    this->fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
            token_size,FIFO_IMREAD_IMGRAY));

    this->fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
            token_size,FIFO_IMGRAY_IMWRITE));
    /* Create and connect the actors. */
    /* imread actor */
    this->actors.push_back(new welt_cpp_imread(this->fifos[FIFO_IMREAD_IMGRAY],
            in_img_file, ACTOR_IMREAD));
    this->descriptors.push_back((char*)"actor img read");

    (this->actors[ACTOR_IMREAD])->connect((welt_cpp_graph*)this);

    this->actors.push_back(new welt_cpp_im_gray(this->fifos[FIFO_IMREAD_IMGRAY],
                                                 this->fifos[FIFO_IMGRAY_IMWRITE], ACTOR_IMGRAY));
    this->descriptors.push_back((char*)"actor gray");
    /*  actor */
    this->actors.push_back(new welt_cpp_imwrite(this->fifos[FIFO_IMGRAY_IMWRITE], out_file,
                                                ACTOR_IMWRITE));
    this->descriptors.push_back((char*)"actor img write");

    /* following two members are initialized but never used */
    this->actor_count = ACTOR_COUNT;
    this->fifo_count = FIFO_COUNT;
}

void welt_cpp_im_gray_graph::scheduler() {
    /* static scheduler */
    //welt_cpp_util_simple_scheduler(this->actors,this->actor_count,this->descriptors);
    this->actors[0]->invoke();
    this->actors[1]->invoke();
    this->actors[2]->invoke();
}

// destructor
welt_cpp_im_gray_graph::~welt_cpp_im_gray_graph() {
    cout << "delete im graph" << endl;
}
void welt_cpp_im_gray_graph_terminate(
        welt_cpp_im_gray_graph *context){

    /* Terminate Actors*/
    /*welt_cpp_imread_terminate((welt_cpp_imread*)
                              context->actors[ACTOR_IMREAD]);

    welt_cpp_im_gray_terminate((welt_cpp_im_gray*)
                                        context->actors[ACTOR_IMGRAY]);
    welt_cpp_imwrite_terminate((welt_cpp_imwrite*)
                                        context->actors[ACTOR_IMWRITE]);*/

}
