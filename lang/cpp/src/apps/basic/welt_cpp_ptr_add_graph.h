#ifndef _welt_cpp_ptr_add_graph_h
#define _welt_cpp_ptr_add_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
extern "C" {
#include "welt_c_basic.h"
#include "welt_c_actor.h"
//#include "lide_c_fifo.h"
#include "welt_c_fifo.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"
}
#include "welt_cpp_file_source.h"
#include "welt_cpp_add.h"
#include "welt_cpp_file_sink.h"
#include "welt_cpp_dereference.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"

#define BUFFER_CAPACITY 1024

/* An enumeration of the actors in this application. */
#define ACTOR_XSOURCE    0
#define ACTOR_YSOURCE    1
#define ACTOR_ZSOURCE    2
#define ACTOR_XDEREF     3
#define ACTOR_YDEREF     4
#define ACTOR_ZDEREF     5
#define ACTOR_ADD        6
#define ACTOR_SNK        7

#define FIFO_XSRC_XDEREF 0
#define FIFO_YSRC_YDEREF 1
#define FIFO_ZSRC_ZDEREF 2
#define FIFO_XDEREF_ADD  3
#define FIFO_YDEREF_ADD  4
#define FIFO_ZDEREF_ADD  5
#define FIFO_ADD_SNK     6

/* The total number of actors in the application. */
#define ACTOR_COUNT     8
#define FIFO_COUNT      7

/* token type definition
 * 1: token type is a pointer to any variables or objects, e.g. *int
 * 2: token is primitive e.g. int
 * 3: token is an object */
#define WELT_CPP_TOKEN_TYPE_POINTER   1
#define WELT_CPP_TOKEN_TYPE_PRIMITIVE 2
#define WELT_CPP_TOKEN_TYPE_OBJECT    3


/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

class welt_cpp_ptr_add_graph : public welt_cpp_graph{
public:
    welt_cpp_ptr_add_graph(char *x_file, char *y_file, char *z_file,
                           char *out_file);
    ~welt_cpp_ptr_add_graph();

    void scheduler() override;

private:
    char* x_file;
    char* y_file;
    char* z_file;
    char* out_file;
};

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

void welt_cpp_ptr_add_graph_terminate(
        welt_cpp_ptr_add_graph *context);


#endif
