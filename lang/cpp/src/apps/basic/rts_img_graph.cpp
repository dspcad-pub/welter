
#include <iostream>
#include "rts_img_graph.h"

#define STORE_SZ       1

using namespace std;
using namespace cv;

rts_img_graph::rts_img_graph(char *in_file, char *out_file) {
    int token_size = sizeof(Mat*);
    size_t rd_size = sizeof(Mat);


    fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new(
            BUFFER_CAPACITY, token_size, FIFO_RTS_READ));
    fifos.push_back( (welt_c_fifo_pointer)welt_c_fifo_new(
            BUFFER_CAPACITY, token_size, FIFO_READ_WRITE));

    /* initialize actors */
    actors.push_back(new rts_generic(fifos[FIFO_RTS_READ], STORE_SZ, rd_size));
    descriptors.push_back((char*)"rts actor");

    /* img read */
    actors.push_back(new rts_imread(fifos[FIFO_RTS_READ],
                                    fifos[FIFO_READ_WRITE], in_file, ACTOR_READER));
    descriptors.push_back((char*)"img read actor");

    /* img write */
    actors.push_back(new welt_cpp_imwrite(fifos[FIFO_READ_WRITE],
                                          out_file, ACTOR_WRITER));
    descriptors.push_back((char*)"img write actor");

    actor_count = ACTOR_COUNT;
    fifo_count = FIFO_COUNT;
}
void rts_img_graph::scheduler() {
    for (int i = 0; i < actor_count; i++){
        welt_cpp_util_guarded_execution(actors[i], descriptors[i]);
    }
}

rts_img_graph::~rts_img_graph() {
    /* Terminate Actors*/
    for (int actor_idx = 0; actor_idx < ACTOR_COUNT; actor_idx++){
        delete actors[actor_idx];
    }
}