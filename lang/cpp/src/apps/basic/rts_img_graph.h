
#ifndef RTS_IMGREAD_RTS_IMG_GRAPH_H
#define RTS_IMGREAD_RTS_IMG_GRAPH_H


extern "C" {
#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"
}
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"
#include "welt_cpp_imwrite.h"

#include "rts_generic.h"
#include "rts_imread.h"


/* The capacity of all FIFOs in the graph. */
#define BUFFER_CAPACITY 1024

/* An enumeration of the actors in this application. */
#define ACTOR_RTS       0
#define ACTOR_READER    1
#define ACTOR_WRITER    2

/* An enumeration of the edges in this application. The naming convention
for the constants is FIFO_<source actor>_<sink actor>. */
#define FIFO_RTS_READ   0
#define FIFO_READ_WRITE 1

/* The total number of actors in the application. */
#define ACTOR_COUNT     3
#define FIFO_COUNT      2

/* Graph class definition*/
class rts_img_graph : public welt_cpp_graph{
public:

    rts_img_graph(char *in_file, char *out_file);
    ~rts_img_graph();

    /* Scheduler for this graph */
    void scheduler() override;
};



#endif //RTS_IMGREAD_RTS_IMG_GRAPH_H
