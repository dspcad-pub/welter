#ifndef _rts_graph_h
#define _rts_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
/*******************************************************************************
This is a simple dataflow graph that demonstrates use of the
reference_token_source base class. As a demo, vector rts actor, and the
txt_img_reader are used with the base class.

Parameter(s): file_name indicates the name of file to be read. nRows and
nCols indicates the txt image size to be read.
*******************************************************************************/

extern "C" {
#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"
}
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"

#include "rts_vector.h"
#include "txt_img_reader.h"
#include "txt_img_write.h"

/* The capacity of all FIFOs in the graph. */
#define BUFFER_CAPACITY 1024

/* An enumeration of the actors in this application. */
#define ACTOR_RTS       0
#define ACTOR_READER    1
#define ACTOR_WRITER    2

/* An enumeration of the edges in this application. The naming convention
for the constants is FIFO_<source actor>_<sink actor>. */
#define FIFO_RTS_READ   0
#define FIFO_READ_WRITE 1

/* The total number of actors in the application. */
#define ACTOR_COUNT     3
#define FIFO_COUNT      2

/* Graph class definition*/
class rts_graph : public welt_cpp_graph{
public:
    /* Construct an instance of this dataflow graph. The arguments are,
     * respectively, the name of the file that contains the input image, the
     * name of the file that contains the output image, and the number of rows,
     * the number of columns of the txt image to be read. */
    rts_graph(char *in_file, char *out_file, int nRows, int nCols);
    ~rts_graph();

    /* Scheduler for this graph */
    void scheduler() override;
};

#endif
