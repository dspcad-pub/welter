#ifndef _welt_cpp_add_graph_h
#define _welt_cpp_add_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

extern "C" {
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_util.h"
}

#include "file_source_int.h"
#include "welt_cpp_add.h"
#include "file_sink_int.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"

#define BUFFER_CAPACITY (1024)

/* An enumeration of the actors in this application. */
#define ACTOR_XSOURCE    (0)
#define ACTOR_YSOURCE    (1)
#define ACTOR_ADD        (2)
#define ACTOR_SNK        (3)

#define FIFO_XSRC_ADD      (0)
#define FIFO_YSRC_ADD      (1)
#define FIFO_ADD_SNK       (2)

/* The total number of actors in the application. */
#define ACTOR_COUNT     (4)
#define FIFO_COUNT      (3)

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

    class welt_cpp_add_graph : public welt_cpp_graph {
    public:
        welt_cpp_add_graph(char *x_file, char *y_file,
                           char *out_file);

        ~welt_cpp_add_graph();

        void scheduler() override;

    private:
        char *x_file;
        char *y_file;
        char *out_file;
    };

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/
    void welt_cpp_add_graph_terminate(
            welt_cpp_add_graph *context);

#endif
