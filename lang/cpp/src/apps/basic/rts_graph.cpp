/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "rts_graph.h"

#define NAME_LENGTH 20

using namespace std;

rts_graph::rts_graph(char *in_file, char *out_file,
                     int nRows, int nCols) {
    /* Initialize fifos. */
    int token_size;
    /***************************************************************************
    there are three kinds of tokens used in this demo but their size are
    both 8.
    ***************************************************************************/
    token_size = sizeof(void*); /* sizeof(vector<vector<int>>*) */

    for (int fifo_idx = 0; fifo_idx < FIFO_COUNT; fifo_idx++)
        fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
                token_size, fifo_idx));

    /***************************************************************************
    Create actors in the actors vector and put descriptions
    for each actor in the descriptions vector.
    ***************************************************************************/
    /* rts actor */
    /* Set store_size of the rts_vector actor */
    int store_size = 10;
    actors.push_back(new rts_vector(fifos[FIFO_RTS_READ],
            store_size, nRows, nCols));
    descriptors.push_back((char*)"actor vector rts");
    actors[ACTOR_RTS]->connect((welt_cpp_graph*)this);

    /* reader actor */
    actors.push_back(new txt_img_reader(fifos[FIFO_RTS_READ],
            fifos[FIFO_READ_WRITE], in_file, nRows, nCols, ACTOR_READER));
    descriptors.push_back((char*)"actor reader");
    actors[ACTOR_READER]->connect((welt_cpp_graph*)this);

    /* write actor */
    actors.push_back(new txt_img_write(fifos[FIFO_READ_WRITE],
                                    out_file, ACTOR_WRITER));
    descriptors.push_back((char*)"actor write");
    actors[ACTOR_WRITER]->connect((welt_cpp_graph*)this);

    /* following two members are initialized but never used */
    actor_count = ACTOR_COUNT;
    fifo_count = FIFO_COUNT;
}

void rts_graph::scheduler() {
    for (int i = 0; i < actor_count; i++){
        welt_cpp_util_guarded_execution(actors[i], descriptors[i]);
    }
}

rts_graph::~rts_graph() {
    /* Terminate Actors*/
    for (int actor_idx = 0; actor_idx < ACTOR_COUNT; actor_idx++){
        delete actors[actor_idx];
    }
    cout << "delete rts graph" << endl;
}
