/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_im_graph.h"

#define NAME_LENGTH 20

using namespace std;


// constructor
welt_cpp_im_graph::welt_cpp_im_graph(char* im_file) {
    /* Initialize fifos. */
    int token_size;
    token_size = sizeof(void*);
    fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new(BUFFER_CAPACITY,
            token_size,FIFO_IMREAD_IMSHOW));

    /* Create and connect the actors. */
    /* imread actor */
    actors.push_back(new welt_cpp_imread(fifos[FIFO_IMREAD_IMSHOW],im_file,
            ACTOR_IMREAD));
    descriptors.push_back((char*)"actor img read");
    actors[ACTOR_IMREAD]->connect((welt_cpp_graph*)this);

    /* imshow actor */
    actors.push_back(new welt_cpp_imshow(fifos[FIFO_IMREAD_IMSHOW],
            ACTOR_IMSHOW));
    descriptors.push_back((char*)"actor img show");
    actors[ACTOR_IMSHOW]->connect((welt_cpp_graph*)this);

    /* Initialize source array and sink array */
//    FIXME: source array and sink array
//    context->source_array = (lide_c_actor_type **)lide_c_util_malloc(
//            context->fifo_count * sizeof(lide_c_actor_type *));
//
//    context->sink_array = (lide_c_actor_type **)lide_c_util_malloc(
//            context->fifo_count * sizeof(lide_c_actor_type *));

    /* following two members are initialized but never used */
    actor_count = ACTOR_COUNT;
    fifo_count = FIFO_COUNT;
}

void welt_cpp_im_graph::scheduler() {
    /* static scheduler */
    actors[ACTOR_IMREAD]->invoke();
    actors[ACTOR_IMSHOW]->invoke();
   // welt_cpp_util_simple_scheduler(actors,actor_count,descriptors);
}

// destructor
welt_cpp_im_graph::~welt_cpp_im_graph() {
    cout << "delete im graph" << endl;
}
void welt_cpp_im_graph_terminate(
        welt_cpp_im_graph *context){
//    int i;
//    /* Terminate FIFO*/ FIXME
//    for(i = 0; i<context->fifo_count; i++){
//        welt_c_fifo_free((welt_c_fifo_pointer)context->fifos[i]);
//    }

    /* Terminate Actors*/
    delete context->actors[ACTOR_IMREAD];
    delete context->actors[ACTOR_IMSHOW];
//    welt_cpp_imread_terminate((welt_cpp_imread*)
//                                      context->actors[ACTOR_IMREAD]);
//    welt_cpp_imshow_terminate((welt_cpp_imshow*)
//                                      context->actors[ACTOR_IMSHOW]);
//    free(context->actors);
//    free(context);
    delete context;
}
