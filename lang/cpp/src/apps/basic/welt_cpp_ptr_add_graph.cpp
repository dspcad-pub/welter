/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_ptr_add_graph.h"

#define NAME_LENGTH 20

using namespace std;



// constructor
welt_cpp_ptr_add_graph::welt_cpp_ptr_add_graph(char *x_file, char *y_file,
        char *z_file, char *out_file) {
    /* token_type */
    int token_type = WELT_CPP_TOKEN_TYPE_POINTER;
    this->x_file = x_file;
    this->y_file = y_file;
    this->z_file = z_file;
    this->out_file = out_file;

    this->actor_count = ACTOR_COUNT;
    this->fifo_count = FIFO_COUNT;

    /* Initialize fifos. */
    int token_size;
    for (int i = FIFO_XSRC_XDEREF; i<=FIFO_ZSRC_ZDEREF; i++){
        fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new
                (BUFFER_CAPACITY,sizeof(int*),i));
    }
    for (int i = FIFO_XDEREF_ADD; i<this->fifo_count; i++){
        fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new
                (BUFFER_CAPACITY,sizeof(int),i));
    }
    /* Create and connect the actors. */
    /* x source actor */
    actors.push_back(new welt_cpp_file_source
    (fifos[FIFO_XSRC_XDEREF], this->x_file,
                                     ACTOR_XSOURCE,
                              token_type));
    descriptors.push_back((char*)"actor x source");
    actors[ACTOR_XSOURCE]->connect((welt_cpp_graph*)this);

    /* y source actor */
    actors.push_back(new welt_cpp_file_source(fifos[FIFO_YSRC_YDEREF],
                                              this->y_file,
                                              ACTOR_YSOURCE, token_type));
    descriptors.push_back((char*)"actor y source");
    actors[ACTOR_YSOURCE]->connect((welt_cpp_graph*)this);

    /* z source actor */
    actors.push_back(new welt_cpp_file_source(fifos[FIFO_ZSRC_ZDEREF],
                                              this->z_file,
                                              ACTOR_ZSOURCE, token_type));
    descriptors.push_back((char*)"actor z source");
    actors[ACTOR_ZSOURCE]->connect((welt_cpp_graph*)this);

    /* x dereference actor */
    actors.push_back(new welt_cpp_dereference
                             (fifos[FIFO_XSRC_XDEREF], fifos[FIFO_XDEREF_ADD],
                              ACTOR_XDEREF, sizeof(int*)));
    descriptors.push_back((char*)"actor x dereference");
    actors[ACTOR_XDEREF]->connect((welt_cpp_graph*)this);

    /* y dereference actor */
    actors.push_back(new welt_cpp_dereference
                             (fifos[FIFO_YSRC_YDEREF], fifos[FIFO_YDEREF_ADD],
                              ACTOR_YDEREF, sizeof(int*)));
    descriptors.push_back((char*)"actor y dereference");
    actors[ACTOR_YDEREF]->connect((welt_cpp_graph*)this);

    /* z dereference actor */
    actors.push_back(new welt_cpp_dereference
                             (fifos[FIFO_ZSRC_ZDEREF], fifos[FIFO_ZDEREF_ADD],
                              ACTOR_ZDEREF, sizeof(int*)));
    descriptors.push_back((char*)"actor z dereference");
    actors[ACTOR_ZDEREF]->connect((welt_cpp_graph*)this);

    /* add actor */
    actors.push_back(new welt_cpp_add(fifos[FIFO_XDEREF_ADD],
                                      fifos[FIFO_YDEREF_ADD],
                                      fifos[FIFO_ADD_SNK], ACTOR_ADD));
    descriptors.push_back((char*)"actor add");
    actors[ACTOR_ADD]->connect((welt_cpp_graph*)this);

    /* sink actor */
    actors.push_back(new welt_cpp_file_sink
                             (fifos[FIFO_ADD_SNK], this->out_file, ACTOR_SNK));
    descriptors.push_back((char*)"actor sink");
    actors[ACTOR_SNK]->connect((welt_cpp_graph*)this);

    /* Initialize source array and sink array */
//    FIXME: source array and sink array
//    context->source_array = (lide_c_actor_context_type **)lide_c_util_malloc(
//            context->fifo_count * sizeof(lide_c_actor_context_type *));
//
//    context->sink_array = (lide_c_actor_context_type **)lide_c_util_malloc(
//            context->fifo_count * sizeof(lide_c_actor_context_type *));


}

void welt_cpp_ptr_add_graph::scheduler() {
   // welt_cpp_util_simple_scheduler(actors,actor_count,descriptors);

    /* static scheduler */
    actors[ACTOR_XSOURCE]->invoke();
    actors[ACTOR_YSOURCE]->invoke();
    actors[ACTOR_ADD]->invoke();
    actors[ACTOR_SNK]->invoke();
}

// destructor
welt_cpp_ptr_add_graph::~welt_cpp_ptr_add_graph() {
    cout << "delete add graph" << endl;
}
void welt_cpp_ptr_add_graph_terminate(
        welt_cpp_ptr_add_graph *context){
//    int i;
//    /* Terminate FIFO*/ FIXME
//    for(i = 0; i<context->fifo_count; i++){
//        welt_c_fifo_free((welt_c_fifo_pointer)context->fifos[i]);
//    }

    /* Terminate Actors*/
    welt_cpp_file_source_terminate((welt_cpp_file_source*)
                                      context->actors[ACTOR_XSOURCE]);
    welt_cpp_file_source_terminate((welt_cpp_file_source*)
                                           context->actors[ACTOR_YSOURCE]);
    welt_cpp_file_source_terminate((welt_cpp_file_source*)
                                           context->actors[ACTOR_ZSOURCE]);
    welt_cpp_add_terminate((welt_cpp_add*)
    context->actors[ACTOR_ADD]);
    welt_cpp_file_sink_terminate((welt_cpp_file_sink*)
                                      context->actors[ACTOR_SNK]);
//    free(context->actors);
//    free(context);
    delete context;
}
