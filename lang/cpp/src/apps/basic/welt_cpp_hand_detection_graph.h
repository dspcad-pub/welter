#ifndef _welt_cpp_hand_detection_graph_h
#define _welt_cpp_hand_detection_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
extern "C" {
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"
}
#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"
#include "welt_cpp_util.h"
#include "welt_cpp_imread.h"
#include "welt_cpp_hand_detection.h"
#include "welt_cpp_file_source.h" //new
#include "welt_cpp_file_sink.h"   //new
//#include "welt_cpp_imwrite.h"
//#include "welt_cpp_imshow.h"


#define BUFFER_CAPACITY 1024

/* An enumeration of the actors in this application. */
#define ACTOR_IMREAD           0
#define ACTOR_RANGESOURCE      1
#define ACTOR_HANDDETECTION    2
#define ACTOR_SINK             3

#define FIFO_IMREAD_HANDDETECTION       0
#define FIFO_RANGESOURCE_HANDDETECTION  1
#define FIFO_HANDDETECTION_SINK         2

/* The total number of actors in the application. */
#define ACTOR_COUNT     4
#define FIFO_COUNT      3


/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

class welt_cpp_hand_detection_graph : public welt_cpp_graph{
public:
    welt_cpp_hand_detection_graph(char *in_img_file, char *in_range_file, char *out_file );
    ~welt_cpp_hand_detection_graph();

    void scheduler() override;

private:
    char *img_file;
    char *range_file;
    char *out_file;
    
};

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

void welt_cpp_hand_detection_graph_terminate(
        welt_cpp_hand_detection_graph *context);

#endif
