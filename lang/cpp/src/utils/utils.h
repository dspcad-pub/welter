#ifndef UTILS_H
#define UTILS_H
/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/*******************************************************************************
DESCRIPTION:
This is a header file of utility functions in image rotation application.
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <vector>
#include <iostream>
extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}

using namespace std;

/* function reads a 2D vector from an external file.
 * The format of the input external file indicated by fileName:
 * <value>,<value>,<value>,<value>,...,<value>
 * <value>,<value>,<value>,<value>,...,<value>
 * <value>,<value>,<value>,<value>,...,<value>
 * <value>,<value>,<value>,<value>,...,<value>
 * ...
 * <value>,<value>,<value>,<value>,...,<value>
 * nRows, nCols are the number of rows and columns of a 2d vector
 * srcGrid should be an initialized nRows by nCols 2d vector */
void read_txt_image(vector<vector<int> > *srcGrid, char *fileName, int nRows,
					int nCols);

/* function prints a 2D vector out */
void print_grid (vector<vector<int> > srcGrid);

/* function rotates an image that is a 2D vector to 45 degree clockwise. The
 * output grid size is bigger than the input vector because there are some zeros
 * inserted. */
vector<vector<int> > imgRotate(vector<vector<int> > img);

/* function writes a 2D vector to an external file. The size of 2D vector is
 * automatically determined according to the vector. Arg file_name indicates
 * the external file name. Arg grid indicates the 2D vector to be written */
void write_txt_image(char * file_name, vector<vector<int> > grid);

#endif //UTILS_H
