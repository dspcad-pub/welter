/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include "utils.h"

void
read_txt_image(vector<vector<int>> *grid_ptr, char *fileName, int nRows, int
nCols) {
    FILE *filePtr = nullptr;
    filePtr = fopen(fileName, "r");
    if (filePtr == nullptr) {
        perror("fopen: ");
        exit(EXIT_FAILURE);
    }
    for (int iRow = 0; iRow < nRows; iRow++)
    {
        for (int iCol = 0; iCol < nCols;iCol++)
        {
            int data = 0;
            if (!fscanf(filePtr, "%d,", &data)){
                perror("fscanf: ");
            }
            (*grid_ptr)[iRow][iCol] = data;
        }
    }
    fclose(filePtr);
}

void print_grid (vector<vector<int>> srcGrid){
    for (int iRow = 0; iRow < srcGrid.size(); iRow++){
        for (int iCol = 0; iCol < srcGrid[0].size(); iCol++){
            cout << srcGrid[iRow][iCol] << " ";
        }
        cout << endl;
    }
}

vector<vector<int>> imgRotate(vector<vector<int>> img)
{
    auto dst = vector<vector<int>>();
    int nRows = img.size();
    int nCols = img[0].size();

    // Counter Variable
    int ctr = 0;

    while (ctr < 2 * nRows - 1)
    {
        vector<int> diag;

        for(int i = 0; i < abs(nRows - ctr - 1); i++) {
            diag.push_back(0);
        }

        for(int iCol = 0; iCol < nCols; iCol++)
        {

            // Iterate [0, n]
            for(int iRow = 0; iRow < nRows; iRow++)
            {

                // Diagonal Elements
                // Condition
                if (iRow + iCol == ctr)
                {

                    // Appending the
                    // Diagonal Elements
                    diag.push_back(img[iRow][iCol]);
                    diag.push_back(0);
                }
            }
        }
        diag.pop_back();

        for(int i = 0;
            i < abs(nRows - ctr - 1);
            i++)
        {
            diag.push_back(0);
        }

        ctr += 1;
        dst.push_back(diag);
    }
    return dst;
}

void write_txt_image(char * file_name, vector<vector<int>> grid){
    FILE *fp;
    fp = welt_c_util_fopen((const char *)file_name, "w");
    for (auto pixel_line : grid) {
        for (auto it = pixel_line.begin(); it != pixel_line.end(); it++) {
            if (next(it)==pixel_line.end())
                fprintf(fp, "%d", *it);
            else
                fprintf(fp, "%d,", *it);
        }
        fprintf(fp, "\n");
    }
    fclose(fp);
}
