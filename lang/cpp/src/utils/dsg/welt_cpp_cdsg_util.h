#ifndef _welt_cpp_cdsg_util_h
#define _welt_cpp_cdsg_util_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>

#include "welt_cpp_actor.h"
#include <sys/types.h>
#include <pthread.h>

/*******************************************************************************
This is a CDSG scheduler that keeps executing actors in a graph
from [start] actor to [end] actor.
The "actors" argument gives an array of the actors that makes up
the graph; the "actors_count" argument gives the number of actors,
and the "descriptors" argument is an array of strings such that the
i'th string gives a diagnostic descriptor for the ith actor.
*******************************************************************************/

bool welt_cpp_util_basic_execution(welt_cpp_actor *context,
        char *descriptor);

/*******************************************************************************
This is an CFDF canonical scheduler.
*******************************************************************************/
void welt_cpp_util_cdsg_scheduler(vector<welt_cpp_actor *>&actors,
        int start, int end, vector<char *>&descriptors);

#endif
