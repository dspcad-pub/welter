ADD_LIBRARY(welt_cpp_cdsg_util welt_cpp_cdsg_util.cpp)

INCLUDE_DIRECTORIES(
        $ENV{UXWELTERC}/src/gems/actors/basic
        $ENV{UXWELTERC}/src/gems/actors/common
        $ENV{UXWELTERC}/src/gems/edges
        $ENV{UXWELTERC}/src/tools/runtime
        $ENV{UXWELTERC}/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/dsg_actors/ref_actors
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/common
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/basic
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/runtime
        $ENV{UXWELTER}/lang/cpp/src/apps/basic
        $ENV{UXWELTER}/lang/cpp/src/utils
        $ENV{UXDICELANG}/c/src/util
)
INSTALL(TARGETS welt_cpp_cdsg_util DESTINATION $ENV{WELTERGEN})
