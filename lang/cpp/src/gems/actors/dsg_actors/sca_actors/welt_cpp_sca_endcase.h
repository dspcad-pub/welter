#ifndef _welt_cpp_sca_endcase_h
#define _welt_cpp_sca_endcase_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_cpp_actor.h"
#include "welt_cpp_dsg_actor.h"

extern "C"{
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo_unit_size.h"

};

/* Actor modes */
#define WELT_CPP_SCA_ENDCASE_MODE_ERROR      0
#define WELT_CPP_SCA_ENDCASE_MODE_PROCESS    1

class welt_cpp_sca_endcase: public welt_cpp_dsg_actor{
public:
    welt_cpp_sca_endcase(welt_c_fifo_pointer *in, welt_c_fifo_pointer out1,
            int input_port_count, int index);
    bool enable() override;
    void invoke() override;
    void reset() override;

    /* Set and get for input_port_count function*/
    void set_input_port_count(int input_port_count);
    int get_input_port_count();

    /* Set and get function for pointer to input FIFO array*/
    void set_in_pointer(welt_c_fifo_unit_size_pointer *in);

    welt_c_fifo_unit_size_pointer* get_in_pointer();

    //void set_index(int index);
    //int get_index();
    void connect(welt_cpp_graph *graph) override;

private:
    /* FIFO ports. */
/*  pointer to SCA FI actor's input FIFOs array; number of elements in the
    array is input_port_count; data type of element in the array is
    welt_c_fifo_unit_size_pointer */
    welt_c_fifo_unit_size_pointer *in;
/* output FIFO*/
    welt_c_fifo_unit_size_pointer out1;

/* total number of input ports in the actor*/
    int input_port_count;
/* index of the input port which has tokens storing in the FIFO*/
    int pindex;

};

#endif
