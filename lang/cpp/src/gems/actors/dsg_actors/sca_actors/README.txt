This is a directory for placing development of SCA (Schedule Control  Actor). 
SCA actor is not a typical CFDF actor. The dataflow behavior of an SCA follows 
feature of lumped homogeneous synchronous dataflow (LHSDF). The total number of 
tokens consumed by SCA is the same as the total number of tokens produced 
acrross all output ports of SCA during a firing which is 1.

