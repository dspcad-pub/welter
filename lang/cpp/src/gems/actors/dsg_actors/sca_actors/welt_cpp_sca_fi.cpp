/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_cpp_sca_fi.h"
#include "welt_cpp_graph.h"
extern "C"{
#include "welt_c_util.h"

}

#define MAX_FIFO_COUNT 3

welt_cpp_sca_fi::welt_cpp_sca_fi(welt_c_fifo_pointer in1,
        welt_c_fifo_pointer in2, welt_c_fifo_pointer out1,
        int input_port_count, int index) {
    int i;
    this->mode = WELT_CPP_SCA_FI_MODE_PROCESS;

    this->input_port_count = 2;
    this->in.reserve(this->input_port_count);

    this->in.push_back((welt_c_fifo_unit_size_pointer)in1);
    this->in.push_back((welt_c_fifo_unit_size_pointer)in2);
    this->out1 = (welt_c_fifo_unit_size_pointer)out1;

    this->pindex = 0;
    this->last_fifo = NULL;

    /* Initialize portrefs*/
    this->actor_set_index(index);
    this->actor_init_portrefs();
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->in[0]));
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->in[1]));
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->out1));

    return;

}

/* by default: there is only one input port not empty if actor is enabled*/
bool welt_cpp_sca_fi::enable() {
    bool result = false;
    int i, tmp;
    tmp = false;
    switch (this->mode) {
        case WELT_CPP_SCA_FI_MODE_PROCESS:
            for(i = 0; i<this->input_port_count; i++){
                if(welt_c_fifo_unit_size_population(this->in[i]) >=1){
                    tmp = true;
                    this->pindex = i;
                    break;
                }
            }
            result = (welt_c_fifo_unit_size_population(this->out1) <
                    welt_c_fifo_unit_size_capacity(this->out1)) && tmp;

            break;
        default:
            result = false;
            break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned true so that the required data is available.
*******************************************************************************/
void welt_cpp_sca_fi::invoke() {
    switch (this->mode) {
        case WELT_CPP_SCA_FI_MODE_PROCESS:
            welt_c_fifo_unit_size_read_advance(this->in[this->pindex]);
            welt_c_fifo_unit_size_write_advance(this->out1);
            this->last_fifo = this->out1;

            this->mode = WELT_CPP_SCA_FI_MODE_PROCESS;

            break;
        default:
            this->mode = WELT_CPP_SCA_FI_MODE_ERROR;
            break;
    }
}

void welt_cpp_sca_fi::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;
    int i;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;

    for(i = 0; i<this->input_port_count; i++){
        port_index = i;
        graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    }

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = this->input_port_count ;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

}
void welt_cpp_sca_fi::reset() {
    delete this;
}
