#ifndef _welt_c_sca_if_h
#define _welt_c_sca_if_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_cpp_actor.h"
#include "welt_cpp_dsg_actor.h"

extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_basic.h"
#include "welt_c_fifo_unit_size.h"

};

/* Actor modes */
#define WELT_CPP_SCA_IF_MODE_ERROR      0
#define WELT_CPP_SCA_IF_MODE_PROCESS    1

class welt_cpp_sca_if: public welt_cpp_dsg_actor{
public:
    welt_cpp_sca_if(welt_c_fifo_pointer in1,
                    welt_c_fifo_pointer out1,
                    welt_c_fifo_pointer out2,
                    int output_port_count, int index);
    bool enable() override;
    void invoke() override;
    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:

/* FIFO ports. */

/*input*/
    welt_c_fifo_unit_size_pointer in1;
/*
    pointer to SCA IF actor's input FIFOs array; number of elements in the
    array is input_port_count; data type of element in the array is
    welt_c_fifo_unit_size_pointer
*/
    vector< welt_c_fifo_unit_size_pointer > out;

/* number of output ports in the actor*/
    int output_port_count;

/* index of output port where output token is produced*/
    int pindex;

/* number of fifos in the actor*/
//int fifo_count;

};
#endif
