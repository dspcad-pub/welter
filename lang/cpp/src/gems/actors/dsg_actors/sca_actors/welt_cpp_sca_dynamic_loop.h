#ifndef _welt_cpp_sca_dynamic_loop_h
#define _welt_cpp_sca_dynamic_loop_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_cpp_actor.h"
#include "welt_cpp_dsg_actor.h"

extern "C"{
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo_unit_size.h"
};

/* Actor modes */
#define WELT_CPP_SCA_DYNAMIC_LOOP_MODE_ERROR      0
#define WELT_CPP_SCA_DYNAMIC_LOOP_MODE_PROCESS    1
#define WELT_CPP_SCA_DYNAMIC_LOOP_MODE_WAIT       2
#define WELT_CPP_SCA_DYNAMIC_LOOP_SUBMODE_PROCESS_A  3
#define WELT_CPP_SCA_DYNAMIC_LOOP_SUBMODE_PROCESS_B  4

class welt_cpp_sca_dynamic_loop: public welt_cpp_dsg_actor{
public:
    welt_cpp_sca_dynamic_loop(
            welt_c_fifo_pointer in1,
            welt_c_fifo_pointer in2,
            welt_c_fifo_pointer out1,
            welt_c_fifo_pointer out2,
            int index);
    bool enable() override;
    void invoke() override;
    void reset() override;

    /* Set and get for current_iteration function*/
    void set_current_iteration(int current_iteration);
    int get_current_iteration();
    /* Set and get for index function: we can use it with actor_set_index*/
    //void set_index(int index);
    //int get_index();
    void connect(welt_cpp_graph *graph) override;

private:
    /* sub mode*/
    int process_submode;

    /* FIFO ports. */
    welt_c_fifo_unit_size_pointer in1; /*loop_start*/
    welt_c_fifo_unit_size_pointer in2; /*next_iteration*/
    welt_c_fifo_unit_size_pointer out1; /*iteration_start*/
    welt_c_fifo_unit_size_pointer out2; /*loop_end*/

    /* Number of rest iterations */
    int current_iteration;
    /* Total number of iterations in the loop*/
    int iteration_count;


};


#endif
