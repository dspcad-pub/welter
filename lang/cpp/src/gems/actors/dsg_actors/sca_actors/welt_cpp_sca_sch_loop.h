#ifndef _welt_cpp_sca_sch_loop_h
#define _welt_cpp_sca_sch_loop_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_cpp_actor.h"
#include "welt_cpp_dsg_actor.h"

extern"C"{
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo_unit_size.h"
}


/* Actor modes */
#define WELT_CPP_SCA_SCH_LOOP_MODE_ERROR      0
#define WELT_CPP_SCA_SCH_LOOP_MODE_PROCESS    1
#define WELT_CPP_SCA_SCH_LOOP_MODE_END        2
#define WELT_CPP_SCA_SCH_LOOP_SUBMODE_PROCESS_A  3
#define WELT_CPP_SCA_SCH_LOOP_SUBMODE_PROCESS_B  4


class welt_cpp_sca_sch_loop: public welt_cpp_dsg_actor{
public:
    welt_cpp_sca_sch_loop(
            welt_c_fifo_pointer in1,
            welt_c_fifo_pointer out1,
            int iteration_count, int index);
    bool enable() override;
    void invoke() override;
    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    /* Set and get for current_iteration and iteration_count function*/
    void set_current_iteration(int current_iteration);

    int get_current_iteration();

    void set_iteration_count(int iteration_count);

    int get_iteration_count();

private:
    /* sub mode*/
    int process_submode;

    /* FIFO ports. */
    welt_c_fifo_unit_size_pointer in1; /*loop_start*/
    welt_c_fifo_unit_size_pointer out1; /*iteration_start*/

    /* Number of rest iterations */
    int current_iteration;

    /* Total number of iterations in the loop*/
    int iteration_count;

};

#endif
