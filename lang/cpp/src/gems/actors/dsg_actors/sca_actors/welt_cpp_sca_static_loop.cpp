/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "welt_cpp_sca_static_loop.h"
#include "welt_cpp_graph.h"
extern "C"{
#include "welt_c_util.h"
}

#define MAX_FIFO_COUNT 4

welt_cpp_sca_static_loop::welt_cpp_sca_static_loop(welt_c_fifo_pointer in1,
        welt_c_fifo_pointer in2, welt_c_fifo_pointer out1,
        welt_c_fifo_pointer out2, int iteration_count, int index) {


    this->mode = WELT_CPP_SCA_STATIC_LOOP_MODE_WAIT;
    this->process_submode = WELT_CPP_SCA_STATIC_LOOP_SUBMODE_PROCESS_A;

    this->in1 = (welt_c_fifo_unit_size_pointer)in1;
    this->in2 = (welt_c_fifo_unit_size_pointer)in2;
    this->out1 = (welt_c_fifo_unit_size_pointer)out1;
    this->out2 = (welt_c_fifo_unit_size_pointer)out2;
    this->last_fifo = NULL;

    this->current_iteration = 0;
    this->iteration_count = iteration_count;
    this->in1_ctrl = 0;

    //this->index = index;
    //this->subgraphs = NULL;
    //this->subgraph_count = 0;

    /* Initialize portrefs*/
    this->actor_set_index(index);
    this->actor_init_portrefs();
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->in1));
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->in2));
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->out1));
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->out2));


    /* iteration_count should be positive */
    if(this->iteration_count < 1){
        this->mode = WELT_CPP_SCA_STATIC_LOOP_MODE_ERROR;
    }

    return;

}

bool welt_cpp_sca_static_loop::enable() {
    bool result = false;

    switch (this->mode) {
        case WELT_CPP_SCA_STATIC_LOOP_MODE_PROCESS:
            //printf("sca_static result %d\n",result);
            result = (welt_c_fifo_unit_size_population(this->in2) >= 1) &&
                    ((welt_c_fifo_unit_size_population(this->out1) <
                    welt_c_fifo_unit_size_capacity(this->out1)) ||
                    (welt_c_fifo_unit_size_population(this->out2) <
                    welt_c_fifo_unit_size_capacity(this->out2)));
            break;
        case WELT_CPP_SCA_STATIC_LOOP_MODE_WAIT:
            //printf("sca_static wait result %d\n",result);

            result = (welt_c_fifo_unit_size_population(this->in1) >= 1) &&
                    (welt_c_fifo_unit_size_population(this->out1) <
                    welt_c_fifo_unit_size_capacity(this->out1));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned true so that the required data is available.
*******************************************************************************/
void welt_cpp_sca_static_loop::invoke() {

    switch (this->mode) {
        case WELT_CPP_SCA_STATIC_LOOP_MODE_PROCESS:
            welt_c_fifo_unit_size_read_advance(this->in2);

            this->current_iteration = this->current_iteration-1;

            if(this->current_iteration > 0)
                this->process_submode =
                        WELT_CPP_SCA_STATIC_LOOP_SUBMODE_PROCESS_A;
            else
                this->process_submode =
                        WELT_CPP_SCA_STATIC_LOOP_SUBMODE_PROCESS_B;


            if(this->process_submode ==
                    WELT_CPP_SCA_STATIC_LOOP_SUBMODE_PROCESS_A){

                welt_c_fifo_unit_size_write_advance(this->out1);

                this->last_fifo = this->out1;
                this->mode = WELT_CPP_SCA_STATIC_LOOP_MODE_PROCESS;

            }else{
                welt_c_fifo_unit_size_write(this->out2, &this->in1_ctrl);

                this->last_fifo = this->out2;
                this->mode = WELT_CPP_SCA_STATIC_LOOP_MODE_WAIT;
            }
            break;
        case WELT_CPP_SCA_STATIC_LOOP_MODE_WAIT:
            welt_c_fifo_unit_size_read(this->in1, &this->in1_ctrl);
            this->current_iteration = this->iteration_count;
            welt_c_fifo_unit_size_write_advance(this->out1);
            this->last_fifo = this->out1;
            this->mode = WELT_CPP_SCA_STATIC_LOOP_MODE_PROCESS;
            this->process_submode = WELT_CPP_SCA_STATIC_LOOP_SUBMODE_PROCESS_A;
            break;
        default:
            this->mode = WELT_CPP_SCA_STATIC_LOOP_MODE_ERROR;
            break;
    }
}

void welt_cpp_sca_static_loop::set_current_iteration(int current_iteration) {
    this->current_iteration = current_iteration;
    return;
}

int welt_cpp_sca_static_loop::get_current_iteration() {
    return this->current_iteration;

}

void welt_cpp_sca_static_loop::set_iteration_count(int iteration_count) {
    this->iteration_count = iteration_count;
    if(this->mode == WELT_CPP_SCA_STATIC_LOOP_MODE_ERROR){
        this->mode = WELT_CPP_SCA_STATIC_LOOP_MODE_WAIT;
    }

    if(this->iteration_count < 1){
        this->mode = WELT_CPP_SCA_STATIC_LOOP_MODE_ERROR;
    }
    return;
}

int welt_cpp_sca_static_loop::get_iteration_count() {
    return this->iteration_count;

}

void welt_cpp_sca_static_loop::connect(welt_cpp_graph *graph) {

    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    /* input 2*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 3;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

}

void welt_cpp_sca_static_loop::reset() {
    delete this;
}
