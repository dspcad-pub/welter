/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_cpp_sca_case.h"
#include "welt_cpp_graph.h"
extern "C"{
#include "welt_c_util.h"
}

#define MAX_FIFO_COUNT  100

welt_cpp_sca_case::welt_cpp_sca_case(
        welt_c_fifo_pointer in,
        welt_c_fifo_pointer *out,
        int output_port_count, int index){
    int i;

    this->mode = WELT_CPP_SCA_CASE_MODE_PROCESS;
    this->in = (welt_c_fifo_unit_size_pointer)in;
    this->last_fifo = NULL;
    this->actor_set_index(index);
    this->output_port_count = output_port_count;
    this->pindex = 0;

    /* pout_num is positive */
    if(this->output_port_count < 1 || this->output_port_count >
        MAX_FIFO_COUNT-1){
        this->mode = WELT_CPP_SCA_CASE_MODE_ERROR;
        this->out = NULL;
        return;
    }else{
        this->out = (welt_c_fifo_unit_size_pointer *)out;
    }
    this->actor_set_max_port_count(MAX_FIFO_COUNT);

    /* Initialize portrefs*/
    this->actor_set_index(index);
    this->actor_init_portrefs();
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->in));
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->out));

}



bool welt_cpp_sca_case::enable() {
    bool result = false;
    int i, tmp;
    tmp = false;
    switch (this->mode) {
    case WELT_CPP_SCA_CASE_MODE_PROCESS:
        for(i = 0; i<this->output_port_count; i++){
            if((welt_c_fifo_unit_size_population(this->out[i]) <
                welt_c_fifo_unit_size_capacity(this->out[i]))){
                tmp = true;
                break;
            }
        }
        result = (welt_c_fifo_unit_size_population(this->in) >= 1) && tmp;
                
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.
*******************************************************************************/
void welt_cpp_sca_case::invoke() {

    switch (this->mode) {
    case WELT_CPP_SCA_CASE_MODE_PROCESS:
        welt_c_fifo_unit_size_read(this->in, &this->pindex);
        if(this->pindex >= this->output_port_count){
            fprintf(stderr, "invalid input token in if actor\n");
            this->mode = WELT_CPP_SCA_CASE_MODE_ERROR;
            break;
        }
        if(welt_c_fifo_unit_size_population(this->out[this->pindex]) <
                welt_c_fifo_unit_size_capacity(this->out[this->pindex])){
            welt_c_fifo_unit_size_write_advance(this->out[this->pindex]);
            this->last_fifo = this->out[this->pindex];
        }else{
            welt_c_fifo_unit_size_write(this->in, &this->pindex);
            this->last_fifo = this->in;
        }
        this->mode = WELT_CPP_SCA_CASE_MODE_PROCESS;
        break;
 
    default:
        this->mode = WELT_CPP_SCA_CASE_MODE_ERROR;
        break;
    }
}

//void welt_c_sca_case_terminate(welt_c_sca_case_context_type *context) {
//    free(context);
//}


/* Set and get for pout_num function*/
void welt_cpp_sca_case::set_output_port_count(int output_port_count){
    this->output_port_count = output_port_count;
    if(this->output_port_count < 1){
        this->mode = WELT_CPP_SCA_CASE_MODE_ERROR;
    }else{
        this->mode = WELT_CPP_SCA_CASE_MODE_PROCESS;
    }
    return;
}

int welt_cpp_sca_case::get_output_port_count(){
    return this->output_port_count;
}

/* Set and get function for pointer to input FIFO array*/
void welt_cpp_sca_case::set_out_pointer(welt_c_fifo_unit_size_pointer *out){
    this->out = out;
    return;
}

welt_c_fifo_unit_size_pointer *welt_cpp_sca_case::get_out_pointer(){
    return this->out;
}

void welt_cpp_sca_case::set_index(int index){
    this->actor_set_index(index);
    return;
} 

int welt_cpp_sca_case::get_index(){
    return this->actor_get_index();
} 

void welt_cpp_sca_case::connect(welt_cpp_graph *graph){
    int port_index;
    int direction;
    int i; 
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    for(i = 1; i< this->output_port_count+1; i++){
        port_index = i;
        graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    }
}

void welt_cpp_sca_case::reset() {
    delete this;
}
