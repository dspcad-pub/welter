/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "welt_cpp_ref_actor.h"
extern "C"{
#include "welt_c_util.h"
}
#include "welt_cpp_graph.h"
#include "welt_cpp_ref_pre_post_functions.h"
#define MAX_FIFO_COUNT 2

welt_cpp_ref_actor::welt_cpp_ref_actor(welt_c_fifo_pointer in,
        welt_c_fifo_pointer out, int index, welt_cpp_actor *ref_actor,
        int selected_input_count, int selected_output_count){

    this->mode = WELT_CPP_REF_ACTOR_MODE_PROCESS;
    this->selected_input_count = selected_input_count;
    this->selected_output_count = selected_output_count;
    this->ref_actor = ref_actor;
    if (this->selected_input_count < 0 ||
            this->selected_output_count < 0){
        this->mode = WELT_CPP_REF_ACTOR_MODE_ERROR;
    }else {
        this->selected_app_gr_inputs.reserve(this->selected_input_count);
        this->selected_app_gr_outputs.reserve(this->selected_output_count);
    }
    this->in = (welt_c_fifo_unit_size_pointer)in;
    this->out = (welt_c_fifo_unit_size_pointer)out;
    this->last_fifo = this->out;

    this->actor_set_index(index);
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs((welt_c_fifo_pointer*)&this->in);
    this->actor_add_portrefs((welt_c_fifo_pointer*)&this->out);
    };


bool welt_cpp_ref_actor::enable() {
    bool result = false;

    switch (this->mode) {
    case WELT_CPP_REF_ACTOR_MODE_PROCESS:
        result = (welt_c_fifo_unit_size_population(this->in) >= 1) &&
                ((welt_c_fifo_unit_size_population(this->out) <
                welt_c_fifo_unit_size_capacity(this->out)));
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.
*******************************************************************************/

void welt_cpp_ref_actor::invoke() {
    switch (this->mode) {
    case WELT_CPP_REF_ACTOR_MODE_PROCESS:
        welt_c_fifo_unit_size_read(this->in, &this->common_state);
            if ((this->ref_actor)->enable()) {
                (this->ref_actor)->invoke();
            }
        welt_c_fifo_unit_size_write(this->out, &this->common_state);
        this->mode = WELT_CPP_REF_ACTOR_MODE_PROCESS;
        break;
    default:
        this->mode = WELT_CPP_REF_ACTOR_MODE_PROCESS;
        break;
    }
}

welt_c_fifo_unit_size_pointer welt_cpp_ref_actor::get_dsg_fifo_output(){
    return this->last_fifo;
}

void welt_cpp_ref_actor::set_selected_input_count(int selected_input_count){
    this->selected_input_count = selected_input_count;
    if (this->selected_input_count < 0){
        this->mode = WELT_CPP_REF_ACTOR_MODE_ERROR;
    }else{
        this->mode = WELT_CPP_REF_ACTOR_MODE_PROCESS;
        if(this->selected_app_gr_inputs.size() != 0){
            this->selected_app_gr_inputs.clear();
        }
        this->selected_app_gr_inputs.reserve(this->selected_input_count);
        
    }
    
}

int welt_cpp_ref_actor::get_selected_input_count(){
    return this->selected_input_count;
}
    
void welt_cpp_ref_actor::set_selected_output_count(int selected_output_count){
    this->selected_output_count = selected_output_count;
    if (this->selected_output_count < 0){
        this->mode = WELT_CPP_REF_ACTOR_MODE_ERROR;
    }else{
        this->mode = WELT_CPP_REF_ACTOR_MODE_PROCESS;
        if(this->selected_app_gr_outputs.size() != 0){
            this->selected_app_gr_outputs.clear();
        }
        this->selected_app_gr_outputs.reserve(this->selected_output_count);

    }
        
}

int welt_cpp_ref_actor::get_selected_output_count(){
    return this->selected_output_count;
}
    
void welt_cpp_ref_actor::set_selected_app_gr_inputs(welt_c_fifo_pointer input,
    int index){
    this->selected_app_gr_inputs[index] = &input;
    return;    
}

welt_c_fifo_pointer welt_cpp_ref_actor::get_selected_app_gr_inputs(
    int index){
    return(*this->selected_app_gr_inputs[index]);
}

void welt_cpp_ref_actor::set_selected_app_gr_outputs(
    welt_c_fifo_pointer output,
    int index){
    this->selected_app_gr_outputs[index] = &output;
    return;       
}

welt_c_fifo_pointer welt_cpp_ref_actor::get_selected_app_gr_outputs(int index){
    return(*this->selected_app_gr_outputs[index]);
}

void welt_cpp_ref_actor::set_index(int index){
    this->actor_get_index();
    return;
} 

int welt_cpp_ref_actor::get_index(){
    return this->actor_get_index();
}

void welt_cpp_ref_actor::connect(welt_cpp_graph *graph){
    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection( this, port_index, direction);
    return;
}
void welt_cpp_ref_actor::reset() {
    delete this;
}
