#ifndef _welt_cpp_ref_actor_h
#define _welt_cpp_ref_actor_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
extern "C"{

#include "welt_c_fifo.h"
#include "welt_c_basic.h"
#include "welt_c_fifo_unit_size.h"
}
#include "welt_cpp_actor.h"
#include "welt_cpp_dsg_actor.h"

/* Actor modes */
#define WELT_CPP_REF_ACTOR_MODE_PROCESS  0
#define WELT_CPP_REF_ACTOR_MODE_ERROR    1

class welt_cpp_ref_actor: public welt_cpp_dsg_actor{
public:
    welt_cpp_ref_actor(welt_c_fifo_pointer in, welt_c_fifo_pointer out,
            int index,welt_cpp_actor *ref_actor, int selected_input_count,
            int selected_output_count);
    bool enable() override;
    void invoke() override;
    void reset() override;

    void connect(welt_cpp_graph *graph) override;

    int get_preA();
    int get_postA();
    int get_exeA();
    void set_selected_input_count(int selected_input_count);
    int get_selected_input_count();
    void set_selected_output_count(int selected_output_count);
    int get_selected_output_count();
    void set_selected_app_gr_inputs(welt_c_fifo_pointer input, int index);
    welt_c_fifo_pointer get_selected_app_gr_inputs( int index);
    void set_selected_app_gr_outputs(welt_c_fifo_pointer output,int index);
    welt_c_fifo_pointer get_selected_app_gr_outputs(int index);
    void set_index(int index);
    int get_index();
    welt_c_fifo_unit_size_pointer get_dsg_fifo_output();

private:
    /* Actor interface ports. */
    welt_c_fifo_unit_size_pointer in;
    welt_c_fifo_unit_size_pointer out;

    /* context of the referenced actor*/
    welt_cpp_actor* ref_actor;
    /* The number of elements in the selected_app_gr_inputs array */
    int selected_input_count;

    /* The number of elements in the selected_app_gr_output array */
    int selected_output_count;

/* Array of pointers to selected FIFOs in the application graph that are input
FIFOs of the referenced actor. Each element of this array is of type
welt_c_fifo_pointer. A NULL pointer here indicates there are no elements in
the array (i.e., no input FIFOs referenced by the RA).
*/
    vector<welt_c_fifo_pointer*> selected_app_gr_inputs;

/* Array of pointers to selected FIFOs in the application graph that are
output FIFOs of the referenced actor. Each element of this array is of type
welt_c_fifo_pointer. A NULL pointer here indicates there are no elements in
the array (i.e., no output FIFOs referenced by the RA).
*/
    vector<welt_c_fifo_pointer*> selected_app_gr_outputs;

/* state variable*/
/* The common_state variable could store any data in the actor's execution.
For example, in RA actor, token will be read from input FIFO in invoke
function; if the token is integer, then common_state here could be used for
storing the token from FIFO. For instance, if input token is the loop count,
then we could use common_state to store the number of iterations from input
FIFO. */
    int common_state;
};


#endif
