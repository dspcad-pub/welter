/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pthread.h>
#include <iostream>

#include "welt_cpp_snd_actor.h"
extern "C"{
#include "welt_c_util.h"
}
#include "welt_cpp_graph.h"
#define MAX_FIFO_COUNT 3

welt_cpp_snd_actor::welt_cpp_snd_actor(welt_c_fifo_pointer in_pc,
        welt_c_fifo_pointer out_pc, welt_c_fifo_pointer out_ipc,
        pthread_mutex_t *mutex, pthread_cond_t *cond, welt_c_fifo_pointer fifo,
        int block_size, int index) {

    this->mode = WELT_CPP_SND_ACTOR_MODE_PROCESS;

    this->in_pc = (welt_c_fifo_unit_size_pointer)in_pc;
    this->out_pc = (welt_c_fifo_unit_size_pointer)out_pc;
    this->out_ipc = (welt_c_fifo_pointer)out_ipc;
    this->last_fifo = (welt_c_fifo_unit_size_pointer)out_pc;

    this->mutex = mutex;
    this->cond = cond;
    this->fifo = (welt_c_fifo_pointer)fifo;
    this->block_size = block_size;

    /* Initialize portrefs*/
    this->actor_set_index(index);
    this->actor_init_portrefs();
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->in_pc));
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->out_pc));
    this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->out_ipc));
    return;
}

bool welt_cpp_snd_actor::enable() {
    bool result = false;

    switch (this->mode) {
        case WELT_CPP_SND_ACTOR_MODE_PROCESS:
            result = (welt_c_fifo_unit_size_population(this->in_pc) >= 1)
                    && (welt_c_fifo_population(this->out_ipc) <
                    welt_c_fifo_capacity(this->out_ipc))
                    && (welt_c_fifo_unit_size_population(this->out_pc) <
                    welt_c_fifo_unit_size_capacity(this->out_pc));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.
*******************************************************************************/

void welt_cpp_snd_actor::invoke() {
    switch (this->mode) {
        case WELT_CPP_SND_ACTOR_MODE_PROCESS:

            pthread_mutex_lock(this->mutex);
            welt_c_fifo_unit_size_read_advance(this->in_pc);
            welt_c_fifo_unit_size_write_advance(this->out_pc);
            welt_c_fifo_write_advance(this->out_ipc);

            if (welt_c_fifo_population(this->fifo) <=
                    welt_c_fifo_capacity(this->fifo) - this->block_size){
                pthread_cond_signal(this->cond);
            }

            pthread_mutex_unlock(this->mutex);

            break;
        default:
            this->mode = WELT_CPP_SND_ACTOR_MODE_PROCESS;
            break;
    }

}
void welt_cpp_snd_actor::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    /* output 2*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    return;
}

welt_c_fifo_unit_size_pointer welt_cpp_snd_actor::dsg_fifo_output() {
    return this->get_last_fifo();
}

void welt_cpp_snd_actor::reset() {
    delete this;
}

