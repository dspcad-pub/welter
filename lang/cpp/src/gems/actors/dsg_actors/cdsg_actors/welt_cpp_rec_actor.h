#ifndef _welt_cpp_rec_actor_h
#define _welt_cpp_rec_actor_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <pthread.h>
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_fifo_unit_size.h"
}
#include "welt_cpp_actor.h"
#include "welt_cpp_dsg_actor.h"

/* Actor modes */
#define WELT_CPP_REC_ACTOR_MODE_PROCESS  0
#define WELT_CPP_REC_ACTOR_MODE_ERROR    1

/*******************************************************************************
This REC is an inter-SDSG coordination actor (ICA) for a communication between
concurrent SDSGs (CDSGs). 'rec' represents the receipt of
a single token. This actor has one pair of input and output ports (in_pc,
out_pc) and input interprocessor communicator edge (in_ipc). The in_ipc of
this acotr is connected to the out_ipc of SND actor. 
There is a one mode associated with this actor. At the PROCESS mode, this
actor consumes a token from in_pc and wait for there is a token in in_ipc
edge. When token is arrived to in_ipc edge from SND actor, this actor
consumes token from in_pc and in_ipc, and produces token in out_pc edge.

More details can be found at:
[wu2011x1] H. Wu, C. Shen, N. Sane, W. Plishker, and S. S. Bhattacharyya.
A model-based schedule representation for heterogeneous mapping of dataflow
graphs. In Proceedings of the International Heterogeneity in Computing
Workshop, pages 66-77, Anchorage, Alaska, May 2011.
*******************************************************************************/

class welt_cpp_rec_actor:public welt_cpp_dsg_actor{

public:
    welt_cpp_rec_actor(welt_c_fifo_pointer in_ipc, welt_c_fifo_pointer in_pc,
            welt_c_fifo_pointer out_pc, pthread_mutex_t *mutex,
            pthread_cond_t *cond, welt_c_fifo_pointer fifo,
            int block_size, int index);

    bool enable() override;
    void invoke() override;
    void reset() override;

    void connect(welt_cpp_graph* graph) override;
    welt_c_fifo_unit_size_pointer dsg_fifo_output();

private:
/* Actor interface ports. */
welt_c_fifo_pointer in_ipc; /*Input IPC edge*/
welt_c_fifo_unit_size_pointer in_pc; /*Input PC edge*/
welt_c_fifo_unit_size_pointer out_pc; /*Output PC edge*/
pthread_mutex_t *mutex;
pthread_cond_t *cond;
welt_c_fifo_pointer fifo;
int block_size;
};

    
#endif
