#ifndef _welt_cpp_param_snd_actor_h
#define _welt_cpp_param_snd_actor_h

#include <pthread.h>
extern "C" {
#include "welt_c_fifo.h"
#include "welt_c_fifo_unit_size.h"
}
#include "welt_cpp_actor.h"
#include "welt_cpp_dsg_actor.h"

/* Actor modes */
#define WELT_CPP_PARAM_SND_ACTOR_MODE_PROCESS  (0)
#define WELT_CPP_PARAM_SND_ACTOR_MODE_ERROR    (1)

/*******************************************************************************
This SND is an inter-SDSG coordination actor (ICA) for a communication between
concurrent SDSGs (CDSGs). 'snd' represents the sender of
a single token. This actor has one pair of input and output ports (in_pc,
out_pc) and output interprocessor communicator edge (out_ipc). The out_ipc of
this actor is connected to the in_ipc of REC actor. There is a one
mode associated with this actor. At the PROCESS mode, this actor consumes
a token from in_pc and produces token in out_pc and out_ipc edge.

More details can be found at:
[wu2011x1] H. Wu, C. Shen, N. Sane, W. Plishker, and S. S. Bhattacharyya.
A model-based schedule representation for heterogeneous mapping of dataflow
graphs. In Proceedings of the International Heterogeneity in Computing
Workshop, pages 66-77, Anchorage, Alaska, May 2011.
*******************************************************************************/

class welt_cpp_param_snd_actor:public welt_cpp_dsg_actor{
public:
    welt_cpp_param_snd_actor(welt_c_fifo_pointer in_pc, welt_c_fifo_pointer
            out_pc, welt_c_fifo_pointer out_ipc, pthread_mutex_t *mutex,
            pthread_cond_t *cond, int* param,
            int block_size, int index);

    bool enable() override;
    void invoke() override;
    void reset() override;
    void connect(welt_cpp_graph* graph) override;
    welt_c_fifo_unit_size_pointer dsg_fifo_output();

private:
    /* Actor interface ports. */
    welt_c_fifo_unit_size_pointer in_pc; /*Input PC edge*/
    welt_c_fifo_unit_size_pointer out_pc; /*Output PC edge*/
    welt_c_fifo_pointer out_ipc; /*Output IPC edge*/
    pthread_mutex_t *mutex;
    pthread_cond_t *cond;
    int* param;
    int block_size;
};

#endif
