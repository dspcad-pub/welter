#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include <iostream>

#include "welt_cpp_param_rec_actor.h"
extern "C"{
#include "welt_c_util.h"
}
#include "welt_cpp_graph.h"

#define MAX_FIFO_COUNT (3)

welt_cpp_param_rec_actor::welt_cpp_param_rec_actor(welt_c_fifo_pointer in_ipc,
        welt_c_fifo_pointer in_pc, welt_c_fifo_pointer out_pc,
        pthread_mutex_t *mutex, pthread_cond_t *cond, int* param,
        int block_size, int index){

this->mode = WELT_CPP_PARAM_REC_ACTOR_MODE_PROCESS;
this->in_pc = (welt_c_fifo_unit_size_pointer)in_pc;
this->in_ipc = (welt_c_fifo_pointer)in_ipc;
this->out_pc = (welt_c_fifo_unit_size_pointer)out_pc;
this->last_fifo = (welt_c_fifo_unit_size_pointer)out_pc;

this->mutex = mutex;
this->cond = cond;
this->param = param;
this->block_size = block_size;

/* Initialize portrefs*/
this->actor_set_index(index);
this->actor_init_portrefs();
this->actor_set_max_port_count(MAX_FIFO_COUNT);
this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->in_ipc));
this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->in_pc));
this->actor_add_portrefs((welt_c_fifo_pointer*)(&this->out_pc));

}
bool welt_cpp_param_rec_actor::enable() {
    bool result = false;

    switch (this->mode) {
        case WELT_CPP_PARAM_REC_ACTOR_MODE_PROCESS:
            result = (welt_c_fifo_unit_size_population(this->in_pc) >= 1)
                    && (welt_c_fifo_unit_size_population(this->out_pc) <
                    welt_c_fifo_unit_size_capacity(this->out_pc));
            break;
        case WELT_CPP_PARAM_REC_ACTOR_MODE_ERROR:
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}
void welt_cpp_param_rec_actor::invoke() {
    switch (this->mode) {
        case WELT_CPP_PARAM_REC_ACTOR_MODE_PROCESS:
            if (welt_c_fifo_population(this->in_ipc) >= 1) {
                welt_c_fifo_unit_size_read_advance(this->in_pc);
                welt_c_fifo_read_advance(this->in_ipc);
                welt_c_fifo_unit_size_write_advance(this->out_pc);
            }
            break;
        default:
            this->mode = WELT_CPP_PARAM_REC_ACTOR_MODE_PROCESS;
            break;
    }
}
void welt_cpp_param_rec_actor::connect(welt_cpp_graph* graph) {
    int port_index;
    int direction;

    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    /* output 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    /* output 2*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection( (welt_cpp_actor*)this, port_index, direction);

    return;
}

welt_c_fifo_unit_size_pointer welt_cpp_param_rec_actor::dsg_fifo_output() {
    return this->get_last_fifo();
}

void welt_cpp_param_rec_actor::reset() {
    return;
}
