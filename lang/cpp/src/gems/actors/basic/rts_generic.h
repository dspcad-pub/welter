#ifndef _RTS_VECTOR_H
#define _RTS_VECTOR_H

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/*******************************************************************************
This actor inheriting from reference_token_source base class generates an array
of data where each data size is rd_size. Each reference token is a pointer data.

Output(a): The output is a reference token.

Parameter(s): rd_size gives the number of bytes in a
single token slot (RD stands for "referenced data").
store_size gives the number of token slots that
are maintained in this actor --- that is, the number of
referenced tokens that can coexist before the old one
gets reused by a new referenced token.

Actor modes and transitions: The PROCESS mode consumes
one token on each input and produces one token on the output.
It counts the threshold-exceeding pixels in the image input
and outputs the count on its output. The threshold is given
by the value of the token consumed on the threshold input.

Initial mode: The actor starts out in the PROCESS mode.
*******************************************************************************/

extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"
#include "reference_token_source.h"

class rts_generic : public reference_token_source {
public:
    /*************************************************************************
    Construct a rts_generic actor with store_size, rd_size, and the specified
    input FIFO connections, and the specified output FIFO connection.
    *************************************************************************/
    rts_generic(welt_c_fifo_pointer out, int store_size, size_t rd_size);

    ~rts_generic() override;

    /* Override the virtual rd_allocate function in the base class. This
     * function allocates rd_size memory space and return the pointer to
     * allocated memory */
    void *rd_allocate() final ;

    void rd_deallocate(void *data) override;

private:
    size_t rd_size;
};


#endif //_RTS_VECTOR_H
