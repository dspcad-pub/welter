#ifndef txt_img_reader_h
#define txt_img_reader_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/*******************************************************************************
This actor using the vector RTS actor is a new version of the txt_img_read
actor, which takes a reference token T as input. Output image is placed in the
token slot pointed to by T.
Input(s): The token type of the input is pointer to a 2d vector.

Output(a): The token type of the output is a pointer to 2d vector filled with
read content.

Parameter(s): file_name indicates the name of file to be read. nRows and
nCols indicates the txt image size to be read.

Actor modes and transitions: READ mode will consume a token that is a pointer to
a 2d vector, read image from an external file and store the image into the 2D
vector pointed by the pointer it consumed. Then the pointer of the result will
be written into the output FIFO. INACTIVE mode will not do anything. If there is
no input in the tokens, the program will be in the INACTIVE mode.

Initial mode: The actor starts out in the READ mode.
*******************************************************************************/
extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"

#define TXT_IMG_READER_MODE_READ        1
#define TXT_IMG_READER_MODE_INACTIVE     2

class txt_img_reader : public welt_cpp_actor{
public:
    /*************************************************************************
    Construct a new txt_img_reader actor that is associated with the given
    external file name and size of txt image, and connect the new actor to the
    given output FIFO.
    *************************************************************************/
    txt_img_reader(welt_c_fifo_pointer in_fifo, welt_c_fifo_pointer out_fifo,
                   char *file_name, int nRows, int nCols, int index);
    ~txt_img_reader() override;

    /* The enable function checks whether the output fifo has enough free space.
    */
    bool enable() override;

    /* Fires the actor in the current mode. */
    void invoke() override;

    /* Reset method: reset the actor.*/
    void reset() override;

    /* Associate the actor with a graph. */
    void connect(welt_cpp_graph *graph) override;

    /* Set fileName */
    void setFileName(char *fileName);

    /* Get fileName */
    char *getFileName() const;

private:
    /* Input FIFO */
    welt_c_fifo_pointer in;
    /* Output FIFO */
    welt_c_fifo_pointer out;
    /* File name to read */
    char* file_name;
    /* Size of the image to read */
    int nRows;
    int nCols;
};

#endif
