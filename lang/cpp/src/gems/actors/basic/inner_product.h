#ifndef _inner_product_h
#define _inner_product_h

#include "welt_cpp_actor.h"
extern "C"{
#include "welt_c_fifo.h"
#include "welt_c_util.h"
#include "welt_c_graph.h"
};

/*******************************************************************************
Overview: This actor read vector length (the number of elements in each
vector) and vectors and computes the inner product.

Input m: The token type is int.

Input x: The token type is int.

Input y: The token type is int.

Output out : The token type is int.

Actor modes and transitions: The STORE_LENGTH mode reads an integer to set the
length of vectors from m. After reading it, the actor transitions to PROCESS
mode. In PROCESS mode, the actor reads two vectors from x and y. Then it
computes the inner production, send the result to output FIFO and sets the
STORE_LENGTH mode as the next mode.

Initial mode: The actor starts out in the STORE_LENGTH mode.

Mode             in_fifo  out_fifo
      ----------------------------
      STORE_LENGTH   0       -1
      PROCESS		1,2       3

*******************************************************************************/

/* Actor modes */
#define INNER_PRODUCT_MODE_STORE_LENGTH   (1)
#define INNER_PRODUCT_MODE_PROCESS   (2)

/* Port index*/
#define INNER_PRODUCT_PORT_M    (0)
#define INNER_PRODUCT_PORT_X    (1)
#define INNER_PRODUCT_PORT_Y    (2)
#define INNER_PRODUCT_PORT_OUT  (3)

#define INNER_PRODUCT_MAX_FIFO_COUNT (4)

class inner_product: public welt_cpp_actor{
public:
    inner_product(welt_c_fifo_pointer m, welt_c_fifo_pointer x,
            welt_c_fifo_pointer y, welt_c_fifo_pointer out);

    ~inner_product() override;

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:

    /* Vector length variable. */
    int length;

    /* Input ports. */
    welt_c_fifo_pointer m;
    welt_c_fifo_pointer x;
    welt_c_fifo_pointer y;

    /* Output port. */
    welt_c_fifo_pointer out;

};

#endif
