/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>

#include "welt_cpp_imshow.h"

#define MAX_FIFO_COUNT 1

using namespace cv;
using namespace std;

welt_cpp_imshow::welt_cpp_imshow(welt_c_fifo_pointer in, int index) {
    this->in = (welt_c_fifo_pointer)in;
    this->mode = WELT_CPP_IMSHOW_MODE_SHOW;
    this->actor_set_index(index);
    actor_set_max_port_count(MAX_FIFO_COUNT);
}

bool welt_cpp_imshow::enable() {
    bool result = false;
    switch (mode) {
        case WELT_CPP_IMSHOW_MODE_SHOW:
            result = (welt_c_fifo_population(in) >= 1);
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_imshow::invoke() {
    switch (mode) {
        case WELT_CPP_IMSHOW_MODE_SHOW: {
            Mat *img_in = nullptr;
            welt_c_fifo_read(in, &img_in);

            namedWindow("Display window MC", WINDOW_AUTOSIZE);
            imshow("Display window MC", *img_in);
            waitKey(0);
            break;
        }
        default:
            mode = WELT_CPP_IMSHOW_MODE_SHOW;
            break;
    }
}

void welt_cpp_imshow::reset() {
    mode = WELT_CPP_IMSHOW_MODE_SHOW;
}

void welt_cpp_imshow::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

//    following lines are waiting for the util functions being ported
//    /* input 1*/
//    direction = GRAPH_IN_CONN_DIRECTION;
//    port_index = 0;
//    lide_c_graph_add_connection(graph, (lide_c_actor_type *)context,
//                                port_index, direction);
}

welt_cpp_imshow::~welt_cpp_imshow() {
    //cout << "delete imshow actor" << endl;
}

