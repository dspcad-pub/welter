

#ifndef _welt_cpp_imwrite_h
#define _welt_cpp_imwrite_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


/* Actor modes */
#define WELT_CPP_IMWRITE_MODE_WRITE        1
#define WELT_CPP_IMWRITE_MODE_INACTIVE     2

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/
/*****************************************************************************
Construct function of the welt_cpp_imwrite actor. Create a new
welt_cpp_imwrite with the specified file pointer, and the specified output
FIFO pointer. Data format in the input file should be all integers.
*****************************************************************************/


/* Structure and pointer types associated with file source objects. */
class welt_cpp_imwrite : public welt_cpp_actor{
public:
    welt_cpp_imwrite(welt_c_fifo_pointer in, char *filename,
            int index);
    ~welt_cpp_imwrite() override;

    bool enable() override;
    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    welt_c_fifo_pointer in;
    cv::Mat img_out;
    char* img_file;
};

/*****************************************************************************
Terminate function of the welt_cpp_imwrite actor.
*****************************************************************************/
void welt_cpp_imwrite_terminate(welt_cpp_imwrite *context);

#endif
