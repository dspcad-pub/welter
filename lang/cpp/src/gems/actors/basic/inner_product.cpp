/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include "inner_product.h"

inner_product::inner_product(welt_c_fifo_pointer m,
        welt_c_fifo_pointer x, welt_c_fifo_pointer y, welt_c_fifo_pointer out){

    /* Input ports. */
    this->m = m;
    this->x = x;
    this->y = y;

    /* Output port. */
    this->out = out;

    mode = INNER_PRODUCT_MODE_STORE_LENGTH;
}

bool inner_product::enable() {
    bool result = false;

    switch (mode) {
        case INNER_PRODUCT_MODE_STORE_LENGTH:
            result = welt_c_fifo_population(m) >= 1;
            break;
        case INNER_PRODUCT_MODE_PROCESS:
            result = (welt_c_fifo_population(x) >= getBlockLength())
                    && (welt_c_fifo_population(y)>= length) &&
                    (welt_c_fifo_population(out) < 	welt_c_fifo_capacity(out));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void inner_product::invoke() {
    int i = 0;
    int sum = 0;
    int x_value = 0;
    int y_value = 0;

    switch (mode) {
        case INNER_PRODUCT_MODE_STORE_LENGTH:
            welt_c_fifo_read(m, &(length));
            setBlockLength(length);
            if (length <= 0) {
                mode = INNER_PRODUCT_MODE_STORE_LENGTH;
                return;
            }
            this->mode = INNER_PRODUCT_MODE_PROCESS;
            break;

        case INNER_PRODUCT_MODE_PROCESS:
            for (i = 0; i < getBlockLength(); i++){
                welt_c_fifo_read(x, &(x_value));
                welt_c_fifo_read(y, &(y_value));
                sum += (x_value * y_value);
            }
            welt_c_fifo_write(out, &sum);
            mode = INNER_PRODUCT_MODE_STORE_LENGTH;
            break;
        default:
            mode = INNER_PRODUCT_MODE_STORE_LENGTH;
            break;
    }
    return;
}

void inner_product::reset() {
    this-> mode = INNER_PRODUCT_MODE_STORE_LENGTH ;
}

void inner_product::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;
    /* Register the port in enclosing graph. */
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = INNER_PRODUCT_PORT_M;
    graph->add_connection( this, port_index, direction);

    /* input 2*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = INNER_PRODUCT_PORT_X;
    graph->add_connection( this, port_index, direction);

    /* input 3*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = INNER_PRODUCT_PORT_Y;
    graph->add_connection( this, port_index, direction);

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = INNER_PRODUCT_PORT_OUT;
    graph->add_connection( this, port_index, direction);
}

inner_product::~inner_product(){
}
