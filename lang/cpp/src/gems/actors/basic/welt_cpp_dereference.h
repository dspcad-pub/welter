//
// Created by 谢景 on 5/8/20.
//

#ifndef _welt_cpp_dereference_h
#define _welt_cpp_dereference_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

extern "C" {
//    following head file is no longer needed
//#include "lide_c_actor.h"
#include "welt_c_util.h"
//#include "lide_c_fifo.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"

/* Actor modes */
#define WELT_CPP_DEREFERENCE_MODE_PROCESS        1

/* token type definition
* 1: token type is a pointer to any variables or objects, e.g. *int
* 2: token is primitive e.g. int
* 3: token is an object */
#define WELT_CPP_TOKEN_TYPE_POINTER   1
#define WELT_CPP_TOKEN_TYPE_PRIMITIVE 2
#define WELT_CPP_TOKEN_TYPE_OBJECT    3

/* This is a pointer-to-data actor, which takes a data type size as parameter.
* That actor has a single input (pointer token) and single output. The output
* token will be a copy of the data pointed to by the input token. The data type
* can be anything (not necessarily primitive) */

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with file source objects. */
class welt_cpp_dereference : public welt_cpp_actor{
public:
    welt_cpp_dereference(
            welt_c_fifo_pointer in, welt_c_fifo_pointer out, int index,
            size_t ptr_token_size);
    ~welt_cpp_dereference() override;

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    /* Actor interface ports. */
    welt_c_fifo_pointer in;
    welt_c_fifo_pointer out;
    size_t ptr_token_size;
};

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

/*****************************************************************************
Construct function of the welt_cpp_dereference actor. Create a new
welt_cpp_dereference with the specified file pointer, and the specified output
FIFO pointer. Data format in the input file should be all integers.
*****************************************************************************/
//welt_cpp_dereference_context *welt_cpp_dereference_new(lide_c_fifo_pointer
// in);

/*****************************************************************************
Enable function of the welt_cpp_dereference actor.
*****************************************************************************/
//bool welt_cpp_dereference_enable(welt_cpp_dereference_context *context);

/*****************************************************************************
Invoke function of the welt_cpp_dereference actor.
*****************************************************************************/
//void welt_cpp_dereference_invoke(welt_cpp_dereference_context *context);

/*****************************************************************************
Terminate function of the welt_cpp_dereference actor.
*****************************************************************************/
void welt_cpp_dereference_terminate(welt_cpp_dereference *context);

#endif
