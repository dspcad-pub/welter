//
// Created by 谢景 on 5/8/20.
//

#ifndef _welt_cpp_file_source_h
#define _welt_cpp_file_source_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

extern "C" {
//    following head file is no longer needed
//#include "lide_c_actor.h"
#include "welt_c_util.h"
//#include "lide_c_fifo.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"
//#include "welt_cpp_graph_context.h"


/* Actor modes */
#define WELT_CPP_FILE_SOURCE_MODE_WRITE        1
#define WELT_CPP_FILE_SOURCE_MODE_INACTIVE     2

/* token type definition
* 1: token type is a pointer to any variables or objects, e.g. *int
* 2: token is primitive e.g. int
* 3: token is an object */
#define WELT_CPP_TOKEN_TYPE_POINTER   1
#define WELT_CPP_TOKEN_TYPE_PRIMITIVE 2
#define WELT_CPP_TOKEN_TYPE_OBJECT    3

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with file source objects. */
class welt_cpp_file_source : public welt_cpp_actor{
public:
    welt_cpp_file_source(welt_c_fifo_pointer out, char *file, int index,
            int token_type);
    ~welt_cpp_file_source() override;

    bool enable() override;
    void invoke() override;

    void reset() override;
    void connect(welt_cpp_graph *graph) override;

private:
    welt_c_fifo_pointer out;
    FILE *fp;
    char *file;
    int data;
    /* the token_type on fifo */
    int token_type;
    /* A buffer storing data read from files */
    vector<int> data_buffer;
    /* An iterator to tell actor which data (in this file, integer) to output */
    vector<int>::iterator iter;
};

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

/*****************************************************************************
Construct function of the welt_cpp_file_source actor. Create a new
welt_cpp_file_source with the specified file pointer, and the specified output
FIFO pointer. Data format in the input file should be all integers.
*****************************************************************************/
//welt_cpp_file_source_context *welt_cpp_file_source_new
// (lide_c_fifo_pointer
// in);

/*****************************************************************************
Enable function of the welt_cpp_file_source actor.
*****************************************************************************/
//bool welt_cpp_file_source_enable(welt_cpp_file_source_context
// *context);

/*****************************************************************************
Invoke function of the welt_cpp_file_source actor.
*****************************************************************************/
//void welt_cpp_file_source_invoke(welt_cpp_file_source_context *context);

/*****************************************************************************
Terminate function of the welt_cpp_file_source actor.
*****************************************************************************/
void welt_cpp_file_source_terminate(welt_cpp_file_source *context);

#endif
