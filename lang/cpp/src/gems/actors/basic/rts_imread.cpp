
#include <iostream>

#include "rts_imread.h"

#define MAX_FIFO_COUNT 2

using namespace cv;
using namespace std;

rts_imread::rts_imread(welt_c_fifo_pointer in_fifo, welt_c_fifo_pointer out_fifo,
                       char* img, int index) {
    this->in = (welt_c_fifo_pointer)in_fifo;
    this->out = (welt_c_fifo_pointer)out_fifo;
    this->actor_set_index(index);
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->mode = RTS_IMREAD_MODE_WRITE;
    this->img_file = img;
}


bool rts_imread::enable() {
    bool result = false;

    switch (mode) {
        case RTS_IMREAD_MODE_WRITE:
            result = (welt_c_fifo_population(out) < welt_c_fifo_capacity(out))
                     && (welt_c_fifo_population(in) > 0);
            break;
        case RTS_IMREAD_MODE_INACTIVE:
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void rts_imread::invoke() {

    switch (mode) {
        case RTS_IMREAD_MODE_WRITE: {
            Mat *img_ptr = nullptr;
            welt_c_fifo_read(in, &img_ptr);

            *img_ptr = imread(img_file, IMREAD_COLOR);
            welt_c_fifo_write(out, &img_ptr);
            mode = RTS_IMREAD_MODE_WRITE;

            break;
        }
        case RTS_IMREAD_MODE_INACTIVE:
            mode = RTS_IMREAD_MODE_INACTIVE;
            break;
        default:
            mode = RTS_IMREAD_MODE_INACTIVE;
            break;
    }
}

void rts_imread::reset() {
    mode = RTS_IMREAD_MODE_WRITE;
}

void rts_imread::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* input */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);
}

rts_imread::~rts_imread() {
}
