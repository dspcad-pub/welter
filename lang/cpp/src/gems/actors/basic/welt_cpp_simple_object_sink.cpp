/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>

#include "welt_cpp_simple_object_sink.h"

#include "token_object_example.h"

#define MAX_FIFO_COUNT  1

using namespace std;

welt_cpp_simple_object_sink::welt_cpp_simple_object_sink
        (welt_c_fifo_pointer in, char *file, int index) {
    this->file = file;
    this->fp = welt_c_util_fopen((const char *)this->file, "w");
    this->mode = WELT_CPP_SIMPLE_OBJECT_SINK_MODE_READ;
    this->in = (welt_c_fifo_pointer)in;
    this->actor_set_index(index);
    actor_set_max_port_count(MAX_FIFO_COUNT);

}

bool welt_cpp_simple_object_sink::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_SIMPLE_OBJECT_SINK_MODE_READ:
            result = welt_c_fifo_population(in) >= 1;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_simple_object_sink::invoke() {
    switch (mode) {
        case WELT_CPP_SIMPLE_OBJECT_SINK_MODE_READ: {
            /* create a temporary object */
            token_object_example tokenObjectType(0, 0, 0, nullptr, nullptr);
            /* read the object from fifo */
            welt_c_fifo_read(in, &tokenObjectType);
            /* display the object content */
            fprintf(fp, "Object info:{\n  name: %s\n", tokenObjectType.getName
            ());
            fprintf(fp, "  number1: %d\n", tokenObjectType.getNum1());
            fprintf(fp, "  number2: %f\n", tokenObjectType.getNum2());
            fprintf(fp, "  number3: %f\n", tokenObjectType.getNum3());
            fprintf(fp, "  description: %s\n}\n\n", tokenObjectType.getDesc()
            ->c_str());
            mode = WELT_CPP_SIMPLE_OBJECT_SINK_MODE_READ;
            break;
        }
        default:
            mode = WELT_CPP_SIMPLE_OBJECT_SINK_MODE_READ;
            break;
    }
}

void welt_cpp_simple_object_sink::reset() {
    mode = WELT_CPP_SIMPLE_OBJECT_SINK_MODE_READ;
}

void welt_cpp_simple_object_sink::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

}

welt_cpp_simple_object_sink::~welt_cpp_simple_object_sink() {
    fclose(fp);
    cout << "delete file sink actor" << endl;
}

void welt_cpp_simple_object_sink_terminate(welt_cpp_simple_object_sink
*context) {
    delete context;
}
