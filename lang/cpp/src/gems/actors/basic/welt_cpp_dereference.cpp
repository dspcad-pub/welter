/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>

#include "welt_cpp_dereference.h"

#define MAX_FIFO_COUNT  1

using namespace std;

welt_cpp_dereference::welt_cpp_dereference
        (
                welt_c_fifo_pointer in, welt_c_fifo_pointer out, int index,
                size_t ptr_token_size) {
    this->mode = WELT_CPP_DEREFERENCE_MODE_PROCESS;
    this->in = (welt_c_fifo_pointer)in;
    this->out = (welt_c_fifo_pointer)out;
    this->actor_set_index(index);
//    this->subgraphs = nullptr;
//    this->subgraph_count = 0;
    actor_set_max_port_count(MAX_FIFO_COUNT);
    this->ptr_token_size = ptr_token_size;
//    FIXME: port init_portrefs and add_portrefs functions to cpp
    /* Initialize portrefs*/
    /*
    lide_c_actor_init_portrefs((lide_c_actor_context_type *)context,
        FIFO_COUNT, &context->in1, &context->in2, &context->out);
    */
//    lide_c_actor_init_portrefs((lide_c_actor_context_type *)context);
//    lide_c_actor_add_portrefs((lide_c_actor_context_type *)context,
//                              (lide_c_fifo_pointer *)&context->in1);
//    lide_c_actor_add_portrefs((lide_c_actor_context_type *)context,
//                              (lide_c_fifo_pointer *)&context->in2);
//    lide_c_actor_add_portrefs((lide_c_actor_context_type *)context,
//                              (lide_c_fifo_pointer *)&context->out);
}

bool welt_cpp_dereference::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_DEREFERENCE_MODE_PROCESS:

            result = (welt_c_fifo_population(in) >= 1)
                    && (welt_c_fifo_population(out) <
                    welt_c_fifo_capacity(out));

            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_dereference::invoke() {
    switch (mode) {
        case WELT_CPP_DEREFERENCE_MODE_PROCESS: {
            void **ptr2ptr = (void **)welt_c_util_malloc(ptr_token_size);
            int value_out = 0;
            welt_c_fifo_read(in, ptr2ptr);
            welt_c_fifo_write(out, *ptr2ptr);
            mode = WELT_CPP_DEREFERENCE_MODE_PROCESS;
            break;
        }
        default:
            mode = WELT_CPP_DEREFERENCE_MODE_PROCESS;
            break;
    }
}

void welt_cpp_dereference::reset() {
    mode = WELT_CPP_DEREFERENCE_MODE_PROCESS;
}

void welt_cpp_dereference::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

//    FIXME: following lines are waiting for the util functions being ported
//    /* input 1*/
//    direction = GRAPH_IN_CONN_DIRECTION;
//    port_index = 0;
//    lide_c_graph_add_connection(graph, (lide_c_actor_context_type *)context,
//                                port_index, direction);
//
//    /* output 1*/
//    direction = GRAPH_OUT_CONN_DIRECTION;
//    port_index = 1;
//    lide_c_graph_add_connection(graph, (lide_c_actor_context_type *)context,
//                                port_index, direction);
}

welt_cpp_dereference::~welt_cpp_dereference() {
    //cout << "delete dereference actor" << endl;
}

void welt_cpp_dereference_terminate(welt_cpp_dereference *context) {
    delete context;
}
