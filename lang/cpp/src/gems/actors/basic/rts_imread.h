//
// Created by Yan Zhang on 7/29/2021.
//

#ifndef RTS_IMGREAD_RTS_IMREAD_H
#define RTS_IMGREAD_RTS_IMREAD_H


extern "C" {
#include "welt_c_util.h"
//#include "lide_c_actor.h"
//#include "lide_c_fifo.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>


/* Actor modes */
#define RTS_IMREAD_MODE_WRITE        1
#define RTS_IMREAD_MODE_INACTIVE     2


/* Structure and pointer types associated with file source objects. */
/* developed from welt_cpp_imread */
class rts_imread : public welt_cpp_actor{
public:
    rts_imread(welt_c_fifo_pointer in, welt_c_fifo_pointer out,
               char* img, int index);
    ~rts_imread() override;

    bool enable() override;
    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;

private:
    welt_c_fifo_pointer in;
    welt_c_fifo_pointer out;
//    cv::Mat img_in;
    char* img_file;
};



#endif //RTS_IMGREAD_RTS_IMREAD_H
