/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include <typeinfo>
#include "file_sink_int.h"

using namespace std;

file_sink_int::file_sink_int (welt_c_fifo_pointer in_fifo, char* output_file_name) {
    in = (welt_c_fifo_pointer)in_fifo;
    file_name = output_file_name;
    reset();
	/* Set maximum port count */
	this->actor_set_max_port_count(FILE_SINK_INT_MAX_FIFO_COUNT);
	/* Set portrefs*/
	this->actor_add_portrefs(&this->in);

}

bool file_sink_int::enable() {
    bool result = false;
    switch (mode) {
        case FILE_SINK_INT_MODE_PROCESS:
            result =(welt_c_fifo_population(in) > 0);
            break;
        case FILE_SINK_INT_MODE_ERROR:
            /* Modes that don't produce or consume data are always enabled. */
            result = true;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void file_sink_int::invoke() {
    switch (mode) {
        case FILE_SINK_INT_MODE_PROCESS: {
            welt_c_fifo_read(in, &data);
            outStream << data << endl;
            mode = FILE_SINK_INT_MODE_PROCESS;
            break;
        }
        case FILE_SINK_INT_MODE_ERROR: {
            /* Remain in the same mode, and do nothing. */
            break;
        }
        default:
            mode = FILE_SINK_INT_MODE_ERROR;
        break;
    }
}

void file_sink_int::reset() {
    /* Close the file it is open, and then open/re-open the file so that
    subsequent invocations of the PROCESS MODE write from the beginning of 
    the file. 
    */
    if (outStream.is_open()) {
        outStream.close();
    }
    outStream.open(file_name);
    if (outStream.fail()) {
        cerr << "Could not open file \"" << file_name << "\"" << endl;
        mode = FILE_SINK_INT_MODE_ERROR;
    } else {
        mode = FILE_SINK_INT_MODE_PROCESS;
    }
}

file_sink_int::~file_sink_int() {
    outStream.close();
}

void file_sink_int::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);

}

