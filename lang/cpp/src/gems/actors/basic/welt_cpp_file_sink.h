//
// Created by 谢景 on 5/8/20.
//

#ifndef _welt_cpp_file_sink_h
#define _welt_cpp_file_sink_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

extern "C" {
//    following head file is no longer needed
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}
#include "welt_cpp_actor.h"

/* Actor modes */
#define WELT_CPP_FILE_SINK_MODE_READ        1

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with file source objects. */
class welt_cpp_file_sink : public welt_cpp_actor{
public:
    welt_cpp_file_sink(welt_c_fifo_pointer in, char *file, int index);
    ~welt_cpp_file_sink() override;

    bool enable() override;

    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    FILE *fp;
    char *file;
    welt_c_fifo_pointer in;
};

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

/*****************************************************************************
Construct function of the welt_cpp_file_sink actor. Create a new
welt_cpp_file_sink with the specified file pointer, and the specified output
FIFO pointer. Data format in the input file should be all integers.
*****************************************************************************/
//welt_cpp_file_sink_context *welt_cpp_file_sink_new(lide_c_fifo_pointer
// in);

/*****************************************************************************
Enable function of the welt_cpp_file_sink actor.
*****************************************************************************/
//bool welt_cpp_file_sink_enable(welt_cpp_file_sink_context *context);

/*****************************************************************************
Invoke function of the welt_cpp_file_sink actor.
*****************************************************************************/
//void welt_cpp_file_sink_invoke(welt_cpp_file_sink_context *context);

/*****************************************************************************
Terminate function of the welt_cpp_file_sink actor.
*****************************************************************************/
void welt_cpp_file_sink_terminate(welt_cpp_file_sink *context);

#endif
