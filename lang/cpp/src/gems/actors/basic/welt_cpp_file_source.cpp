/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include <fstream>

#include "welt_cpp_file_source.h"

#define MAX_FIFO_COUNT  1

using namespace std;

welt_cpp_file_source::welt_cpp_file_source
        (welt_c_fifo_pointer out, char *file, int index, int token_type) {
    this->token_type = token_type;
    this->file = file;
    this->fp = welt_c_util_fopen((const char *)this->file, "r");
    switch (this->token_type) {
        case WELT_CPP_TOKEN_TYPE_PRIMITIVE:
            if (fscanf(this->fp, "%d", &this->data) != 1) {
                /* End of input */
                this->mode = WELT_CPP_FILE_SOURCE_MODE_INACTIVE;
                this->data = 0;
            }else{
                this->mode = WELT_CPP_FILE_SOURCE_MODE_WRITE;
            }
            break;
        case WELT_CPP_TOKEN_TYPE_POINTER: {
            /* read all integers from input file, and store them in a buffer */
            int tmp_read_data;
            /* try to read the first integer */
            if (fscanf(this->fp, "%d", &tmp_read_data) != 1) {
                /* End of input */
                this->mode = WELT_CPP_FILE_SOURCE_MODE_INACTIVE;
            }else{
                this->mode = WELT_CPP_FILE_SOURCE_MODE_WRITE;
                /* push the first integer to the buffer */
                this->data_buffer.push_back(tmp_read_data);
                /* read and push the rest of integers */
                while (fscanf(this->fp, "%d", &tmp_read_data) == 1) {
                    this->data_buffer.push_back(tmp_read_data);
                }
                /* initialize the iterator pointing to the first element */
                this->iter = this->data_buffer.begin();
            }
            break;
        }
        case WELT_CPP_TOKEN_TYPE_OBJECT:
            break;
        default:
            break;
    }
    this->out = (welt_c_fifo_pointer)out;
    this->actor_set_index(index);
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
	/* Initialize portrefs*/
	this->actor_add_portrefs(&this->out);
}

bool welt_cpp_file_source::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_FILE_SOURCE_MODE_WRITE:
            result = (welt_c_fifo_population(out) <
                    welt_c_fifo_capacity(out));
            break;
        case WELT_CPP_FILE_SOURCE_MODE_INACTIVE:
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_file_source::invoke() {
    switch (mode) {
        case WELT_CPP_FILE_SOURCE_MODE_WRITE:
            //int data;
            switch (token_type) {
                case WELT_CPP_TOKEN_TYPE_PRIMITIVE:
                    welt_c_fifo_write(out, &data);
                    if (fscanf(fp, "%d", &data) != 1) {
                        /* End of input */
                        mode = WELT_CPP_FILE_SOURCE_MODE_INACTIVE;
                    }else{
                        mode = WELT_CPP_FILE_SOURCE_MODE_WRITE;
                    }
                    break;
                case WELT_CPP_TOKEN_TYPE_POINTER: {
                    /* If the iterator hits the end, there is no more data to
                    * be send to fifo. The actor will be inactivated. */
                    if (iter == data_buffer.end()){
                        /* End of input */
                        mode = WELT_CPP_FILE_SOURCE_MODE_INACTIVE;
                        break;
                    }
                    /* write the iterator (which is a pointer to the element
                    * to be output) to the fifo */
                    welt_c_fifo_write(this->out, &iter);
                    /* iterator moves forward to the next element */
                    iter++;
                    break;
                }
                case WELT_CPP_TOKEN_TYPE_OBJECT:
                    //FIXME
                    break;
                default:
                    //       this->getindex());
                    mode = WELT_CPP_FILE_SOURCE_MODE_INACTIVE;
                    break;
            }

            break;
        case WELT_CPP_FILE_SOURCE_MODE_INACTIVE:
            mode = WELT_CPP_FILE_SOURCE_MODE_INACTIVE;
            break;
        default:
            mode = WELT_CPP_FILE_SOURCE_MODE_INACTIVE;
            break;
    }

//    welt_c_fifo_write(out, &img_send);
}

void welt_cpp_file_source::reset() {
    fclose(fp);
    fp = welt_c_util_fopen((const char *)file, "r");
    if (fscanf(fp, "%d", &data) != 1) {
        mode = WELT_CPP_FILE_SOURCE_MODE_INACTIVE;
        data = 0;

    }else{
        mode = WELT_CPP_FILE_SOURCE_MODE_WRITE;
    }
}

void welt_cpp_file_source::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* output 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);
}

welt_cpp_file_source::~welt_cpp_file_source() {
    switch (token_type) {
        case WELT_CPP_TOKEN_TYPE_PRIMITIVE:
            fclose(fp);
            break;
        default:
            break;
    }
    //cout << "delete file source actor" << endl;
}

void welt_cpp_file_source_terminate(welt_cpp_file_source
*context) {
    delete context;
}
