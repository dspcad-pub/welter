#ifndef _RTS_VECTOR_H
#define _RTS_VECTOR_H

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/*******************************************************************************
This actor inheriting from reference_token_source base class generates vector
type reference tokens. Each reference token is a pointer to a 2D vector of
integers.

Output(s): The token type of the output is a reference token.

Parameter(s): rd_size gives the number of bytes in a
single token slot (RD stands for "referenced data").
store_size gives the number of token slots that
are maintained in this actor --- that is, the number of
referenced tokens that can coexist before the old one
gets reused by a new referenced token.
*******************************************************************************/

extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"
#include "reference_token_source.h"

template<typename T, std::size_t sz>
struct vector_type {
    using type = std::vector<typename vector_type<T, sz - 1>::type>;
};
template<typename T>
struct vector_type<T, 1> {
    using type = T;
};
template<typename T, std::size_t sz>
using vector_type_t = typename vector_type<T, sz>::type;

template <typename T>
struct VectorGenerator {
    static auto Generate(std::size_t last_arg) {
        return new std::vector<T>(last_arg);
    }

    template <typename ...Args>
    static auto Generate(std::size_t first_arg, Args... args) {
        return new std::vector<vector_type_t<T, 1 + sizeof...(args)>>(first_arg,
                *VectorGenerator<T>::Generate(args...));
    }
};

class rts_vector : public reference_token_source {
public:
    /*************************************************************************
    Construct a rts_vector actor with store_size, 2d vector size, and the
    specified input FIFO connections, and the specified output FIFO connection.
    *************************************************************************/
    /* constructor/function with template has to be implemented in place in
     * header file */
    template <typename ...Args>
    rts_vector(welt_c_fifo_pointer out, int store_size, std::size_t
    first_arg, Args... args): reference_token_source
        (out, store_size){
//    allocate_store();
        for (int i =0; i<get_store_size(); i++){
            get_data()[i] = vectorGen(first_arg, args...);
        }
//#define NDIM sizeof...(args)+2
//        using type2delete = vector_type_t<int, sizeof...(args)+2>;
    };

    ~rts_vector() override;

    /* Override the virtual rd_allocate function in the base class. This
     * function allocates space for a 2d vector and return the pointer of that
     * vector */
    void *rd_allocate() final ;

    void rd_deallocate(void *data) override;

//    template <typename ...Args>
//    void *vectorGen(std::size_t first_arg, Args... args);
    template <typename ...Args>
    void *vectorGen(std::size_t first_arg, Args... args){
        auto grid_ptr = VectorGenerator<int>::Generate (first_arg, args...);
        return grid_ptr;
    }
private:
};


#endif //_RTS_VECTOR_H
