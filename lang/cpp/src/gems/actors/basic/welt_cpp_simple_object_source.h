//
// Created by 谢景 on 5/8/20.
//

#ifndef _welt_cpp_simple_object_source_h
#define _welt_cpp_simple_object_source_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/* This actor is designed to test the functionality of transmitting a object
* through fifos. There is a parser inside, requiring a specific input format
* file. The output is a token_object_type object. Please see
* token_object_type.h for details of the object.
* The input file format should be fit in the format shown below:
* <object name>|<number1>|<number2>|<number3>|<description>
* ,where <object name> is a string no longer than the
* TOKEN_OBJECT_TYPE_C_STRING_MAX_LENGTH,
* <number1> is an integer number,
* <number2> is a double number,
* <number3> is a float number,
* <description> is a string.
* e.g. object 1|1|3.4|4.5|The first object */

extern "C" {
//    following head file is no longer needed
//#include "lide_c_actor.h"
#include "welt_c_util.h"
//#include "lide_c_fifo.h"
#include "welt_c_fifo.h"

#include "string_parsing.h"
}
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include "welt_cpp_actor.h"
//#include "welt_cpp_graph_context.h"


/* Actor modes */
#define WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_WRITE        1
#define WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE     2

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with file source objects. */
class welt_cpp_simple_object_source : public welt_cpp_actor{
public:
    welt_cpp_simple_object_source(
            welt_c_fifo_pointer out, char *file, int index);
    ~welt_cpp_simple_object_source() override;

    bool enable() override;
    void invoke() override;

    void reset() override;

    void connect(welt_cpp_graph *graph) override;


private:
    welt_c_fifo_pointer out;
    char *file;
    char *char_data;
    string *string_data;

    fstream fp;
};

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

/*****************************************************************************
Construct function of the welt_cpp_simple_object_source actor. Create a new
welt_cpp_simple_object_source with the specified file pointer, and the
specified output
FIFO pointer. Data format in the input file should be all integers.
*****************************************************************************/
//welt_cpp_simple_object_source_context *welt_cpp_simple_object_source_new
// (lide_c_fifo_pointer
// in);

/*****************************************************************************
Enable function of the welt_cpp_simple_object_source actor.
*****************************************************************************/
//bool welt_cpp_simple_object_source_enable
// (welt_cpp_simple_object_source_context
// *context);

/*****************************************************************************
Invoke function of the welt_cpp_simple_object_source actor.
*****************************************************************************/
//void welt_cpp_simple_object_source_invoke
// (welt_cpp_simple_object_source_context
// *context);

/*****************************************************************************
Terminate function of the welt_cpp_simple_object_source actor.
*****************************************************************************/
void welt_cpp_simple_object_source_terminate(welt_cpp_simple_object_source
*context);

#endif
