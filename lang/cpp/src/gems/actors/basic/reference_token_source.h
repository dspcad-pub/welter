#ifndef _reference_token_source_h
#define _reference_token_source_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
/*******************************************************************************
Overview: This actor maintains an internal storage (store) of token slots of a
specific size, and produces output tokens that encapsulate pointers to token
slots in the store. The actor can therefore be used to implement tokens that
are passed along dataflow graph edges by reference rather than by value. Since
they point to contiguous regions of memory that can hold individual tokens (to
be produced or consumed by other actors), tokens that are output by this actor
are referred to as "reference tokens". We refer to a "referenced token" as a
token that is produced by another actor, and whose storage is maintained in a
token slot produced by this actor.

Each firing of the actor outputs a reference token that points to the next
token slot in the store, where the slots are visited in a circular fashion.
Thus, if a token slot S is pointed to by the referenced token produced by the
ith firing of this actor, then S will be pointed to by the referenced tokens
produced by firings i + N, i + 2N, ..., where N is the number of slots in the
store (store_size). This means that the token slots are reused with a
periodicity of N firings. It is the scheduler's responsibility to ensure that
when a token slot is reused, the token data in that slot is no longer needed in
the graph.  In other words, when a reference token T is produced by the ith
firing of this actor, all use of the token data referenced by T should be
complete before the actor starts its (i + N)th firing. This simple rule is
important to apply when performing analysis to properly schedule the
enclosing graph and configure the store size parameter N.

Input(s): This is a source actor; it has no input edges.

Output(s):  The output token type is a "reference token", which
is a pointer to (address of) a token slot.

Parameter(s): rd_size gives the number of bytes in a
single token slot (RD stands for "referenced data").
store_size gives the number of token slots that
are maintained in this actor --- that is, the number of
referenced tokens that can coexist before the old one
gets reused by a new referenced token.

Mode Transitions: The actor starts in the PROCESS mode and remains in
this mode unless an error is encountered. The actor
transitions to the ERROR mode if an error is encountered.
*******************************************************************************/

extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}
#include "welt_cpp_actor.h"

/* reference_token_source class, it inherits from the actor class. */
class reference_token_source : public welt_cpp_actor{
public:
    /***************************************************************************
    Modes
    ***************************************************************************/
    static const int mode_process = 0;
    static const int mode_error = 1;

    /*************************************************************************
    Construct a new reference_token_source source actor that is associated with
    the given referenced data size rd_size and token store size store_size, and
    connect the new actor to the given output FIFO.
    *************************************************************************/
    reference_token_source(welt_c_fifo_pointer out, int store_size);

    /* Deallocate the token store before terminating the actor.
    */
    ~reference_token_source() override;

    /* The enable function checks whether the output fifo has enough free space.
    */
    bool enable() override;

    /* Fires the actor in the current mode. */
    void invoke() override;

    /* Reset method: reset the actor.*/
    void reset() override;

    /* Associate the actor with a graph. */
    void connect(welt_cpp_graph *graph) override;

    /* Get store_size */
    int get_store_size();

    /* A token slot is consider "used" if it has been referenced by
       at least one produced referenced token. This method returns
       the number of used token slots in the store. */
    int get_population();

    /* The free space is the number of slots that have not yet been
       used. Equivalently it is the store size minus the population. */
    int get_free_space();

    /* The rd_allocate method of a reference token source (RTS) actor
     * corresponds to the rd_allocate function that allocates space for a single
     * data element. Every RTS actor must have an rd_allocate function. */
    virtual void* rd_allocate() = 0;

    /* The rd_deallocate method of a reference token source (RTS) actor
     * corresponds to the rd_deallocate function that free space for a single
     * data element. Every RTS actor must have an rd_allocate function. */
    virtual void rd_deallocate(void* data) = 0;

    /* The allocate_store method of a reference token source (RTS) actor
     * corresponds to the allocate_store function that allocates space for all
     * of the data elements by calling derived rd_allocate() iteratively. */
    void allocate_store();

    /* The deallocate_store method of a reference token source (RTS) actor
     * corresponds to the deallocate_store function that free space for all of
     * the data elements by calling derived rd_deallocate() iteratively. */
    void deallocate_store();

    /*diagnostic for debugging*/
    int diagnostic();

protected:
    /* get the pointer to the head of the data pointer array */
    void** get_data();

private:
    /* The number of data pointers in the array */
    int _store_size;
    /* Output FIFO */
    welt_c_fifo_pointer out;
    /*Population*/
    int pop;
    /* Current index of the ptrs */
    int curr;
    /* An array of pointers, each element in the array points to a
    distinct token slot. */
    void ** ptrs;
};

#endif
