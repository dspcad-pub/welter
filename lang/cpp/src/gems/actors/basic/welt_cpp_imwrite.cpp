/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>

#include "welt_cpp_imwrite.h"

#define MAX_FIFO_COUNT 1

using namespace cv;
using namespace std;

// constructor
welt_cpp_imwrite::welt_cpp_imwrite(welt_c_fifo_pointer in, char *filename,
        int index) {
    this->in = (welt_c_fifo_pointer)in;
    this->actor_set_index(index);
    actor_set_max_port_count(MAX_FIFO_COUNT);
    this->mode = WELT_CPP_IMWRITE_MODE_WRITE;
    this->img_file= filename;
}

bool welt_cpp_imwrite::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_IMWRITE_MODE_WRITE:
            result = (welt_c_fifo_population(this->in) <
                    welt_c_fifo_capacity(this->in));
            break;
        case WELT_CPP_IMWRITE_MODE_INACTIVE:
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_imwrite::invoke() {

    switch (mode) {
        case WELT_CPP_IMWRITE_MODE_WRITE: {
            Mat* img_in= nullptr;
            welt_c_fifo_read(this->in, &img_in);
            Mat temp_in = *img_in;
            imwrite(this->img_file, temp_in);
            mode = WELT_CPP_IMWRITE_MODE_WRITE;
            break;
        }
        case WELT_CPP_IMWRITE_MODE_INACTIVE:
            mode = WELT_CPP_IMWRITE_MODE_INACTIVE;
            break;
        default:
            mode = WELT_CPP_IMWRITE_MODE_INACTIVE;
            break;
    }
}

void welt_cpp_imwrite::reset() {
    mode = WELT_CPP_IMWRITE_MODE_WRITE;
}

void welt_cpp_imwrite::connect(welt_cpp_graph *graph) {

    int port_index;
    int direction;

    /* Input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);
}

welt_cpp_imwrite::~welt_cpp_imwrite() {
    //cout << "delete imwrite actor" << endl;
}

void welt_cpp_imwrite_terminate(welt_cpp_imwrite *context) {

}
