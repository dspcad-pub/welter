#ifndef _welt_cpp_add_h
#define _welt_cpp_add_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}
#include "welt_cpp_actor.h"


/* Actor modes */
#define WELT_CPP_ADD_MODE_PROCESS        1

/*Maximum number of FIFO*/
#define MAX_FIFO_COUNT  4

/* Structure and pointer types associated with file source objects. */
class welt_cpp_add : public welt_cpp_actor{
public:
    /*Constructor*/
    welt_cpp_add(welt_c_fifo_pointer in1, welt_c_fifo_pointer in2,
                welt_c_fifo_pointer out, int index);
    /*Deconstructor*/
    ~welt_cpp_add() override;
    bool enable() override;
    void invoke() override;
    void reset() override;
    void connect(welt_cpp_graph *graph) override;

private:
    /* Actor interface ports. */
    welt_c_fifo_pointer in1;
    welt_c_fifo_pointer in2;
    welt_c_fifo_pointer out;
};

/*****************************************************************************
Terminate function of the welt_cpp_add actor.
*****************************************************************************/
void welt_cpp_add_terminate(welt_cpp_add *context);

#endif
