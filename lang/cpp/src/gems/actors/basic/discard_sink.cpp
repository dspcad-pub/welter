/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "discard_sink.h"

using namespace std;

discard_edge_sink::discard_edge_sink (welt_c_fifo_pointer in) {
    mode = mode_process;
    this->in = (welt_c_fifo_pointer)in;
};

bool discard_edge_sink::enable() {
    /* The enable method should check whether the actor is ready
    * to be fired by examining the input/output fifos.
    */
    bool result = false;
    switch (mode) {
        case mode_process:
            result =(welt_c_fifo_population(in) > 0);
            break;
        case mode_error:
            result = true;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void discard_edge_sink::invoke() {
    switch (mode) {
        case mode_process: {
            cout << "removing the pointer from discard_sink" << endl;
            /* Reads the data and does nothing */
            welt_c_fifo_read_advance(in);
            break;
        }
        case mode_error: {
            /* Remain in the same mode, and do nothing. */
            break;
        }
        default:
            mode = mode_process;
            break;
    }
}

void discard_edge_sink::reset() {
    mode = mode_process;
}

discard_edge_sink::~discard_edge_sink() {
}

void discard_edge_sink::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);
}
