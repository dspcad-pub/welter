/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/



#include "welt_cpp_simple_object_source.h"

#include "token_object_example.h"


#define MAX_FIFO_COUNT  1

using namespace std;

// constructor
welt_cpp_simple_object_source::welt_cpp_simple_object_source
        (welt_c_fifo_pointer out, char *file, int index) {
    this->file = file;
    this->fp.open(this->file);
    this->out = (welt_c_fifo_pointer)out;
    this->actor_set_index(index);
//    this->subgraph_count = 0;
    actor_set_max_port_count(MAX_FIFO_COUNT);
    this->char_data = new char[TOKEN_OBJECT_TYPE_C_STRING_MAX_LENGTH + 1];
    this->string_data = new string();
    this->mode = WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_WRITE;


}

bool welt_cpp_simple_object_source::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_WRITE:
            result = (welt_c_fifo_population(out) <
                    welt_c_fifo_capacity(out));
            break;
        case WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE:
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_simple_object_source::invoke() {
    switch (mode) {
        case WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_WRITE: {
            if (!fp.is_open()) {
                cout << "cannot open file" << endl;
                mode = WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE;
                break;
            }
            /* read line by line and convert to C string */
            string newline;
            getline(fp, newline);
            if (newline.empty()){
                mode = WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE;
                cout << "end of input" << endl;
                break;
            }
            const char* newCLine = newline.c_str();

            /* Parser */
            std::vector<char*> items;
            char* tmp_char_ptr = (char*)(newCLine);
            char* char_extracted = strtok(tmp_char_ptr, "|");
            while(char_extracted)
            {
                std::cout << char_extracted << '\n';
                items.push_back(char_extracted);
                char_extracted = strtok(NULL, "|");
            }

            /* Check the format of input */
            if (!(sp_is_numeric(items[1]) && sp_is_numeric(items[2])
                    && sp_is_numeric(items[3]) && (strlen(items[0]) <=
                    TOKEN_OBJECT_TYPE_C_STRING_MAX_LENGTH)
            )) {
                mode = WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE;
                cout << "invalid input format: " << newline << endl;
                cout << "c string strlen: " << strlen(items[3]) << endl;
                break;
            }
            /* convert strings to numbers */
            int num1 = 0;
            double num2 = 0;
            float num3 = 0;
            sscanf(items[1], "%d", &num1);
            sscanf(items[2], "%lf", &num2);
            sscanf(items[3], "%f", &num3);
            /* Copy to save the strings into the actor */
            strcpy(this->char_data, items[0]);
            string_data->assign(items[4]);
            /* Create and write the object */
            token_object_example tokenObjectType = token_object_example(num1,
                    num2, num3, this->char_data, string_data);
            welt_c_fifo_write(out, &tokenObjectType);
            break;
        }
        case WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE:
            mode = WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE;
            break;
        default:
            mode = WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE;
            break;
    }

}

void welt_cpp_simple_object_source::reset() {
    fp.close();
    fp.open(this->file,ios::out);
    if (!fp){
        mode = WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_INACTIVE;
    }else{
        mode = WELT_CPP_SIMPLE_OBJECT_SOURCE_MODE_WRITE;
    }
}

void welt_cpp_simple_object_source::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

}

welt_cpp_simple_object_source::~welt_cpp_simple_object_source() {
    fp.close();
    delete char_data;
    delete string_data;
    //cout << "delete file source actor" << endl;
}

void welt_cpp_simple_object_source_terminate(welt_cpp_simple_object_source
*context) {
    delete context;
}
