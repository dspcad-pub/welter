#ifndef _fork_h
#define _fork_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/*******************************************************************************
Overview: This actor broadcast tokens from one input port to two output ports.
The token itself is deep copied. Users have to specify the size of the token
when constructing the actor.

Input(s): Token of any type to be broadcast.

Output(s):  The two broadcast tokens.

Actor Modes and transitions:
INACTIVE mode will not do anything. If there is no input in the tokens, the
program will be in INACTIVE mode.
ACTIVE mode will read one token from the input FIFO according to the token_size
given, then propagates to two tokens to the output FIFOs.

Initial mode: The actor starts out in the ACTIVE mode if there is
a token to be read in the input file. Otherwise, it starts out
in the INACTIVE mode.
*******************************************************************************/

extern "C" {
#include "welt_c_util.h"
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"
#include "welt_cpp_graph.h"

#define FORK_MODE_INACTIVE              1
#define FORK_MODE_ACTIVE                2

class fork : public welt_cpp_actor{
public:
    /*************************************************************************
    Construct a new fork actor that is associated with the given
    token size, and connect the new actor to the given output FIFO.
    *************************************************************************/
    fork(welt_c_fifo_pointer in, welt_c_fifo_pointer out1,
         welt_c_fifo_pointer out2, size_t token_size, int index);

    virtual ~fork();

    /* Deallocate the token store before terminating the actor.
    */
    bool enable() override;

    /* Fires the actor in the current mode. */
    void invoke() override;

    /* Reset method: reset the actor.*/
    void reset() override;

    /* Associate the actor with a graph. */
    void connect(welt_cpp_graph *graph) override;

private:
    /*Actor interface ports.*/
    welt_c_fifo_pointer in;
    welt_c_fifo_pointer out1;
    welt_c_fifo_pointer out2;
    /* size of the token */
    size_t size;
};

#endif

