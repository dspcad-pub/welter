/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>

#include "welt_cpp_imread.h"

#define MAX_FIFO_COUNT 1

using namespace cv;
using namespace std;

// constructor
welt_cpp_imread::welt_cpp_imread(welt_c_fifo_pointer out_fifo, char* img,
        int index) {
    this->out = (welt_c_fifo_pointer)out_fifo;
    this->actor_set_index(index);
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    this->mode = WELT_CPP_IMREAD_MODE_WRITE;
    this->img_file = img;
}

bool welt_cpp_imread::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_IMREAD_MODE_WRITE:
            result = (welt_c_fifo_population(out) <
                    welt_c_fifo_capacity(out));
            break;
        case WELT_CPP_IMREAD_MODE_INACTIVE:
            result = false;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_imread::invoke() {

    switch (mode) {
        case WELT_CPP_IMREAD_MODE_WRITE: {
            Mat img_read = imread(this->img_file);
            if (!img_read.data) {
                /* End of input */
                mode = WELT_CPP_IMREAD_MODE_INACTIVE;
                cout <<"There is no input image"<<endl;
            } else {
                img_read.copyTo(this->img_in);
                Mat *img_send = &(this->img_in);
                welt_c_fifo_write(this->out, &img_send);
                mode = WELT_CPP_IMREAD_MODE_WRITE;
            }
            break;
        }
        case WELT_CPP_IMREAD_MODE_INACTIVE:
            mode = WELT_CPP_IMREAD_MODE_INACTIVE;
            break;
        default:
            mode = WELT_CPP_IMREAD_MODE_INACTIVE;
            break;
    }
}

void welt_cpp_imread::reset() {
    mode = WELT_CPP_IMREAD_MODE_WRITE;
}

void welt_cpp_imread::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);
    //cout << "in_image" << endl;


}

welt_cpp_imread::~welt_cpp_imread() {
}

