/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_add.h"

using namespace std;
/*******************************************************************************
ADD STRUCTURE DEFINITION
*******************************************************************************/

/*****************************************************************************
Construct welt_cpp_add actor class. Create a new
welt_cpp_inner_product with the specified input FIFO pointers x for the
first vector, the specified input FIFO pointer y for the second vector, and the
specified output FIFO pointer.
*****************************************************************************/
welt_cpp_add::welt_cpp_add
        (welt_c_fifo_pointer in1, welt_c_fifo_pointer in2,
        welt_c_fifo_pointer out, int index) {
    /* Set mode */
    this->mode = WELT_CPP_ADD_MODE_PROCESS;
    /*Set inputs and outputs(fifo)*/
    this->in1 = (welt_c_fifo_pointer)in1;
    this->in2 = (welt_c_fifo_pointer)in2;
    this->out = (welt_c_fifo_pointer)out;
    /* Set index */
    this->actor_set_index(index);
    /* Set maximum port count */
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    /* Set portrefs*/
    this->actor_add_portrefs(&this->in1);
    this->actor_add_portrefs(&this->in2);
    this->actor_add_portrefs(&this->out);

}

bool welt_cpp_add::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_ADD_MODE_PROCESS:
            result = (welt_c_fifo_population(in1) >= 1)
                    && (welt_c_fifo_population(in2) >= 1)
                    && (welt_c_fifo_population(out) <
                    welt_c_fifo_capacity(out));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_add::invoke() {
    switch (mode) {
        case WELT_CPP_ADD_MODE_PROCESS: {
            int value1 = 0;
            int value2 = 0;
            int sum = 0;
            welt_c_fifo_read(in1, &value1);
            welt_c_fifo_read(in2, &value2);
            sum = value1 + value2;
            welt_c_fifo_write(out, &sum);
            mode = WELT_CPP_ADD_MODE_PROCESS;
            break;
        }
        default:
            mode = WELT_CPP_ADD_MODE_PROCESS;
            break;
    }
}

void welt_cpp_add::reset() {
    mode = WELT_CPP_ADD_MODE_PROCESS;
}

void welt_cpp_add::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);
    //cout << "in1" << endl;

    /* input 2*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection( this, port_index, direction);
    //cout << "in2" << endl;

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);
    //cout << "out" << endl;
}

welt_cpp_add::~welt_cpp_add() {
    //cout << "delete add actor" << endl;
}

void welt_cpp_add_terminate(welt_cpp_add *context) {
    delete context;
}
