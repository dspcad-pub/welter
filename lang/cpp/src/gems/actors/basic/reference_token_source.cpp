/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "reference_token_source.h"
extern "C" {
#include "stlib.h"
}

using namespace std;

reference_token_source::reference_token_source (welt_c_fifo_pointer out, int
store_size_in) {
    mode = mode_process;
    this->out = (welt_c_fifo_pointer)out;
	this->_store_size = store_size_in;
    ptrs = (void**)xcalloc(store_size_in, sizeof(void*));
//    for (int i =0; i<get_store_size(); i++){
//        ptrs[i] = xcalloc(get_rd_size(),1);
//    }
    curr = 0;
    pop = 0;
};

bool reference_token_source::enable() {
    /* The enable method should check whether the actor is ready
    * to be fired by examining the input/output fifos.
    */
    bool result = false;
    switch (mode) {
        case mode_process:
            result =(welt_c_fifo_population(out) < welt_c_fifo_capacity(out));
            break;
        case mode_error:
            result = true;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void reference_token_source::invoke() {
    switch (mode) {
        case mode_process: {
            /* Write the pointer to output FIFO */
            welt_c_fifo_write(out, &(ptrs[curr]));
            /* This actor uses circular fashion*/
            if(curr == _store_size-1){
                curr=0;
            }else{
                curr++;
            }
            /* Increase the population by one in each invocation until the
            storage is full */
            if (pop < _store_size){
                pop++;
            }
            break;
        }
        case mode_error: {
            /* Remain in the same mode, and do nothing. */
            break;
        }
        default:
            mode = mode_process;
            break;
    }
}

void reference_token_source::reset() {
    mode = mode_process;
    curr = 0;
}

reference_token_source::~reference_token_source() {
    free(ptrs);
}

void reference_token_source::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* Register the port in enclosing graph. */
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);
}

int reference_token_source::get_store_size(){
    return _store_size;
}

int reference_token_source::get_population(){
    return pop;
}

int reference_token_source::get_free_space(){
    return _store_size-pop;
}

void **reference_token_source::get_data() {
    return ptrs;
}
int reference_token_source::diagnostic(){
	cout << "rts actor, store_size: " << _store_size << endl;
	return 0;
}

void reference_token_source::allocate_store() {
    for (int i =0; i<get_store_size(); i++){
        get_data()[i] = rd_allocate();
    }
}

void reference_token_source::deallocate_store() {
    for (int i = 0; i<get_store_size(); i++){
        rd_deallocate(get_data()[i]);
    }
}
