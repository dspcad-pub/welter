/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include <cstring>

#include "fork.h"

#define MAX_FIFO_COUNT  3

using namespace std;

fork::fork(welt_c_fifo_pointer in, welt_c_fifo_pointer out1,
           welt_c_fifo_pointer out2, size_t token_size, int index) {
    /* Set actor mode to ACTIVE */
    this->mode = FORK_MODE_ACTIVE;
    /* input fifo */
    this->in = (welt_c_fifo_pointer) in;
    /* two output fifos */
    this->out1 = (welt_c_fifo_pointer) out1;
    this->out2 = (welt_c_fifo_pointer) out2;
    this->size = token_size;
    this->actor_set_index(index);
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
}

fork::~fork() {
}

bool fork::enable() {
    bool retval = false;
    switch (mode) {
        case FORK_MODE_INACTIVE:retval = false;
            break;
        case FORK_MODE_ACTIVE:
            retval = (welt_c_fifo_population(in) >= 1) &&
                    (welt_c_fifo_population(out1)<welt_c_fifo_capacity(out1)) &&
                    (welt_c_fifo_population(out2)<welt_c_fifo_capacity(out2));
            break;
        default:retval = false;
            break;
    }
    return retval;
}

void fork::invoke() {
    switch (mode) {
        /* Inactive actor state */
        case FORK_MODE_INACTIVE:mode = FORK_MODE_INACTIVE;
            break;
            /* Active actor state */
        case FORK_MODE_ACTIVE: {
            void* token_in = malloc(this->size);
            welt_c_fifo_read(in, token_in);
            welt_c_fifo_write(out1, token_in);

            void *token_clone = malloc(this->size);
            /*form a new token*/
            memcpy(token_clone, token_in, this->size);

            welt_c_fifo_write(out2, (token_clone));
            free(token_clone);

            mode = FORK_MODE_ACTIVE;
            break;
        }
        default:mode = FORK_MODE_INACTIVE;
            break;
    }
}

void fork::reset() {
    mode = FORK_MODE_ACTIVE;
}

void fork::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection(this, port_index, direction);

    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection(this, port_index, direction);

    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);
}
