#ifndef _discard_edge_sink_h
#define _discard_edge_sink_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
/*******************************************************************************
Overview: This actor discards the token which is in the connected FIFO. For
each invocation, it will discard one token from the FIFO.

Input(s): The input token type can be any type of tokens.

Output(s): This is a sink actor; it has no output edges.

Mode Transitions: The actor starts in the PROCESS mode and remains in
this mode unless an error is encountered. The actor
transitions to the ERROR mode if an error is encountered.

Dataflow table:
Mode       in_fifo
----------------------------
ERROR       -1
PROCESS     0

*******************************************************************************/

extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}

#include "welt_cpp_actor.h"

/* discard_sink class, it inherits from the actor class. */
class discard_edge_sink : public welt_cpp_actor{
public:
    /***************************************************************************
    Modes
    ***************************************************************************/
    static const int mode_process = 0;
    static const int mode_error = 1;

    /*************************************************************************
    Construct a new discard sink actor and connect the new actor to the given
    input FIFO.
    *************************************************************************/
    discard_edge_sink(welt_c_fifo_pointer input);

    ~discard_edge_sink() override;

    /* The enable function checks whether there is a token in input fifo
    */
    bool enable() override;

    /* Fires the actor in the current mode. */
    void invoke() override;

    /* Reset method: reset the actor.*/
    void reset() override;

    /* Associate the actor with a graph. */
    void connect(welt_cpp_graph *graph) override;

private:

    /* Input FIFO */
    welt_c_fifo_pointer in;
};

#endif
