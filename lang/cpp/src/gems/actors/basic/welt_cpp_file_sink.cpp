/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2021
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>

#include "welt_cpp_file_sink.h"

#define MAX_FIFO_COUNT  1

using namespace std;

welt_cpp_file_sink::welt_cpp_file_sink
        (welt_c_fifo_pointer in, char *file,
        int index) {
    this->file = file;
    this->fp = welt_c_util_fopen((const char *)this->file, "w");
    this->mode = WELT_CPP_FILE_SINK_MODE_READ;
    this->in = (welt_c_fifo_pointer)in;

	/* Set index */
	this->actor_set_index(index);
	/* Set maximum port count */
	this->actor_set_max_port_count(MAX_FIFO_COUNT);
    /* Initialize portrefs*/
    this->actor_add_portrefs(&this->in);
}

bool welt_cpp_file_sink::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_FILE_SINK_MODE_READ:
            result = welt_c_fifo_population(in) >= 1;
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_file_sink::invoke() {
    switch (mode) {
        case WELT_CPP_FILE_SINK_MODE_READ: {

            int value = 0;
            //welt_c_fifo_read(
            //        (welt_c_fifo_pointer)*(this->portrefs[0]), &value);
            welt_c_fifo_read(this->in, &value);
            fprintf(this->fp, "%d\n", value);
            mode = WELT_CPP_FILE_SINK_MODE_READ;
            break;
        }
        default:
            mode = WELT_CPP_FILE_SINK_MODE_READ;
            break;
    }
}

void welt_cpp_file_sink::reset() {
    mode = WELT_CPP_FILE_SINK_MODE_READ;
}

void welt_cpp_file_sink::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

//    following lines are waiting for the util functions being ported
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);

}

welt_cpp_file_sink::~welt_cpp_file_sink() {
    //cout << "delete file sink actor" << endl;
}

void welt_cpp_file_sink_terminate(welt_cpp_file_sink *context) {
    delete context;
}
