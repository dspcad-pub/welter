#ifndef _welt_cpp_hand_detection_h
#define _welt_cpp_hand_detection_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
extern "C" {
#include "welt_c_util.h"
#include "welt_c_fifo.h"
}
#include "welt_cpp_actor.h"
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

/* Actor modes */
#define WELT_CPP_HAND_DETECTION_MODE_PROCESS        1

/*Maximum number of FIFO*/
#define MAX_FIFO_COUNT  3

/* Structure and pointer types associated with file source objects. */
class welt_cpp_hand_detection : public welt_cpp_actor{
public:
    /*Constructor*/
    welt_cpp_hand_detection(welt_c_fifo_pointer img_in,
                 welt_c_fifo_pointer val_in,
                 welt_c_fifo_pointer out,
                 int index);
    ~welt_cpp_hand_detection() override;
    bool enable() override;
    void invoke() override;
    void reset() override;
    void connect(welt_cpp_graph *graph) override;

private:
    /* Actor interface ports. */
    welt_c_fifo_pointer img_in; /* Input Image */
    welt_c_fifo_pointer val_in; /* Input Range */
    welt_c_fifo_pointer out; /* Output Image */
};

/*****************************************************************************
Terminate function of the welt_cpp_hand_detection actor.
*****************************************************************************/
void welt_cpp_hand_detection_terminate(welt_cpp_hand_detection *context);

#endif
