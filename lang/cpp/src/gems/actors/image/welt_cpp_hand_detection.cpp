/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_hand_detection.h"

using namespace std;
using namespace cv;

welt_cpp_hand_detection::welt_cpp_hand_detection
        (welt_c_fifo_pointer img_in,
         welt_c_fifo_pointer in2,
         welt_c_fifo_pointer out,
         int index) {
    this->mode = WELT_CPP_HAND_DETECTION_MODE_PROCESS;

    this->img_in = (welt_c_fifo_pointer)img_in;   /*image input*/
    this->val_in = (welt_c_fifo_pointer)in2;   /*file input*/
    this->out = (welt_c_fifo_pointer)out;
    this->actor_set_index(index);
    this->actor_set_max_port_count(MAX_FIFO_COUNT);


}

bool welt_cpp_hand_detection::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_HAND_DETECTION_MODE_PROCESS:
            result = (welt_c_fifo_population(this->img_in) >= 1)
                     && (welt_c_fifo_population(this->val_in) >= 1)
                     && (welt_c_fifo_population(this->out) <
                         welt_c_fifo_capacity(this->out));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_hand_detection::invoke() {
    switch (mode) {
        case WELT_CPP_HAND_DETECTION_MODE_PROCESS: {

            int values[6]={0,};
            int sum = 0;
            Mat *img;

            welt_c_fifo_read(this->img_in, &img);
            welt_c_fifo_read_block(this->val_in, values, 6);
            
            Mat mask= Mat::zeros((*img).size(),(*img).type()); 
            Mat desimg= Mat::zeros((*img).size(),(*img).type());

            Vec3b intensity;  
            Vec3b* mptr;

            uchar blue_upper= values[0] , blue_lower=values[1];
            uchar green_upper=values[2], green_lower=values[3];
            uchar red_upper=values[4], red_lower=values[5];

            int count=0;

            for(int r=0;r<(*img).rows;++r){
                mptr=mask.ptr<Vec3b>(r);  
                for(int c=0;c<(*img).cols;++c){
                    intensity=(*img).at<uchar>(r,c); 
                    if(intensity.val[0]<=blue_upper&&intensity.val[0]>=blue_lower){
                        if(intensity.val[1]<=green_upper&&intensity.val[1]>=green_lower){
                            if(intensity.val[2]<=red_upper&&intensity.val[2]>=red_lower){
                                ++count;
                                mptr[c]=Vec3b(255,255,255); //make this pixel white
                            }
                        }
                    }
                }
            }

            welt_c_fifo_write(out, &count);
            cout << " write " <<endl;


            (*img).copyTo(desimg,mask);
            namedWindow("Hand-masked image");
            imshow("Hand-masked image", desimg);
            waitKey(0);

            mode = WELT_CPP_HAND_DETECTION_MODE_PROCESS;
            break;
        }
        default:
            mode = WELT_CPP_HAND_DETECTION_MODE_PROCESS;
            break;
    }
}

void welt_cpp_hand_detection::reset() {
    mode = WELT_CPP_HAND_DETECTION_MODE_PROCESS;
}

void welt_cpp_hand_detection::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);
    cout << "in1" << endl;

    /* input 2*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection( this, port_index, direction);
    cout << "in2" << endl;

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);
    cout << "out" << endl;
}

welt_cpp_hand_detection::~welt_cpp_hand_detection() {
    cout << "delete add actor" << endl;
}

void welt_cpp_hand_detection_terminate(welt_cpp_hand_detection *context) {
    delete context;
}
