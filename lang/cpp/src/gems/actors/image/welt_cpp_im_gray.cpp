/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_im_gray.h"

using namespace std;
using namespace cv;

welt_cpp_im_gray::welt_cpp_im_gray
        (welt_c_fifo_pointer img_in,
         welt_c_fifo_pointer out,
         int index) {
    this->mode = WELT_CPP_IM_GRAY_MODE_PROCESS;

    this->img_in = (welt_c_fifo_pointer)img_in;   /*image input*/
    this->out = (welt_c_fifo_pointer)out;
    this->actor_set_index(index);
    this->actor_set_max_port_count(MAX_FIFO_COUNT);
    /* Set portrefs*/
    //this->actor_add_portrefs(&this->img_in);
    //this->actor_add_portrefs(&this->out);

}

bool welt_cpp_im_gray::enable() {
    bool result = false;

    switch (mode) {
        case WELT_CPP_IM_GRAY_MODE_PROCESS:
            result = (welt_c_fifo_population(this->img_in) >= 1)
                     && (welt_c_fifo_population(this->out) <
                         welt_c_fifo_capacity(this->out));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_cpp_im_gray::invoke() {
    switch (mode) {
        case WELT_CPP_IM_GRAY_MODE_PROCESS: {
            Mat* img_in;
            welt_c_fifo_read(this->img_in, &img_in);
            Mat temp_in = *img_in;
            Mat temp_out;
            cvtColor(temp_in, temp_out, COLOR_BGR2GRAY);
            Mat* img_out = new cv::Mat();
            temp_out.copyTo(*img_out);
            welt_c_fifo_write(this->out, &img_out);
            mode = WELT_CPP_IM_GRAY_MODE_PROCESS;
            break;
        }
        default:
            mode = WELT_CPP_IM_GRAY_MODE_PROCESS;
            break;
    }
}

void welt_cpp_im_gray::reset() {
    mode = WELT_CPP_IM_GRAY_MODE_PROCESS;
}

void welt_cpp_im_gray::connect(welt_cpp_graph *graph) {
    int port_index;
    int direction;

    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    graph->add_connection( this, port_index, direction);
    cout << "in1" << endl;

    /* input 2*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    graph->add_connection( this, port_index, direction);
    cout << "in2" << endl;

    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    graph->add_connection(this, port_index, direction);
    cout << "out" << endl;
}

welt_cpp_im_gray::~welt_cpp_im_gray() {
    cout << "delete add actor" << endl;
}

void welt_cpp_im_gray_terminate(welt_cpp_im_gray *context) {
    delete context;
}
