/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2016
Maryland DSPCAD Research Group, The University of Maryland at College Park

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "welt_cpp_actor.h"
#include "welt_c_util.h"

int welt_cpp_actor::actor_get_index(){
    return this->index;
}

void welt_cpp_actor::actor_set_index(int index){
    this->index = index;
    return;
}

void welt_cpp_actor::actor_set_portref(
                              int index, welt_c_fifo_pointer fifo){
    welt_c_fifo_pointer *p;
    p = (this->portrefs)[index];
    (*p) = fifo;
    return;
}

welt_c_fifo_pointer welt_cpp_actor::actor_get_portref(int index){
    welt_c_fifo_pointer *p;
    p = (this->portrefs)[index];
    return *p;
}

/*****************************************************************************
Initialize portrefs array: allocate memory for the array and initialize every
element.
*****************************************************************************/
void welt_cpp_actor::actor_init_vari_portrefs(int count, ...){
    int i;
    va_list valist;
    welt_c_fifo_pointer *port;

    this->actor_init_portrefs();

    if (this->max_port_count < count){
        fprintf(stderr, "number of ports exceeds the maximum number of \
                ports\n");
        this->port_count = 0;
    }else{
        this->port_count = 0;
        va_start(valist, count);
        for(i = 0; i< count; i++){
            port = va_arg(valist, welt_c_fifo_pointer*);
            this->actor_add_portrefs(port);
        }
        va_end(valist);
    }
    return;
}

/*****************************************************************************
Initialize portrefs array: allocate memory for the array and initialize every
element to NULL.
*****************************************************************************/
void welt_cpp_actor::actor_init_portrefs(){
    int i;
    this->portrefs.clear();
    this->portrefs.reserve(this->max_port_count);
    //for(i = 0; i < this->max_port_count; i++){
    //        this->portrefs[i] = NULL;
    //}
    //this->port_count = 0;
    return;
}

/*****************************************************************************
Add portrefs: add one port to the portrefs array.
*****************************************************************************/

void welt_cpp_actor::actor_add_portrefs(
                               welt_c_fifo_pointer *port){

    /*if(this->port_count > (this->max_port_count - 1)){
        fprintf(stderr, "add portrefs: the portrefs array is full; no more \
                ports could be added.\n");
    }else{
        this->portrefs[this->port_count] = port;
        this->port_count += 1;
    }*/
    //this->portrefs.push_back(port);
    this->portrefs.insert(this->portrefs.begin() + this->port_count, port);
    this->port_count += 1;
    return;

}

/*****************************************************************************
Add portrefs array: add a port array to the portrefs array.
*****************************************************************************/
void welt_cpp_actor::actor_add_portrefs_array(
        welt_c_fifo_pointer *port_array, int length){
    int i;
    if(this->port_count > (this->max_port_count - length)){
        fprintf(stderr, "add portrefs: the portrefs array is full; no more \
                ports could be added.\n");
    }else{
        for(i = 0; i < length; i++){
            this->portrefs[this->port_count + i] = &(port_array[i]);
        }
        this->port_count += length;
    }
    return;
}





/*****************************************************************************
Reset portrefs array: de-allocate the array and set portrefs to NULL
*****************************************************************************/
void welt_cpp_actor::actor_reset_portrefs(){
    if(this->portrefs.size()!=0){
        //free(this->portrefs);
        //this->portrefs = NULL;
        this->port_count = 0;
    }
    return;
}

/*****************************************************************************
Set the parameter in the actor.
*****************************************************************************/
void welt_cpp_actor::actor_set_params_vec(
                               int param_index, void *data){
    void *p;
    p = (this->params_vec)[param_index];
    int param_size = (this->params_length)[param_index];
    //int param_size = 0;
    /* The following assumes that sizeof(char) = 1. */
    memcpy(p, data, param_size);
    return;
}

/*****************************************************************************
Get the parameter in the actor.
*****************************************************************************/
void welt_cpp_actor::actor_get_params_vec(
                               int param_index, void *data){
    void *p;
    p = (this->params_vec)[param_index];
    //int param_size = 0 ;
	int param_size = (this->params_length)[param_index];
    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, p, param_size);
    return;

}

/*****************************************************************************
Set the parameter length in the actor.
*****************************************************************************/
void welt_cpp_actor::actor_set_param_length(int param_index, int length){
    (this->params_length)[param_index] = length;
    return;
}

/*****************************************************************************
Get the parameter length in the actor.
*****************************************************************************/
/*int welt_cpp_actor::actor_get_param_length(int param_index){
    //int length = (this->param_length)[param_index];
    return param_index;
}*/

void welt_cpp_actor::actor_init_vari_params_vec(
         int *data_length,
        int count, ...){
    int i;
    va_list valist;
    void *data;

    this -> actor_init_params_vec();

    if (this->max_param_count < count){
        fprintf(stderr, "number of parameters exceeds the maximum number of \
                parameters\n");
        this->param_count = 0;
        return;
    }else{
        this->param_count = 0;
        va_start(valist, count);
        for(i = 0; i < count; i++){
            data = va_arg(valist, void*);
            this->actor_add_params_vec(data, data_length[i]);
        }
        va_end(valist);
    }
    return;
}

/*****************************************************************************
Initialize params_vec array: allocate memory for the array and initialize every
element and initialize each element.
*****************************************************************************/
void welt_cpp_actor::actor_init_params_vec(){
    int i;
    /*??what is the param_length*/
    //this->param_length = param_length;

    //this->params_vec = (void **)welt_c_util_malloc(
    //        sizeof(void *) * this->max_param_count);

//this->param_length = (int *)welt_c_util_malloc(
//            sizeof(int) * this->max_param_count);

    for(i = 0; i < this->max_param_count; i++){
        this->params_vec[i] = NULL;
        //this->param_length[i] = 0;
    }
    return;
}

/*****************************************************************************
Add params_vec array: add a parameter array to the params_vec array.
*****************************************************************************/
void welt_cpp_actor::actor_add_paramtrefs_array(void *data_array,
		int *data_length, int length){
    int i;
    int accumu_length;
    accumu_length = 0;
    if(this->param_count > (this->max_param_count - length)){
        fprintf(stderr, "add paramtrefs: the paramtrefs array is full; no more \
                paramter could be added.\n");
    }else{
        for(i = 0; i < length; i++){
            this->params_vec[this->param_count + i] =
                    (void *)((unsigned char*)data_array+accumu_length);
            //this->param_length[this->param_count + i] = data_length[i];
            //accumu_length += data_length[i];
        }
        this->param_count += length;
    }
    return;
}


/*****************************************************************************
Add params_vec: add one parameter to the params_vec array.
*****************************************************************************/
void welt_cpp_actor::actor_add_params_vec(
                                void *data, int data_length){

    if(this->param_count > (this->max_param_count - 1)){
        fprintf(stderr, "add params_vec: the params_vec array is full; no more \
                parameter could be added.\n");
        return;
    }else{
        this->params_vec[this->param_count] = data;
        //this->param_length[this->param_count] = data_length;
        this->param_count += 1;
    }
    return;
}


/*****************************************************************************
Reset params_vec array: de-allocate the array and set params_vec to NULL
*****************************************************************************/
/*
void welt_cpp_actor_reset_params_vec(struct welt_cpp_actor_this_struct *this){
    if(this->params_vec){
        free(this->params_vec);
        this->params_vec = NULL;
        this->param_count = 0;
    }
    return;
}
*/

void welt_cpp_actor::actor_reset_params_vec(){
    if(this->params_vec.size() !=0){
        //free(this->params_vec);
        //this->params_vec = NULL;
        this->param_count = 0;
    }
//    if(this->param_length){
//        free(this->param_length);
//    }
    return;
}

/*****************************************************************************
Invoke diagnostic function:
- Invoke function of hierarchical actors (H-actors) could use this
function for testing if corresponding DSG or scheduler is not available
- This function usually is applied early in the development phase
*****************************************************************************/
void welt_cpp_actor::actor_invoke_diagnostic(){
    int index = this->index;

    printf("invoke_diagnostic: invoking actor %d\n", index);

    return;
}

/*****************************************************************************
Fifo-port connect function:
- The "actor_this" should be the actor you want to connect.
- The "port_index" is the port index to specify which port to connect.
- The "fifo_pointer" is the pointer of the fifo to connect.
*****************************************************************************/
void welt_cpp_actor::connect_fifo(
                         int port_index, welt_c_fifo_pointer* fifo_pointer){
    this->portrefs[port_index] = fifo_pointer;
}

// FIXME: To be verified (another option: const std::vector<void*>&
//  actor_get_params_vec() const {return params_vec;})
vector<void*> welt_cpp_actor::actor_get_params_vec(){
    return this->params_vec;
}

/* mode getter and setter */
int welt_cpp_actor::getMode() const {
    return mode;
}

void welt_cpp_actor::setMode(int mode) {
    welt_cpp_actor::mode = mode;
}

int welt_cpp_actor::actor_get_max_port_count() const {
    return max_port_count;
}

void welt_cpp_actor::actor_set_max_port_count(int maxPortCount) {
    this->max_port_count = maxPortCount;
}

int welt_cpp_actor::getBlockLength(){
	return blockLength;
}

void welt_cpp_actor::setBlockLength(int blockLen){
	blockLength = blockLen;
}