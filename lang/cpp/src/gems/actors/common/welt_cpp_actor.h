#ifndef _welt_cpp_actor_h
#define _welt_cpp_actor_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_cpp_graph.h"

/* This is an abstract base class for the run-time context of an actor instance.
 * */
class welt_cpp_actor{
/* Common elements across context type of actors. */
protected:
    /* The mode of an actor */
    int mode;
private:
    /* The number of ports of an actor */
    int port_count;
    /* The maximum number of ports of an actor */
    int max_port_count;

private:
    /* The members void **params_vec, int param_count, int *param_length, int
     * max_param_count are replaced by the following params_vec vector. The
     * params_vec vector stores the pointers of parameters that should be
     * initialized and allocated in an actor. */
    int max_param_count;

    int param_count;
    /* The index of an actor */
    int index;
    /* The number of times to process using the current vector length */
	int blockLength;

public:

    std::vector<void*> params_vec;

	std::vector<char*> descriptor;

	std::vector<int> params_length;
    /* The enable method of a welt-cpp actor corresponds to the enable function
     * that enables an actor of the lightweight/welterweight dataflow API. Every
     * actor must have an enable function. */
    virtual bool enable() = 0;

    /* The invoke method of a welt-cpp actor corresponds to the invoke function
     * that invokes an actor of the lightweight/welterweight dataflow API. Every
     * actor must have an invoke function. */
    virtual void invoke() = 0;

    /* The reset method of a welt-cpp actor corresponds to the reset function
     * that resets an actor of the lightweight/welterweight dataflow API. Every
     * actor must have an reset function. */
    virtual void reset() = 0;

    /* The connect method of a welt-cpp actor corresponds to the connect
     * function that adds an actor to a dataflow graph of the
     * lightweight/welterweight API. Every actor must have an connect function.
     * */
    virtual void connect(welt_cpp_graph *graph) = 0;

    /* The destructor of a welt-cpp actor corresponds to the terminate function
     * of the lightweight/welterweight dataflow API. Every actor must have a
     * terminate function. This base class destructor does nothing, and just
     * serves to ensure that all actors have terminate functions even if they
     * are not explicitly defined for the concrete classes. */
    virtual ~welt_cpp_actor() = default;

    /* A collection of pointers to subgraphs that are associated with the actor
     * for a hierarchical actor. The collection will be empty for
     * non-hierarchical actors. A hierarchical actor may have multiple subgraphs
     * associated with it --- for example, if different actor modes use
     * different subgraphs. */
    std::vector<welt_cpp_graph*> subgraphs;

    /* port references */
    std::vector<welt_c_fifo_pointer*> portrefs;

    /* Return the index of the actor. Each actor in a dataflow graph G
     * can be associated with a unique index from 0 to (N-1), where N is
     * the number of actors in G. Such an index can be useful, for example,
     * to maintain tables of actor-related data associated with G.
     * If an actor is simultaneously contained in multiple graphs,
     * then it is the client's responsibility to keep track of which graph the
     * actor's index is associated with. The implementation of G is responsible
     * for ensuring that actor indices are assigned so that actors have unique
     * indices and the index numbering starts at 0. */
    int actor_get_index();

    /* Set the index of the actor. See getIndex() for more details about actor
     * indices. */
    void actor_set_index(int index);

    int getMode() const;

    void setMode(int mode);

    /* Set Set the element in portrefs. */
    void actor_set_portref(int index, welt_c_fifo_pointer fifo);

    /* Get the element in portrefs */
    welt_c_fifo_pointer actor_get_portref(int index);

    /* Initialize portrefs array: allocate memory for the array and initialize
    * every
	element. Number of ports listed in the argument list could be variable. */
    void actor_init_vari_portrefs(int count, ...);

    /* Initialize portrefs array: allocate memory for the array and initialize
    * every
	element to NULL. */
    void actor_init_portrefs();

    /* Add portrefs: add one port to the portrefs array. */
    void actor_add_portrefs(welt_c_fifo_pointer *port);

    /* Add portrefs array: add a port array to the portrefs array. */
    void actor_add_portrefs_array(welt_c_fifo_pointer *port_array, int length);

    /* Reset portrefs array: de-allocate the array and set portrefs to NULL */
    void actor_reset_portrefs();

    /* Set the parameter in the actor. */
    void actor_set_params_vec(int param_index, void *data);

    /* Get the parameter in the actor. */
    void actor_get_params_vec(int param_index, void *data);

    /* Set the parameter length in the actor. */
    void actor_set_param_length(int param_index, int length);

    /* Get the parameter length in the actor. */
    int actor_get_param_length(int param_index);

    /*Initialize params_vec array: allocate memory for the array and initialize
    * every
	element. Number of parameters listed in the argument list could be variable.*/
    void actor_init_vari_params_vec(int *data_length,int count, ...);

    /* Initialize params_vec array: allocate memory for the array and initialize
    * every
	element. */
    void actor_init_params_vec();

    /* Add params_vec: add one parameter to the params_vec array. */
    void actor_add_params_vec(void *data, int data_length);

    /* Add params_vec array: add a parameter array to the params_vec array. */
    void actor_add_paramtrefs_array(void *data_array,int *data_length, int length);

    /* Reset params_vec array: de-allocate the array and set
    * params_vec to NULL */
    void actor_reset_params_vec();

    /* Set Invoke Function */
    void actor_invoke_diagnostic();

    /* Fifo-port connect function:
    * - The "port_index" is the port index to specify which port to connect.
    * - The "fifo_pointer" is the pointer of the fifo to connect. */
    void connect_fifo(int port_index, welt_c_fifo_pointer* fifo_pointer);

    /* Get params_vec */
    std::vector<void*> actor_get_params_vec();

    int actor_get_max_port_count() const;

    void actor_set_max_port_count(int maxPortCount) ;

    int getBlockLength();

    void setBlockLength(int blockLen);
};

#endif //_welt_cpp_actor_h
