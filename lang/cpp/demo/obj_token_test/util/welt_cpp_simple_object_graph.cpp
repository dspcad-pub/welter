/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <iostream>
#include "welt_cpp_simple_object_graph.h"

#include "token_object_example.h"

#define NAME_LENGTH 20

using namespace std;



// constructor
welt_cpp_simple_object_graph::welt_cpp_simple_object_graph(char *in_file,
                                                           char *out_file) {
    /* token_type */
    this->in_file = in_file;
    this->out_file = out_file;

    this->actor_count = ACTOR_COUNT;
    this->fifo_count = FIFO_COUNT;

    /* Initialize fifos. */
    int token_size = sizeof(token_object_example);
    cout << "the size of token_object_type is: " << token_size << endl;
    for (int i = 0; i<this->fifo_count; i++){
        fifos.push_back((welt_c_fifo_pointer)welt_c_fifo_new
                (BUFFER_CAPACITY,token_size,i));
    }
    /* Create and connect the actors. */
    /* x source actor */
    actors.push_back(new welt_cpp_simple_object_source (fifos[FIFO_SRC_SINK],
                                                        this->in_file, ACTOR_SIMPLE_OBJ_SOURCE));
    descriptors.push_back((char*)"actor simple object source");
    actors[ACTOR_SIMPLE_OBJ_SOURCE]->connect((welt_cpp_graph*)this);

    /* sink actor */
    actors.push_back(new welt_cpp_simple_object_sink
                             (fifos[FIFO_SRC_SINK], this->out_file, ACTOR_SIMPLE_OBJ_SNK));
    descriptors.push_back((char*)"actor simple object sink");
    actors[ACTOR_SIMPLE_OBJ_SNK]->connect((welt_cpp_graph*)this);

    /* Initialize source array and sink array */
//    FIXME: source array and sink array
//    context->source_array = (lide_c_actor_context_type **)lide_c_util_malloc(
//            context->fifo_count * sizeof(lide_c_actor_context_type *));
//
//    context->sink_array = (lide_c_actor_context_type **)lide_c_util_malloc(
//            context->fifo_count * sizeof(lide_c_actor_context_type *));


}

void welt_cpp_simple_object_graph::scheduler() {
//    welt_cpp_util_guarded_execution(actors[ACTOR_SIMPLE_OBJ_SOURCE],
//            descriptors[ACTOR_SIMPLE_OBJ_SOURCE]);
//    welt_cpp_util_guarded_execution(actors[ACTOR_SIMPLE_OBJ_SNK],
//            descriptors[ACTOR_SIMPLE_OBJ_SNK]);
    welt_cpp_util_simple_scheduler(actors,actor_count,descriptors);
}

// destructor
welt_cpp_simple_object_graph::~welt_cpp_simple_object_graph() {
    cout << "delete add graph" << endl;
}
void welt_cpp_simple_object_graph_terminate(
        welt_cpp_simple_object_graph *context){
//    int i;
//    /* Terminate FIFO*/ FIXME
//    for(i = 0; i<context->fifo_count; i++){
//        lide_c_fifo_basic_free((lide_c_fifo_basic_pointer)context->fifos[i]);
//    }

    /* Terminate Actors*/
    welt_cpp_simple_object_source_terminate((welt_cpp_simple_object_source*)
                                                    context->actors[ACTOR_SIMPLE_OBJ_SOURCE]);
    welt_cpp_simple_object_sink_terminate((welt_cpp_simple_object_sink*)
                                                  context->actors[ACTOR_SIMPLE_OBJ_SNK]);
//    free(context->actors);
//    free(context);
    delete context;
}