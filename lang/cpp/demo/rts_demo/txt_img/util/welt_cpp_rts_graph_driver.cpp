/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <cstdio>

#include <ctime>
#include <queue>
#include <iostream>

extern "C" {
#include "welt_c_fifo.h"
#include "welt_c_add.h"
#include "welt_c_util.h"
}

#include "rts_graph.h"

using namespace std;


int main(int argc, char **argv) {
    char *in_file = nullptr;
    char *out_file = nullptr;

    int i = 0;
    int arg_count = 5;

    /* Check program usage. */
    if (argc != arg_count) {
        fprintf(stderr, "welt_c_add_driver.exe error: arg count\n");
        exit(1);
    }

    /* Open the input and output file(s). */
    i = 1;
    in_file = argv[i++];
    out_file = argv[i++];
    int nRows = atoi(argv[i++]);
    int nCols = atoi(argv[i++]);

    auto* app_graph = new rts_graph(in_file, out_file, nRows, nCols);
    /* Execute the graph. */
    app_graph->scheduler();

    /* Normal termination. */
    delete app_graph;
    return 0;
}
