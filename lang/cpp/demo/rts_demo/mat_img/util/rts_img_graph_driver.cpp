#include <iostream>
extern "C" {
#include "welt_c_fifo.h"
#include "welt_c_util.h"
}
#include "welt_cpp_graph.h"
#include "rts_img_graph.h"

using namespace std;
using namespace cv;
int main(int argc, char **argv) {
    if (argc != 3){
        cerr << "arg count" << endl;
        exit(EXIT_FAILURE);
    }

    char* infile = argv[1];
    char* outfile = argv[2];

    auto* graph = new rts_img_graph(infile, outfile);
    graph -> scheduler();
    delete graph;

    return 0;
}
