CMAKE_MINIMUM_REQUIRED(VERSION 2.8 FATAL_ERROR)

PROJECT(pointer_add_graph)

SET(CMAKE_CXX_STANDARD 14)

SET(CMAKE_EXPORT_COMPILE_COMMANDS on)

find_package(OpenCV REQUIRED)

INCLUDE_DIRECTORIES(
        $ENV{UXWELTER}/lang/c/src/gems/actors/basic
        $ENV{UXWELTER}/lang/c/src/gems/actors/common
#        $ENV{UXWELTERC}/src/gems/edges/basic
#        $ENV{UXWELTERC}/src/gems/edges/common
        $ENV{UXWELTER}/lang/c/src/gems/edges
        $ENV{UXWELTER}/lang/c/src/tools/runtime
        $ENV{UXWELTER}/lang/c/src/tools/graph

        $ENV{UXWELTER}/lang/cpp/src/gems/actors/common
        $ENV{UXWELTER}/lang/cpp/src/gems/actors/basic
        $ENV{UXWELTER}/lang/cpp/src/tools/graph
        $ENV{UXWELTER}/lang/cpp/src/tools/runtime
        $ENV{UXWELTER}/lang/cpp/src/apps/basic

)

LINK_DIRECTORIES(
        $ENV{WELTERGEN}
)

ADD_EXECUTABLE(welt_cpp_im_graph_driver.exe
        welt_cpp_im_graph_driver.cpp
        )

TARGET_LINK_LIBRARIES(
        welt_cpp_im_graph_driver.exe
#        $ENV{WELTERCGEN}/lide_c_actors_basic.a
#        $ENV{WELTERCGEN}/lide_c_actors_common.a
#
#        $ENV{WELTERCGEN}/lide_c_edges_basic.a
#        $ENV{WELTERCGEN}/lide_c_graph_common.a
#        $ENV{WELTERCGEN}/lide_c_runtime.a
#
#        welt_cpp_graph_basic.a
#        welt_cpp_actors_basic.a
#        welt_cpp_runtime.a
        welt_c_actors_basic.a
        welt_c_actors_common.a
        welt_c_edges.a
        welt_c_graph_common.a
        welt_c_runtime.a

        welt_cpp_actors_basic.a
        welt_cpp_actor.a
        welt_cpp_graph_basic.a
        welt_cpp_runtime.a
        welt_cpp_utils.a
        welt_cpp_graph.a
        ${OpenCV_LIBS}
)

INSTALL(TARGETS welt_cpp_im_graph_driver.exe DESTINATION ${PROJECT_SOURCE_DIR})

