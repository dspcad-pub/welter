/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>

#include <ctime>
#include <queue>

extern "C" {
#include "welt_c_fifo.h"
#include "welt_c_add.h"
#include "welt_c_util.h"
}

#include "welt_cpp_im_graph.h"

using namespace std;


int main(int argc, char **argv) {
    char* im_file=NULL;


    int i = 0;
    int arg_count = 2;

    /* Check program usage. */
    if (argc != arg_count) {
        fprintf(stderr, "welt_c_add_driver.exe error: arg count\n");
    }

    /* Open the input and output file(s). */
    i = 1;
    im_file = argv[i++];
    auto* imgraph = new welt_cpp_im_graph(im_file);
    /* Execute the graph. */
    imgraph->scheduler();

    /* Normal termination. */
    welt_cpp_im_graph_terminate(imgraph);
    return 0;
}
