/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <iostream>
#include <ctime>
#include <queue>

extern "C" {
#include "welt_c_fifo.h"
#include "welt_c_util.h"
}
#include "welt_cpp_add.h"
#include "welt_cpp_file_sink.h"
#include "welt_cpp_file_source.h"
#include "welt_cpp_actor.h"

#include "welt_cpp_graph.h"
#include "welt_cpp_add_graph.h"

using namespace std;

int main(int argc, char **argv) {
    char *x_file = NULL;
    char *y_file = NULL;
    char *out_file = NULL;

    int i = 0;
    int arg_count = 4;

    /* Check program usage. */
    if (argc != arg_count) {
        fprintf(stderr, "welt_c_add_driver.exe error: arg count\n");
        exit(1);
    }

    /* Open the input and output file(s). */
    i = 1;
    x_file = argv[i++];
    y_file = argv[i++];
    out_file = argv[i++];
    auto* addgraph = new welt_cpp_add_graph(x_file, y_file, out_file);

    /* Execute the graph. */
    addgraph->scheduler();

    delete addgraph;
    /* Normal termination. */
    //welt_cpp_add_graph_terminate(addgraph);
    return 0;
}
