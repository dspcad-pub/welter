/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pthread.h>

#include "welt_c_snd_actor.h"
#include "welt_c_util.h"
#include "welt_c_graph.h"
#define MAX_FIFO_COUNT 3

struct _welt_c_snd_actor_context_struct {
#include "welt_c_cdsg_actor_context_type_common.h"

/* Actor interface ports. */
welt_c_fifo_unit_size_pointer in_pc; /*Input PC edge*/
welt_c_fifo_unit_size_pointer out_pc; /*Output PC edge*/
welt_c_fifo_pointer out_ipc; /*Output IPC edge*/
pthread_mutex_t *mutex;
pthread_cond_t *cond;
welt_c_fifo_pointer fifo;
int block_size;
};

welt_c_snd_actor_context_type *welt_c_snd_actor_new(
        welt_c_fifo_pointer in_pc, welt_c_fifo_pointer out_pc,
        welt_c_fifo_pointer out_ipc, pthread_mutex_t *mutex,
        pthread_cond_t *cond, welt_c_fifo_pointer fifo,
        int block_size, int index){

    welt_c_snd_actor_context_type *context = NULL;

    context = (welt_c_snd_actor_context_type *)welt_c_util_malloc(
                sizeof(welt_c_snd_actor_context_type));
    context->mode = welt_c_SND_ACTOR_MODE_PROCESS;
    context->enable = (welt_c_actor_enable_ftype)
                        welt_c_snd_actor_enable;
    context->invoke = (welt_c_actor_invoke_ftype)
                        welt_c_snd_actor_invoke;
    
    context->in_pc = (welt_c_fifo_unit_size_pointer)in_pc;
    context->out_pc = (welt_c_fifo_unit_size_pointer)out_pc;
    context->out_ipc = (welt_c_fifo_pointer)out_ipc;
    context->last_fifo = (welt_c_fifo_unit_size_pointer)out_pc;

    context->mutex = mutex;
    context->cond = cond;
    context->fifo = (welt_c_fifo_pointer)fifo;
    context->block_size = block_size;
    context->index = index;
    
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)
                        welt_c_snd_actor_connect;
    context->max_port_count = MAX_FIFO_COUNT;
    
    /* Initialize portrefs*/
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->in_pc);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->out_pc);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->out_ipc);
    
    return context;
}


bool welt_c_snd_actor_enable(welt_c_snd_actor_context_type *context) {
    bool result = false;

    switch (context->mode) {
    case welt_c_SND_ACTOR_MODE_PROCESS:
        result = (welt_c_fifo_unit_size_population(context->in_pc) >= 1)
         && (welt_c_fifo_population(context->out_ipc) <
                    welt_c_fifo_capacity(context->out_ipc))
         && (welt_c_fifo_unit_size_population(context->out_pc) <
                    welt_c_fifo_unit_size_capacity(context->out_pc));
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.
*******************************************************************************/

/* In the next step, use dlpp to avoid if checkings in invoke function*/
void welt_c_snd_actor_invoke(welt_c_snd_actor_context_type *context) {
    // int check = 1;
    switch (context->mode) {
    case welt_c_SND_ACTOR_MODE_PROCESS:

    pthread_mutex_lock(context->mutex);

    welt_c_fifo_unit_size_read_advance(context->in_pc);
    welt_c_fifo_unit_size_write_advance(context->out_pc);
    welt_c_fifo_write_advance(context->out_ipc);

    if (welt_c_fifo_population(context->fifo) <=
        welt_c_fifo_capacity(context->fifo) - context->block_size){
        // while (check==1)
            // check = pthread_cond_broadcast(context->cond);
        //pthread_cond_broadcast(context->cond);
        pthread_cond_signal(context->cond);
       // printf("SND actor fired\n");
    }
            
	pthread_mutex_unlock(context->mutex);

        break;
    default:
        context->mode = welt_c_SND_ACTOR_MODE_PROCESS;
        break;
    }
}

void welt_c_snd_actor_terminate(welt_c_snd_actor_context_type *context) {
    free(context);
}

void welt_c_snd_actor_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){
    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context,
                                port_index, direction);
    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context,
                                port_index, direction);
    /* output 2*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context,
                                port_index, direction);
    return;
}

welt_c_fifo_unit_size_pointer welt_c_snd_actor_dsg_fifo_output(
        welt_c_snd_actor_context_type *context){
    return context->last_fifo;
}

