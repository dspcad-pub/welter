/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/* Common elements across context type of reference actors(RA). */
#include "welt_c_cdsg_actor_context_type_common.h"

/* Actor interface ports. */
welt_c_fifo_unit_size_pointer in;
welt_c_fifo_unit_size_pointer out;

/* preA, postA and exeA function pointers */
welt_c_ref_cdsg_actor_preA_ftype preA;
welt_c_ref_cdsg_actor_postA_ftype postA;
welt_c_ref_cdsg_actor_exeA_ftype exeA;
/* function pointer for getting output FIFO of the RA*/
welt_c_ref_cdsg_actor_dsg_fifo_output_ftype dsg_fifo_output;

/* context of the referenced actor*/
welt_c_actor_context_type *ref_context;

/* The number of elements in the selected_app_gr_inputs array */
int selected_input_count;

/* The number of elements in the selected_app_gr_output array */
int selected_output_count;

/* Array of pointers to selected FIFOs in the application graph that are input 
FIFOs of the referenced actor. Each element of this array is of type 
welt_c_fifo_pointer. A NULL pointer here indicates there are no elements in
the array (i.e., no input FIFOs referenced by the RA).
*/
welt_c_fifo_pointer *selected_app_gr_inputs;

/* Array of pointers to selected FIFOs in the application graph that are 
output FIFOs of the referenced actor. Each element of this array is of type 
welt_c_fifo_pointer. A NULL pointer here indicates there are no elements in
the array (i.e., no output FIFOs referenced by the RA).
*/
welt_c_fifo_pointer *selected_app_gr_outputs;


/* state variable*/
/* The common_state variable could store any data in the actor's execution. 
For example, in RA actor, token will be read from input FIFO in invoke 
function; if the token is integer, then common_state here could be used for 
storing the token from FIFO. For instance, if input token is the loop count, 
then we could use common_state to store the number of iterations from input 
FIFO. */
int common_state;
