SET(source_list
        welt_c_snd_actor.c
        welt_c_rec_actor.c
        )

ADD_LIBRARY(welt_c_cdsg_actor ${source_list})

INSTALL(TARGETS welt_c_cdsg_actor DESTINATION .)
