#ifndef _welt_c_dsg_actor_h
#define _welt_c_dsg_actor_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdarg.h>
#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_graph_def.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo_unit_size.h"

struct welt_c_dsg_actor_context_struct;

/* Function prototype for static functions in dsg actor class*/ 

/*****************************************************************************
Get the last written fifo of a DSG actor in the dataflow graph
*****************************************************************************/     
welt_c_fifo_unit_size_pointer welt_c_dsg_actor_get_last_fifo(
        struct welt_c_dsg_actor_context_struct *context);

        
typedef struct welt_c_dsg_actor_context_struct {
#include "welt_c_dsg_actor_context_type_common.h"
} welt_c_dsg_actor_context_type;

#endif
