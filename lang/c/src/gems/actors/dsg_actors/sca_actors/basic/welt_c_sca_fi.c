/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_c_sca_fi.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"

#define MAX_FIFO_COUNT 3

struct _welt_c_sca_fi_context_struct {
#include "welt_c_sca_context_type_common.h"

/* FIFO ports. */
/* 
    pointer to SCA FI actor's input FIFOs array; number of elements in the 
    array is input_port_count; data type of element in the array is 
    welt_c_fifo_unit_size_pointer
*/
welt_c_fifo_unit_size_pointer *in;
/* output FIFO*/
welt_c_fifo_unit_size_pointer out1;

/* total number of input ports in the actor*/
int input_port_count;
/* index of the input port which has tokens storing in the FIFO*/
int pindex;

};

welt_c_sca_fi_context_type *welt_c_sca_fi_new(
        //welt_c_fifo_unit_size_pointer in1,
        welt_c_fifo_pointer in1,
        //welt_c_fifo_unit_size_pointer in2,
        welt_c_fifo_pointer in2,
        //welt_c_fifo_unit_size_pointer out1,
        welt_c_fifo_pointer out1,
        int input_port_count, int index){
    int i;
    welt_c_sca_fi_context_type *context = NULL;

    context = (welt_c_sca_fi_context_type *)welt_c_util_malloc(
                sizeof(welt_c_sca_fi_context_type));
                
    context->mode = welt_c_SCA_FI_MODE_PROCESS;
    
    context->enable = (welt_c_actor_enable_ftype)welt_c_sca_fi_enable;
    context->invoke = (welt_c_actor_invoke_ftype)welt_c_sca_fi_invoke;
   
    context->input_port_count = 2;
    context->in = (welt_c_fifo_unit_size_pointer *)welt_c_util_malloc(
                context->input_port_count * 
                sizeof(welt_c_fifo_unit_size_pointer));
    context->in[0] = (welt_c_fifo_unit_size_pointer)in1;
    context->in[1] = (welt_c_fifo_unit_size_pointer)in2;
    context->out1 = (welt_c_fifo_unit_size_pointer)out1;
    
    context->pindex = 0;
    context->last_fifo = NULL;
    
    context->index = index;
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)
                        welt_c_sca_fi_connect;
    context->max_port_count = MAX_FIFO_COUNT;
        
    /* Initialize portrefs*/
    /*
    context->port_count = context->input_port_count + 1;
    context->portrefs = (welt_c_fifo_pointer **)welt_c_util_malloc(
                        sizeof(welt_c_fifo_pointer *) * context->port_count);
    
    
    for(i = 0; i<context->input_port_count; i++){
        context->portrefs[i] = (welt_c_fifo_pointer*)context->in[i];
    }
    context->portrefs[context->port_count-1] = (welt_c_fifo_pointer*)context->out1;
    */
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->in[0]);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->in[1]);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->out1);
   
    return context;
}

/* by default: there is only one input port not empty if actor is enabled*/
bool welt_c_sca_fi_enable(welt_c_sca_fi_context_type *context) {
    bool result = false;
    int i, tmp;
    tmp = false;
    switch (context->mode) {
    case welt_c_SCA_FI_MODE_PROCESS:
        for(i = 0; i<context->input_port_count; i++){
            if(welt_c_fifo_unit_size_population(context->in[i]) >=1){
                tmp = TRUE;
                context->pindex = i;
                break;
            }
        }
        result = (welt_c_fifo_unit_size_population(context->out1) <
                welt_c_fifo_unit_size_capacity(context->out1)) && tmp;
                
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.
*******************************************************************************/
void welt_c_sca_fi_invoke(welt_c_sca_fi_context_type *context) {

    switch (context->mode) {
    case welt_c_SCA_FI_MODE_PROCESS:
        welt_c_fifo_unit_size_read_advance(context->in[context->pindex]);
        welt_c_fifo_unit_size_write_advance(context->out1);
        context->last_fifo = context->out1;
        
        context->mode = welt_c_SCA_FI_MODE_PROCESS;

        break;
    default:
        context->mode = welt_c_SCA_FI_MODE_ERROR;
        break;
    }
}

void welt_c_sca_fi_terminate(welt_c_sca_fi_context_type *context) {
    free(context);
}

welt_c_fifo_unit_size_pointer welt_c_sca_fi_last_dsg_fifo_output(
        welt_c_sca_fi_context_type *context){
    return context->last_fifo;
}

/* Set and get function for pointer to input FIFO array*/
void welt_c_sca_fi_set_index(
welt_c_sca_fi_context_type *context, int index){
    context->index = index;
    return;
} 

int welt_c_sca_fi_get_index(
welt_c_sca_fi_context_type *context){
    return context->index;
} 

void welt_c_sca_fi_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){
    int port_index;
    int direction;
    int i; 
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    
    for(i = 0; i<context->port_count-1; i++){
        port_index = i;
        welt_c_graph_add_connection(graph,
                                    (welt_c_actor_context_type *)context,
                                    port_index, direction);
    }
    
    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = context->port_count - 1;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context, 
                                port_index, direction);
}
