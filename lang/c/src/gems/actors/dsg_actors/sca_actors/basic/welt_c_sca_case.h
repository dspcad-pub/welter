#ifndef _welt_c_sca_case_h
#define _welt_c_sca_case_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_basic.h"
#include "welt_c_fifo_unit_size.h"

/* Actor modes */
#define welt_c_SCA_CASE_MODE_ERROR      0
#define welt_c_SCA_CASE_MODE_PROCESS    1


/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with sca_case objects. */
struct _welt_c_sca_case_context_struct;
typedef struct _welt_c_sca_case_context_struct
        welt_c_sca_case_context_type;
        
/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

welt_c_sca_case_context_type *welt_c_sca_case_new(
        //welt_c_fifo_unit_size_pointer in1,
        welt_c_fifo_pointer in1,
        //welt_c_fifo_unit_size_pointer *out,
        welt_c_fifo_pointer *out,
        int output_port_count, int index);

bool welt_c_sca_case_enable(
        welt_c_sca_case_context_type *context);

void welt_c_sca_case_invoke(
        welt_c_sca_case_context_type *context);

void welt_c_sca_case_terminate(
        welt_c_sca_case_context_type *context);
      
/* Set and get for pout_num function*/
void welt_c_sca_case_set_output_port_count(
    welt_c_sca_case_context_type *context,
    int output_port_count);

int welt_c_sca_case_get_output_port_count(
    welt_c_sca_case_context_type *context);

void welt_c_sca_case_set_out_pointer(
    welt_c_sca_case_context_type *context,
    welt_c_fifo_unit_size_pointer *out);

welt_c_fifo_unit_size_pointer *welt_c_sca_case_get_out_pointer(
    welt_c_sca_case_context_type *context);

void welt_c_sca_case_set_index(
welt_c_sca_case_context_type *context, int index);

int welt_c_sca_case_get_index(
welt_c_sca_case_context_type *context);

void welt_c_sca_case_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph);  
    
#endif
