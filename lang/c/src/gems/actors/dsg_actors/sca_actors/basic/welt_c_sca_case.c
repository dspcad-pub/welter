/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_c_sca_case.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"

#define MAX_FIFO_COUNT  100


struct _welt_c_sca_case_context_struct {
#include "welt_c_sca_context_type_common.h"


/* FIFO ports. */
/*input*/
welt_c_fifo_unit_size_pointer in1;
/*    
    pointer to SCA IF actor's input FIFOs array; number of elements in the 
    array is input_port_count; data type of element in the array is 
    welt_c_fifo_unit_size_pointer
*/
welt_c_fifo_unit_size_pointer *out;


/* number of output ports in the actor*/
int output_port_count;

/* index of output port where output token is produced*/
int pindex;

};

/* Comment: output port of case actor is a FIFO array*/
welt_c_sca_case_context_type *welt_c_sca_case_new(
        //welt_c_fifo_unit_size_pointer in1,
        welt_c_fifo_pointer in1,
        //welt_c_fifo_unit_size_pointer *out,
        welt_c_fifo_pointer *out,
        int output_port_count, int index){
    
    int i;
    welt_c_sca_case_context_type *context = NULL;

    context = (welt_c_sca_case_context_type *)welt_c_util_malloc(
                sizeof(welt_c_sca_case_context_type));
                
    context->mode = welt_c_SCA_CASE_MODE_PROCESS;
    
    context->enable = (welt_c_actor_enable_ftype)welt_c_sca_case_enable;
    context->invoke = (welt_c_actor_invoke_ftype)welt_c_sca_case_invoke;
          
    context->in1 = (welt_c_fifo_unit_size_pointer)in1;
    context->last_fifo = NULL;
    context->index = index;
    context->output_port_count = output_port_count;
    context->pindex = 0;
    
    context->index = index;
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)
                        welt_c_sca_case_connect;
    /* pout_num is positive */
    if(context->output_port_count < 1 || context->output_port_count > MAX_FIFO_COUNT-1){
        context->mode = welt_c_SCA_CASE_MODE_ERROR;
        context->out = NULL;
        return context;
    }else{
        context->out = (welt_c_fifo_unit_size_pointer *)out;
    }
    context->port_count = 0; 
    context->max_port_count = MAX_FIFO_COUNT;
    
    /* Initialize portrefs*/
    /*
    context->port_count = context->output_port_count + 1;
    context->portrefs = (welt_c_fifo_pointer **)welt_c_util_malloc(
                        sizeof(welt_c_fifo_pointer *) * context->port_count);
    
    context->portrefs[0] = (welt_c_fifo_pointer*)context->in1;
    for(i = 0; i<context->output_port_count; i++){
        context->portrefs[i+1] = (welt_c_fifo_pointer*)context->out[i];
    }
    */
    
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->in1);
    welt_c_actor_add_portrefs_array((welt_c_actor_context_type *)context,
            (welt_c_fifo_pointer *)context->out, context->output_port_count);
    
    return context;
}


bool welt_c_sca_case_enable(welt_c_sca_case_context_type *context) {
    bool result = false;
    int i, tmp;
    tmp = false;
    switch (context->mode) {
    case welt_c_SCA_CASE_MODE_PROCESS:
        for(i = 0; i<context->output_port_count; i++){
            if((welt_c_fifo_unit_size_population(context->out[i]) <
                welt_c_fifo_unit_size_capacity(context->out[i]))){
                tmp = TRUE;
                break;
            }
        }
        result = (welt_c_fifo_unit_size_population(context->in1) >= 1) && tmp;
                
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.
*******************************************************************************/
void welt_c_sca_case_invoke(welt_c_sca_case_context_type *context) {

    switch (context->mode) {
    case welt_c_SCA_CASE_MODE_PROCESS:
        welt_c_fifo_unit_size_read(context->in1, &context->pindex);
        if(context->pindex >= context->output_port_count){
            fprintf(stderr, "invalid input token in if actor\n");
            context->mode = welt_c_SCA_CASE_MODE_ERROR;
            break;
        }
        if(welt_c_fifo_unit_size_population(context->out[context->pindex]) <
                welt_c_fifo_unit_size_capacity(context->out[context->pindex])){
            welt_c_fifo_unit_size_write_advance(context->out[context->pindex]);
            context->last_fifo = context->out[context->pindex];
        }else{
            welt_c_fifo_unit_size_write(context->in1, &context->pindex);
            context->last_fifo = context->in1;
        }
        context->mode = welt_c_SCA_CASE_MODE_PROCESS;
        break;
 
    default:
        context->mode = welt_c_SCA_CASE_MODE_ERROR;
        break;
    }
}

void welt_c_sca_case_terminate(welt_c_sca_case_context_type *context) {
    free(context);
}


/* Set and get for pout_num function*/
void welt_c_sca_case_set_output_port_count(
    welt_c_sca_case_context_type *context,
    int output_port_count){
    context->output_port_count = output_port_count;
    if(context->output_port_count < 1){
        context->mode = welt_c_SCA_CASE_MODE_ERROR;
    }else{
        context->mode = welt_c_SCA_CASE_MODE_PROCESS;
    }
    return;
}

int welt_c_sca_case_get_output_port_count(
    welt_c_sca_case_context_type *context){
    return context->output_port_count;
}

/* Set and get function for pointer to input FIFO array*/
void welt_c_sca_case_set_out_pointer(
    welt_c_sca_case_context_type *context,
    welt_c_fifo_unit_size_pointer *out){
    context->out = out;
    return;
}

welt_c_fifo_unit_size_pointer *welt_c_sca_case_get_out_pointer(
    welt_c_sca_case_context_type *context){
    return context->out;
}

void welt_c_sca_case_set_index(
welt_c_sca_case_context_type *context, int index){
    context->index = index;
    return;
} 

int welt_c_sca_case_get_index(
welt_c_sca_case_context_type *context){
    return context->index;
} 

void welt_c_sca_case_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){
    int port_index;
    int direction;
    int i; 
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    
    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    for(i = 1; i<context->port_count; i++){
        port_index = i;
        welt_c_graph_add_connection(graph,
                                    (welt_c_actor_context_type *)context, 
                                    port_index, direction);
    }
}
