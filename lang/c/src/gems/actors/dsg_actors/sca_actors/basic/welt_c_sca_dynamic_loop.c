/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_c_sca_dynamic_loop.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"

#define MAX_FIFO_COUNT 4

struct _welt_c_sca_dynamic_loop_context_struct {
#include "welt_c_sca_context_type_common.h"

/* sub mode*/
int process_submode;

/* FIFO ports. */
welt_c_fifo_unit_size_pointer in1; /*loop_start*/
welt_c_fifo_unit_size_pointer in2; /*next_iteration*/
welt_c_fifo_unit_size_pointer out1; /*iteration_start*/
welt_c_fifo_unit_size_pointer out2; /*loop_end*/

/* Number of rest iterations */
int current_iteration;
/* Total number of iterations in the loop*/
int iteration_count;

};

welt_c_sca_dynamic_loop_context_type *welt_c_sca_dynamic_loop_new(
        //welt_c_fifo_unit_size_pointer in1,
        welt_c_fifo_pointer in1,
        //welt_c_fifo_unit_size_pointer in2,
        welt_c_fifo_pointer in2,
        //welt_c_fifo_unit_size_pointer out1,
        welt_c_fifo_pointer out1,
        //welt_c_fifo_unit_size_pointer out2,
        welt_c_fifo_pointer out2,
        int index){

    welt_c_sca_dynamic_loop_context_type *context = NULL;

    context = (welt_c_sca_dynamic_loop_context_type *)welt_c_util_malloc(
                sizeof(welt_c_sca_dynamic_loop_context_type));
                
    context->mode = welt_c_SCA_DYNAMIC_LOOP_MODE_WAIT;
    context->process_submode = welt_c_SCA_DYNAMIC_LOOP_SUBMODE_PROCESS_A;
    
    context->enable = (welt_c_actor_enable_ftype)welt_c_sca_dynamic_loop_enable;
    context->invoke = (welt_c_actor_invoke_ftype)welt_c_sca_dynamic_loop_invoke;

    context->in1 = (welt_c_fifo_unit_size_pointer)in1;
    context->in2 = (welt_c_fifo_unit_size_pointer)in2;
    context->out1 = (welt_c_fifo_unit_size_pointer)out1;
    context->out2 = (welt_c_fifo_unit_size_pointer)out2;
    context->last_fifo = NULL;
    
    context->index = index;
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)
                        welt_c_sca_dynamic_loop_connect;
    context->max_port_count = MAX_FIFO_COUNT;
    
    /* Initialize portrefs*/
    /*
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context,
        FIFO_COUNT, &context->in1, &context->in2, &context->out1, 
        &context->out2);
    */
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->in1);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->in2);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->out1);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->out2);
    
    context->current_iteration = 0;
    context->iteration_count = 0;
     
    return context;
}


bool welt_c_sca_dynamic_loop_enable(welt_c_sca_dynamic_loop_context_type *context) {
    bool result = false;

    switch (context->mode) {
    case welt_c_SCA_DYNAMIC_LOOP_MODE_PROCESS:
        result = (welt_c_fifo_unit_size_population(context->in2) >= 1) &&
                ((welt_c_fifo_unit_size_population(context->out1) <
                welt_c_fifo_unit_size_capacity(context->out1)) ||
                (welt_c_fifo_unit_size_population(context->out2) <
                welt_c_fifo_unit_size_capacity(context->out2)));
        break;
    case welt_c_SCA_DYNAMIC_LOOP_MODE_WAIT:
        result = (welt_c_fifo_unit_size_population(context->in1) >= 1) &&
                (welt_c_fifo_unit_size_population(context->out1) <
                welt_c_fifo_unit_size_capacity(context->out1));
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.
*******************************************************************************/
void welt_c_sca_dynamic_loop_invoke(welt_c_sca_dynamic_loop_context_type *context) {

    switch (context->mode) {
    case welt_c_SCA_DYNAMIC_LOOP_MODE_PROCESS:
        welt_c_fifo_unit_size_read_advance(context->in2);
        
        context->current_iteration = context->current_iteration-1;
        
        if(context->current_iteration > 0)
            context->process_submode = welt_c_SCA_DYNAMIC_LOOP_SUBMODE_PROCESS_A;
        else
            context->process_submode = welt_c_SCA_DYNAMIC_LOOP_SUBMODE_PROCESS_B;
        
        
        if(context->process_submode == welt_c_SCA_DYNAMIC_LOOP_SUBMODE_PROCESS_A){
            welt_c_fifo_unit_size_write_advance(context->out1);
            context->last_fifo = context->out1;
            context->mode = welt_c_SCA_DYNAMIC_LOOP_MODE_PROCESS;
            
        }else{
            welt_c_fifo_unit_size_write_advance(context->out2);
            context->last_fifo = context->out2;
            context->mode = welt_c_SCA_DYNAMIC_LOOP_MODE_WAIT;
        }
        break;
    case welt_c_SCA_DYNAMIC_LOOP_MODE_WAIT:
        welt_c_fifo_unit_size_read(context->in1, &context->iteration_count);
        if(context->iteration_count <= 0){
            context->mode = welt_c_SCA_DYNAMIC_LOOP_MODE_ERROR;
            break;
        }
        context->current_iteration = context->iteration_count;
        welt_c_fifo_unit_size_write_advance(context->out1);
        context->last_fifo = context->out1;
        context->mode = welt_c_SCA_DYNAMIC_LOOP_MODE_PROCESS;
        context->process_submode = welt_c_SCA_DYNAMIC_LOOP_SUBMODE_PROCESS_A;
        break;
    default:
        context->mode = welt_c_SCA_DYNAMIC_LOOP_MODE_ERROR;
        break;
    }
}

void welt_c_sca_dynamic_loop_terminate(
    welt_c_sca_dynamic_loop_context_type *context) {
    free(context);
}

void welt_c_sca_dynamic_loop_set_current_iteration(
    welt_c_sca_dynamic_loop_context_type *context,
    int current_iteration){
    context->current_iteration = current_iteration;
    return;
}

int welt_c_sca_dynamic_loop_get_current_iteration(
    welt_c_sca_dynamic_loop_context_type *context){
    return context->current_iteration;
}

void welt_c_sca_dynamic_loop_set_index(
welt_c_sca_dynamic_loop_context_type *context, int index){
    context->index = index;
    return;
} 

int welt_c_sca_dynamic_loop_get_index(
welt_c_sca_dynamic_loop_context_type *context){
    return context->index;
} 


void welt_c_sca_dynamic_loop_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){
    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context,
                                port_index, direction);
    /* input 2*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 3;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context, 
                                port_index, direction);
    
    return;
}
