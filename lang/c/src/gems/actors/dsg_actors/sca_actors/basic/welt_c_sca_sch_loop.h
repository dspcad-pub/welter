#ifndef _welt_c_sca_sch_loop_h
#define _welt_c_sca_sch_loop_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_actor.h"
#include "welt_c_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_graph_def.h"
#include "welt_c_fifo_unit_size.h"

/* Actor modes */
#define welt_c_SCA_SCH_LOOP_MODE_ERROR      0
#define welt_c_SCA_SCH_LOOP_MODE_PROCESS    1
#define welt_c_SCA_SCH_LOOP_MODE_END        2
#define welt_c_SCA_SCH_LOOP_SUBMODE_PROCESS_A  3
#define welt_c_SCA_SCH_LOOP_SUBMODE_PROCESS_B  4



/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with sca_sch_loop_actor objects. */
struct _welt_c_sca_sch_loop_context_struct;
typedef struct _welt_c_sca_sch_loop_context_struct
        welt_c_sca_sch_loop_context_type;
        
/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

welt_c_sca_sch_loop_context_type *welt_c_sca_sch_loop_new(
        //welt_c_fifo_unit_size_pointer in1,
        welt_c_fifo_pointer in1,
        //welt_c_fifo_unit_size_pointer out1,
        welt_c_fifo_pointer out1,
        int iteration_count, int index);

bool welt_c_sca_sch_loop_enable(
        welt_c_sca_sch_loop_context_type *context);

void welt_c_sca_sch_loop_invoke(
        welt_c_sca_sch_loop_context_type *context);

void welt_c_sca_sch_loop_terminate(
        welt_c_sca_sch_loop_context_type *context);

/* Set and get for current_iteration and iteration_count function*/
void welt_c_sca_sch_loop_set_current_iteration(
    welt_c_sca_sch_loop_context_type *context,
    int current_iteration);

int welt_c_sca_sch_loop_get_current_iteration(
    welt_c_sca_sch_loop_context_type *context);

void welt_c_sca_sch_loop_set_iteration_count(
    welt_c_sca_sch_loop_context_type *context,
    int iteration_count);

int welt_c_sca_sch_loop_get_iteration_count(
    welt_c_sca_sch_loop_context_type *context);

void welt_c_sca_sch_loop_set_index(
welt_c_sca_sch_loop_context_type *context, int index);

int welt_c_sca_sch_loop_get_index(
welt_c_sca_sch_loop_context_type *context);
 
void welt_c_sca_sch_loop_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph); 

 
#endif
