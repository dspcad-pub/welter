#ifndef _welt_c_ref_actor_h
#define _welt_c_ref_actor_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_actor.h"
#include "welt_c_dsg_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_basic.h"
#include "welt_c_graph_def.h"
#include "welt_c_fifo_unit_size.h"


/* Actor modes */
#define welt_c_REF_ACTOR_MODE_PROCESS  0
#define welt_c_REF_ACTOR_MODE_ERROR    1


/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with ref_actor objects. */
struct _welt_c_ref_actor_context_struct;
typedef struct _welt_c_ref_actor_context_struct welt_c_ref_actor_context_type;

typedef void (*welt_c_ref_actor_preA_ftype)
        (welt_c_ref_actor_context_type *context);
        
typedef void (*welt_c_ref_actor_postA_ftype)
        (welt_c_ref_actor_context_type *context);
        
typedef void (*welt_c_ref_actor_exeA_ftype)
        (welt_c_ref_actor_context_type *context);
        
typedef void (*welt_c_ref_actor_dsg_fifo_output_ftype)
        (welt_c_ref_actor_context_type *context);

struct _welt_c_ref_actor_context_struct {
#include "welt_c_ref_actor_context_type_common.h"
};

        
/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

welt_c_ref_actor_context_type *welt_c_ref_actor_new(
        //welt_c_fifo_unit_size_pointer in, welt_c_fifo_unit_size_pointer out,
        welt_c_fifo_pointer in, welt_c_fifo_pointer out,
        int index,
        welt_c_actor_context_type *ref_context,
        int selected_input_count, int selected_output_count, 
        welt_c_ref_actor_preA_ftype preA,
        welt_c_ref_actor_postA_ftype postA,
        welt_c_ref_actor_exeA_ftype exeA);

bool welt_c_ref_actor_enable(welt_c_ref_actor_context_type *context);

void welt_c_ref_actor_invoke(welt_c_ref_actor_context_type *context);

void welt_c_ref_actor_terminate(welt_c_ref_actor_context_type *context);

/* Get the only DSG output FIFO in RA*/
welt_c_fifo_unit_size_pointer welt_c_ref_actor_dsg_fifo_output(
        welt_c_ref_actor_context_type *context);


/* Set and get function for parameters*/
void welt_c_ref_actor_set_preA_function(
    welt_c_ref_actor_context_type *context,
    welt_c_ref_actor_preA_ftype preA);

int welt_c_ref_actor_get_preA_function(
    welt_c_ref_actor_context_type *context);
    
void welt_c_ref_actor_set_postA_function(
    welt_c_ref_actor_context_type *context,
    welt_c_ref_actor_postA_ftype postA);
    
int welt_c_ref_actor_get_postA_function(
    welt_c_ref_actor_context_type *context);
    
void welt_c_ref_actor_set_exeA_function(
    welt_c_ref_actor_context_type *context,
    welt_c_ref_actor_exeA_ftype exeA);

int welt_c_ref_actor_get_exeA_function(
    welt_c_ref_actor_context_type *context);
    
void welt_c_ref_actor_set_selected_input_count(
    welt_c_ref_actor_context_type *context, int selected_input_count);

int welt_c_ref_actor_get_selected_input_count(
    welt_c_ref_actor_context_type *context);
    
void welt_c_ref_actor_set_selected_output_count(
    welt_c_ref_actor_context_type *context, int selected_output_count);

int welt_c_ref_actor_get_selected_output_count(
    welt_c_ref_actor_context_type *context);
    
void welt_c_ref_actor_set_selected_app_gr_inputs(
    welt_c_ref_actor_context_type *context, welt_c_fifo_pointer input,
    int index);

welt_c_fifo_pointer welt_c_ref_actor_get_selected_app_gr_inputs(
    welt_c_ref_actor_context_type *context, int index);

void welt_c_ref_actor_set_selected_app_gr_outputs(
    welt_c_ref_actor_context_type *context, welt_c_fifo_pointer output,
    int index);

welt_c_fifo_pointer welt_c_ref_actor_get_selected_app_gr_outputs(
    welt_c_ref_actor_context_type *context, int index);


void welt_c_ref_actor_set_index(welt_c_ref_actor_context_type *context,
    int index);  

int welt_c_ref_actor_get_index(welt_c_ref_actor_context_type *context);


void welt_c_ref_actor_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph);   
    
#endif
