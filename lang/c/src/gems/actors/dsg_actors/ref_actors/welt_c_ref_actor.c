/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_c_ref_actor.h"
#include "welt_c_util.h"
#include "welt_c_graph.h"
#include "welt_c_ref_pre_post_functions.h"
#define MAX_FIFO_COUNT 2

welt_c_ref_actor_context_type *welt_c_ref_actor_new(
        //welt_c_fifo_unit_size_pointer in, welt_c_fifo_unit_size_pointer out,
        welt_c_fifo_pointer in, welt_c_fifo_pointer out,
        int index,
        welt_c_actor_context_type *ref_context,
        int selected_input_count, int selected_output_count, 
        welt_c_ref_actor_preA_ftype preA,
        welt_c_ref_actor_postA_ftype postA,
        welt_c_ref_actor_exeA_ftype exeA) {

    welt_c_ref_actor_context_type *context = NULL;

    context = (welt_c_ref_actor_context_type *)welt_c_util_malloc(
                sizeof(welt_c_ref_actor_context_type));
    context->mode = welt_c_REF_ACTOR_MODE_PROCESS;
    context->enable = (welt_c_actor_enable_ftype)
                        welt_c_ref_actor_enable;
    context->invoke = (welt_c_actor_invoke_ftype)
                        welt_c_ref_actor_invoke;
    
    context->ref_context = ref_context;

    context->preA = (welt_c_ref_actor_preA_ftype)preA;
    context->postA = (welt_c_ref_actor_postA_ftype)postA;
    context->exeA = (welt_c_ref_actor_exeA_ftype)exeA;
    
    context->selected_input_count = selected_input_count;
    context->selected_output_count = selected_output_count;
    if (context->selected_input_count < 0 || 
        context->selected_output_count < 0){
        context->mode = welt_c_REF_ACTOR_MODE_ERROR;
    }else{
        context->selected_app_gr_inputs = 
            (welt_c_fifo_pointer *)welt_c_util_malloc(
            sizeof(welt_c_fifo_pointer)*context->selected_input_count);
        context->selected_app_gr_outputs = 
            (welt_c_fifo_pointer *)welt_c_util_malloc(
            sizeof(welt_c_fifo_pointer)*context->selected_output_count);
    }
    
    
    context->in = (welt_c_fifo_unit_size_pointer)in;
    context->out = (welt_c_fifo_unit_size_pointer)out;
    context->last_fifo = context->out;
    
    context->index = index;
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)
                        welt_c_ref_actor_connect;
    context->max_port_count = MAX_FIFO_COUNT;
    
    /* Initialize portrefs*/
    /*
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context,
        FIFO_COUNT, &context->in, &context->out);
    */
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->in);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->out);
    
    return context;
}


bool welt_c_ref_actor_enable(welt_c_ref_actor_context_type *context) {
    bool result = false;

    switch (context->mode) {
    case welt_c_REF_ACTOR_MODE_PROCESS:
        result = (welt_c_fifo_unit_size_population(context->in) >= 1) &&
                ((welt_c_fifo_unit_size_population(context->out) <
                welt_c_fifo_unit_size_capacity(context->out)));
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.
*******************************************************************************/

/* In the next step, use dlpp to avoid if checkings in invoke function*/
void welt_c_ref_actor_invoke(welt_c_ref_actor_context_type *context) {
    //printf("start ref\n");
    switch (context->mode) {
    case welt_c_REF_ACTOR_MODE_PROCESS:
        welt_c_fifo_unit_size_read(context->in, &context->common_state);
        if(context->preA != NULL)
            context->preA(context);
        
        if(context->exeA != NULL){
            context->exeA(context);
        }else{
            if (context->ref_context->enable(context->ref_context)) {
                context->ref_context->invoke(context->ref_context);
            }
        }
        
        if(context->postA != NULL){
            //printf("do post\n");
            context->postA(context);
            
        }
        
        welt_c_fifo_unit_size_write(context->out, &context->common_state);
        context->mode = welt_c_REF_ACTOR_MODE_PROCESS;
            //printf("end ref\n");
        break;
    default:
        context->mode = welt_c_REF_ACTOR_MODE_PROCESS;
        break;
    }
}

void welt_c_ref_actor_terminate(welt_c_ref_actor_context_type *context) {
    if(context->selected_app_gr_inputs != NULL){
        free(context->selected_app_gr_inputs);
    }
    if(context->selected_app_gr_outputs != NULL){
        free(context->selected_app_gr_outputs);
    }
    free(context);
}


welt_c_fifo_unit_size_pointer welt_c_ref_actor_dsg_fifo_output(
        welt_c_ref_actor_context_type *context){
    return context->last_fifo;
}



void welt_c_ref_actor_set_preA_function(welt_c_ref_actor_context_type *context,
    welt_c_ref_actor_preA_ftype preA){
    context->preA = preA;
    return;
}

int welt_c_ref_actor_get_preA_function(welt_c_ref_actor_context_type *context){
    return context->mode;
}
    
void welt_c_ref_actor_set_postA_function(welt_c_ref_actor_context_type *context,
    welt_c_ref_actor_postA_ftype postA){
    context->postA = postA;
    return;
}
    
int welt_c_ref_actor_get_postA_function(welt_c_ref_actor_context_type *context){
    return context->mode;
}

void welt_c_ref_actor_set_exeA_function(welt_c_ref_actor_context_type *context,
    welt_c_ref_actor_exeA_ftype exeA){
    context->exeA = exeA;
    return;
}
    
int welt_c_ref_actor_get_exeA_function(welt_c_ref_actor_context_type *context){
    return context->mode;
}


    
void welt_c_ref_actor_set_selected_input_count(
    welt_c_ref_actor_context_type *context, int selected_input_count){
    context->selected_input_count = selected_input_count;
    if (context->selected_input_count < 0){
        context->mode = welt_c_REF_ACTOR_MODE_ERROR;
    }else{
        context->mode = welt_c_REF_ACTOR_MODE_PROCESS;
        if(context->selected_app_gr_inputs != NULL){
            free(context->selected_app_gr_inputs);
        }
        context->selected_app_gr_inputs = 
            (welt_c_fifo_pointer *)welt_c_util_malloc(
            sizeof(welt_c_fifo_pointer)*context->selected_input_count);
        
    }
    
}

int welt_c_ref_actor_get_selected_input_count(
    welt_c_ref_actor_context_type *context){
    return context->selected_input_count;
}
    
void welt_c_ref_actor_set_selected_output_count(
    welt_c_ref_actor_context_type *context, int selected_output_count){
    context->selected_output_count = selected_output_count;
    if (context->selected_output_count < 0){
        context->mode = welt_c_REF_ACTOR_MODE_ERROR;
    }else{
        context->mode = welt_c_REF_ACTOR_MODE_PROCESS;
        if(context->selected_app_gr_outputs != NULL){
            free(context->selected_app_gr_outputs);
        }
        context->selected_app_gr_outputs = 
            (welt_c_fifo_pointer *)welt_c_util_malloc(
            sizeof(welt_c_fifo_pointer)*context->selected_output_count);
        
    }
        
}

int welt_c_ref_actor_get_selected_output_count(
    welt_c_ref_actor_context_type *context){
    return context->selected_output_count;
}
    
void welt_c_ref_actor_set_selected_app_gr_inputs(
    welt_c_ref_actor_context_type *context, welt_c_fifo_pointer input,
    int index){
    context->selected_app_gr_inputs[index] = input;
    return;    
}

welt_c_fifo_pointer welt_c_ref_actor_get_selected_app_gr_inputs(
    welt_c_ref_actor_context_type *context, int index){
    return(context->selected_app_gr_inputs[index]);
}

void welt_c_ref_actor_set_selected_app_gr_outputs(
    welt_c_ref_actor_context_type *context, welt_c_fifo_pointer output,
    int index){
    context->selected_app_gr_outputs[index] = output;
    return;       
}

welt_c_fifo_pointer welt_c_ref_actor_get_selected_app_gr_outputs(
    welt_c_ref_actor_context_type *context, int index){
    return(context->selected_app_gr_outputs[index]);
}

void welt_c_ref_actor_set_index(welt_c_ref_actor_context_type *context,
    int index){
    context->index = index;
    return;
} 

int welt_c_ref_actor_get_index(welt_c_ref_actor_context_type *context){
    return context->index;
} 


void welt_c_ref_actor_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){
    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    
    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 1;
    welt_c_graph_add_connection(graph,
                                (welt_c_actor_context_type *)context, 
                                port_index, direction);
    return;
}
