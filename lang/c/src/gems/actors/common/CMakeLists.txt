SET(source_list
        welt_c_actor.c
        )

ADD_LIBRARY(welt_c_actors_common ${source_list})

INSTALL(TARGETS welt_c_actors_common DESTINATION .)
