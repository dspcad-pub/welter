/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2016
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "welt_c_actor.h"
#include "welt_c_util.h"



int welt_c_actor_get_index(struct welt_c_actor_context_struct *context){
    return context->index;
}
   
void welt_c_actor_set_index(struct welt_c_actor_context_struct *context,
        int index){
    context->index = index;
    return;
}
 
 
void welt_c_actor_set_portref(struct welt_c_actor_context_struct *context,
        int index, welt_c_fifo_pointer fifo){
    welt_c_fifo_pointer *p;
    p = (context->portrefs)[index];
    (*p) = fifo;
    return;
} 
      
welt_c_fifo_pointer welt_c_actor_get_portref(
        struct welt_c_actor_context_struct *context, int index){
    welt_c_fifo_pointer *p;
    p = (context->portrefs)[index];
    return *p;
} 

/*****************************************************************************
Initialize portrefs array: allocate memory for the array and initialize every 
element.
*****************************************************************************/          
void welt_c_actor_init_vari_portrefs(struct welt_c_actor_context_struct *context,
        int count, ...){
    int i;
    va_list valist;
    welt_c_fifo_pointer *port;
    
    welt_c_actor_init_portrefs(context);
    
    if (context->max_port_count < count){
        fprintf(stderr, "number of ports exceeds the maximum number of \
                ports\n");
        context->port_count = 0;
    }else{
        context->port_count = 0;
        va_start(valist, count);
        for(i = 0; i< count; i++){
            port = va_arg(valist, welt_c_fifo_pointer*);
            welt_c_actor_add_portrefs(context, port);
        }
        va_end(valist); 
    }
    return;
}
    
/*****************************************************************************
Initialize portrefs array: allocate memory for the array and initialize every 
element to NULL.
*****************************************************************************/          
void welt_c_actor_init_portrefs(struct welt_c_actor_context_struct *context){
    int i;
    
    context->portrefs = (welt_c_fifo_pointer **)welt_c_util_malloc(
                    sizeof(welt_c_fifo_pointer *) * context->max_port_count);
    for(i = 0; i < context->max_port_count; i++){
        context->portrefs[i] = NULL;
    }   
    context->port_count = 0;
    return;
}
        
/*****************************************************************************
Add portrefs: add one port to the portrefs array.
*****************************************************************************/  

void welt_c_actor_add_portrefs(struct welt_c_actor_context_struct *context,
        welt_c_fifo_pointer *port){
    
    if(context->port_count > (context->max_port_count - 1)){
       fprintf(stderr, "add portrefs: the portrefs array is full; no more \
                ports could be added.\n");
   }else{
       context->portrefs[context->port_count] = port;
       context->port_count += 1;
   }
   return;
}       

/*****************************************************************************
Add portrefs array: add a port array to the portrefs array.
*****************************************************************************/          
void welt_c_actor_add_portrefs_array(struct welt_c_actor_context_struct *context,
        welt_c_fifo_pointer *port_array, int length){
    int i;
    if(context->port_count > (context->max_port_count - length)){
       fprintf(stderr, "add portrefs: the portrefs array is full; no more \
                ports could be added.\n");
    }else{
        for(i = 0; i < length; i++){
            context->portrefs[context->port_count + i] = &(port_array[i]);
        }
        context->port_count += length;
   }
   return;
}   





/*****************************************************************************
Reset portrefs array: de-allocate the array and set portrefs to NULL
*****************************************************************************/ 
void welt_c_actor_reset_portrefs(struct welt_c_actor_context_struct *context){
    if(context->portrefs){
        free(context->portrefs);
        context->portrefs = NULL;
        context->port_count = 0;
    }
    return;
}

/*****************************************************************************
Set the parameter in the actor.
*****************************************************************************/        
void welt_c_actor_set_paramref(struct welt_c_actor_context_struct *context,
        int param_index, void *data){
    void *p;
    p = (context->paramrefs)[param_index];
	
    int param_size = (context->param_length)[param_index];
    /* The following assumes that sizeof(char) = 1. */
    memcpy(p, data, param_size);
    return;
}

/*****************************************************************************
Get the parameter in the actor.
*****************************************************************************/          
void welt_c_actor_get_paramref(struct welt_c_actor_context_struct *context,
        int param_index, void *data){
    void *p;
    p = (context->paramrefs)[param_index];
    
	int param_size = (context->param_length)[param_index];
    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, p, param_size);
    return;
    
}

/*****************************************************************************
Set the parameter length in the actor.
*****************************************************************************/        
void welt_c_actor_set_param_length(struct welt_c_actor_context_struct *context,
        int param_index, int length){
	(context->param_length)[param_index] = length;
    return;
}

/*****************************************************************************
Get the parameter length in the actor.
*****************************************************************************/          
int welt_c_actor_get_param_length(struct welt_c_actor_context_struct *context,
        int param_index){
    int length = (context->param_length)[param_index];
    return length;
}

/*****************************************************************************
Initialize portrefs array: allocate memory for the array and initialize every 
element.
*****************************************************************************//*          
void welt_c_actor_init_paramrefs(struct welt_c_actor_context_struct *context,
        int count, int *param_length, ...){
    int i;
    va_list valist;
    
    context->param_count = count;
	context->param_length = param_length;
    context->paramrefs = welt_c_util_malloc(
                sizeof(void *) * context->param_count);    
    
    //va_start(valist, context->param_count);
    va_start(valist, param_length);
    
    for(i = 0; i<context->param_count; i++){
        context->paramrefs[i] = va_arg(valist, void*);
    }
    va_end(valist);
    
    return;
}
*/
void welt_c_actor_init_vari_paramrefs(
        struct welt_c_actor_context_struct *context, int *data_length,
        int count, ...){
    int i;
    va_list valist;
    void *data;
    
    welt_c_actor_init_paramrefs(context);
    
    if (context->max_param_count < count){
        fprintf(stderr, "number of parameters exceeds the maximum number of \
                parameters\n");
        context->param_count = 0;
        return;
    }else{
        context->param_count = 0;
        va_start(valist, count);
        for(i = 0; i < count; i++){
            data = va_arg(valist, void*);
            welt_c_actor_add_paramrefs(context, data, data_length[i]);
        }
        va_end(valist); 
    }
    return; 
}

/*****************************************************************************
Initialize paramrefs array: allocate memory for the array and initialize every 
element and initialize each element.
*****************************************************************************/          
void welt_c_actor_init_paramrefs(struct welt_c_actor_context_struct *context){
    int i;
    /*??what is the param_length*/
    //context->param_length = param_length;
    
    context->paramrefs = (void **)welt_c_util_malloc(
                    sizeof(void *) * context->max_param_count);
    
    context->param_length = (int *)welt_c_util_malloc(
                    sizeof(int) * context->max_param_count);
    
    for(i = 0; i < context->max_param_count; i++){
        context->paramrefs[i] = NULL;
        context->param_length[i] = 0;
    }               
    return;
}

/*****************************************************************************
Add paramrefs array: add a parameter array to the paramrefs array.
*****************************************************************************/          
void welt_c_actor_add_paramtrefs_array(
        struct welt_c_actor_context_struct *context, void *data_array,
        int *data_length, int length){
    int i;
    int accumu_length;
    accumu_length = 0;
    if(context->param_count > (context->max_param_count - length)){
       fprintf(stderr, "add paramtrefs: the paramtrefs array is full; no more \
                paramter could be added.\n");
    }else{
        for(i = 0; i < length; i++){
            context->paramrefs[context->param_count + i] = 
                                (void *)((unsigned char*)data_array+accumu_length);
            context->param_length[context->param_count + i] = data_length[i];
            accumu_length += data_length[i];
        }
        context->param_count += length;
   }
   return;
} 


/*****************************************************************************
Add paramrefs: add one parameter to the paramrefs array.
*****************************************************************************/ 
void welt_c_actor_add_paramrefs(struct welt_c_actor_context_struct *context,
        void *data, int data_length){
    
    if(context->param_count > (context->max_param_count - 1)){
       fprintf(stderr, "add paramrefs: the paramrefs array is full; no more \
                parameter could be added.\n");
       return;
   }else{
       context->paramrefs[context->param_count] = data;
       context->param_length[context->param_count] = data_length;
       context->param_count += 1;
   }
   return;
} 


/*****************************************************************************
Reset paramrefs array: de-allocate the array and set paramrefs to NULL
*****************************************************************************/ 
/*
void welt_c_actor_reset_paramrefs(struct welt_c_actor_context_struct *context){
    if(context->paramrefs){
        free(context->paramrefs);
        context->paramrefs = NULL;
        context->param_count = 0;
    }
    return;
}
*/

void welt_c_actor_reset_paramrefs(struct welt_c_actor_context_struct *context){
    if(context->paramrefs){
        free(context->paramrefs);
        context->paramrefs = NULL;
        context->param_count = 0;
    }
    if(context->param_length){
        free(context->param_length);
    }
    return;
}


/*****************************************************************************
Set Invoke Function
*****************************************************************************/ 
void welt_c_actor_setInvoke(struct welt_c_actor_context_struct *context,
            welt_c_actor_invoke_ftype invoke){
    context->invoke = invoke;
    
    return;
}

/*****************************************************************************
Invoke diagnostic function:
- Invoke function of hierarchical actors (H-actors) could use this 
function for testing if corresponding DSG or scheduler is not available 
- This function usually is applied early in the development phase
*****************************************************************************/ 
void welt_c_actor_invoke_diagnostic(
        struct welt_c_actor_context_struct *context){
    int index = context->index;
    
    printf("invoke_diagnostic: invoking actor %d\n", index);
    
    return;
}

/*****************************************************************************
Fifo-port connect function:
- The "actor_context" should be the actor you want to connect.
- The "port_index" is the port index to specify which port to connect.
- The "fifo_pointer" is the pointer of the fifo to connect.
*****************************************************************************/
void welt_c_connect_fifo(welt_c_actor_context_type *actor_context,
        int port_index, welt_c_fifo_pointer* fifo_pointer){
    actor_context->portrefs[port_index] = fifo_pointer;
}
