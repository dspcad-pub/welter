#ifndef _welt_c_actor_h
#define _welt_c_actor_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdarg.h>
#include "welt_c_basic.h"
#include "welt_c_graph_def.h"
#include "welt_c_fifo.h"

struct welt_c_actor_context_struct;

/*****************************************************************************
A pointer to a "welt_c_actor_invoke_function", which is a function that invokes
an actor with a given context.
*****************************************************************************/
typedef void (*welt_c_actor_invoke_ftype)
        (struct welt_c_actor_context_struct *context);

/*****************************************************************************
A pointer to a "welt_c_actor_enable_function", which is a function that enables
an actor with a given context.
*****************************************************************************/
typedef bool (*welt_c_actor_enable_ftype)
        (struct welt_c_actor_context_struct *context);


/*****************************************************************************
A pointer to a "welt_c_actor_connect", which adds an actor to a dataflow graph
*****************************************************************************/
typedef void (*welt_c_actor_connect_ftype)
        (struct welt_c_actor_context_struct *context,
        struct welt_c_graph_context_struct *graph);

/*****************************************************************************
A pointer to a "welt_c_actor_reset", which resets an actor in a dataflow graph
*****************************************************************************/
typedef void (*welt_c_actor_reset_ftype)
        (struct welt_c_actor_context_struct *context);
        

/* Function prototype for static functions in actor class*/ 
/*****************************************************************************
Set the index of an actor in the dataflow graph
*****************************************************************************/
void welt_c_actor_set_index(struct welt_c_actor_context_struct *context,
        int index);

/*****************************************************************************
Get the index of an actor in the dataflow graph
*****************************************************************************/     
int welt_c_actor_get_index(struct welt_c_actor_context_struct *context);

/*****************************************************************************
Set the parameter in the actor.
*****************************************************************************/        
void welt_c_actor_set_paramref(struct welt_c_actor_context_struct *context,
        int param_index, void *data);  

/*****************************************************************************
Get the parameter in the actor.
*****************************************************************************/          
void welt_c_actor_get_paramref(struct welt_c_actor_context_struct *context,
        int param_index, void *data);

/*****************************************************************************
Set the parameter length in the actor.
*****************************************************************************/ 		
void welt_c_actor_set_param_length(struct welt_c_actor_context_struct *context,
        int param_index, int length);

/*****************************************************************************
Get the parameter length in the actor.
*****************************************************************************/   
int welt_c_actor_get_param_length(struct welt_c_actor_context_struct *context,
        int param_index);

/*****************************************************************************
Initialize paramrefs array: allocate memory for the array and initialize every 
element. Number of parameters listed in the argument list could be variable.
*****************************************************************************//*          
void welt_c_actor_init_paramrefs(struct welt_c_actor_context_struct *context,
        int count, int *param_length, ...);       
*/
void welt_c_actor_init_vari_paramrefs(
        struct welt_c_actor_context_struct *context, int *data_length,
        int count, ...);  

/*****************************************************************************
Initialize paramrefs array: allocate memory for the array and initialize every 
element. 
*****************************************************************************/          
void welt_c_actor_init_paramrefs(struct welt_c_actor_context_struct *context);
        
/*****************************************************************************
Add paramrefs: add one parameter to the paramrefs array.
*****************************************************************************/  
void welt_c_actor_add_paramrefs(struct welt_c_actor_context_struct *context,
        void *data, int data_length);

/*****************************************************************************
Add paramrefs array: add a parameter array to the paramrefs array.
*****************************************************************************/          
void welt_c_actor_add_paramtrefs_array(
        struct welt_c_actor_context_struct *context, void *data_array,
        int *data_length, int length);      
        
/*****************************************************************************
Reset paramrefs array: de-allocate the array and set paramrefs to NULL
*****************************************************************************/ 
void welt_c_actor_reset_paramrefs(struct welt_c_actor_context_struct *context);


/*****************************************************************************
Set the element in portrefs 
*****************************************************************************/  
void welt_c_actor_set_portref(struct welt_c_actor_context_struct *context,
        int index, welt_c_fifo_pointer fifo);

/*****************************************************************************
Get the element in portrefs 
*****************************************************************************/          
welt_c_fifo_pointer welt_c_actor_get_portref(
        struct welt_c_actor_context_struct *context, int index);

/*****************************************************************************
Initialize portrefs array: allocate memory for the array and initialize every 
element. Number of ports listed in the argument list could be variable.
*****************************************************************************/          
void welt_c_actor_init_vari_portrefs(struct welt_c_actor_context_struct *context,
        int count, ...); 
       
/*****************************************************************************
Initialize portrefs array: allocate memory for the array and initialize every 
element to NULL.
*****************************************************************************/          
void welt_c_actor_init_portrefs(struct welt_c_actor_context_struct *context);
        
/*****************************************************************************
Add portrefs: add one port to the portrefs array.
*****************************************************************************/          
void welt_c_actor_add_portrefs(struct welt_c_actor_context_struct *context,
        welt_c_fifo_pointer *port);

/*****************************************************************************
Add portrefs array: add a port array to the portrefs array.
*****************************************************************************/          
void welt_c_actor_add_portrefs_array(struct welt_c_actor_context_struct *context,
        welt_c_fifo_pointer *port_array, int length);
        
/*****************************************************************************
Reset portrefs array: de-allocate the array and set portrefs to NULL
*****************************************************************************/ 
void welt_c_actor_reset_portrefs(struct welt_c_actor_context_struct *context);

/*****************************************************************************
Set Invoke Function
*****************************************************************************/ 
void welt_c_actor_setInvoke(struct welt_c_actor_context_struct *context,
            welt_c_actor_invoke_ftype invoke);

/*****************************************************************************
Invoke diagnostic function:
- Invoke function of hierarchical actors (H-actors) could use this 
function for testing if corresponding DSG or scheduler is not available 
- This function usually is applied early in the development phase
*****************************************************************************/ 
void welt_c_actor_invoke_diagnostic(
        struct welt_c_actor_context_struct *context);

typedef struct welt_c_actor_context_struct {
#include "welt_c_actor_context_type_common.h"
} welt_c_actor_context_type;

/*****************************************************************************
 Fifo-port connect function:
 - The "actor_context" should be the actor you want to connect.
 - The "port_index" is the port index to specify which port to connect.
 - The "fifo_pointer" is the pointer of the fifo to connect.
*****************************************************************************/
void welt_c_connect_fifo(welt_c_actor_context_type *actor_context,
        int port_index, welt_c_fifo_pointer* fifo_pointer);

#endif
