/*******************************************************************************
 @ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

 @ddblock_end copyright
 *******************************************************************************/

#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "welt_c_fork.h"
#include "welt_c_util.h"
#include "welt_c_graph.h"

struct _welt_c_fork_context_struct
{
#include "welt_c_actor_context_type_common.h"
    void *temp;
    /* The size of input and output data */
    size_t size;
};

welt_c_fork_context_type *welt_c_fork_new(size_t size, int output_cnt){
    welt_c_fork_context_type *context = NULL;
    context = (welt_c_fork_context_type *)welt_c_util_malloc
        (sizeof(welt_c_fork_context_type));
    /* Handle malloc error */
    if(context == NULL)
    {
        /* Print to stderr */
        fprintf(stderr,"Malloc failure\n");
        exit(1); /* Error code with exit */
    }
    context->enable = (welt_c_actor_enable_ftype)welt_c_fork_enable;
    context->invoke = (welt_c_actor_invoke_ftype)welt_c_fork_invoke;
    context->reset = (welt_c_actor_reset_ftype)welt_c_fork_reset;
    /* Fork actor has only on mode */
    context->mode = welt_c_FORK_SOLE_MODE;
    context->size = size;
    context->temp = malloc(context->size);
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)welt_c_fork_connect;
    context->max_port_count = output_cnt+1;

    /* Initialize portrefs to NULL*/
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    return context;
}

bool welt_c_fork_enable(welt_c_fork_context_type *context) {
    bool result = false;
    switch (context->mode) {
        case welt_c_FORK_SOLE_MODE:
            result = (welt_c_fifo_population(
                    (welt_c_fifo_pointer)*(context->portrefs[0])) >= 1) &&
                     (welt_c_fifo_population(
                             (welt_c_fifo_pointer)*(context->portrefs[1])) <
                      welt_c_fifo_capacity(
                              (welt_c_fifo_pointer)*(context->portrefs[1])));
            break;
//        case RNDCLAB_MC_FINDECC_MODE_INACTIVE:
//            result = false;
//            break;
        default:
            result = false;
            break;
    }
    return result;
}

void welt_c_fork_invoke(welt_c_fork_context_type *context) {

//    clock_t begin = clock();

    /* Get input */
    welt_c_fifo_read ((welt_c_fifo_pointer)
        *(context->portrefs[0]),(context->temp));

    /* The first output */
    welt_c_fifo_write((welt_c_fifo_pointer)
                                    *(context->portrefs[1]), (context->temp));

    /* More outputs */
    int i = 2;
    for (i=2;i<context->max_port_count;i++) {
        void *newtoken = malloc(context->size);
        /*form a new token*/
        memcpy(newtoken, context->temp, context->size);

        welt_c_fifo_write((welt_c_fifo_pointer)
                                        *(context->portrefs[i]), (newtoken));
        free(newtoken);
    }

//    clock_t end = clock();
//    double elapsed_secs = ((double)(end - begin)) / CLOCKS_PER_SEC;
//    printf("fork execution time %f\n:",elapsed_secs);
    context->mode = welt_c_FORK_SOLE_MODE;

}

void welt_c_fork_terminate(welt_c_fork_context_type *context) {
    free(context->temp);
    free(context);
}

void welt_c_fork_reset(welt_c_fork_context_type *context) {
    context->mode = welt_c_FORK_SOLE_MODE;
    return;
}

void welt_c_fork_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph, int output_cnt){
    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context,
                                port_index, direction);
    int i = 1;
    for (int i=1;i<=output_cnt;i++) {
        /* output i*/
        direction = GRAPH_OUT_CONN_DIRECTION;
        port_index = i;
        welt_c_graph_add_connection(graph, (welt_c_actor_context_type *) context,
                                    port_index, direction);
    }
    return;
} 
