#ifndef _welt_c_add_de_h
#define _welt_c_add_de_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_graph_def.h"
/* Actor modes */
#define welt_c_ADD_DE_MODE_PROCESS  1

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with add objects. */
struct _welt_c_add_de_context_struct;
typedef struct _welt_c_add_de_context_struct welt_c_add_de_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

/*****************************************************************************
Construct function of the welt_c_add actor.
The first two ports are for the input ports, and the rest one port is for the 
output port. 
*****************************************************************************/
welt_c_add_de_context_type *welt_c_add_de_new(/*welt_c_fifo_pointer in1,
        welt_c_fifo_pointer in2, welt_c_fifo_pointer out, */int index);

/*****************************************************************************
Enable function of the welt_c_add actor.
*****************************************************************************/
bool welt_c_add_de_enable(welt_c_add_de_context_type *context);

/*****************************************************************************
Invoke function of the welt_c_add actor.
*****************************************************************************/
void welt_c_add_de_invoke(welt_c_add_de_context_type *context);

/*****************************************************************************
Terminate function of the welt_c_add actor.
*****************************************************************************/
void welt_c_add_de_terminate(welt_c_add_de_context_type *context);

/*****************************************************************************
Reset function of the welt_c_add actor.
*****************************************************************************/
void welt_c_add_de_reset(welt_c_add_de_context_type *context);

/*****************************************************************************
Add connection function of the welt_c_add actor.
*****************************************************************************/
void welt_c_add_de_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph);        




#endif
