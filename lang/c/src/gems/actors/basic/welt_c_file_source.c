/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_c_file_source.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"

#define MAX_FIFO_COUNT  1

struct _welt_c_file_source_context_struct {
#include "welt_c_actor_context_type_common.h"
    FILE *fp; 
    char *file;
    int data;
    welt_c_fifo_pointer out;
};


welt_c_file_source_context_type *welt_c_file_source_new(char *file,
        welt_c_fifo_pointer out, int index) {

    welt_c_file_source_context_type *context = NULL;
    //printf("file source init\n");
    context = welt_c_util_malloc(sizeof(welt_c_file_source_context_type));
    context->enable =
            (welt_c_actor_enable_ftype)welt_c_file_source_enable;
    context->invoke =
            (welt_c_actor_invoke_ftype)welt_c_file_source_invoke;
    context->reset =
            (welt_c_actor_invoke_ftype)welt_c_file_source_reset;
    context->file = file;
    context->fp = welt_c_util_fopen((const char *)context->file, "r");
    if (fscanf(context->fp, "%d", &context->data) != 1) { 
            /* End of input */
        context->mode = welt_c_FILE_SOURCE_MODE_INACTIVE;
        context->data = 0;
        
    }else{
        context->mode = welt_c_FILE_SOURCE_MODE_WRITE;
    }
    
    context->out = (welt_c_fifo_pointer)out;
    
    context->index = index;
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)welt_c_file_source_connect;
    context->max_port_count = MAX_FIFO_COUNT;

    /* Initialize portrefs*/
    /*
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context,
        FIFO_COUNT, &context->out);
    */
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->out);
    
    return context;
}

bool welt_c_file_source_enable(welt_c_file_source_context_type *context) {
    bool result = false;

    switch (context->mode) {
    case welt_c_FILE_SOURCE_MODE_WRITE:
    /*
        result = (welt_c_fifo_population(context->out) <
                welt_c_fifo_capacity(context->out));
   */
        result = (welt_c_fifo_population(
                (welt_c_fifo_pointer)*(context->portrefs[0])) <
                welt_c_fifo_capacity(
                (welt_c_fifo_pointer)*(context->portrefs[0])));
   
  
    break;
    case welt_c_FILE_SOURCE_MODE_INACTIVE:
        result = false;
        break;
    default:
        result = false;
        break;
    }
    return result;
}

void welt_c_file_source_invoke(welt_c_file_source_context_type *context) {
    switch (context->mode) {
    case welt_c_FILE_SOURCE_MODE_WRITE:
        //welt_c_fifo_write(context->out, &context->data);
          //  printf("file source");
        welt_c_fifo_write(
            (welt_c_fifo_pointer)*(context->portrefs[0]),
            &context->data);
        if (fscanf(context->fp, "%d", &context->data) != 1) { 
            /* End of input */
            context->mode = welt_c_FILE_SOURCE_MODE_INACTIVE;
        }else{
            context->mode = welt_c_FILE_SOURCE_MODE_WRITE;;
        }
        break;
    case welt_c_FILE_SOURCE_MODE_INACTIVE:
        context->mode = welt_c_FILE_SOURCE_MODE_INACTIVE;
        break;
    default:
        context->mode = welt_c_FILE_SOURCE_MODE_INACTIVE;
        break;
    }
}

void welt_c_file_source_terminate(welt_c_file_source_context_type *context) {
    fclose(context->fp);
    free(context);
}

void welt_c_file_source_reset(welt_c_file_source_context_type *context){
    fclose(context->fp);
    context->fp = welt_c_util_fopen((const char *)context->file, "r");
    if (fscanf(context->fp, "%d", &context->data) != 1) { 
        context->mode = welt_c_FILE_SOURCE_MODE_INACTIVE;
        context->data = 0;
        
    }else{
        context->mode = welt_c_FILE_SOURCE_MODE_WRITE;
    }
    return;
}


void welt_c_file_source_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){
    
    int port_index;
    int direction;
    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 0;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context, 
                                port_index, direction);
    
    return;
}


