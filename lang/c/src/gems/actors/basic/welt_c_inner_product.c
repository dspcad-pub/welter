/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "welt_c_inner_product.h"
#include "welt_c_util.h"
#include "welt_c_graph.h"

#define MAX_FIFO_COUNT  4

/*******************************************************************************
INNER PRODUCT STRUCTURE DEFINITION
*******************************************************************************/

struct _welt_c_inner_product_context_struct {
#include "welt_c_actor_context_type_common.h"

    /* Vector length variable. */
    int length;  

    /* Input ports. */
    welt_c_fifo_pointer m;
    welt_c_fifo_pointer x;
    welt_c_fifo_pointer y;

    /* Output port. */
    welt_c_fifo_pointer out;
};

/*******************************************************************************
IMPLEMENTATIONS OF INTERFACE FUNCTIONS.
*******************************************************************************/

welt_c_inner_product_context_type *welt_c_inner_product_new(
        welt_c_fifo_pointer m, welt_c_fifo_pointer x, welt_c_fifo_pointer y,
        welt_c_fifo_pointer out, int index) {

    welt_c_inner_product_context_type *context = NULL;

    context = welt_c_util_malloc(sizeof(welt_c_inner_product_context_type));
    context->mode = welt_c_INNER_PRODUCT_MODE_STORE_LENGTH;
    context->enable =
            (welt_c_actor_enable_ftype)welt_c_inner_product_enable;
    context->invoke = 
            (welt_c_actor_invoke_ftype)welt_c_inner_product_invoke;
    context->reset = (welt_c_actor_reset_ftype)welt_c_inner_product_reset;
    context->length = 0;
    context->m = (welt_c_fifo_pointer)m;
    context->x = (welt_c_fifo_pointer)x;
    context->y = (welt_c_fifo_pointer)y;
    context->out = (welt_c_fifo_pointer)out;
    
    context->index = index;
    context->connect = (welt_c_actor_connect_ftype)welt_c_inner_product_connect;
    context->max_port_count = MAX_FIFO_COUNT;
    
    
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->m);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->x);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->y);
    welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                (welt_c_fifo_pointer *)&context->out);
    
    
    return context;
}

bool welt_c_inner_product_enable(
        welt_c_inner_product_context_type *context) {
    bool result = false;

    switch (context->mode) {
    case welt_c_INNER_PRODUCT_MODE_STORE_LENGTH:
        result = welt_c_fifo_population(context->m) >= 1;
        break;
    case welt_c_INNER_PRODUCT_MODE_PROCESS:
        result = (welt_c_fifo_population(context->x) >= context->length)
                    && (welt_c_fifo_population(context->y)
                    >= context->length) && 
                    (welt_c_fifo_population(context->out) <
                    welt_c_fifo_capacity(context->out));
        break;
    default:
            result = false;
            break;
    }
    return result;
}

void welt_c_inner_product_invoke(welt_c_inner_product_context_type *context) {
    int i = 0;
    int sum = 0;
    int x_value = 0;
    int y_value = 0;

    switch (context->mode) {
    case welt_c_INNER_PRODUCT_MODE_STORE_LENGTH:
        welt_c_fifo_read(context->m, &(context->length));
        if (context->length <= 0) {
            context->mode = welt_c_INNER_PRODUCT_MODE_STORE_LENGTH;
            return;
        }
        context->mode = welt_c_INNER_PRODUCT_MODE_PROCESS;
        break;

    case welt_c_INNER_PRODUCT_MODE_PROCESS:
        for (i = 0; i < context->length; i++){
            welt_c_fifo_read(context->x, &(x_value));
            welt_c_fifo_read(context->y, &(y_value));
            sum += (x_value * y_value);
        }
        welt_c_fifo_write(context->out, &sum);
        context->mode = welt_c_INNER_PRODUCT_MODE_STORE_LENGTH;
        break; 
    default:
        context->mode = welt_c_INNER_PRODUCT_MODE_STORE_LENGTH;
        break;
    } 
    return;
}

void welt_c_inner_product_terminate(
        welt_c_inner_product_context_type *context) {
    free(context);
}

void welt_c_inner_product_reset(welt_c_inner_product_context_type *context){
    context->mode = welt_c_INNER_PRODUCT_MODE_STORE_LENGTH;
    context->length = 0;
    return;
}


void welt_c_inner_product_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){
    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = welt_c_IP_PORT_M;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    /* input 2*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = welt_c_IP_PORT_X;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    /* input 3*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = welt_c_IP_PORT_Y;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = welt_c_IP_PORT_OUT;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context, 
                                port_index, direction);
    return;
} 
