#ifndef _welt_c_file_source_h
#define _welt_c_file_source_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_graph_def.h"

/* Actor modes */
#define welt_c_FILE_SOURCE_MODE_WRITE        1
#define welt_c_FILE_SOURCE_MODE_INACTIVE     2

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with file source objects. */
struct _welt_c_file_source_context_struct;
typedef struct _welt_c_file_source_context_struct
        welt_c_file_source_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

/*****************************************************************************
Construct function of the welt_c_file_source actor. Create a new
welt_c_file_source with the specified file pointer, and the specified output
FIFO pointer. Data format in the input file should be all integers.
*****************************************************************************/
welt_c_file_source_context_type *welt_c_file_source_new(char *file,
        welt_c_fifo_pointer out, int index);

/*****************************************************************************
Enable function of the welt_c_file_source actor.
*****************************************************************************/
bool welt_c_file_source_enable(welt_c_file_source_context_type *context);

/*****************************************************************************
Invoke function of the welt_c_file_source actor.
*****************************************************************************/
void welt_c_file_source_invoke(welt_c_file_source_context_type *context);

/*****************************************************************************
Terminate function of the welt_c_file_source actor.
*****************************************************************************/
void welt_c_file_source_terminate(welt_c_file_source_context_type *context);

/*****************************************************************************
Reset function of the welt_c_file_source actor.
*****************************************************************************/
void welt_c_file_source_reset(welt_c_file_source_context_type *context);

/*****************************************************************************
Add connection function of the welt_c_file_source actor.
*****************************************************************************/
void welt_c_file_source_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph);  

#endif
