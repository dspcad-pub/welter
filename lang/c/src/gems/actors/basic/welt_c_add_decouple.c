/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_c_add_decouple.h"
#include "welt_c_util.h"
#include "welt_c_graph.h"

#define MAX_FIFO_COUNT  3

struct _welt_c_add_de_context_struct {
#include "welt_c_actor_context_type_common.h"

    /* Actor interface ports. */
    // welt_c_fifo_pointer in1;
    // welt_c_fifo_pointer in2;
    // welt_c_fifo_pointer out;
};

welt_c_add_de_context_type *welt_c_add_de_new(/*welt_c_fifo_pointer in1,
        welt_c_fifo_pointer in2, welt_c_fifo_pointer out, */int index) {

    welt_c_add_de_context_type *context = NULL;

    context = welt_c_util_malloc(sizeof(welt_c_add_de_context_type));
    context->mode = welt_c_ADD_DE_MODE_PROCESS;
    context->enable = (welt_c_actor_enable_ftype)welt_c_add_de_enable;
    context->invoke = (welt_c_actor_invoke_ftype)welt_c_add_de_invoke;
    context->reset = (welt_c_actor_reset_ftype)welt_c_add_de_reset;
    // context->in1 = (welt_c_fifo_pointer)in1;
    // context->in2 = (welt_c_fifo_pointer)in2;
    // context->out = (welt_c_fifo_pointer)out;
    
    context->index = index;
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)welt_c_add_de_connect;
    context->max_port_count = MAX_FIFO_COUNT;
    
    /* Initialize portrefs*/
    /*
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context,
        FIFO_COUNT, &context->in1, &context->in2, &context->out);
    */
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    // welt_c_actor_add_de_portrefs((welt_c_actor_context_type *)context,
    //                             (welt_c_fifo_pointer *)&context->in1);
    // welt_c_actor_add_de_portrefs((welt_c_actor_context_type *)context,
    //                             (welt_c_fifo_pointer *)&context->in2);
    // welt_c_actor_add_de_portrefs((welt_c_actor_context_type *)context,
    //                             (welt_c_fifo_pointer *)&context->out);
                                
    return context;
}

bool welt_c_add_de_enable(welt_c_add_de_context_type *context) {
    bool result = false;

    switch (context->mode) {
    case welt_c_ADD_DE_MODE_PROCESS:
        
        /*
        result = (context->in1->get_population(context->in1) >= 1) &&
                (context->in2->get_population(context->in2) >= 1) &&
                ((context->out->get_population(context->out) < 
                context->out->get_capacity(context->out)));
        */
        
        result = (welt_c_fifo_population(
                (welt_c_fifo_pointer)*(context->portrefs[0])) >= 1)
                && (welt_c_fifo_population(
                (welt_c_fifo_pointer)*(context->portrefs[1])) >= 1)
                && ((welt_c_fifo_population(
                (welt_c_fifo_pointer)*(context->portrefs[2])) <
                welt_c_fifo_capacity(
                (welt_c_fifo_pointer)*(context->portrefs[2]))));
        
        break;
    default:
        result = false;
        break;
    }
    return result;
}

/*******************************************************************************
The invoke method assumes that a corresponding call to enable has
returned TRUE so that the required data is available.

*******************************************************************************/
void welt_c_add_de_invoke(welt_c_add_de_context_type *context) {
    // printf("add decouple actor invoked\n");
    int value1 = 0;
    int value2 = 0;
    int sum = 0;

    switch (context->mode) {
    case welt_c_ADD_DE_MODE_PROCESS:
        //welt_c_fifo_read(context->in1, &value1);
        welt_c_fifo_read(
            (welt_c_fifo_pointer)*(context->portrefs[0]), &value1);
        //welt_c_fifo_read(context->in2, &value2);
        welt_c_fifo_read(
            (welt_c_fifo_pointer)*(context->portrefs[1]), &value2);
        sum = value1 + value2;
        //welt_c_fifo_write(context->out, &sum);
        welt_c_fifo_write(
            (welt_c_fifo_pointer)*(context->portrefs[2]), &sum);
        context->mode = welt_c_ADD_DE_MODE_PROCESS;
        break;
    default:
        context->mode = welt_c_ADD_DE_MODE_PROCESS;
        break;
    }
}

void welt_c_add_de_terminate(welt_c_add_de_context_type *context) {
    free(context);
}

void welt_c_add_de_reset(welt_c_add_de_context_type *context){
    
    context->mode = welt_c_ADD_DE_MODE_PROCESS;
    return;
}
void welt_c_add_de_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){
    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    /* input 2*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 1;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context,
                                port_index, direction);
    
    /* output 1*/
    direction = GRAPH_OUT_CONN_DIRECTION;
    port_index = 2;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context, 
                                port_index, direction);
    return;
} 
