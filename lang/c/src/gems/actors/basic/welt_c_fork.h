#ifndef _welt_c_fork_h
#define _welt_c_fork_h
/*******************************************************************************
 @ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

 @ddblock_end copyright
 *******************************************************************************/

#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_graph_def.h"

/* Fork actor has only on mode */
#define welt_c_FORK_SOLE_MODE    	  		1

struct _welt_c_fork_context_struct;
typedef struct _welt_c_fork_context_struct
        welt_c_fork_context_type;

/* - Function to fork the tokens to different subsequent actors with
 * memory copy.
 * - The "size" parameter should give the number of bytes in a single
 * input token.
 * - The "output_cnt" parameter should give the number of output ports
 * of the fork actor.
 * - e.g., a three output fork actor that operates on int :
 * welt_c_fork_context_type *welt_c_fork_new(4, 3); */
welt_c_fork_context_type *welt_c_fork_new(size_t size, int output_cnt);

/* Enable function */
bool welt_c_fork_enable(welt_c_fork_context_type *context);

/* Invoke function */
void welt_c_fork_invoke(welt_c_fork_context_type *context);

/* Terminate function */
void welt_c_fork_terminate(welt_c_fork_context_type *context);

void welt_c_fork_reset(welt_c_fork_context_type *context);

void welt_c_fork_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph, int output_cnt);

#endif
