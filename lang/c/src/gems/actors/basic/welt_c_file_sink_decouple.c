/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "welt_c_file_sink_decouple.h"
#include "welt_c_util.h"
#include "welt_c_graph.h"

#define MAX_FIFO_COUNT 1


struct _welt_c_file_sink_decouple_context_struct {
#include "welt_c_actor_context_type_common.h"
    FILE *fp;  
    char *file;
    // welt_c_fifo_pointer in;
};


welt_c_file_sink_decouple_context_type *welt_c_file_sink_decouple_new(char *file/*,
        welt_c_fifo_pointer in*/, int index) {

    welt_c_file_sink_decouple_context_type *context = NULL;

    context = welt_c_util_malloc(sizeof(welt_c_file_sink_decouple_context_type));
    context->invoke = 
            (welt_c_actor_invoke_ftype)welt_c_file_sink_decouple_invoke;
    context->enable = 
            (welt_c_actor_enable_ftype)welt_c_file_sink_decouple_enable;
    context->reset = 
            (welt_c_actor_reset_ftype)welt_c_file_sink_decouple_reset;
    context->file = file;
    context->fp = welt_c_util_fopen((const char *)context->file, "w");
    context->mode = welt_c_FILE_SINK_DECOUPLE_MODE_READ;
    // context->in = (welt_c_fifo_pointer)in;
    
    context->index = index;
    context->subgraphs = NULL;
    context->subgraph_count = 0;
    context->connect = (welt_c_actor_connect_ftype)welt_c_file_sink_decouple_connect;
    context->max_port_count = MAX_FIFO_COUNT;
    
    /* Initialize portrefs*/
    /*
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context,
        FIFO_COUNT, &context->in);
    */
    welt_c_actor_init_portrefs((welt_c_actor_context_type *)context);
    // welt_c_actor_add_portrefs((welt_c_actor_context_type *)context,
                                // (welt_c_fifo_pointer *)&context->in);
    
    return context;
}

bool welt_c_file_sink_decouple_enable(welt_c_file_sink_decouple_context_type *context) {
    bool result = false;

    switch (context->mode) {
    case welt_c_FILE_SINK_DECOUPLE_MODE_READ:
        //result = welt_c_fifo_population(context->in) >= 1;
        result = welt_c_fifo_population(
                    (welt_c_fifo_pointer)*(context->portrefs[0])) >= 1;
        break;
    default:
        result = false;
        break;
    }
    return result;
}

void welt_c_file_sink_decouple_invoke(welt_c_file_sink_decouple_context_type *context) {
    int value = 0;

    switch (context->mode) {
    case welt_c_FILE_SINK_DECOUPLE_MODE_READ:
        //welt_c_fifo_read(context->in, &value);
        welt_c_fifo_read(
            (welt_c_fifo_pointer)*(context->portrefs[0]), &value);
        fprintf(context->fp, "%d\n", value);
        context->mode = welt_c_FILE_SINK_DECOUPLE_MODE_READ;
        break;
    default:
        context->mode = welt_c_FILE_SINK_DECOUPLE_MODE_READ;
        break;
    }
}

void welt_c_file_sink_decouple_terminate(welt_c_file_sink_decouple_context_type *context) {
    fclose(context->fp);
    free(context);
}

void welt_c_file_sink_decouple_reset(welt_c_file_sink_decouple_context_type *context){
    fclose(context->fp);
    context->fp = welt_c_util_fopen((const char *)context->file, "w");
    context->mode = welt_c_FILE_SINK_DECOUPLE_MODE_READ;
    return;
}


void welt_c_file_sink_decouple_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph){

    int port_index;
    int direction;
    /* input 1*/
    direction = GRAPH_IN_CONN_DIRECTION;
    port_index = 0;
    welt_c_graph_add_connection(graph, (welt_c_actor_context_type *)context, 
                                port_index, direction);
    
    return;


}
