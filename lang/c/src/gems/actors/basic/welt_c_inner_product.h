#ifndef _welt_c_inner_product_h
#define _welt_c_inner_product_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_graph_def.h"

/*******************************************************************************
This inner product actor consumes vector length (the number of elements in each
vector) and vectors and computes the inner product. There are two modes
associated with this actor. At the STORE_LENGTH mode, this actor consumes
vector length, sets the length variable, and sets the PROCESS mode as the next
mode. At the PROCESS mode, this actor consumes vectors based on the configured
vector length, computes the inner product, and sets the STORE_LENGTH mode as
the next mode.
*******************************************************************************/

/* Actor modes */
#define welt_c_INNER_PRODUCT_MODE_STORE_LENGTH   1
#define welt_c_INNER_PRODUCT_MODE_PROCESS        2

/* Port index*/
#define welt_c_IP_PORT_M    0
#define welt_c_IP_PORT_X    1
#define welt_c_IP_PORT_Y    2
#define welt_c_IP_PORT_OUT  3

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with inner product objects. */
struct _welt_c_inner_product_context_struct;
typedef struct _welt_c_inner_product_context_struct
        welt_c_inner_product_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

/*****************************************************************************
Construct function of the welt_c_inner_product actor. Create a new
welt_c_inner_product with the specified input FIFO pointer m for the
configuration of vector length, the specified input FIFO pointer x for the
first vector, the specified input FIFO pointer y for the second vector, and the
specified output FIFO pointer.
*****************************************************************************/
welt_c_inner_product_context_type *welt_c_inner_product_new(
        welt_c_fifo_pointer m, welt_c_fifo_pointer x, welt_c_fifo_pointer y,
        welt_c_fifo_pointer out, int index);

/*****************************************************************************
Enable function of the welt_c_inner_product actor.
*****************************************************************************/
bool welt_c_inner_product_enable(welt_c_inner_product_context_type *context);

/*****************************************************************************
Invoke function of the welt_c_inner_product actor.
*****************************************************************************/
void welt_c_inner_product_invoke(welt_c_inner_product_context_type *context);

/*****************************************************************************
Terminate function of the welt_c_inner_product actor.
*****************************************************************************/
void welt_c_inner_product_terminate(welt_c_inner_product_context_type *context);

/*****************************************************************************
Reset function of the welt_c_inner_product actor.
*****************************************************************************/
void welt_c_inner_product_reset(welt_c_inner_product_context_type *context);

/*****************************************************************************
Add connection function of the welt_c_inner_product actor.
*****************************************************************************/
void welt_c_inner_product_connect(welt_c_actor_context_type *context,
        struct welt_c_graph_context_struct *graph);  

#endif
