/****************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2017
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "welt_c_util.h"
#include "welt_fork_router.h"
//#include "welt_router.h"

/*****************************************************************************
Public functions.
*****************************************************************************/

/* router port construct function */
/*  input port label:INPUT_PORT --> 0; 
    output port label: OUTPUT_PORT --> 1 */

struct _welt_fork_router_context_struct {
    
    void **read_pointer;
    
    void **write_pointer;
    
   // int population;
    /* The number of tokens that all the buffers can hold. */
    int capacity;
    
    //welt_router_buffer_pointer fork_router;
    /*  The number of bytes in a single token. */
    int token_size;
    
    /* router port array*/
    welt_fork_router_port_pointer *ports;
    /* input_ports_count*/
    int input_ports_count;
    /* output_ports_count*/
    int output_ports_count;
    /* port counts*/
    int ports_count;
    /* index of the router in a dataflow graph. It is flexible and set by the
     designer*/
    int index;
    
    void *buffer_start;
    void *buffer_end;
    /* methods in router*/
    welt_fork_router_read_ftype read;
    welt_fork_router_write_ftype write;
    welt_fork_router_get_fork_router_port_ftype get_port;
    
    
};

struct _welt_fork_router_port_struct {
    
    
    /* label for input or output port group*/
    int group_label;
    /* index in the group*/
    int group_idx;
    /* index in the router */
    int index;
    /* Port's current "population" for every port*/
    int population;
    /* Router's current "capacity" for every port*/
    int capacity;
    
    int token_size;
    /* Router context */
    welt_fork_router_context_pointer  fork_router_context;
    
    /* Get context*/
    //welt_fork_router_port_get_fork_router_context_ftype get_context;
    welt_fork_router_port_population_ftype pop;
   
    
};

welt_fork_router_port_pointer welt_fork_router_port_new(
        welt_fork_router_context_pointer fork_router,
        int group_label, int group_idx, int index){
    
    welt_fork_router_port_type *port = NULL;

    port = welt_c_util_malloc(sizeof(welt_fork_router_port_type));
    port->group_label = group_label;
    port->group_idx = group_idx;
    port->population = 0;
    port->capacity = fork_router->capacity;
    port->index = index + fork_router->input_ports_count * port->group_label +
            port->group_idx;
    port->fork_router_context = fork_router;
  
    port-> pop = welt_fork_router_port_population;
   
    return port;
    
}

welt_fork_router_context_pointer welt_fork_router_context_new(int capacity,
        int token_size, int input_ports_count, int output_ports_count,
        int index){
    
    int i;
    int tmp;
    
    welt_fork_router_context_pointer context = NULL;

    context = (welt_fork_router_context_pointer)welt_c_util_malloc(
            sizeof(welt_fork_router_context_type));
//context->population = 0;
    context->capacity = capacity;
    context->token_size = token_size;
    context->input_ports_count = input_ports_count;
    context->output_ports_count = output_ports_count;
    context->ports_count = context->input_ports_count + 
                            context->output_ports_count;
    context->index = index;
    
    /* Memory initialization*/
    context->buffer_start = NULL;
    context->buffer_start = welt_c_util_malloc((size_t)(context->capacity *
                                                context->token_size));
    if (context->buffer_start == NULL){
        free(context);
        return NULL;
    }
    context->buffer_end = ((char*)context->buffer_start) + 
            ((context->capacity - 1) * context->token_size);
    
    /*write_pointer initialization*/
    context->write_pointer = NULL;
    
    /* all ports would read data in the router*/
    context->write_pointer = welt_c_util_malloc(sizeof(void *) *
            context->input_ports_count);
    
    /*Read_pointer initialization*/
    context->read_pointer = NULL;
    /* all ports would read data in the router*/
    context->read_pointer = welt_c_util_malloc(sizeof(void *) *
            context->output_ports_count);
    
    /*port initialization*/
    context->ports = NULL;
    context->ports = welt_c_util_malloc(sizeof(welt_fork_router_port_pointer)*
                                        context->ports_count);
    /* input port initialization*/
    for (i = 0; i < context->input_ports_count; i++){
        context->ports[i] = welt_fork_router_port_new(context, INPUT_PORT, i,
                                                context->index);
    }
    /* output port initialization*/
    for (i = context->input_ports_count; i < context->ports_count; i++){
        tmp = i - context->input_ports_count;
        context->ports[i] = welt_fork_router_port_new(context,OUTPUT_PORT,tmp,
                                                context->index);
        
    }

    for (i = 0;i<context->input_ports_count; i++){
        context->write_pointer[i] = context->buffer_start;
    }
    for (i = 0; i<context->ports_count - context->input_ports_count; i++){
        context->read_pointer[i] = context->buffer_start;
    }
   
    /* methods */
   // context->read = welt_fork_router_read;
   // context->write = welt_fork_router_write;
    //context->get_port_withidx = welt_router_port_get_router_port_pointer;
   // context->get_port = welt_router_get_port;
  //  context->get_context = welt_router_port_get_router_context;
    
   
    return context;
}

int welt_fork_router_port_capacity(welt_fork_router_port_pointer port) {
    return port->capacity;
}

int welt_fork_router_port_token_size(welt_fork_router_port_pointer port) {
    return port->token_size;
}

void welt_fork_router_read(welt_fork_router_port_pointer port, void *data){
    
    welt_fork_router_context_pointer fork_router_context =
            port->fork_router_context;
    int index = port->index - fork_router_context->input_ports_count;
    printf("\n read index:%d\n ",index);
    /* Get context of the router */
    
    memcpy(data, fork_router_context->read_pointer[index],
            fork_router_context->token_size);

    fork_router_context->read_pointer[index]=
            (char*)(fork_router_context->read_pointer[index])+
            fork_router_context->token_size;
    if (fork_router_context->read_pointer[index] >
            fork_router_context->buffer_end){
        fork_router_context->read_pointer[index] =
                (fork_router_context->buffer_start);
    }
    
    port->population--;
    return;
}

void welt_fork_router_write(welt_fork_router_port_pointer port, void *data){
    
    int i;
    welt_fork_router_context_pointer fork_router_context =
            port->fork_router_context;

    memcpy( fork_router_context->write_pointer[0], data,
                fork_router_context->token_size);
    
    fork_router_context->write_pointer[0] =
            (char*)(fork_router_context->write_pointer[0]) +
            fork_router_context->token_size ;
    
    if (fork_router_context->write_pointer[0]>fork_router_context->buffer_end){
        fork_router_context->write_pointer[0]=fork_router_context->buffer_start;
    }
    
    for (i=0; i< fork_router_context->output_ports_count;i++){
        fork_router_context->ports[fork_router_context->input_ports_count+i]
                ->population++;
    }
    
    return;
}


welt_fork_router_port_pointer welt_fork_router_get_port(
                welt_fork_router_context_pointer fork_router,
                        int group_label, int group_idx){
    int port_index;
    port_index = fork_router->input_ports_count * group_label + group_idx;
    return fork_router->ports[port_index];
}

int welt_fork_router_port_population(welt_fork_router_port_pointer port){
    welt_fork_router_context_pointer fork_router_context =
            port->fork_router_context;
    int index = port->index;
    return fork_router_context->ports[index]->population;
}

welt_fork_router_context_pointer
        welt_fork_router_port_get_fork_router_context(
        welt_fork_router_port_pointer port){
    
    return port->fork_router_context;
}

welt_fork_router_port_pointer
        welt_fork_router_port_get_fork_router_port_pointer(
        welt_fork_router_context_pointer fork_router, int index){
            
    if(index < fork_router-> ports_count && index >= 0){
        return fork_router->ports[index];
    }else{
        fprintf(stderr, "port index is out of range.\n");
        return NULL;
    }
}

int welt_fork_router_port_get_index(welt_fork_router_port_pointer port){
    return port->index;
}

int welt_fork_router_port_freespace(welt_fork_router_port_pointer port){
    welt_fork_router_context_pointer
            fork_router_context = port->fork_router_context;
    if (port->pop(fork_router_context->
            ports[fork_router_context->input_ports_count+0]) >
            port->pop(fork_router_context->
            ports[fork_router_context->input_ports_count+1])){
        return (fork_router_context->capacity) -
                (port->pop(fork_router_context->
                ports[fork_router_context->input_ports_count+0]));
    }
    else{
        return (fork_router_context->capacity) -
                (port->pop(fork_router_context->
                ports[fork_router_context->input_ports_count+1]));
    }
    
}
