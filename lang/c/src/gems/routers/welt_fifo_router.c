/******************************************************************************
 @ddblock_begin copyright
 
 Copyright (c) 1997-2017
 Maryland DSPCAD Research Group, The University of Maryland at College Park
 
 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.
 
 IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
 THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 
 THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
 ENHANCEMENTS, OR MODIFICATIONS.
 
 @ddblock_end copyright
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "welt_c_util.h"
#include "welt_fifo_router.h"

struct _welt_fifo_router_context_struct {
    /* The number of tokens that all the buffers can hold. */
    int capacity;
    
    welt_router_buffer_pointer buf;
    
    /*  The number of bytes in a single token. */
    int token_size;
    
    /* router port array*/
    welt_fifo_router_port_pointer *ports;
    
    /* input_ports_count*/
    int input_ports_count;
    
    /* output_ports_count*/
    int output_ports_count;
    
    /* port counts*/
    int ports_count;
    
    /* index of the router in a dataflow graph. It is flexible and set by the
     designer*/
    int index;
    
    /* methods in router*/
    welt_fifo_router_read_ftype read;
    
   // welt_fifo_router_read_block_ftype read_block;
    welt_fifo_router_write_ftype write;
  //  welt_fifo_router_free_ftype free;
    welt_fifo_router_get_fifo_router_port_ftype get_port;
    
};

struct _welt_fifo_router_port_struct {
    
    
    /* label for input or output port group*/
    int group_label;
    /* index in the group*/
    int group_idx;
    /* index in the router */
    int index;
    /* Port's current "population" for every port*/
    int population;
    /* Router's current "capacity" for every port*/
    int capacity;
    
    /* Router context */
    welt_fifo_router_context_pointer  fifo_router_context;
    
    /* Get context*/
    welt_fifo_router_port_get_fifo_router_context_ftype get_context;
    welt_fifo_router_port_population_ftype pop;
    welt_fifo_router_port_get_index_ftype get_port_index;
    welt_fifo_router_port_freespace_ftype fsp;
    
};



/*****************************************************************************
 Public functions.
 *****************************************************************************/

/* fifo_router port construct function */
/*  input port label:INPUT_PORT --> 0;
 output port label: OUTPUT_PORT --> 1 */

welt_fifo_router_port_pointer welt_fifo_router_port_new(
        welt_fifo_router_context_pointer fifo_router,
        int group_label, int group_idx, int index){
    
    welt_fifo_router_port_type *port = NULL;
    
    port = welt_c_util_malloc(sizeof(welt_fifo_router_port_type));
    port->group_label = group_label;
    port->group_idx = group_idx;
    port->population = 0;
    port->capacity = fifo_router->capacity;
    port->index = index + fifo_router->input_ports_count * port->group_label +
    port->group_idx;
    port->fifo_router_context = fifo_router;
    port->get_context = welt_fifo_router_port_get_fifo_router_context;
    port->pop = welt_fifo_router_port_population;
    port->get_port_index = welt_fifo_router_port_get_index;
    port->fsp = welt_fifo_router_freespace;
    
    return port;
}

welt_fifo_router_context_pointer welt_fifo_router_context_new(
        int capacity, int token_size,int input_ports_count,
        int output_ports_count, int index){
    
    int i;
    int tmp;
    
    welt_fifo_router_context_pointer context = NULL;
   
    if (capacity < 1) {
        return NULL;
    }
    if (token_size <= 0) {
        return NULL;
    }
    
    context = (welt_fifo_router_context_pointer)welt_c_util_malloc(
            sizeof(welt_fifo_router_context_type));
   
    context->capacity = capacity;
    context->token_size = token_size;
    context->input_ports_count = input_ports_count;
    context->output_ports_count = output_ports_count;
    context->ports_count = context->input_ports_count +
    context->output_ports_count;
    context->index = index;
    
    /*port initialization*/
    context->ports = NULL;
    context->ports = welt_c_util_malloc(sizeof
            (welt_fifo_router_port_pointer)* context->ports_count);

    /* input port initialization*/
   
    i = 0;
    context->ports[i] = welt_fifo_router_port_new(context, 0, i,
            context->index);
 
    for (i = 1; i < context->input_ports_count; i++){
        context->ports[i] = welt_fifo_router_port_new(context, 0,
                                                          i, context->index);
       
    }
 
    /* output port initialization*/
    for (i = context->input_ports_count; i < context->ports_count; i++){
        tmp = i - context->input_ports_count;
        context->ports[i] = welt_fifo_router_port_new(context,
                    1, tmp, context->index);
       
    }

 
    context->buf = welt_router_buffer_new ( context->capacity,
            context-> token_size);

    /* methods */
    context->read = (welt_fifo_router_read_ftype) welt_fifo_router_read;
    context->write = (welt_fifo_router_write_ftype) welt_fifo_router_write;
    
    return context;
}


void welt_fifo_router_read
        (welt_fifo_router_context_pointer fifo_router,
        welt_fifo_router_port_pointer port, void *d){
    welt_router_bufread(fifo_router->buf,d);
    return;
}

void welt_fifo_router_read_block
(welt_fifo_router_context_pointer fifo_router,
 welt_fifo_router_port_pointer port, int num, void *d){
    welt_router_bufread_block(fifo_router->buf, num, d);
    return;
}


void welt_fifo_router_write( welt_fifo_router_context_pointer
        fifo_router, welt_fifo_router_port_pointer port, void *d){
    welt_router_bufwrite(fifo_router->buf, d);
    return;
}

void welt_fifo_router_write_block( welt_fifo_router_context_pointer
        fifo_router, welt_fifo_router_port_pointer port, int num, void *d){
    welt_router_bufwrite_block(fifo_router->buf, num, d);
    return;
}

welt_fifo_router_port_pointer welt_fifo_router_get_port(
            welt_fifo_router_context_pointer fifo_router,
            int group_label, int group_idx){
    
    int port_index;
    port_index = fifo_router->input_ports_count * group_label + group_idx;
    return fifo_router->ports[port_index];
}


int welt_fifo_router_port_population(welt_fifo_router_port_pointer port)
{
    return port->population;
}

welt_fifo_router_context_pointer
welt_fifo_router_port_get_fifo_router_context(
        welt_fifo_router_port_pointer port){
    
    return port->fifo_router_context;
}

welt_fifo_router_port_pointer
welt_fifo_router_port_get_fifo_router_port_pointer(
        welt_fifo_router_context_pointer fifo_router, int index){
    if(index < fifo_router -> ports_count && index >= 0){
        return fifo_router->ports[index];
    }else{
        fprintf(stderr, "port index is out of range.\n");
        return NULL;
    }
    
}

int welt_fifo_router_port_get_index(welt_fifo_router_port_pointer port){
    return port->index;
}

int welt_fifo_router_freespace(welt_fifo_router_port_pointer port){
  
    return port->fifo_router_context->buf->capacity -
            port->fifo_router_context->buf->population;
  
}

