/******************************************************************************
 @ddblock_begin copyright
 
 Copyright (c) 1997-2017
 Maryland DSPCAD Research Group, The University of Maryland at College Park
 
 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.
 
 IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
 THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 
 THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
 ENHANCEMENTS, OR MODIFICATIONS.
 
 @ddblock_end copyright
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "welt_c_util.h"
#include "welt_buffer.h"


welt_router_buffer_pointer welt_router_buffer_new (int capacity,
        int token_size){
    welt_router_buffer_type *buffer = NULL;
    buffer =(welt_router_buffer_pointer)
    welt_c_util_malloc(sizeof(welt_router_buffer_type));
    void * buffer_space;
    buffer_space = welt_c_util_malloc( token_size * capacity );
   //buffer-> port = port ;
    buffer -> buffer_start= buffer_space;
    buffer-> capacity = capacity;
    buffer-> buffer_size = capacity/token_size;
    buffer-> token_size= token_size ;
    buffer -> buffer_end = ((char*)buffer->buffer_start) +
    (buffer->capacity - 1)*(buffer->token_size);
    buffer -> read_pointer = buffer -> buffer_start ;
    buffer -> write_pointer = buffer -> buffer_start ;
    
    return buffer;
}


/* buffer write function :
it's the client's responsibility to ensure that the buffer has space for at
least one token. */
void welt_router_bufwrite( welt_router_buffer_pointer buffer,
        void* d ){
    
    /* Circular buffer */
    if (buffer->write_pointer > buffer->buffer_end) {
        buffer->write_pointer = buffer->buffer_start;
    }
    
    memcpy(buffer-> write_pointer, d, buffer->token_size);
    buffer->population ++;
    buffer-> write_pointer =(char*) buffer-> write_pointer + buffer->token_size;

    return;
}

void welt_router_bufwrite_block( welt_router_buffer_pointer buffer, int num,
                            void* d ){
    int size=num;
    /* Circular buffer */
    if (buffer->write_pointer > buffer->buffer_end) {
        buffer->write_pointer = buffer->buffer_start;
    }
    
    memcpy(buffer-> write_pointer, d, buffer->token_size * size);
    buffer->population = buffer->population + size;
    buffer-> write_pointer =(char*) buffer-> write_pointer +
            buffer->token_size* size;
    
    return;
}

/* buffer read function :
 it's the client's responsibility to ensure that the buffer at
 least one token. */

void welt_router_bufread( welt_router_buffer_pointer buffer,
        void * d){
   
    /* Circular buffer */
    
    if (buffer->read_pointer > buffer->buffer_end) {
        buffer->read_pointer = buffer->buffer_start;
    }
    memcpy( d, buffer-> read_pointer, buffer->token_size);
    buffer->population --;
    buffer->read_pointer =  (char*)buffer-> read_pointer + buffer->token_size ;
   
    return;
}

void welt_router_bufread_block( welt_router_buffer_pointer buffer, int num,
                           void * d){
    int size=num;
    /* Circular buffer */
    if (buffer->read_pointer > buffer->buffer_end) {
        buffer->read_pointer = buffer->buffer_start;
    }
    memcpy( d, buffer-> read_pointer, buffer->token_size*size);
    buffer->population = buffer->population - size;
    buffer->read_pointer =  (char*)buffer-> read_pointer +
            buffer->token_size*size ;
    
    return;
}


void * welt_router_bufpeek(welt_router_buffer_pointer buffer){
    if (buffer->population == 0) {
        return NULL;
    } else {
        return buffer->read_pointer;
    }
    
}

void welt_router_bufadvance
        (welt_router_buffer_pointer buffer){
            
    /* Circular buffer */
    if (buffer->read_pointer > buffer->buffer_end) {
        buffer->read_pointer = buffer->buffer_start;
    }
    buffer->population --;
    buffer->read_pointer =  (char*)buffer-> read_pointer + buffer->token_size ;
      
    return;
    
}


