/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2017
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/* Common elements across context type of routers. */

/* The number of tokens that the buffer can hold. */
int capacity;

/* Pointer to the first token placeholider in the buffer. */
void *buffer_start;

/* Pointer to the last token placeholider in the buffer. This member
   is here for convenience (it can also be derived from the 
   buffer start, capacity, and token_size). 
*/
void *buffer_end;

/* The number of tokens that are currently in the router. */
int population;

/* Pointer to the next token placeholider to be read from. */
void **read_pointer;

/* Pointer to the next token placeholider to be written into. */
void *write_pointer;

/* The number of bytes in a single token. */
int token_size;

/* router port array*/
welt_router_port_pointer *ports;

/* input_ports_count*/
int input_ports_count;

/* output_ports_count*/
int output_ports_count;

/* port counts*/
int ports_count;

/* index of the router in a dataflow graph. It is flexible and set by the 
designer*/
int index;

/* methods in router*/
welt_router_read_ftype read;
welt_router_peek_ftype peek;
welt_router_read_advance_ftype read_advance;
welt_router_read_block_ftype read_block;
welt_router_write_ftype write;
//welt_router_write_advance_ftype write_advance;
welt_router_write_block_ftype write_block;

welt_router_context_population_ftype get_cpop;
welt_router_port_population_ftype get_ppop;
welt_router_population_ftype get_pop;

welt_router_capacity_ftype get_cap;
welt_router_context_capacity_ftype get_ccap;
welt_router_port_capacity_ftype get_pcap;

welt_router_token_size_ftype  get_token_size;
welt_router_reset_ftype reset;
welt_router_free_ftype free;


welt_router_get_router_port_with_index_ftype get_port_withidx;
welt_router_get_router_port_ftype get_port;


/* The token printing function (if any) associated with this router. */
welt_router_f_token_printing print; 
