#ifndef _welt_buffer_h
#define _welt_buffer_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2017
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include "welt_basic.h"
#include "welt_router.h"
#include "welt_c_fifo.h"
#include "welt_c_actor.h"
#include <stdbool.h>

typedef struct _welt_router_buffer_struct welt_router_buffer_type;

struct _welt_router_buffer_struct {
    
    void *buffer_start;
    void *buffer_end ;   //points to the last byte in the buffer
    void *read_pointer ;
    void *write_pointer ;
    int capacity ;
    int token_size ;
    int population ;
    int buffer_size;
   // welt_router_port_pointer port;
    
};


typedef welt_router_buffer_type *welt_router_buffer_pointer;

typedef int (*welt_router_port_get_index_ftype)
            (welt_router_port_pointer port);
     
typedef void (*welt_router_bufwrite_ftype)
(welt_router_buffer_pointer buffer, void *d);

typedef void (*welt_router_bufread_ftype)
(welt_router_buffer_pointer buffer, void *d);

typedef void (*welt_router_bufpeek_ftype)
(welt_router_buffer_pointer buffer);

welt_router_buffer_pointer welt_router_buffer_new
( int capacity, int token_size);

void * welt_router_bufpeek( welt_router_buffer_pointer buffer);
void welt_router_bufread( welt_router_buffer_pointer buffer , void * d);
void welt_router_bufread_block( welt_router_buffer_pointer buffer,int size,
        void * d);
void welt_router_bufwrite( welt_router_buffer_pointer buffer, void * d);
void welt_router_bufwrite_block( welt_router_buffer_pointer buffer,
        int size, void * d);
void welt_router_bufadvance (welt_router_buffer_pointer buffer);

#endif
