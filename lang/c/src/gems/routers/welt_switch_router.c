/******************************************************************************
 @ddblock_begin copyright
 
 Copyright (c) 1997-2017
 Maryland DSPCAD Research Group, The University of Maryland at College Park
 
 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the above
 copyright notice and the following two paragraphs appear in all copies
 of this software.
 
 IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
 THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 
 THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
 PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
 ENHANCEMENTS, OR MODIFICATIONS.
 
 @ddblock_end copyright
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "welt_c_util.h"
#include "welt_switch_router.h"
//#include <welt_buffer.c>

#define POINTER_TRUE    0
#define POINTER_FALSE    1
#define DATA    2

#define CONTROL_PORT 0
#define TRUE_PORT 2
#define FALSE_PORT 3
//#define INPUT_PORT 1
#define IN_PORT 0
#define OUT_PORT 1

struct _welt_switch_router_context_struct {
    /* The number of tokens that all the buffers can hold. */
    int capacity;
    
    welt_router_buffer_pointer outbufs[DATA];
    
    welt_router_buffer_pointer input_buffer;
    
    welt_router_buffer_pointer control_buffer;
    
    /*  The number of bytes in a single token. */
    int token_size;
    
    /* router port array*/
    welt_switch_router_port_pointer *ports;
    
    /* input_ports_count*/
    int input_ports_count;
    
    /* output_ports_count*/
    int output_ports_count;
    
    /* port counts*/
    int ports_count;
    
    /* index of the router in a dataflow graph. It is flexible and set by the
     designer*/
    int index;
    
    /* methods in router*/
    welt_switch_router_read_ftype read;
    
   // welt_switch_router_read_block_ftype read_block;
    welt_switch_router_write_ftype write;
  //  welt_switch_router_free_ftype free;
    welt_switch_router_get_switch_router_port_ftype get_port;
    
};

struct _welt_switch_router_port_struct {
    
    
    /* label for input or output port group*/
    int group_label;
    /* index in the group*/
    int group_idx;
    /* index in the router */
    int index;
    /* Port's current "population" for every port*/
    int population;
    /* Router's current "capacity" for every port*/
    int capacity;
    
    /* Router context */
    welt_switch_router_context_pointer  switch_router_context;
    
    /* Get context*/
    welt_switch_router_port_get_switch_router_context_ftype get_context;
    welt_switch_router_port_population_ftype pop;
    welt_switch_router_port_get_index_ftype get_port_index;
    welt_switch_router_port_freespace_ftype fsp;
    
};



/*****************************************************************************
 Public functions.
 *****************************************************************************/

/* switch_router port construct function */
/*  input port label:INPUT_PORT --> 0;
 output port label: OUTPUT_PORT --> 1 */

welt_switch_router_port_pointer welt_switch_router_port_new(
        welt_switch_router_context_pointer switch_router,
        int group_label, int group_idx, int index){
    
    welt_switch_router_port_type *port = NULL;
    
    port = welt_c_util_malloc(sizeof(welt_switch_router_port_type));
    port->group_label = group_label;
    port->group_idx = group_idx;
    port->population = 0;
    port->capacity = switch_router->capacity;
    port->index = index + switch_router->input_ports_count * port->group_label +
    port->group_idx;
    port->switch_router_context = switch_router;
    port->get_context = welt_switch_router_port_get_switch_router_context;
    port->pop = welt_switch_router_port_population;
    port->get_port_index = welt_switch_router_port_get_index;
    port->fsp = welt_switch_router_freespace;
    
    return port;
}

welt_switch_router_context_pointer welt_switch_router_context_new(
        int capacity, int token_size,int input_ports_count,
        int output_ports_count, int index){
    
    int i;
    int tmp;
    
    welt_switch_router_context_pointer context = NULL;
   
    if (capacity < 1) {
        return NULL;
    }
    if (token_size <= 0) {
        return NULL;
    }
    
    context = (welt_switch_router_context_pointer)welt_c_util_malloc(
            sizeof(welt_switch_router_context_type));
   
    context->capacity = capacity;
    context->token_size = token_size;
    context->input_ports_count = input_ports_count;
    context->output_ports_count = output_ports_count;
    context->ports_count = context->input_ports_count +
    context->output_ports_count;
    context->index = index;
    
    /*port initialization*/
    context->ports = NULL;
    context->ports = welt_c_util_malloc(sizeof
            (welt_switch_router_port_pointer)* context->ports_count);

    /* input port initialization*/
    /*control port*/
    i = 0;
    context->ports[i] = welt_switch_router_port_new(context, 0, i,
            context->index );
 
    for (i = 1; i < context->input_ports_count; i++){
        context->ports[i] = welt_switch_router_port_new(context, 0,
                                                          i, context->index);
       
    }
 
    /* output port initialization*/
    for (i = context->input_ports_count; i < context->ports_count; i++){
        tmp = i - context->input_ports_count;
        context->ports[i] = welt_switch_router_port_new(context,
                    1, tmp, context->index);
       
    }

 
    context->outbufs[POINTER_TRUE] = welt_router_buffer_new ( context->capacity, context-> token_size);

    context->outbufs[POINTER_FALSE] = welt_router_buffer_new ( context->capacity, context-> token_size);

    context->input_buffer = welt_router_buffer_new (context->capacity, context-> token_size);
    
    context->control_buffer = welt_router_buffer_new ( context->capacity, context-> token_size);
    
    
    /* methods */
    context->read = (welt_switch_router_read_ftype) welt_switch_router_read;
    
    context->write = (welt_switch_router_write_ftype)welt_switch_router_write;
    

    return context;
}


void welt_switch_router_read
        (welt_switch_router_context_pointer switch_router,
        welt_switch_router_port_pointer port, void *d){
    
    int z = port->group_idx;
          //  printf("\n(SWITCH READ)!!\n ");
          //  printf("\nz:%d!\n ",z);

    /* read from t/fbuffer*/
    
    welt_router_bufread(switch_router->outbufs[z],d);
        
    return;
}


void welt_switch_router_write( welt_switch_router_context_pointer
        switch_router, welt_switch_router_port_pointer port, void *d){
    
    int z = port->group_idx; /* Input index */
    int inval =0 ; //value
    void * cval = NULL; //peek
    void * dval = NULL;
  //  printf("\n(SWITCH WRITE)!!\n ");
    if (z!=0){    //when it is not a control
      //  printf("\n Input port in \n");
        cval = welt_router_bufpeek(switch_router->control_buffer);
        //see if there is available control token
      
        if ( cval  == NULL ){
            /* if there is no control token available, store it to input buffer */
            welt_router_bufwrite( switch_router->input_buffer , d);
          //  printf("\n no control available , write %d to input buffer.\n",*(int*)d);
            
        }else{
           /* control available, so move pointer in control buffer and write
            the input into t/f buffer*/
            welt_router_bufadvance(switch_router->control_buffer);
            welt_router_bufwrite(switch_router-> outbufs[*(int*)cval], d);
           // printf("\n control available , write %d to %d buffer.\n", *(int*)d,*(int*)cval);
        }
        
    }else{              /*when it is a control*/
       // printf("\n This is a control for switch.\n");
        dval=welt_router_bufpeek(switch_router->input_buffer);
        /* see if there is available input token. */
        if (dval == NULL){
           // printf("\n no input token available in switch.\n");
            /*if there is no input token available, write it into cbuf*/
            welt_router_bufwrite(switch_router->control_buffer, d);
        }
        else{
          //  printf("\n input token available in switch.\n");
           // welt_router_bufadvance(switch_router->input_buffer);
          //  printf("\n dval:%d value, inval:%d buffer\n",*(int*)dval, *(int*)d);
            /*if there was a input token, write to one of out buffer*/
          //  welt_router_bufadvance(switch_router->input_buffer);
       
            welt_router_bufwrite(switch_router-> outbufs[*(int*)d], dval);
          //  welt_router_bufadvance(switch_router->input_buffer);
        }
    }
    return;
}

welt_switch_router_port_pointer welt_switch_router_get_port(
            welt_switch_router_context_pointer switch_router,
            int group_label, int group_idx){
    
    int port_index;
    port_index = switch_router->input_ports_count * group_label + group_idx;
    return switch_router->ports[port_index];
}


int welt_switch_router_port_population(welt_switch_router_port_pointer port)
{
    return port->population;
}

welt_switch_router_context_pointer
welt_switch_router_port_get_switch_router_context(
        welt_switch_router_port_pointer port){
    
    return port->switch_router_context;
}

welt_switch_router_port_pointer
welt_switch_router_port_get_switch_router_port_pointer(
        welt_switch_router_context_pointer switch_router, int index){
    if(index < switch_router -> ports_count && index >= 0){
        return switch_router->ports[index];
    }else{
        fprintf(stderr, "port index is out of range.\n");
        return NULL;
    }
    
}

/*int welt_switch_router_population
(welt_switch_router_context_pointer switch_router){
    
    int z= 0;
    z = port->group_idx;
    return switch_router->data[z]->population ;
    
}*/
/*int welt_switch_router_port_population
(welt_switch_router_port_pointer port){
    
    int z= 0;
    z = port->group_idx; 
    return switch_router->data[z-2]->population ;
    
}*/


int welt_switch_router_port_get_index(welt_switch_router_port_pointer port){
    return port->index;
}

int welt_switch_router_freespace(welt_switch_router_port_pointer port){
  
    return port->switch_router_context->input_buffer->capacity -
            port->switch_router_context->input_buffer->population;
  
}

