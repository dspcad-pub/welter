#ifndef _welt_fork_router_h
#define _welt_fork_router_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2017
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include "welt_basic.h"
#include "welt_c_fifo.h"
#include "welt_c_actor.h"

//#include "welt_router.h"
#define INPUT_PORT      0
#define OUTPUT_PORT     1


/*****************************************************************************
Each data item in the router is referred to as a "token". Tokens can have
arbitrary types associated with them. For a given router instance, there
is a fixed token size (number of bytes per token). Tokens have arbitrary
types --- e.g., they can be integers, floating point values (float or
double), characters, or pointers (to any kind of data). This organization
allows for flexibility in storing different kinds of data values, 
and efficiency in storing the data values directly (without being
encapsulated in any sort of higher-level "token" object).

actor-->input port--->|_|_|_|_|_|_|-->output port-->actor

*****************************************************************************/

/*****************************************************************************
* Router as a connector
*****************************************************************************/
// Router context
typedef struct _welt_fork_router_context_struct welt_fork_router_context_type;
// A pointer to a router context.
typedef welt_fork_router_context_type *welt_fork_router_context_pointer;

// Router port
typedef struct _welt_fork_router_port_struct welt_fork_router_port_type;

// A pointer to a router port.
typedef welt_fork_router_port_type *welt_fork_router_port_pointer;


/*****************************************************************************
A pointer to a "welt_router_read", which is a function that reads
data from router.
*****************************************************************************/
typedef void (*welt_fork_router_read_ftype)
        (welt_fork_router_port_pointer port, void *data);

/*****************************************************************************
A pointer to a "welt_router_write_function", which is a function that writes
data to the router
*****************************************************************************/
typedef void (*welt_fork_router_write_ftype)
        (welt_fork_router_port_pointer port, void *data);


/*****************************************************************************
A pointer to a "welt_router_port_population", which is a function that
returns the number of tokens available that are currently for the port in the 
router.
*****************************************************************************/
typedef int (*welt_fork_router_port_population_ftype)
        (welt_fork_router_port_pointer port);
        
/*****************************************************************************
A pointer to a "welt_router_capacity", which is a function that gets the
capacity of the router
*****************************************************************************/
typedef int (*welt_fork_router_port_capacity_ftype)
        (welt_fork_router_port_pointer port);
        
/*****************************************************************************
A pointer to a "welt_router_context_capacity", which is a function that gets the
capacity of the router
*****************************************************************************/
typedef int (*welt_fork_router_port_capacity_ftype)
        (welt_fork_router_port_pointer port);
 
/*****************************************************************************
A pointer to a "welt_router_token_size", which is a function that gets the
token size
*****************************************************************************/
typedef int (*welt_fork_router_port_token_size_ftype)
         (welt_fork_router_port_pointer port);

/*****************************************************************************
A pointer to a "welt_router_get_index", which is a function that get index of
router in the dataflow graph
*****************************************************************************/
typedef int (*welt_fork_router_get_index_ftype)
        (welt_fork_router_context_pointer router);
typedef int (*welt_fork_router_port_freespace_ftype)
(welt_fork_router_port_pointer port);

/*****************************************************************************
A pointer to a "welt_router_get_port", which is a function that get
port in the router in the dataflow graph given group index and group label
*****************************************************************************/
typedef welt_fork_router_port_pointer (*welt_fork_router_get_fork_router_port_ftype)
        (welt_fork_router_context_pointer fork_router, int group_label,
        int group_idx);

/*****************************************************************************
A pointer to a "welt_router_get_port_pointer", which is a function that get
index of router in the dataflow graph
*****************************************************************************/      
typedef welt_fork_router_context_pointer (*
        welt_fork_router_port_get_fork_router_context_ftype)
        (welt_fork_router_port_pointer port);

/*****************************************************************************
A pointer to a "welt_router_port_get_index", which is a function that get
index of port in the dataflow graph 
*****************************************************************************/
typedef int (*welt_fork_router_port_get_index_ftype)
            (welt_fork_router_port_pointer port);

// Function declaration
/*****************************************************************************
Create router port pointer 
*****************************************************************************/
welt_fork_router_port_pointer welt_fork_router_port_new(
                                                            welt_fork_router_context_pointer fork_router,
                                                            int group_label, int group_idx, int index);

/*****************************************************************************
Create router context pointer 
*****************************************************************************/
welt_fork_router_context_pointer welt_fork_router_context_new(int capacity,
                                                                  int token_size, int input_ports_count, int output_ports_count,
                                                                  int index);
/*****************************************************************************
Read method
*****************************************************************************/
void welt_fork_router_read(welt_fork_router_port_pointer port, void *data);

/*****************************************************************************
Read method
*****************************************************************************/
void welt_fork_router_read(welt_fork_router_port_pointer port, void *data);

/*****************************************************************************
Write method
*****************************************************************************/
void welt_fork_router_write(welt_fork_router_port_pointer port, void *data);

/*****************************************************************************
Return the number of tokens that are currently in the router.
*****************************************************************************/
int welt_fork_router_port_population(welt_fork_router_port_pointer port);

/*****************************************************************************
Return the capacity of the router.
*****************************************************************************/
int welt_fork_router_port_capacity(welt_fork_router_port_pointer port);

/*****************************************************************************
Return the number of bytes that are used to store a single token in the router.
*****************************************************************************/
int welt_fork_router_token_size(welt_fork_router_context_pointer router);

/*****************************************************************************
Deallocate the storage associated with the given router.
*****************************************************************************/
welt_fork_router_port_pointer welt_fork_router_get_port(
                        welt_fork_router_context_pointer router,
                        int group_label, int group_idx);

/*****************************************************************************
Get router context pointer associated with the router port given
*****************************************************************************/
welt_fork_router_context_pointer welt_fork_router_port_get_fork_router_context(
                                            welt_fork_router_port_pointer port);

/*****************************************************************************
Get router port pointer associated with the router port given
*****************************************************************************/
welt_fork_router_port_pointer welt_fork_router_port_get_fork_router_port_pointer(
                                        welt_fork_router_context_pointer fork_router,
                                        int index);        
                 
/*****************************************************************************
Set index of the router port
*****************************************************************************/
int welt_fork_router_port_get_index(welt_fork_router_port_pointer port);

/*****************************************************************************
* Router as an actor
*****************************************************************************/



#endif
