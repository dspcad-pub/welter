/******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2017
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "welt_c_util.h"
#include "welt_select_router.h"
#include <math.h>

#define min(a, b) ((a) < (b)) ? (a) : (b)

#define POINTER_TRUE    0
#define POINTER_FALSE    1
#define DATA    2

#define SELECT_INPUT_PORT 0
#define SELECT_CONTROL_PORT 0
#define SELECT_TRUE_PORT 1
#define SELECT_FALSE_PORT 2
#define SELECT_OUTPUT_PORT 3


struct _welt_select_router_context_struct {
    /* The number of tokens that all the buffers can hold. */
    int capacity;
  
    welt_router_buffer_pointer data[DATA];
    
    welt_router_buffer_pointer output_buffer;
    
    welt_router_buffer_pointer control_buffer;

    /*  The number of bytes in a single token. */
    int token_size;

    /* router port array*/
    welt_select_router_port_pointer *ports;

    /* input_ports_count*/
    int input_ports_count;

    /* output_ports_count*/
    int output_ports_count;

    /* port counts*/
    int ports_count;

    /* index of the router in a dataflow graph. It is flexible and set by the 
    designer*/
    int index;
    
    /* methods in router*/
    welt_select_router_read_ftype read;

    welt_select_router_read_block_ftype read_block;
    welt_select_router_write_ftype write;
    welt_select_router_free_ftype free;
    welt_select_router_get_select_router_port_ftype get_port;

};

struct _welt_select_router_port_struct {
 
    /* label for input or output port group*/
    int group_label;    
    /* index in the group*/
    int group_idx;    
    /* index in the router */    
    int index;   
    /* Port's current "population" for every port*/    
    int population;
    /* Router's current "capacity" for every port*/    
    int capacity;   
    
    /* Router context */
    welt_select_router_context_pointer  select_router_context;
    
    /* Get context*/
    welt_select_router_port_get_select_router_context_ftype get_context;
    welt_select_router_population_ftype pop;
    welt_select_router_port_get_index_ftype get_port_index;
    welt_select_router_port_freespace_ftype fsp;
    
};



/*****************************************************************************
Public functions.
*****************************************************************************/

/* select_router port construct function */
/*  input port label:INPUT_PORT --> 0; 
    output port label: OUTPUT_PORT --> 1 */
welt_select_router_port_pointer welt_select_router_port_new(
                        welt_select_router_context_pointer select_router,
                        int group_label, int group_idx, 
                        int index){
    welt_select_router_port_type *port = NULL;

    port = welt_c_util_malloc(sizeof(welt_select_router_port_type));
    port->group_label = group_label;
    port->group_idx = group_idx;
    port->population = 0;
    port->capacity = select_router->capacity;
    port->index = index + select_router->input_ports_count * port->group_label + 
                    port->group_idx;
    port->select_router_context = select_router;
    port->get_context = welt_select_router_port_get_select_router_context;
    //port->pop = welt_select_router_port_population;
    port->get_port_index = welt_select_router_port_get_index;
    port->fsp = welt_select_router_freespace;

    return port;
}

welt_select_router_context_pointer welt_select_router_context_new(
        int capacity, int token_size,int input_ports_count,
        int output_ports_count, int index){
    int i;
    int tmp;
    
    welt_select_router_context_pointer context = NULL;
    
    if (capacity < 1) {
        return NULL;
    }
    if (token_size <= 0) {
        return NULL;
    }

    context = (welt_select_router_context_pointer)welt_c_util_malloc(
            sizeof(welt_select_router_context_type));
    
    context->capacity = capacity;
    
    context->token_size = token_size;
    
    context->input_ports_count = input_ports_count;
    context->output_ports_count = output_ports_count;
    context->ports_count = context->input_ports_count + 
            context->output_ports_count;
    context->index = index;
    
    /*port initialization*/
    context->ports = NULL;
    context->ports = welt_c_util_malloc(sizeof
            (welt_select_router_port_pointer)* context->ports_count);
    
    /* input port initialization*/
    /* control port */
    i = 0;
    context->ports[i] = welt_select_router_port_new(context, SELECT_INPUT_PORT, i,
            context->index );
    
    for (i = 1; i < context->input_ports_count; i++){
        context->ports[i] = welt_select_router_port_new(context, SELECT_INPUT_PORT + i,
                i, context->index);
    }
    
    /* output port initialization*/
    for (i = context->input_ports_count; i < context->ports_count; i++){
        tmp = i - context->input_ports_count;
        context->ports[i] = welt_select_router_port_new(context,
                                                        SELECT_OUTPUT_PORT, tmp, context->index);
    }
    
    
    context->data[POINTER_TRUE] = welt_router_buffer_new (
            context->capacity, context-> token_size);
    
    context->data[POINTER_FALSE] = welt_router_buffer_new (
            context->capacity, context-> token_size);

    context->output_buffer = welt_router_buffer_new (
            context->capacity, context-> token_size);
    
  
    context->control_buffer = welt_router_buffer_new (
            context->capacity, context-> token_size);
    
    /* methods */

    context->read = (welt_select_router_read_ftype)welt_select_router_read;

    context->write = (welt_select_router_write_ftype)welt_select_router_write;
 
    return context;
}


void welt_select_router_read(welt_select_router_context_pointer select_router ,welt_select_router_port_pointer port,
        void *d){
   // printf("\n (SELECT READ)\n");
    welt_router_bufread(select_router->output_buffer, d);
 
}

void process_control_tokens(welt_select_router_context_pointer select_router){
    void * v;
    void * w;
    void * z;
    void * d;
    //printf("\n Processing control tokens\n");
    /*Check if there is available control token*/
    v = welt_router_bufpeek(select_router->control_buffer);
    //printf("\n Processing control tokens :  control\n");
    if ( v != NULL){
        /*if there is available control token, see if there is available token
         in t/f buffer*/
        //printf("\n Processing control tokens :  v notnull\n");
    w = welt_router_bufpeek(select_router->data[(*(int*)v)]);
        //printf("\n Processing control tokens :peek\n");
    }
    while(( v != NULL) && w!= NULL) {
       /* move pointer in control buffer*/
        //printf("\n Processing control tokens :ad\n");

        welt_router_bufadvance(select_router->control_buffer);
        /* read the data from the t/f buffer*/
        welt_router_bufread(select_router->data[(*(int*)v)], &d);
        //printf("\n Processing control tokens :read\n");
         /* write the data to the output buffer*/
        welt_router_bufwrite(select_router->output_buffer, &d);
        select_router->ports[select_router->input_ports_count]->population++;
        //printf("\n Processing control tokens :write\n");
        /*check next available control token*/
        v = welt_router_bufpeek(select_router->control_buffer);
        if (v != NULL){
            //printf("\n v null \n");
            w = welt_router_bufpeek(select_router->data[(*(int*)v)]);
        }
        
    }
    return;
}


void welt_select_router_write( welt_select_router_context_pointer select_router, welt_select_router_port_pointer port,
        void *d){
   
    int z= 0;

    z = port->group_idx; /* Input index */
    int p1 ; //value
    int * v =NULL; //peek
    int * w = NULL;
    //printf("\n (SELECT WRITE) \n");
    //printf("select_router->output_buffer %p",select_router->output_buffer );
    if (port->group_idx!=0){    //when it is not a control
        z = z - 1;
        v = welt_router_bufpeek(select_router->control_buffer);
        
        if ( v  == NULL ){
            /* if there is no control token available, store it to t/f buffer */
            welt_router_bufwrite( select_router->data[z] , d);
            //printf("\n no control available , written to %d buffer!!\n",z);
            
        }else{
            //printf("\n control token available\n");
            /* control available */
            welt_router_bufread(select_router->control_buffer, &p1);
        
             if ( (z) == (p1) ){
                /* write data to directly to the output buffer */
                welt_router_bufwrite(select_router->output_buffer , d);
                select_router->ports[select_router->input_ports_count]->population++;
                // printf(" pop input_ports_count %d!\n",select_router->ports[select_router->input_ports_count]->population);
                //printf(" control available , written to output buffer from %d!\n",z);
                
             } else{
                /* if there is control token available, but different, put the input in
                 t/f buffer */
                welt_router_bufwrite(select_router-> data[z] , d);
                //printf("\n control is %d , but %d buffer!!\n", (p1) ,z);
            }
            
        }
        
        process_control_tokens(select_router);
        //printf("\n PROCESSING END \n");
        
    }else{
        /* write to control input */
        welt_router_bufwrite(select_router->control_buffer, d);
        //printf("\n control token written to control buffer, value: %d\n", *(int*)d);
      //printf("\n its control signal %p\n", select_router->control_buffer->write_pointer);
    }
    
    process_control_tokens(select_router);
    //printf("\n PROCESSING END2 \n");
    
    return;
    
}

welt_select_router_port_pointer welt_select_router_get_port(
                        welt_select_router_context_pointer select_router,
                        int group_label, int group_idx){
    int port_index;
    port_index = select_router->input_ports_count * group_label + group_idx;
    return select_router->ports[port_index];
}

welt_select_router_context_pointer
        welt_select_router_port_get_select_router_context(
        welt_select_router_port_pointer port){
            
    return port->select_router_context;
}

/* get router port from router context index==0 control, index==1 true port,
 index==2 false port, index==3 output buffer */

welt_select_router_port_pointer
        welt_select_router_port_get_select_router_port_pointer(
        welt_select_router_context_pointer select_router, int index){
    if(index < select_router -> ports_count && index >= 0){
        return select_router->ports[index];
    }else{
        fprintf(stderr, "port index is out of range.\n");
        return NULL;
    }
        
}

int welt_select_router_port_population (
        welt_select_router_port_pointer port){
    welt_select_router_context_pointer select_router;
    select_router = port->select_router_context;
    
            return select_router->output_buffer->population ;

}

int welt_select_router_port_get_index(welt_select_router_port_pointer port){
    return port->index;
}

int welt_select_router_freespace(welt_select_router_port_pointer port){
    
    if (port->group_idx==1){
        if (port->select_router_context->data[SELECT_TRUE_PORT-1]->capacity
            - (port->select_router_context)-> data[SELECT_TRUE_PORT-1]
                    ->population > port->select_router_context->
                output_buffer->population){
            return port->select_router_context->
                    output_buffer->population;
        }else{
            return port->select_router_context->data[SELECT_TRUE_PORT-1]->capacity
                   - (port->select_router_context)-> data[SELECT_TRUE_PORT-1]
                           ->population;
        }
        /*return min(port->select_router_context->data[SELECT_TRUE_PORT]->capacity
                - (port->select_router_context)-> data[SELECT_TRUE_PORT]
                ->population, port->select_router_context->
                output_buffer->population); */
    }else{
        if (((port->select_router_context)->data[SELECT_TRUE_PORT-1])->capacity
            - (port->select_router_context)-> data[SELECT_TRUE_PORT-1]
                    ->population > port->select_router_context->
                output_buffer->population){
            return port->select_router_context->
                    output_buffer->population;
        }else{
            return ((port->select_router_context)->data[SELECT_FALSE_PORT-1])->capacity
                   - (port->select_router_context)-> data[SELECT_FALSE_PORT-1]
                           ->population;
        }
        /*return min(port->select_router_context->data[SELECT_FALSE_PORT]->capacity
                   - (port->select_router_context)-> data[SELECT_FALSE_PORT]
                           ->population, port->select_router_context->
                output_buffer->population);*/
        
    }
    
}
int welt_select_router_capacity (welt_select_router_context_pointer select_router) {
    return select_router->capacity;
}
