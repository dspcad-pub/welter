#ifndef _welt_c_fifo_unit_size_h
#define _welt_c_fifo_unit_size_h

/****************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/
#include <stdio.h>
#include "welt_c_fifo.h"

#define UNIT_CAPACITY 1

/*****************************************************************************
This is a FIFO with that allows for a maximum of one token in the buffer. 
Therefore, the capacity  in this FIFO is one. The restriction to unit capacity 
allows for certain kinds of streamlining that are supported in this FIFO type. 
This FIFO type can be applied, for example, in dataflow schedule  graphs 
(DSGs), which impose a maximum of one token per edge for intra-processor 
subgraphs.  The token size (number of bytes per token)  in this FIFO type is 
fixed. Data of any arbitrary type that conforms to this fixed token size can 
be stored in the FIFO.
*****************************************************************************/
  
/* A FIFO. */
typedef struct _welt_c_fifo_unit_size_struct welt_c_fifo_unit_size_type;

/* A pointer to a fifo_unit_size. */
typedef welt_c_fifo_unit_size_type *welt_c_fifo_unit_size_pointer;

/*****************************************************************************
A pointer to a "token printing function", which is a function that 
prints the data value encapsulated by a single token to a given file.
This type is normally used just for diagnostic purposes.
*****************************************************************************/
typedef void (*welt_c_fifo_unit_size_f_token_printing)(FILE *output,
                                                        void *token);

/* ----------------------------  Public functions --------------------- */

/*****************************************************************************
Display information about the FIFO to the specified file.  The display output
is written to the specified file, which must be open for writing prior to
calling this function.  This function is primarily provided for diagnostic
purposes.
*****************************************************************************/
void welt_c_fifo_unit_size_display_status(
        welt_c_fifo_unit_size_pointer fifo, FILE *output);

/*****************************************************************************
Display the contents of the fifo_unit_size, one byte at time, with each byte 
displayed in hexadecimal format. The bytes that correspond to a given token 
are printed on the same line, and successive tokens in the FIFO (starting 
with the olidest token) are displayed on successive lines. The display output 
is written to the specified file, which must be open for writing prior to 
calling this function. This function is primarily for diagnostic purposes.
*****************************************************************************/
void welt_c_fifo_unit_size_display_contents(
        welt_c_fifo_unit_size_pointer fifo_unit_size, FILE *output);

/*****************************************************************************
Create a new FIFO with one token and the speicified token size.If the 
specified token size is less than or equal to zero,  the function fails 
silently and returns NULL.
*****************************************************************************/
welt_c_fifo_unit_size_pointer welt_c_fifo_unit_size_new(int token_size,
        int index);

/*****************************************************************************
Return the number of tokens that are currently in the fifo_unit_size.
*****************************************************************************/
int welt_c_fifo_unit_size_population(welt_c_fifo_unit_size_pointer fifo);

/*****************************************************************************
Return the capacity of the fifo_unit_size.
*****************************************************************************/
int welt_c_fifo_unit_size_capacity(welt_c_fifo_unit_size_pointer fifo);

/*****************************************************************************
Return the number of bytes that are used to store a single token in the 
fifo_unit_size.
*****************************************************************************/
int welt_c_fifo_unit_size_token_size(welt_c_fifo_unit_size_pointer fifo);

/*****************************************************************************
Insert a new token into the buffer. This function copies N bytes, where N is
the fifo_unit_size token size. If the FIFO is already full, the function 
fails silently(without modifying the FIFO).
*****************************************************************************/
void welt_c_fifo_unit_size_write(
        welt_c_fifo_unit_size_pointer fifo, void *data);

/*****************************************************************************
Update the FIFO population as a FIFO write, but do not write the token to the 
buffer. write_advance function will update (move forward) the write pointer by 
one token. Then update the FIFO population. The funtion will fails silently if 
there is no enough empty space in FIFO(without modifying the FIFO). 
*****************************************************************************/
void welt_c_fifo_unit_size_write_advance(welt_c_fifo_unit_size_pointer fifo);

        
/*****************************************************************************
Read a token from the buffer, and remove the token from the buffer.  It is
assumed that the data argument points to a block of memory that consists of at
least N contiguous bytes, where N is the token size associated with the FIFO.
The function fails silently (without modifying the FIFO) if the FIFO
is empty.
*****************************************************************************/
void welt_c_fifo_unit_size_read(
        welt_c_fifo_unit_size_pointer fifo, void *data);

/*****************************************************************************
Read a token from the buffer, but do NOT remove it from the buffer.  It is
assumed that the data argument points to a block of memory that consists of at
least N contiguous bytes, where N is the token size associated with the FIFO.
The function copies N bytes of memory from the current read position in the
FIFO (the oldest token) into the block of memory pointed to by the data
argument. The function fails silently (without modifying the FIFO) if the FIFO
is empty upon entry to this function.
*****************************************************************************/
void welt_c_fifo_unit_size_peek(welt_c_fifo_unit_size_pointer fifo, void *data);


/*****************************************************************************
Update the FIFO population as a FIFO read, but do not read the token from the  
buffer. read_advance function will update (move forward) the read pointer by 
one token. Then update the FIFO population. The funtion will fails silently if 
there is no token in FIFO(without modifying the FIFO). 
*****************************************************************************/
void welt_c_fifo_unit_size_read_advance(welt_c_fifo_unit_size_pointer fifo);

/*****************************************************************************
Reset the FIFO so that it contains no tokens --- that is, reset to an
empty FIFO.
*****************************************************************************/
void welt_c_fifo_unit_size_reset(welt_c_fifo_unit_size_pointer fifo);

/*****************************************************************************
Deallocate the storage associated with the given fifo_unit_size.
*****************************************************************************/
void welt_c_fifo_unit_size_free(welt_c_fifo_unit_size_pointer fifo);

/*****************************************************************************
Set the token printing function that is associated with this fifo_unit_size.
*****************************************************************************/
void welt_c_fifo_unit_size_set_token_printing(
        welt_c_fifo_unit_size_pointer fifo,
        welt_c_fifo_unit_size_f_token_printing f);

/*****************************************************************************
Print to the given file the token in the fifo_unit_size using the token printing
function that is associated with the fifo_unit_size. If no token printing
function is associated with the fifo_unit_size, then the function does nothing.  
A newline is inserted after printing the token. 
*****************************************************************************/
void welt_c_fifo_unit_size_print_tokens(
        welt_c_fifo_unit_size_pointer fifo, FILE *output);
 
/*****************************************************************************
Set index of FIFO in a dataflow graph
*****************************************************************************/
void welt_c_fifo_unit_size_set_index(welt_c_fifo_unit_size_pointer fifo,
        int index);

/*****************************************************************************
Get index of FIFO in a dataflow graph
*****************************************************************************/
int welt_c_fifo_unit_size_get_index(welt_c_fifo_unit_size_pointer fifo);
 
 
#endif

