/****************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "welt_c_util.h"
#include "discardEdge.h"

typedef struct _discardEdge_struct discardEdge_type;

/*****************************************************************************
Public functions.
*****************************************************************************/

discardEdge_pointer discardEdge_new(int capacity, int token_size, int index) {
    discardEdge_pointer fifo = NULL;

    if (token_size <= 0) {
        return NULL;
    }
    fifo = (discardEdge_pointer)welt_c_util_malloc(
            sizeof(discardEdge_type));
    if (fifo == NULL) {
        return NULL;
    }
    fifo->index = index;
    fifo->capacity = capacity;
    fifo->token_size = token_size;
    fifo->buffer_start = NULL;
    fifo->buffer_start = welt_c_util_malloc((size_t)(token_size));
    if (fifo->buffer_start == NULL) {
        free(fifo);
        return NULL;
    }
    /* Assign methods */
    fifo->write = (welt_c_fifo_write_ftype) discardEdge_write;
    //fifo->get_population = (discardEdge_pointer_population_ftype)
    //        discardEdge_population;
    //fifo->get_capacity = (discardEdge_pointer_capacity_ftype)
    //        discardEdge_capacity;
    //fifo->get_token_size = (discardEdge_pointer_token_size_ftype)
    //        discardEdge_token_size;
    //fifo->set_index = (discardEdge_pointer_set_index_ftype)
    //        discardEdge_set_index;
    //fifo->get_index = (discardEdge_pointer_get_index_ftype)
    //        discardEdge_get_index;
    return fifo;
}

int discardEdge_population(discardEdge_pointer fifo) {
    return fifo->population;
}

int discardEdge_capacity(discardEdge_pointer fifo) {
    return fifo->capacity;
}

int discardEdge_token_size(discardEdge_pointer fifo) {
    return fifo->token_size;
}

void discardEdge_write(discardEdge_pointer fifo, void *data) {
    /* It does nothing */
    printf("Discarding the fifo\n");
    return;
}

void discardEdge_set_index(discardEdge_pointer fifo, int index){
    fifo->index = index;
    return;
}

int discardEdge_get_index(discardEdge_pointer fifo){
    return fifo->index;
}
