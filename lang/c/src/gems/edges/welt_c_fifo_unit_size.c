/****************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "welt_c_fifo_unit_size.h"
#include "welt_c_util.h"
/* #define boolean int
#define FALSE   0
#define TRUE    1*/
/* Use of the dlcutil C Boolean datatype is disabled.
Instead, we use the stdbool library for C code, and the bool
datatype for C++ code. This comment and the commented out
type definition can be removed once code is stable
after migrating to the updated C/C++ Boolean usage formats. */
#include <stdbool.h>
#define byte    unsigned char



struct _welt_c_fifo_unit_size_struct {
#include "welt_c_fifo_type_common.h"
}; 

 
/*****************************************************************************
Public functions.
*****************************************************************************/

void welt_c_fifo_unit_size_display_contents(
        welt_c_fifo_unit_size_pointer fifo, FILE *output) {
    char *p = NULL;
    int i = 0;
    int j = 0;
    char value = '\0';

    fprintf(output, "----- fifo contents: -----------\n");
    p = (char *)fifo->buffer_start;
    for (i = 0; i < fifo->population; i++) {
        for (j = 0; j < fifo->token_size; j++) {
            if (j != 0) {
                fprintf(output, " ");
            }
            value = p[j];

            /* In case of issues with sign extension, extract just the last 
            byte of the value before printing. 
            */
            fprintf(output, "%02x", value & 0xff);
        }
        fprintf(output, "\n");
        if (p == fifo->buffer_end) {
            p = (char *)fifo->buffer_start;
        } else {
            p += fifo->token_size;
        }
    }
    fprintf(output, "--------------------------------\n");
}


/*****************************************************************************
Print an error message to stderr and exit. If the specified fifo_unit_size
is non-NULL, also print diagnostic information about the fifo_unit_size to
stderr.
*****************************************************************************/
void _welt_c_fifo_unit_size_error(
        welt_c_fifo_unit_size_pointer fifo, char *message) {
    if (fifo != NULL) {
        welt_c_fifo_unit_size_display_status(fifo, stderr);
    }
    fprintf(stderr, "%s\n", message);
    exit(1);
}

/*****************************************************************************
Public functions.
*****************************************************************************/

void welt_c_fifo_unit_size_display_status(
    welt_c_fifo_unit_size_pointer fifo, FILE *output) {
    fprintf(output, "----- fifo_unit_size status: -----------\n");
    fprintf(output, "population: %d\n", fifo->population);
    fprintf(output, "capacity: %d\n", 1);
    fprintf(output, "token size: %d\n", fifo->token_size);
    fprintf(output, "------------------------------\n");
}

welt_c_fifo_unit_size_pointer welt_c_fifo_unit_size_new(int token_size,
        int index) {
    welt_c_fifo_unit_size_pointer fifo = NULL;

    /*************************************************************************
    The use of generic pointers in this implementation requires 
    that characters have unit size. This should be the case, but
    in case it is not, we abort. It would be better to move this
    check into some sort of platform-level configuration for the ADT.
    A point related to this one is that involving certain typecasts
    to (char*) in the code --- these casts are needed to avoid (probably benign
    in our case) compiler warnings when performing arithmetic on generic 
    pointers.
    *************************************************************************/
 
    if (sizeof(char) != 1) {
        _welt_c_fifo_unit_size_error(NULL,
                (char *)"incompatible target platform");
    }


    fifo = (welt_c_fifo_unit_size_pointer)welt_c_util_malloc(
                        sizeof(welt_c_fifo_unit_size_type));
    if (fifo == NULL) {
        return NULL;
    }
    fifo->token_size = token_size;
    fifo->buffer_start = NULL;
    fifo->buffer_start = welt_c_util_malloc(
                        (size_t)(UNIT_CAPACITY * fifo->token_size));
    fifo->buffer_end = fifo->buffer_start;
    
    fifo->capacity = UNIT_CAPACITY;
    fifo->population = 0;    
    fifo->read_pointer = NULL;
    fifo->write_pointer = NULL;
    fifo->index = index;
    
    /* Methods in FIFO*/
    fifo->read = (welt_c_fifo_read_ftype)
                            welt_c_fifo_unit_size_read;
    fifo->read_block = NULL;
    fifo->write = (welt_c_fifo_write_ftype)
                            welt_c_fifo_unit_size_write;
    fifo->write_block = NULL;                        
    fifo->get_population = (welt_c_fifo_population_ftype)
                                    welt_c_fifo_unit_size_population;
    fifo->get_capacity = (welt_c_fifo_capacity_ftype)
                                    welt_c_fifo_unit_size_capacity;
    fifo->get_token_size = (welt_c_fifo_token_size_ftype)
                                    welt_c_fifo_unit_size_token_size;
    fifo->reset = (welt_c_fifo_reset_ftype)
                            welt_c_fifo_unit_size_reset;
 
    fifo->write_advance = (welt_c_fifo_write_advance_ftype)
                        welt_c_fifo_unit_size_write_advance;
    fifo->peek = (welt_c_fifo_peek_ftype)welt_c_fifo_unit_size_peek;
    fifo->read_advance = (welt_c_fifo_read_advance_ftype)
            welt_c_fifo_unit_size_read_advance;
    fifo->set_index =
            (welt_c_fifo_set_index_ftype)welt_c_fifo_unit_size_set_index;
    fifo->get_index =
            (welt_c_fifo_get_index_ftype)welt_c_fifo_unit_size_get_index;
    
    
    fifo->reset((welt_c_fifo_type *)fifo);
    //fifo->print = NULL;
    return fifo;
}

int welt_c_fifo_unit_size_population(welt_c_fifo_unit_size_pointer fifo) {
    return fifo->population;
}

int welt_c_fifo_unit_size_capacity(welt_c_fifo_unit_size_pointer fifo) {
    return fifo->capacity;
}

int welt_c_fifo_unit_size_token_size(welt_c_fifo_unit_size_pointer fifo) {
    return fifo->token_size;
}

void welt_c_fifo_unit_size_write(welt_c_fifo_unit_size_pointer fifo,
                                    void *data) {
    /* Make sure there is room for the new data. */
    if (fifo->population > 0) {
        return;
    }

    /* The following assumes that sizeof(char) = 1. */
    memcpy(fifo->buffer_start, data, fifo->token_size);

    
    /* Update the token count. */
    fifo->population += 1;
}

void welt_c_fifo_unit_size_write_advance(
        welt_c_fifo_unit_size_pointer fifo){
    /* Make sure there is room for a FIFO-write. */
    if(fifo->population > 0){
        return;
    }
    /* Update the token count. */
    fifo->population += 1;
    return;
}




void welt_c_fifo_unit_size_read(welt_c_fifo_unit_size_pointer fifo,
                                    void *data) {
    /* Make sure the fifo_unit_size is not empty. */
    if (fifo->population == 0) {
        return;
    }

    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, fifo->buffer_start, fifo->token_size);

    /* Update the token count. */
    fifo->population -= 1; 
}


void welt_c_fifo_unit_size_peek(welt_c_fifo_unit_size_pointer fifo,
        void *data){
        /* Make sure the fifo_unit_size is not empty. */
    if (fifo->population == 0) {
        return;
    }

    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, fifo->buffer_start, fifo->token_size);
    return;
}

void welt_c_fifo_unit_size_read_advance(
        welt_c_fifo_unit_size_pointer fifo){
    /* Make sure the fifo_unit_size is not empty. */
    if (fifo->population == 0) {
        return;
    }

    /* Update the token count. */
    fifo->population -= 1; 
    return;
}   
        
        

void welt_c_fifo_unit_size_free(welt_c_fifo_unit_size_pointer fifo) {
    if (fifo == NULL) {
        return;
    }
    if (fifo->buffer_start != NULL) {
        free(fifo->buffer_start);
    }
    free(fifo);
}

void welt_c_fifo_unit_size_reset(welt_c_fifo_unit_size_pointer fifo) {
    fifo->population = 0;
}

void welt_c_fifo_unit_size_set_index(welt_c_fifo_unit_size_pointer fifo,
                                        int index){
    fifo->index = index;
    return;
}

int welt_c_fifo_unit_size_get_index(welt_c_fifo_unit_size_pointer fifo){
    return fifo->index;
}
