#ifndef _discardEdge_h
#define _discardEdge_h

/****************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/
#include <stdio.h>
#include "welt_c_fifo.h"

/* A FIFO for discardEdge. */
typedef struct _discardEdge_struct discardEdge_type;

/* A pointer to a discardEdge. */
typedef discardEdge_type *discardEdge_pointer;

/*****************************************************************************
Create a new discard edge. This edge type is intended when tokens
produced onto an edge are not used by the rest of the graph. The tokens on this
edge type will not be processed or stored.
*****************************************************************************/
discardEdge_pointer discardEdge_new(int capacity, int token_size, int index);

/* ----------------------------  Public functions --------------------- */
/*****************************************************************************
Return the number of tokens that are currently in the discardEdge. This should
always be 0.
*****************************************************************************/
int discardEdge_population(discardEdge_pointer fifo);
/*****************************************************************************
Return the capacity of the discardEdge.
*****************************************************************************/
int discardEdge_capacity(discardEdge_pointer fifo);
/*****************************************************************************
Return the number of bytes that are used to store a single token in the
discardEdge.
*****************************************************************************/
int discardEdge_token_size(discardEdge_pointer fifo) ;
/*****************************************************************************
Insert a new token into the buffer. This function discards the token.
*****************************************************************************/
void discardEdge_write(discardEdge_pointer fifo, void *data);
/*****************************************************************************
Set index of the discardEdge in a dataflow graph
*****************************************************************************/
void discardEdge_set_index(discardEdge_pointer fifo, int index);
/*****************************************************************************
Get index of the discardEdge in a dataflow graph
*****************************************************************************/
int discardEdge_get_index(discardEdge_pointer fifo);

/*Function pointers*/
typedef void (*discardEdge_write_ftype)(discardEdge_pointer fifo, void *data);
typedef void (*discardEdge_pointer_set_index_ftype)(discardEdge_pointer fifo,
        int index);
typedef int (*discardEdge_pointer_get_index_ftype)(discardEdge_pointer fifo);
typedef int (*discardEdge_pointer_population_ftype)(discardEdge_pointer fifo);
typedef int (*discardEdge_pointer_capacity_ftype)(discardEdge_pointer fifo);
typedef int (*discardEdge_pointer_token_size_ftype)(discardEdge_pointer fifo);

struct _discardEdge_struct {
#include "welt_c_fifo_type_common.h"
};

#endif

