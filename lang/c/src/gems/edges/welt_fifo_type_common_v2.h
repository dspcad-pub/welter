/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2017
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

/* Common elements across context type of fifos. */
lide_c_router_buffer_pointer buffer;

/* methods in fifo*/
welt_fifo_read_ftype read;
welt_fifo_read_block_ftype read_block;
welt_fifo_write_ftype write;
welt_fifo_write_block_ftype write_block;
welt_fifo_write_advance_ftype write_advance;
welt_fifo_peek_ftype peek;
welt_fifo_read_advance_ftype read_advance;
welt_fifo_population_ftype get_population;
welt_fifo_capacity_ftype get_capacity;
welt_fifo_token_size_ftype  get_token_size;
welt_fifo_reset_ftype reset;
welt_fifo_set_index_ftype set_index;
welt_fifo_get_index_ftype get_index;

/* index of the FIFO in a dataflow graph. It is flexible and set by the 
designer*/
int index;
