/****************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "welt_c_util.h"
#include "welt_c_fifo.h"

typedef struct _welt_c_fifo_struct welt_c_fifo_type;

/*****************************************************************************
Private functions.
*****************************************************************************/


/*****************************************************************************
Print diagnostic information relating to internal pointer values associated
with a given FIFO.
*****************************************************************************/
void _welt_c_fifo_diagnostics(welt_c_fifo_pointer fifo, FILE *output) {
    fprintf(output, "RP = %p, WP = %p\n", fifo->read_pointer, 
            fifo->write_pointer);
    fprintf(output, "START = %p, END = %p\n", fifo->buffer_start, 
            fifo->buffer_end);
}
 
/*****************************************************************************
Public functions.
*****************************************************************************/

void welt_c_fifo_display_contents(welt_c_fifo_pointer fifo, FILE *output) {
    char *p = NULL;
    int i = 0;
    int j = 0;
    char value = '\0';

    fprintf(output, "----- fifo contents: -----------\n");

    p = fifo->read_pointer;
    for (i = 0; i < fifo->population; i++) {
        for (j = 0; j < fifo->token_size; j++) {
            if (j != 0) {
                fprintf(output, " ");
            }
            value = p[j];

            /* In case of issues with sign extension, extract just the last 
            byte of the value before printing.
            */
            fprintf(output, "%02x", value & 0xff);
        }
        fprintf(output, "\n");
        if (p == fifo->buffer_end) {
            p = fifo->buffer_start;
        } else {
            p += fifo->token_size;
        }
    }
    fprintf(output, "--------------------------------\n");
}

void welt_c_fifo_print_tokens(welt_c_fifo_pointer fifo, FILE *output) {
    char *p = NULL;
    int i = 0;

    if (fifo->print == NULL) {
        return;
    }
    p = fifo->read_pointer;
    for (i = 0; i < fifo->population; i++) {
        fifo->print(output, p);
        fprintf(output, "\n");
        if (p == fifo->buffer_end) {
            p = fifo->buffer_start;
        } else {
            p += fifo->token_size;
        }
    }
}


/*****************************************************************************
Print an error message to stderr and exit. If the specified fifo
is non-NULL, also print diagnostic information about the fifo to
stderr.
*****************************************************************************/
void _welt_c_fifo_error(welt_c_fifo_pointer fifo, char *message) {
    if (fifo != NULL) {
        welt_c_fifo_display_status(fifo, stderr);
    }
    fprintf(stderr, "%s\n", message);
    exit(1);
}

/*****************************************************************************
Public functions.
*****************************************************************************/

void welt_c_fifo_display_status(welt_c_fifo_pointer fifo, FILE *output) {
    fprintf(output, "----- fifo status: -----------\n");
    fprintf(output, "population: %d\n", fifo->population);
    fprintf(output, "capacity: %d\n", fifo->capacity);
    fprintf(output, "token size: %d\n", fifo->token_size);
    fprintf(output, "write index: %d\n", 
            (int)(((char*)fifo->write_pointer - (char*)fifo->buffer_start) / 
                    fifo->token_size));
    fprintf(output, "read index: %d\n", 
            (int)(((char*)fifo->read_pointer - (char*)fifo->buffer_start) / 
                    fifo->token_size));
    fprintf(output, "------------------------------\n");
}

welt_c_fifo_pointer welt_c_fifo_new(int capacity, int token_size,
                                                int index) {
    welt_c_fifo_pointer fifo = NULL;

    /*************************************************************************
    The use of generic pointers in this implementation requires 
    that characters have unit size. This should be the case, but
    in case it is not, we abort. It would be better to move this
    check into some sort of platform-level configuration for the ADT.
    A point related to this one is that involving certain typecasts
    to (char*) in the code --- these casts are needed to avoid (probably benign
    in our case) compiler warnings when performing arithmetic on generic 
    pointers.
    *************************************************************************/
 
    if (sizeof(char) != 1) {
        _welt_c_fifo_error(NULL, "incompatible target platform");
    }

    if (capacity < 1) {
        return NULL;
    }
    if (token_size <= 0) {
        return NULL;
    }
    fifo = (welt_c_fifo_pointer)welt_c_util_malloc(
            sizeof(welt_c_fifo_type));
    if (fifo == NULL) {
        return NULL;
    }
    fifo->index = index;
    fifo->capacity = capacity;
    fifo->token_size = token_size;
    fifo->buffer_start = NULL;
    fifo->buffer_start = welt_c_util_malloc((size_t)(capacity * token_size));
    if (fifo->buffer_start == NULL) {
        free(fifo);
        return NULL;
    }
    fifo->buffer_end = ((char*)fifo->buffer_start) + 
            ((capacity - 1) * token_size);
            
            
    /* assign methods*/
    fifo->read = (welt_c_fifo_read_ftype)welt_c_fifo_read;
    fifo->read_block = (welt_c_fifo_read_block_ftype)
                        welt_c_fifo_read_block;
    fifo->write = (welt_c_fifo_write_ftype)welt_c_fifo_write;
    fifo->write_block = (welt_c_fifo_write_block_ftype)
                        welt_c_fifo_write_block;
    fifo->write_advance = (welt_c_fifo_write_advance_ftype)
                        welt_c_fifo_write_advance;
    fifo->peek = (welt_c_fifo_peek_ftype)welt_c_fifo_peek;
    fifo->read_advance = (welt_c_fifo_read_advance_ftype)
                        welt_c_fifo_read_advance;
    fifo->get_population = (welt_c_fifo_population_ftype)
                        welt_c_fifo_population;
    fifo->get_capacity = (welt_c_fifo_capacity_ftype)
                        welt_c_fifo_capacity;
    fifo->get_token_size = (welt_c_fifo_token_size_ftype)
                        welt_c_fifo_token_size;
    fifo->reset = (welt_c_fifo_reset_ftype)welt_c_fifo_reset;
    fifo->set_index = (welt_c_fifo_set_index_ftype)welt_c_fifo_set_index;
    fifo->get_index = (welt_c_fifo_get_index_ftype)welt_c_fifo_get_index;
    
    fifo->reset((welt_c_fifo_type *)fifo);
    fifo->print = NULL;
    return fifo;
}

int welt_c_fifo_population(welt_c_fifo_pointer fifo) {
    return fifo->population;
}

int welt_c_fifo_capacity(welt_c_fifo_pointer fifo) {
    return fifo->capacity;
}

int welt_c_fifo_token_size(welt_c_fifo_pointer fifo) {
    return fifo->token_size;
}

void welt_c_fifo_write(welt_c_fifo_pointer fifo, void *data) {
    /* Make sure there is room for the new data. */
    if (fifo->population >= fifo->capacity) {
        return;
    }

    /* The following assumes that sizeof(char) = 1. */
    memcpy(fifo->write_pointer, data, fifo->token_size);

    /* Advance the write pointer, using a circular buffering convention. */
    if (fifo->write_pointer == fifo->buffer_end) {
        fifo->write_pointer = fifo->buffer_start;
    } else {
        fifo->write_pointer = ((char*)fifo->write_pointer) + fifo->token_size;
    }

    /* Update the token count. */
    fifo->population++; 
}

void welt_c_fifo_read(welt_c_fifo_pointer fifo, void *data) {
    /* Make sure the fifo is not empty. */
    if (fifo->population == 0) {
        return;
    }

    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, fifo->read_pointer, fifo->token_size);

    /* Advance the read pointer, using a circular buffering convention. */
    if (fifo->read_pointer == fifo->buffer_end) {
        fifo->read_pointer = fifo->buffer_start;
    } else {
        fifo->read_pointer = ((char*)fifo->read_pointer) + fifo->token_size;
    }

    /* Update the token count. */
    fifo->population--; 
}

void welt_c_fifo_write_block(welt_c_fifo_pointer fifo, void *data, int size) {
    /* Room (number of token spaces) "in front of the write pointer" within 
    the buffer.
    */
    int room_in_front = 0;

    /* Breakdown in bytes of the amounts of data to write before and after,
    respectively, the write pointer wraps around the end of the buffer.
    */
    int part1_size = 0;
    int part2_size = 0;
    
    /* A cache of the FIFO token size. */
    int token_size = 0;

    /* Make sure there is room for the new data. */
    if ((fifo->population + size) > fifo->capacity) {
        return;
    }

    token_size = fifo->token_size;
    room_in_front = ((((char*) (fifo->buffer_end)) - 
            ((char*)(fifo->write_pointer))) / token_size) + 1;

    if (room_in_front >= size) {
        part1_size = size * token_size;
        part2_size = 0;
    } else {
        part1_size = room_in_front * token_size;
        part2_size = (size - room_in_front) * token_size; 
    }
    
    /* The following assumes that sizeof(char) = 1. */
    memcpy(fifo->write_pointer, data, part1_size);
    if (part2_size > 0) {
        data = ((char*)data) + part1_size;
        memcpy(fifo->buffer_start, data, part2_size);
        fifo->write_pointer = ((char*)fifo->buffer_start) + part2_size;
    } else {
        if (size == room_in_front) {
            fifo->write_pointer = fifo->buffer_start;
        } else {
            fifo->write_pointer = ((char*)fifo->write_pointer) + part1_size;
        }
    }

    /* Update the token count. */
    fifo->population += size; 
}

void welt_c_fifo_read_block(welt_c_fifo_pointer fifo, void *data, int size) {
    /* Room (number of token spaces) "in front of the read pointer" within 
    the buffer.*/
    int room_in_front = 0;

    /* Breakdown in bytes of the amounts of data to read before and after,
    respectively, the read pointer wraps around the end of the buffer.*/
    int part1_size = 0;
    int part2_size = 0;

    /* A cache of the FIFO token size. */
    int token_size = 0;

    /* Make sure there are enough tokens in the FIFO. */
    if (fifo->population < size) {
        return;
    }

    token_size = fifo->token_size;
    room_in_front = ((((char*) (fifo->buffer_end)) - 
            ((char*)(fifo->read_pointer))) / token_size) + 1;

    if (room_in_front >= size) {
        part1_size = size * token_size;
        part2_size = 0;
    } else {
        part1_size = room_in_front * token_size;
        part2_size = (size - room_in_front) * token_size; 
    }

    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, fifo->read_pointer, part1_size);
    if (part2_size > 0) {
        data = ((char*)data) + part1_size;
        memcpy(data, fifo->buffer_start, part2_size);
        fifo->read_pointer = ((char*)fifo->buffer_start) + part2_size;
    } else {
        if (size == room_in_front) {
            fifo->read_pointer = fifo->buffer_start;
        } else {
            fifo->read_pointer = ((char*)fifo->read_pointer) + part1_size;
        }
    }

    /* Update the token count. */
    fifo->population -= size; 
}


void welt_c_fifo_free(welt_c_fifo_pointer fifo) {
    if (fifo == NULL) {
        return;
    }
    if (fifo->buffer_start != NULL) {
        free(fifo->buffer_start);
    }
    free(fifo);
}

void welt_c_fifo_reset(welt_c_fifo_pointer fifo) {
    fifo->write_pointer = fifo->buffer_start;
    fifo->read_pointer = fifo->buffer_start;
    fifo->population = 0;
}

/*void welt_c_fifo_set_token_printing(welt_c_fifo_pointer fifo,
        welt_c_fifo_f_token_printing f) {
    fifo->print = f;
} */


void welt_c_fifo_read_advance(welt_c_fifo_pointer fifo){
    /* Make sure the dsg_unit_fifo is not empty. */
    if (fifo->population == 0) {
        return;
    }

    /* Update the token count. */
    fifo->population -= 1; 
    return;
}
void welt_c_fifo_peek(welt_c_fifo_pointer fifo, void *data){
    /* Make sure the fifo is not empty. */
    if (fifo->population == 0) {
        return;
    }

    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, fifo->read_pointer, fifo->token_size);

    return;
}

void welt_c_fifo_peek_block(welt_c_fifo_pointer fifo,
    void *data, int size){
    /* Room (number of token spaces) "in front of the read pointer" within 
    the buffer. */
    int room_in_front = 0;

    /* Breakdown in bytes of the amounts of data to read before and after,
    respectively, the read pointer wraps around the end of the buffer.
    */
    int part1_size = 0;
    int part2_size = 0;
    
    /* A cache of the FIFO token size. */
    int token_size = 0;

    /* Make sure there are enough tokens in the FIFO. */
    if (fifo->population < size) {
        return;
    }

    token_size = fifo->token_size;
    room_in_front = ((((char*) (fifo->buffer_end)) - 
            ((char*)(fifo->read_pointer))) / token_size) + 1;

    if (room_in_front >= size) {
        part1_size = size * token_size;
        part2_size = 0;
    } else {
        part1_size = room_in_front * token_size;
        part2_size = (size - room_in_front) * token_size; 
    }

    /* The following assumes that sizeof(char) = 1. */
    memcpy(data, fifo->read_pointer, part1_size);
    if (part2_size > 0) {
        data = ((char*)data) + part1_size;
        memcpy(data, fifo->buffer_start, part2_size);
    }

    return;
}


void welt_c_fifo_write_advance(welt_c_fifo_pointer fifo){
    /* Make sure there is room for a FIFO-write. */
    if(fifo->population > 0){
        return;
    }
    /* Update the token count. */
    fifo->population += 1;
    return;   
}


void welt_c_fifo_set_index(welt_c_fifo_pointer fifo, int index){
    fifo->index = index;
    return;
}


int welt_c_fifo_get_index(welt_c_fifo_pointer fifo){
	//printf("getting fifo index: %p, %d", fifo, fifo->index ) ;
    return fifo->index;
}
