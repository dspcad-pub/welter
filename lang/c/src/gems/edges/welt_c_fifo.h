#ifndef _welt_c_fifo_h
#define _welt_c_fifo_h

/****************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
******************************************************************************/
#include <stdio.h>

/* A FIFO. */
typedef struct _welt_c_fifo_struct welt_c_fifo_type;

/* A pointer to a fifo. */
typedef welt_c_fifo_type *welt_c_fifo_pointer;

void _welt_c_fifo_diagnostics(welt_c_fifo_pointer fifo, FILE *output);
void welt_c_fifo_display_contents(welt_c_fifo_pointer fifo, FILE *output);
void welt_c_fifo_print_tokens(welt_c_fifo_pointer fifo, FILE *output);
void _welt_c_fifo_error(welt_c_fifo_pointer fifo, char *message);
void welt_c_fifo_display_status(welt_c_fifo_pointer fifo, FILE *output);
welt_c_fifo_pointer welt_c_fifo_new(int capacity, int token_size,
                                int index);
int welt_c_fifo_population(welt_c_fifo_pointer fifo);
int welt_c_fifo_capacity(welt_c_fifo_pointer fifo);
int welt_c_fifo_token_size(welt_c_fifo_pointer fifo) ;
void welt_c_fifo_write(welt_c_fifo_pointer fifo, void *data) ;
void welt_c_fifo_read(welt_c_fifo_pointer fifo, void *data);
void welt_c_fifo_write_block(welt_c_fifo_pointer fifo, void *data, int size);
void welt_c_fifo_read_block(welt_c_fifo_pointer fifo, void *data, int size);
void welt_c_fifo_free(welt_c_fifo_pointer fifo);
void welt_c_fifo_reset(welt_c_fifo_pointer fifo);
//void welt_c_fifo_set_token_printing(welt_c_fifo_pointer fifo,
//                                  welt_c_fifo_f_token_printing f);
void welt_c_fifo_read_advance(welt_c_fifo_pointer fifo);
void welt_c_fifo_peek(welt_c_fifo_pointer fifo, void *data);
void welt_c_fifo_peek_block(welt_c_fifo_pointer fifo, void *data, int size);
void welt_c_fifo_write_advance(welt_c_fifo_pointer fifo);
void welt_c_fifo_set_index(welt_c_fifo_pointer fifo, int index);
int welt_c_fifo_get_index(welt_c_fifo_pointer fifo);


/*****************************************************************************
A pointer to a "token printing function", which is a function that 
prints the data value encapsulated by a single token to a given file.
This type is normally used just for diagnostic purposes.
*****************************************************************************/
typedef void (*welt_c_fifo_f_token_printing)(FILE *output, void *token);

/* ----------------------------  Public functions --------------------- */

/*****************************************************************************
Display information about the FIFO to the specified file.  The display output
is written to the specified file, which must be open for writing prior to
calling this function.  This function is primarily provided for diagnostic
purposes.
*****************************************************************************/
//void welt_c_fifo_display_status(welt_c_fifo_pointer fifo, FILE *output);
typedef void (*welt_c_fifo_display_status_ftype)(welt_c_fifo_pointer fifo,
        FILE *output);

/*****************************************************************************
Display the contents of the fifo, one byte at time, with each byte displayed in
hexadecimal format. The bytes that correspond to a given token are printed on
the same line, and successive tokens in the FIFO (starting with the olidest
token) are displayed on successive lines. The display output is written to the
specified file, which must be open for writing prior to calling this
function. This function is primarily for diagnostic purposes.
*****************************************************************************/
//void welt_c_fifo_display_contents(welt_c_fifo_pointer fifo, FILE *output);
typedef void (*welt_c_fifo_display_contents_ftype)(welt_c_fifo_pointer fifo,
        FILE *output);

/*****************************************************************************
Create a new FIFO with the specified capacity (maximum number of tokens that it
can hold), and the specified token_size (the number of bytes used to store a
single token).  If the specified capacity is less than or equal to zero, the
specified token size is less than or equal to zero, or there is not enough
memory to allocate the FIFO, the function fails silently and returns NULL.
*****************************************************************************/
welt_c_fifo_pointer welt_c_fifo_new(int capacity, int token_size, int index);

/*****************************************************************************
Return the number of tokens that are currently in the fifo.
*****************************************************************************/
typedef int (* welt_c_fifo_population_ftype)(welt_c_fifo_pointer fifo);

/*****************************************************************************
Return the capacity of the fifo.
*****************************************************************************/
typedef int (*welt_c_fifo_capacity_ftype)(welt_c_fifo_pointer fifo);

/*****************************************************************************
Return the number of bytes that are used to store a single token in the fifo.
*****************************************************************************/
typedef int (*welt_c_fifo_token_size_ftype)(welt_c_fifo_pointer fifo);

/*****************************************************************************
Insert a new token into the buffer. This function copies N bytes, where N is
the fifo token size, starting at the address specified by the data argument.
This data is copied into the next available position in the buffer. If the FIFO
is already full upon entry to this function, the function fails silently
(without modifying the FIFO).
*****************************************************************************/
//void welt_c_fifo_write(welt_c_fifo_pointer fifo, void *data);
typedef void (*welt_c_fifo_write_ftype)(welt_c_fifo_pointer fifo, void *data);
/*****************************************************************************
Write a block of "size" tokens into the buffer. The "data" argument points to
the beginning of the block of data to be written. For large block sizes,
calling this function is likely to be significantly more efficient than
repeated calls to welt_c_fifo_write. The function fails silently (without
modifying the FIFO) if there is not enough room in the FIFO to hold "size" new
tokens.
*****************************************************************************/
//void welt_c_fifo_write_block(welt_c_fifo_pointer fifo, void *data, int size);
typedef void (*welt_c_fifo_write_block_ftype)(welt_c_fifo_pointer fifo,
        void *data, int size);


/*****************************************************************************
Update the FIFO population as a FIFO write, but do not write the token to the 
buffer. write_advance function will update (move forward) the write pointer by 
one token. Then update the FIFO population. The funtion will fails silently if 
there is no enough empty space in FIFO(without modifying the FIFO). 
*****************************************************************************/
typedef void (*welt_c_fifo_write_advance_ftype)(welt_c_fifo_pointer fifo);

/*****************************************************************************
Read a token from the buffer, and remove the token from the buffer.  It is
assumed that the data argument points to a block of memory that consists of at
least N contiguous bytes, where N is the token size associated with the FIFO.
The function copies N bytes of memory from the current read position in the
FIFO (the olidest token) into the block of memory pointed to by the data
argument. The function fails silently (without modifying the FIFO) if the FIFO
is empty upon entry to this function.
*****************************************************************************/
typedef void (*welt_c_fifo_read_ftype)(welt_c_fifo_pointer fifo, void *data);

/*****************************************************************************
Read a block of "size" tokens from the buffer, and remove these tokens from the
buffer. It is assumed that the "data" argument points to a block of memory that
consists of at least ("size" * N) contiguous bytes, where N is the token size
associated with this FIFO. The function copies ("size" * token_size) bytes of
memory from the current read position in the fifo (the olidest token) into the
block of memory pointed to by the data argument.  For large block sizes,
calling this function is likely to be significantly more efficient than
repeated calls to welt_c_fifo_read.  The function fails silently (without
modifying the FIFO) if the FIFO contains fewer than "size" tokens upon entry to
this function.
*****************************************************************************/
typedef void (*welt_c_fifo_read_block_ftype)(welt_c_fifo_pointer fifo,
        void *data, int size);


/*****************************************************************************
Read a token from the buffer, but do NOT remove it from the buffer.  It is
assumed that the data argument points to a block of memory that consists of at
least N contiguous bytes, where N is the token size associated with the FIFO.
The function copies N bytes of memory from the current read position in the
FIFO (the oldest token) into the block of memory pointed to by the data
argument. The function fails silently (without modifying the FIFO) if the FIFO
is empty upon entry to this function.
*****************************************************************************/
typedef void (*welt_c_fifo_peek_ftype)(welt_c_fifo_pointer fifo, void *data);

/*****************************************************************************
Read a block of "size" tokens from the buffer, but do NOT remove it from the 
buffer. It is assumed that the data argument points to a block of memory that 
consists of at least ("size" * N) contiguous bytes, where N is the token size 
associated with the FIFO. The function copies ("size" * N) bytes of memory 
from the current read position in the FIFO (the oldest token) into the block 
of memory pointed to by the data argument. The function fails silently 
(without modifying the FIFO) if the FIFO is empty upon entry to this function.
*****************************************************************************/
typedef void (*welt_c_fifo_peek_block_ftype)(welt_c_fifo_pointer fifo,
    void *data, int size);

/*****************************************************************************
Update the FIFO population as a FIFO read, but do not read the token from the  
buffer. read_advance function will update (move forward) the read pointer by 
one token. Then update the FIFO population. The funtion will fails silently if 
there is no token in FIFO(without modifying the FIFO). 
*****************************************************************************/
typedef void (*welt_c_fifo_read_advance_ftype)(welt_c_fifo_pointer fifo);

/*****************************************************************************
Reset the FIFO so that it contains no tokens --- that is, reset to an
empty FIFO.
*****************************************************************************/
typedef void (*welt_c_fifo_reset_ftype)(welt_c_fifo_pointer);

/*****************************************************************************
Deallocate the storage associated with the given fifo.
*****************************************************************************/
typedef void (*welt_c_fifo_free_ftype)(welt_c_fifo_pointer fifo);

/*****************************************************************************
Set the token printing function that is associated with this fifo.
*****************************************************************************/
//typedef void (* welt_c_fifo_set_token_printing)(welt_c_fifo_pointer fifo,
//        welt_c_fifo_f_token_printing f);

/*****************************************************************************
Print to the given file all of the tokens in the fifo using the token printing
function that is associated with the fifo. If no token printing function is
associated with the fifo, then the function does nothing.  A newline is
inserted after printing each token. The tokens are printed sequentially,
starting with the olidest (least-recently written) one.
*****************************************************************************/
typedef void (* welt_c_fifo_print_tokens_ftype)(welt_c_fifo_pointer fifo,
        FILE *output);

/*****************************************************************************
Set index of FIFO in a dataflow graph
*****************************************************************************/
typedef void (*welt_c_fifo_set_index_ftype)(welt_c_fifo_pointer fifo,
        int index);

/*****************************************************************************
Get index of FIFO in a dataflow graph
*****************************************************************************/
typedef int (* welt_c_fifo_get_index_ftype)(welt_c_fifo_pointer fifo);



/*void _welt_c_fifo_diagnostics(welt_c_fifo_pointer fifo, FILE *output);
void welt_c_fifo_display_contents(welt_c_fifo_pointer fifo, FILE *output);
void welt_c_fifo_print_tokens(welt_c_fifo_pointer fifo, FILE *output);
void _welt_c_fifo_error(welt_c_fifo_pointer fifo, char *message);
void welt_c_fifo_display_status(welt_c_fifo_pointer fifo, FILE *output);
welt_c_fifo_pointer welt_c_fifo_new(int capacity, int token_size,
int index);
int welt_c_fifo_population(welt_c_fifo_pointer fifo);
int welt_c_fifo_capacity(welt_c_fifo_pointer fifo);
int welt_c_fifo_token_size(welt_c_fifo_pointer fifo) ;
void welt_c_fifo_write(welt_c_fifo_pointer fifo, void *data) ;
void welt_c_fifo_read(welt_c_fifo_pointer fifo, void *data);
void welt_c_fifo_write_block(welt_c_fifo_pointer fifo, void *data, int size);
void welt_c_fifo_read_block(welt_c_fifo_pointer fifo, void *data, int size);
void welt_c_fifo_free(welt_c_fifo_pointer fifo);
void welt_c_fifo_reset(welt_c_fifo_pointer fifo);
void welt_c_fifo_set_token_printing(welt_c_fifo_pointer fifo,
        welt_c_fifo_f_token_printing f);
void welt_c_fifo_read_advance(welt_c_fifo_pointer fifo);
void welt_c_fifo_peek(welt_c_fifo_pointer fifo, void *data);
void welt_c_fifo_peek_block(welt_c_fifo_pointer fifo,
        void *data, int size);
void welt_c_fifo_write_advance(welt_c_fifo_pointer fifo);
void welt_c_fifo_set_index(welt_c_fifo_pointer fifo, int index);
int welt_c_fifo_get_index(welt_c_fifo_pointer fifo); */

struct _welt_c_fifo_struct {
    
#include "welt_c_fifo_type_common.h"
    
    /* The token printing function (if any) associated with this fifo. */
    welt_c_fifo_f_token_printing print;
    
};


#endif

