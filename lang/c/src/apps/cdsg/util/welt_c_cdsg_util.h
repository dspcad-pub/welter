#ifndef _welt_c_cdsg_util_h
#define _welt_c_cdsg_util_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>

#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_graph_def.h"
#include <sys/types.h>
#include <pthread.h>

/*******************************************************************************
This is a CDSG scheduler that keeps executing actors in a graph
from [start] actor to [end] actor. 
The "actors" argument gives an array of the actors that makes up
the graph; the "actors_count" argument gives the number of actors,
and the "descriptors" argument is an array of strings such that the
i'th string gives a diagnostic descriptor for the ith actor.
*******************************************************************************/


bool welt_c_util_basic_execution(welt_c_actor_context_type *context,
        char *descriptor);

/*******************************************************************************
This is an CFDF canonical scheduler.
*******************************************************************************/
void welt_c_util_cdsg_scheduler(welt_c_actor_context_type *actors[],
        int start, int end, char *descriptors[]);

/*******************************************************************************
This is a CDSG scheduler that keeps executing actors in a graph
only if the descriptor has a string containing word. 
The "actors" argument gives an array of the actors that makes up
the graph; the "actors_count" argument gives the number of actors,
and the "descriptors" argument is an array of strings such that the
i'th string gives a diagnostic descriptor for the ith actor.
*******************************************************************************/
void welt_c_util_part_scheduler(welt_c_actor_context_type *actors[],
        int actor_count, char *word, char *descriptors[]) ;

bool welt_c_cdsg_util_mutex_execution(welt_c_actor_context_type *context,
    pthread_mutex_t **mutex_cdsg, pthread_cond_t **cond_cdsg,
    int **condition, int pid, int num_thread, int i, char *descriptor);

void welt_c_cdsg_util_mutex_scheduler(welt_c_actor_context_type *actors[], 
        int actor_count, char *word, pthread_mutex_t **mutex_cdsg,
        pthread_cond_t **cond_cdsg, int **condition, int pid, int num_thread,
        char *descriptors[]);

#endif
