/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_cdsg_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <pthread.h>

void welt_c_util_cdsg_scheduler(welt_c_actor_context_type *actors[],
        int start, int end, char *descriptors[]) {

    bool progress = false;
    int i = 0;

    do {
        
        progress = 0;
        for (i = start; i <= end; i++) {
            progress |=
                    welt_c_util_basic_execution(actors[i], descriptors[i]);
        }
    } while (progress);
}

// void welt_c_util_part_scheduler(welt_c_actor_context_type *actors[],
//         int actor_count, char *word, char *descriptors[]) {

//     bool progress = false;
//     int i = 0;

//     do {
//         progress = 0;
//         for (i = 0; i <= actor_count; i++) {
//             if(strstr(descriptors[i], word) != NULL)
//             progress |= 
//                     welt_c_util_guarded_execution(actors[i], descriptors[i]);
//         }
//     } while (progress);
// }

bool welt_c_util_basic_execution(welt_c_actor_context_type *context,
        char *descriptor) {

    // pthread_mutex_t **mutex_cdsg = context->mutex_cdsg;
    // pthread_cond_t **cond_cdsg = context->context;

    if (context->enable(context)&&context->mode != 2) {
        // pthread_mutex_lock(mutex_cdsg);
        // pthread_cond_wait(cond_cdsg,mutex_cdsg);
        context->invoke(context);
         //printf("%s visit complete.\n", descriptor);
        //printf("mode:%d\n", context->mode);
        // pthread_mutex_unlock(mutex_cdsg);
        return true;
    } else {
        return false;
    } 
}


void welt_c_util_part_scheduler(welt_c_actor_context_type *actors[],
        int actor_count, char *word, char *descriptors[]) {

    bool progress = false;
    int i = 0;

    do {
        progress = 0;
        for (i = 0; i < actor_count; i++) {
            if(strstr(descriptors[i], word) != NULL)
            progress |= 
                    welt_c_util_basic_execution(actors[i], descriptors[i]);
        }
    } while (progress);
}

bool welt_c_cdsg_util_mutex_execution(welt_c_actor_context_type *context,
    pthread_mutex_t **mutex_cdsg, pthread_cond_t **cond_cdsg,
    int **condition, int pid, int num_thread, int i, char *descriptor)  {

    pthread_mutex_t *mutex_test = &((*mutex_cdsg)[i]);
    pthread_cond_t *cond_test = &((*cond_cdsg)[i]);
    int *condition_test = &((*condition)[i]);
    
    if (context->enable(context)) {

        if (*condition_test != pid){
            while(*condition_test != pid)
                pthread_cond_wait(cond_test,mutex_test);
        }
        
        pthread_mutex_lock(mutex_test);
        printf("%s mutex locked.\n", descriptor);
        context->invoke(context);
        printf("%s visit complete.\n", descriptor);
        
        if (*condition_test != num_thread) (*condition_test)++;
        else (*condition_test) = 1;
        
        pthread_mutex_unlock(mutex_test);
        return true;
    } else {
        return false;
    } 
}

void welt_c_cdsg_util_mutex_scheduler(welt_c_actor_context_type *actors[],
        int actor_count, char *word, pthread_mutex_t **mutex_cdsg,
        pthread_cond_t **cond_cdsg, int **condition, int pid, int num_thread,
        char *descriptors[]) {

     bool progress = false;
    int i = 0;
    int index;
    
    do {
        progress = 0;
        for (i = 0; i < actor_count; i++) {
            if(strstr(descriptors[i], word) != NULL){
            index = i - (pid - 1);
            progress |= 
                    welt_c_cdsg_util_mutex_execution(actors[i], mutex_cdsg, 
                cond_cdsg,condition,pid,num_thread,index,descriptors[i]);
                }
        }
    } while (progress);
}
