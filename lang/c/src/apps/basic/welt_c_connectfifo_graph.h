#ifndef _welt_c_connectfifo_graph_h
#define _welt_c_connectfifo_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo.h"
#include "welt_c_graph.h"
#include "welt_c_file_source_decouple.h"
#include "welt_c_file_sink_decouple.h"
#include "welt_c_add_decouple.h"
#include "welt_c_util.h"

#define BUFFER_CAPACITY 1024

/* An enumeration of the actors in this application. */ 
#define ACTOR_XSOURCE   0
#define ACTOR_YSOURCE   1
#define ACTOR_ADD       2
#define ACTOR_SINK      3

#define FIFO_XSRC_ADD      0
#define FIFO_YSRC_ADD      1
#define FIFO_ADD_SNK       2

/* The total number of actors in the application. */
#define ACTOR_COUNT     4
#define FIFO_COUNT      3


/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with add objects. */
struct _welt_c_connectfifo_graph_context_struct;
typedef struct _welt_c_connectfifo_graph_context_struct
        welt_c_connectfifo_graph_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

welt_c_connectfifo_graph_context_type *welt_c_connectfifo_graph_new(
       char *x_file, char *y_file, char *out_file);

void welt_c_connectfifo_graph_terminate(
        welt_c_connectfifo_graph_context_type *context);

void welt_c_connectfifo_graph_scheduler(welt_c_connectfifo_graph_context_type *graph);  
        
        
#endif
