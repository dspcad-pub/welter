/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "welt_c_param_spec.h"

#define NAME_LENGTH 20

struct _welt_c_param_spec_context_struct {
#include "welt_c_graph_context_type_common.h"

int mode;

};

welt_c_param_spec_context_type *welt_c_param_spec_new(
        welt_c_actor_context_type *init, welt_c_actor_context_type *subinit,
	   welt_c_actor_context_type *body){
    
	int i;
    
    welt_c_param_spec_context_type *context = NULL;

    context = welt_c_util_malloc(sizeof(welt_c_param_spec_context_type));
	context->actor_count = ACTOR_COUNT_PARAM;
	context->actors = (welt_c_actor_context_type **)welt_c_util_malloc(
                    context->actor_count * sizeof(welt_c_actor_context_type *));
    context->descriptors = (char **)welt_c_util_malloc(
                        (context->actor_count) * sizeof(char *));
    for(i = 0; i < (context->actor_count); i++){
        context->descriptors[i] = (char *)welt_c_util_malloc(
                        NAME_LENGTH * sizeof(char));
    }
    strcpy(context->descriptors[INIT],"initial");
    strcpy(context->descriptors[SUBINIT],"subinitial");
    strcpy(context->descriptors[BODY],"body");

	context->actors[INIT] = init;
	context->actors[SUBINIT] = subinit;
	context->actors[BODY] = body;
	
	if(context->actors[INIT] != NULL) {
        context->mode = welt_c_PARAM_SPEC_MODE_HP_SETUP;
	}
    else if (context->actors[SUBINIT] != NULL) {
        context->mode = welt_c_PARAM_SPEC_MODE_P_SETUP;
	}
	context->scheduler = (welt_c_graph_scheduler_ftype)
	                                    welt_c_param_spec_scheduler;
										
    return context;
}

void welt_c_param_spec_scheduler(welt_c_param_spec_context_type *context){
		// The code here could be reduced to avoid code redundant later.	
		switch (context->mode) {
        case welt_c_PARAM_SPEC_MODE_HP_SETUP:
		    welt_c_util_guarded_execution(context->actors[INIT],
	            context->descriptors[INIT]);
			if (context->actors[SUBINIT] != NULL) {
			    welt_c_util_guarded_execution(context->actors[SUBINIT],
	            context->descriptors[SUBINIT]);
		        welt_c_util_guarded_execution(context->actors[BODY],
	            context->descriptors[BODY]);
			context->mode = welt_c_PARAM_SPEC_MODE_P_SETUP;
            }
            else {
				context->mode = welt_c_PARAM_SPEC_MODE_BODY;
			}			
	        break;
		case welt_c_PARAM_SPEC_MODE_P_SETUP:
            welt_c_util_guarded_execution(context->actors[SUBINIT],
	            context->descriptors[SUBINIT]);
		    welt_c_util_guarded_execution(context->actors[BODY],
	            context->descriptors[BODY]);
			context->mode = welt_c_PARAM_SPEC_MODE_P_SETUP;
		break;
		case welt_c_PARAM_SPEC_MODE_BODY:
		     welt_c_util_guarded_execution(context->actors[BODY],
	            context->descriptors[BODY]);
			context->mode = welt_c_PARAM_SPEC_MODE_BODY;
        default:
            break;
    }			
}

void welt_c_param_spec_terminate(
        welt_c_param_spec_context_type *context){
    free(context->actors);
    free(context->descriptors);
    free(context);
    
    return;

}



