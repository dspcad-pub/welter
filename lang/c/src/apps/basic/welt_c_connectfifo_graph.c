/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_connectfifo_graph.h"

#define NAME_LENGTH 20


struct _welt_c_connectfifo_graph_context_struct {
#include "welt_c_graph_context_type_common.h"
    char *m_file; 
    char *x_file; 
    char *y_file; 
    char *out_file;
    //char *descriptors[ACTOR_COUNT];

};



welt_c_connectfifo_graph_context_type *welt_c_connectfifo_graph_new(
        char *x_file, char *y_file, char *out_file){
    int token_size, i;
    
    welt_c_connectfifo_graph_context_type *context = NULL;

    context = welt_c_util_malloc(sizeof(welt_c_connectfifo_graph_context_type));
    context->actor_count = ACTOR_COUNT;
    context->actors = (welt_c_actor_context_type **)welt_c_util_malloc(
                context->actor_count * sizeof(welt_c_actor_context_type *));
    context->fifo_count = FIFO_COUNT;
    context->fifos = (welt_c_fifo_pointer *)welt_c_util_malloc(
                        context->fifo_count * 
                        sizeof(welt_c_fifo_pointer));
    context->descriptors = (char **)welt_c_util_malloc(
                        context->actor_count * sizeof(char *));
                        
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                            ACTOR_XSOURCE, "xsource decouple");                    
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                            ACTOR_YSOURCE, "ysource decouple");  
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                            ACTOR_ADD, "add decouple");  
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                            ACTOR_SINK, "sink decouple");  
                         

    context->x_file = x_file;
    context->y_file = y_file;
    context->out_file = out_file;

    token_size = sizeof(int);
    for(i = 0; i<context->fifo_count; i++){
        context->fifos[i] = (welt_c_fifo_pointer)welt_c_fifo_new
                            (BUFFER_CAPACITY, token_size, i);
    }
    
    /* Initialize source array and sink array */
    context->source_array = (welt_c_actor_context_type **)welt_c_util_malloc(
                context->fifo_count * sizeof(welt_c_actor_context_type *));

    context->sink_array = (welt_c_actor_context_type **)welt_c_util_malloc(
                context->fifo_count * sizeof(welt_c_actor_context_type *));
                
    /* Create and connect the actors. */
    i = 0;

    context->actors[ACTOR_XSOURCE] = (welt_c_actor_context_type
            *)(welt_c_file_source_decouple_new(context->x_file,
            /*context->fifos[FIFO_XSRC_ADD], */ACTOR_XSOURCE));
    welt_c_connect_fifo(context->actors[ACTOR_XSOURCE],0,&context->fifos[FIFO_XSRC_ADD]);
    welt_c_file_source_decouple_connect(context->actors[ACTOR_XSOURCE],
                                (welt_c_graph_context_type *)context);

    context->actors[ACTOR_YSOURCE] = (welt_c_actor_context_type
            *)(welt_c_file_source_decouple_new(context->y_file,
            /*context->fifos[FIFO_YSRC_ADD], */ACTOR_YSOURCE));
    welt_c_connect_fifo(context->actors[ACTOR_YSOURCE],0,&context->fifos[FIFO_YSRC_ADD]);
    welt_c_file_source_decouple_connect(context->actors[ACTOR_YSOURCE],
                                (welt_c_graph_context_type *)context);
    
    context->actors[ACTOR_ADD] = (welt_c_actor_context_type
            *)(welt_c_add_de_new(
            /*context->fifos[FIFO_XSRC_ADD], context->fifos[FIFO_YSRC_ADD],  
            context->fifos[FIFO_ADD_SNK], */ACTOR_ADD));
    printf("new connection in graph\n");
    welt_c_connect_fifo(context->actors[ACTOR_ADD],0,&context->fifos[FIFO_XSRC_ADD]);
    welt_c_connect_fifo(context->actors[ACTOR_ADD],1,&context->fifos[FIFO_YSRC_ADD]);
    welt_c_connect_fifo(context->actors[ACTOR_ADD],2,&context->fifos[FIFO_ADD_SNK]);
    welt_c_add_de_connect(context->actors[ACTOR_ADD],
                        (welt_c_graph_context_type *)context);

    context->actors[ACTOR_SINK] = (welt_c_actor_context_type *)
            (welt_c_file_sink_decouple_new(context->out_file/*,
            context->fifos[FIFO_ADD_SNK]*/, ACTOR_SINK));
    welt_c_connect_fifo(context->actors[ACTOR_SINK],0,&context->fifos[FIFO_ADD_SNK]);
    welt_c_file_sink_decouple_connect(context->actors[ACTOR_SINK],
                            (welt_c_graph_context_type *)context);
    
    context->scheduler = (welt_c_graph_scheduler_ftype)
                        welt_c_connectfifo_graph_scheduler;
    return context;
    
}

void welt_c_connectfifo_graph_terminate(
        welt_c_connectfifo_graph_context_type *context){
    int i;
    /* Terminate FIFO*/
    for(i = 0; i<context->fifo_count; i++){
        welt_c_fifo_free((welt_c_fifo_pointer)context->fifos[i]);
    }
    
    /* Terminate Actors*/
    welt_c_file_source_decouple_terminate((welt_c_file_source_decouple_context_type * )
                                context->actors[ACTOR_XSOURCE]);
    welt_c_file_source_decouple_terminate((welt_c_file_source_decouple_context_type * )
                                context->actors[ACTOR_YSOURCE]);
    welt_c_add_de_terminate((welt_c_add_de_context_type *)
                                (context->actors[ACTOR_ADD]));
    welt_c_file_sink_decouple_terminate((welt_c_file_sink_decouple_context_type * )
                                context->actors[ACTOR_SINK]);
    free(context->fifos);
    free(context->actors);
    free(context->descriptors);
    free(context);
    
    return;

}


void welt_c_connectfifo_graph_scheduler(welt_c_connectfifo_graph_context_type *graph){
    welt_c_util_simple_scheduler(graph->actors, graph->actor_count, 
                                graph->descriptors);
                                
    return;
}  
