#ifndef _welt_c_param_graph_h
#define _welt_c_param_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo.h"
#include "welt_c_graph.h"
#include "welt_c_util.h"


/*.h files of the actors:*/

#include "welt_c_util.h"

#define BUFFER_CAPACITY 1024

/* An enumeration of the subgraphs in parameterized dataflow graph. */
#define INIT      0
#define SUBINIT   1
#define BODY      2

/* The total number of subgraphs in the application. */
#define ACTOR_COUNT_PARAM     3

/* Parameterized graph modes */
#define welt_c_PARAM_SPEC_MODE_HP_SETUP        1
#define welt_c_PARAM_SPEC_MODE_P_SETUP         2
#define welt_c_PARAM_SPEC_MODE_BODY            3
 
/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with add objects. */
struct _welt_c_param_spec_context_struct;
typedef struct _welt_c_param_spec_context_struct
        welt_c_param_spec_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

welt_c_param_spec_context_type *welt_c_param_spec_new(
       welt_c_actor_context_type *init, welt_c_actor_context_type *subinit,
	   welt_c_actor_context_type *body);
	   
void welt_c_param_spec_scheduler(welt_c_param_spec_context_type *context);
	   
void welt_c_param_spec_terminate(
        welt_c_param_spec_context_type *context);


#endif
