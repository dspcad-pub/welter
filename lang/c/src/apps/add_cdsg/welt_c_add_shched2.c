/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>

#include "welt_c_add_shched2.h"
#include "welt_c_dsg_scheduler.h"

#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_add_graph.h"
#include "welt_c_cdsg_util.h"


struct _welt_c_add_shched2_context_struct {
    #include "welt_c_dsg_context_type_common.h"
    
};

welt_c_add_shched2_context_type *welt_c_add_shched2_new(
        welt_c_graph_context_type * contextApp, pthread_mutex_t *mutex,
        pthread_cond_t *cond){
    int i;
    
    welt_c_add_shched2_context_type * add_shched2 = NULL;
   
    add_shched2 = (welt_c_add_shched2_context_type *) welt_c_dsg_new(DSG_ACTOR_COUNT,2);

    welt_c_graph_context_type * context = NULL;
    context = (welt_c_graph_context_type*)welt_c_util_malloc(
                sizeof(welt_c_graph_context_type));
    
    /* Memory allocation foradd_shched2 */
    context->fifo_count = DSG_FIFO_COUNT;
    context->actor_count = DSG_ACTOR_COUNT;
    
    context->actors = (welt_c_actor_context_type **)welt_c_util_malloc
            (context->actor_count * sizeof(welt_c_actor_context_type *));
    context->fifos = (welt_c_fifo_pointer *)welt_c_util_malloc
            (context->fifo_count * sizeof(welt_c_fifo_pointer));
    context->descriptors = (char **)welt_c_util_malloc(
           context->actor_count * sizeof(char*));
    
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_LAE1, "lae1");
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_XREF, "x_ref");
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_YREF, "y_ref");
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_SND, "snd");
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_LAE2, "lae2");
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_REC, "rec");
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_ADDSCA, "sca_add");
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_ADDREF, "ref_add");
    welt_c_graph_set_actor_desc((welt_c_graph_context_type *)context,
                                DSG_ACTOR_SNKREF, "ref_snk");
    
    context->source_array = (welt_c_actor_context_type **)
            welt_c_util_malloc(context->fifo_count *
            sizeof(welt_c_actor_context_type *));
    
    context->sink_array = (welt_c_actor_context_type **)
            welt_c_util_malloc(context->fifo_count *
            sizeof(welt_c_actor_context_type *));
    
    int token_size = sizeof(int);
     for (i =0; i<DSG_FIFO_COUNT; i++){
            context->fifos[i] = (welt_c_fifo_pointer)
                welt_c_fifo_unit_size_new(token_size, i);
    }
    
    /* DSG actor LAE1 */
    context->actors[DSG_ACTOR_LAE1] = (welt_c_actor_context_type *)(welt_c_sca_sch_loop_new(
            context->fifos[DSG_FIFO_SND_LAE1] ,
            context->fifos[DSG_FIFO_LAE1_XREF], 1, DSG_ACTOR_LAE1));
    welt_c_dsg_add_actor(SCA_SCH_LOOP,(welt_c_graph_context_type *) context, context->actors[DSG_ACTOR_LAE1], DSG_ACTOR_LAE1);
    welt_c_dsg_set_first_actor((welt_c_dsg_context_type*)add_shched2, context->actors[DSG_ACTOR_LAE1],DSG_ACTOR_LAE1);

    /* DSG actor Xsource ref */
    context->actors[DSG_ACTOR_XREF] = (welt_c_actor_context_type *)(welt_c_ref_actor_new(
           context->fifos[DSG_FIFO_LAE1_XREF] ,
           context->fifos[DSG_FIFO_XREF_YREF], DSG_ACTOR_XREF,
        contextApp->actors[ACTOR_XSOURCE], 0, 0, NULL, NULL, NULL ));
    welt_c_dsg_add_actor(REF,(welt_c_graph_context_type *) context, context->actors[DSG_ACTOR_XREF], DSG_ACTOR_XREF);

    /* DSG actor Ysource ref */
    context->actors[DSG_ACTOR_YREF] = (welt_c_actor_context_type *)(welt_c_ref_actor_new(
           context->fifos[DSG_FIFO_XREF_YREF],
           context->fifos[DSG_FIFO_YREF_SND], DSG_ACTOR_YREF,
            contextApp->actors[ACTOR_YSOURCE], 0, 0, NULL, NULL, NULL ));
    welt_c_dsg_add_actor(REF, context, context->actors[DSG_ACTOR_YREF], DSG_ACTOR_YREF);
    
    /* DSG actor SND */
    context->actors[DSG_ACTOR_SND] = (welt_c_actor_context_type *)(welt_c_snd_actor_new(
            context->fifos[DSG_FIFO_YREF_SND],
            context->fifos[DSG_FIFO_SND_LAE1],
            context->fifos[DSG_FIFO_SND_REC] ,
            mutex, cond,
        contextApp->fifos[FIFO_YSRC_ADD], 1, DSG_ACTOR_SND));
    welt_c_dsg_add_actor(SND,(welt_c_graph_context_type *) context, context->actors[DSG_ACTOR_SND], DSG_ACTOR_SND);

    /* DSG actor LAE2 */

    context->actors[DSG_ACTOR_LAE2] = (welt_c_actor_context_type *)(welt_c_sca_sch_loop_new(
            context->fifos[DSG_FIFO_SNKREF_LAE2] ,
            context->fifos[DSG_FIFO_LAE2_REC], 1, DSG_ACTOR_LAE2));
    welt_c_dsg_add_actor(SCA_SCH_LOOP,(welt_c_graph_context_type *) context, context->actors[DSG_ACTOR_LAE2], DSG_ACTOR_LAE2);
    welt_c_dsg_set_first_actor((welt_c_dsg_context_type*)add_shched2, context->actors[DSG_ACTOR_LAE2],DSG_ACTOR_LAE2);

    /* DSG actor REC */
    context->actors[DSG_ACTOR_REC] = (welt_c_actor_context_type *) (welt_c_rec_actor_new(
            context->fifos[DSG_FIFO_SND_REC],
            context->fifos[DSG_FIFO_LAE2_REC],
            context->fifos[DSG_FIFO_REC_ADDSCA],
            mutex, cond, contextApp->fifos[FIFO_YSRC_ADD], 1, DSG_ACTOR_REC));
    welt_c_dsg_add_actor(REC,(welt_c_graph_context_type *) context, context->actors[DSG_ACTOR_REC], DSG_ACTOR_REC);
    
    /* DSG actor addSCA */
    context->actors[DSG_ACTOR_ADDSCA] =(welt_c_actor_context_type *) (welt_c_sca_static_loop_new(
           context->fifos[DSG_FIFO_REC_ADDSCA],
           context->fifos[DSG_FIFO_ADDREF_ADDSCA],
           context->fifos[DSG_FIFO_ADDSCA_ADDREF],
           context->fifos[DSG_FIFO_ADDSCA_SNKREF],
            1, DSG_ACTOR_ADDSCA));
    welt_c_dsg_add_actor(SCA_STATIC_LOOP,(welt_c_graph_context_type *) context, context->actors[DSG_ACTOR_ADDSCA], DSG_ACTOR_ADDSCA);
    
    /* DSC actor addRef */
    context->actors[DSG_ACTOR_ADDREF] = (welt_c_actor_context_type *)(welt_c_ref_actor_new(
           context->fifos[DSG_FIFO_ADDSCA_ADDREF],
           context->fifos[DSG_FIFO_ADDREF_ADDSCA], DSG_ACTOR_ADDREF,
            contextApp->actors[ACTOR_ADD], 0, 0, NULL, NULL, NULL ));
    welt_c_dsg_add_actor(REF,(welt_c_graph_context_type *) context, context->actors[DSG_ACTOR_ADDREF], DSG_ACTOR_ADDREF);
    
    /* DSG actor sinkREF */
    context->actors[DSG_ACTOR_SNKREF] = (welt_c_actor_context_type *)(welt_c_ref_actor_new(
       context->fifos[DSG_FIFO_ADDSCA_SNKREF],
       context->fifos[DSG_FIFO_SNKREF_LAE2], DSG_ACTOR_SNKREF,
        contextApp->actors[ACTOR_SINK], 0, 0, NULL, NULL, NULL ));
    welt_c_dsg_add_actor(REF,(welt_c_graph_context_type *) context, context->actors[DSG_ACTOR_SNKREF], DSG_ACTOR_SNKREF);
    
    context->scheduler = (welt_c_graph_scheduler_ftype)
            welt_c_add_shched2_scheduler;
    
    /* Delay */
    welt_c_fifo_write_advance((welt_c_fifo_pointer)context->fifos[DSG_FIFO_SND_LAE1]);
    welt_c_fifo_write_advance((welt_c_fifo_pointer)context->fifos[DSG_FIFO_SNKREF_LAE2]);
  
    add_shched2->sdsg = context;
   
    return add_shched2;
}



void welt_c_add_shched2_scheduler(welt_c_add_shched2_context_type *graph, int thread_ind, int thread_num){
    
    int start_ind;
    int end_ind;
    start_ind = graph->first_actors_index[thread_ind];
    if (thread_ind != thread_num -1){
        end_ind =graph->first_actors_index[ thread_ind+1 ] - 1;
    }else{
        end_ind = graph->dsg_count -1;
    }
    
    /*simple scheduler*/
    //welt_c_util_cdsg_scheduler( graph->sdsg->actors, start_ind, end_ind, graph->sdsg->descriptors );
    /*dtt scheduler*/
    welt_c_util_dsg_optimized_scheduler( graph->sdsg, start_ind, end_ind-start_ind + 1);
    
}
