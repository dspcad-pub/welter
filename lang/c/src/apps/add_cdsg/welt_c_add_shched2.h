#ifndef _welt_c_add_shched2_h
#define _welt_c_add_shched2_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_dsg.h"
#include "welt_c_ref_actor.h"
#include "welt_c_sca_sch_loop.h"
#include "welt_c_sca_static_loop.h"
#include "welt_c_rec_actor.h"
#include "welt_c_snd_actor.h"
#include "welt_c_util.h"
#include "welt_c_fifo.h"
#include "welt_c_fifo.h"

#define DSG_ACTOR_LAE1 0
#define DSG_ACTOR_XREF 1
#define DSG_ACTOR_YREF 2
#define DSG_ACTOR_SND 3
#define DSG_ACTOR_LAE2 4
#define DSG_ACTOR_REC 5
#define DSG_ACTOR_ADDSCA 6
#define DSG_ACTOR_ADDREF 7
#define DSG_ACTOR_SNKREF 8
#define DSG_ACTOR_COUNT 9

#define DSG_FIFO_LAE1_XREF 0
#define DSG_FIFO_XREF_YREF 1
#define DSG_FIFO_YREF_SND 2
#define DSG_FIFO_SND_LAE1 3
#define DSG_FIFO_SND_REC 4
#define DSG_FIFO_LAE2_REC 5
#define DSG_FIFO_REC_ADDSCA 6
#define DSG_FIFO_ADDSCA_ADDREF 7
#define DSG_FIFO_ADDREF_ADDSCA 8
#define DSG_FIFO_ADDSCA_SNKREF 9
#define DSG_FIFO_SNKREF_LAE2 10
#define DSG_FIFO_COUNT 11

#define NAME_LENGTH 20

#define ACTOR_XSOURCE   0
#define ACTOR_YSOURCE   1
#define ACTOR_ADD       2
#define ACTOR_SINK      3

#define FIFO_XSRC_ADD      0
#define FIFO_YSRC_ADD      1
#define FIFO_ADD_SNK       2


typedef struct _welt_c_add_shched2_context_struct
        welt_c_add_shched2_context_type;

welt_c_add_shched2_context_type *welt_c_add_shched2_new(
        welt_c_graph_context_type * contextApp, pthread_mutex_t *mutex,
        pthread_cond_t *cond);
void welt_c_add_shched2_scheduler(welt_c_add_shched2_context_type *graph, int thread_ind, int thread_num);

#endif
