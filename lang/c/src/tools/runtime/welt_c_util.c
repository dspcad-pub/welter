/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2017
Maryland DSPCAD Research Group, The University of Maryland at College Park

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "welt_c_graph.h"
#include "welt_c_util.h"

/* Open a file. Exit with an error message if the file cannot
   be opened with the specified mode ("r", "w", etc. as in
   coventional fopen). Otherwise return the file pointer of
   the opened file.
*/
FILE *welt_c_util_fopen(const char *fname, const char *mode) {
    FILE *fp;

    if ((fp=fopen(fname, mode))==NULL) {
        fprintf(stderr, "could not open file named '%s' with mode '%s'",
                fname, mode);
        exit(1);
    }
    return fp;
}

void *welt_c_util_malloc(size_t size) {
    void *p;

    if ((p = malloc(size)) != NULL) {
        return(p);
    } else {
        fprintf(stderr, "welt_c_util_malloc error: insufficient memory");
        exit(1);
    }
    return NULL;  /*NOTREACHED*/
}

bool welt_c_util_guarded_execution(welt_c_actor_context_type *context,
                                      char *descriptor) {

    if (context->enable(context)) {
        context->invoke(context);
        printf("%s visit complete.\n", descriptor);
        return true;
    } else {
        return false;
    }
}

void welt_c_util_simple_scheduler(welt_c_actor_context_type *actors[],
                                  int actor_count, char *descriptors[]) {

    bool progress = false;
    int i = 0;

    do {
        progress = 0;
        for (i = 0; i < actor_count; i++) {
            progress |=
                    welt_c_util_guarded_execution(actors[i], descriptors[i]);
        }
    } while (progress);
}


void welt_c_graph_set_actor_desc(welt_c_graph_context_type *graph, int index,
                                 const char* desc){
    int length;
    length = strlen(desc)+1;
    graph->descriptors[index] = (char *)welt_c_util_malloc(
            length * sizeof(char));
    strcpy(graph->descriptors[index], desc);
    return;
}
