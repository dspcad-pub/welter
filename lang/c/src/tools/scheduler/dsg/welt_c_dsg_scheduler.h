#ifndef _welt_c_dsg_schedule_h
#define _welt_c_dsg_schedule_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "welt_c_actor.h"
#include "welt_c_graph.h"
#include "welt_c_dsg_actor.h"

bool welt_c_util_dsg_dtt_scheduler(welt_c_graph_context_type* context,
    int start_idx, int end_idx, int actor_count, int *fifo_sink);

bool welt_c_util_dsg_optimised_scheduler(
        welt_c_graph_context_type* context, int start_index, int actor_count);

bool welt_c_util_dsg_optimized_scheduler(
        welt_c_graph_context_type* context, int start_index, int actor_count);
/*
void welt_c_utl_simple_scheduler(welt_c_graph_context_type* context);

bool welt_c_util_dsg_optimised_scheduler_with_loop_cluster(
    welt_c_graph_context_type* context);

bool welt_c_util_dsg_optimised_scheduler_with_cluster(
    welt_c_graph_context_type* context);

bool welt_c_util_dsg_optimised_scheduler_with_loop(
    welt_c_graph_context_type* context);

bool welt_c_utl_guarded_execution(welt_c_actor_context_type* context);
*/
#endif
