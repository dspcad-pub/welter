/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include "welt_c_dsg_scheduler.h"

bool welt_c_util_dsg_dtt_scheduler(welt_c_graph_context_type* context,
    int fifo_delay_idx, int fifo_start_idx, int fifo_end_idx, int *actor_fifo_sink){

    int fifo_next_idx = fifo_delay_idx;
    welt_c_fifo_pointer fifo_next;
    welt_c_dsg_actor_context_type* nextDsg;

    while(1){

        /* 1. Find actor and Invoke nextDSG */
        nextDsg=(welt_c_dsg_actor_context_type *)context->actors[actor_fifo_sink[fifo_next_idx]];
        nextDsg->invoke((welt_c_actor_context_type*)nextDsg);
        
        /* 2. next_fifo = next_fifo + 1; */
        if (fifo_next_idx == fifo_end_idx) fifo_next_idx = fifo_start_idx;
        else fifo_next_idx++;

        /* 3. Check if the next fifo has a token or not. */
        fifo_next = (welt_c_fifo_pointer)context->fifos[fifo_next_idx];
        if (welt_c_fifo_capacity(fifo_next)==0) break;
    }

    return true;

}

/* 
int get_index()
*/

bool welt_c_util_dsg_optimised_scheduler(
        welt_c_graph_context_type* context, int start_index, int actor_count){
	int fifo_index;
	welt_c_fifo_pointer last_fifo;
	welt_c_dsg_actor_context_type* nextDsg;
        welt_c_dsg_actor_context_type** array = (welt_c_dsg_actor_context_type**)
        welt_c_graph_get_sink_array(context);

	if(start_index >=  actor_count){
        return false;
    }

        
    nextDsg=(welt_c_dsg_actor_context_type *)
        (context->actors[start_index]);
    
    while(1){
        nextDsg->invoke((welt_c_actor_context_type*)nextDsg);
        if(((nextDsg)->last_fifo)==NULL){
        	break;
        }
        last_fifo = (welt_c_fifo_pointer)nextDsg->last_fifo;
//welt_c_dsg_actor_get_last_fifo(
  //                                          nextDsg);
        fifo_index = last_fifo->get_index(last_fifo);
        nextDsg=array[fifo_index];
    }
    return true;
}

bool welt_c_util_dsg_optimized_scheduler(
        welt_c_graph_context_type* context, int start_index, int actor_count){
    int fifo_index;
    welt_c_fifo_pointer last_fifo;
    welt_c_dsg_actor_context_type* nextDsg;
    
    welt_c_dsg_actor_context_type** array = (welt_c_dsg_actor_context_type**)
        welt_c_graph_get_sink_array(context);
	if(start_index >=  actor_count){
        return false;
    }

        
    nextDsg=(welt_c_dsg_actor_context_type *)
        (context->actors[start_index]);
    
    while(1){
        
        nextDsg->invoke((welt_c_actor_context_type*)nextDsg);
        printf("invoke,%d\n",fifo_index);
        if(((nextDsg)->last_fifo)==NULL){
            break;
        }
        last_fifo = (welt_c_fifo_pointer)(nextDsg->last_fifo);
	//welt_c_dsg_actor_get_last_fifo(
        //                                    nextDsg);
        fifo_index = last_fifo->get_index(last_fifo);
        nextDsg=array[fifo_index];
    }
    return true;
}
/*
bool welt_c_util_dsg_optimised_scheduler_with_loop_cluster(
    welt_c_graph_context_type*context){


    welt_c_dsg_actor_context_type* nextDsg;
    bool prevDsgLoop;welt_c_dsg_actor_context_type** array =
        welt_c_dsg_jitter_sch1_graph_get_sink_array(context);
    int * type =welt_c_dsg_jitter_sch1_graph_get_type_array(context);
    welt_c_dsg_jitter_sch1_graph_context_type *dsgContext;
    int value = 1,i;
    int lastLoopIter;
    nextDsg=(welt_c_dsg_actor_context_type *)
        (context->actors[DSG_ACTOR_STARTLOOP]);
    prevDsgLoop=0;
    while(1){
        if(prevDsgLoop){
            i = lastLoopIter;
            while(i!=0){
                if (nextDsg->index == DSG_ACTOR_RDVL){
                    nextDsg->invoke(context->actors[DSG_ACTOR_RDVL]);
                    nextDsg->invoke(context->actors[DSG_ACTOR_RSTR]);
                    nextDsg->invoke(context->actors[DSG_ACTOR_RFSM]);
                    nextDsg->invoke(context->actors[DSG_ACTOR_RTRT]);
                    nextDsg->invoke(context->actors[DSG_ACTOR_RRE]);
                    nextDsg->invoke(context->actors[DSG_ACTOR_RRRE]);
                    nextDsg->invoke(context->actors[DSG_ACTOR_RLFT]);

                }
                else{
                    nextDsg->invoke((welt_c_actor_context_type *)nextDsg);
                }
                i--;
                }


            if (nextDsg->index == DSG_ACTOR_RDVL){
                nextDsg = array[welt_c_fifo_unit_size_get_index(context->actors[DSG_ACTOR_RLFT]->last_fifo)]
                    //welt_c_dsg_actor_get_last_fifo((
                    //welt_c_dsg_actor_context_type*)
                    //context->actors[DSG_ACTOR_RLFT]))];

            }
            else{
                nextDsg = array[welt_c_fifo_unit_size_get_index((nextDsg->last_fifo));
                    //welt_c_dsg_actor_get_last_fifo((nextDsg)))];
            }

            if (type[nextDsg->index]==DSG_SCA_DYN_LOOP){
                welt_c_sca_dynamic_loop_set_current_iteration((
                    welt_c_sca_dynamic_loop_context_type*)nextDsg,0);
            }

            if (type[nextDsg->index]==DSG_SCA_STATIC_LOOP){
                welt_c_sca_static_loop_set_current_iteration((
                    welt_c_sca_static_loop_context_type*)nextDsg,0);
            }
            prevDsgLoop=0;
            nextDsg->invoke((welt_c_actor_context_type*)nextDsg);
            nextDsg=array[welt_c_fifo_unit_size_get_index((nextDsg->last_fifo))];
                //welt_c_dsg_actor_get_last_fifo(nextDsg))];
            continue;
        }

        nextDsg->invoke((welt_c_actor_context_type*)nextDsg);

        if(((nextDsg)->last_fifo)==NULL){
        	break;
        }
        if (type[nextDsg->index]==DSG_SCA_DYN_LOOP){
            prevDsgLoop=1;
            lastLoopIter=welt_c_sca_dynamic_loop_get_current_iteration((
                welt_c_sca_dynamic_loop_context_type*)nextDsg);
        }

        else if (type[nextDsg->index]==DSG_SCA_STATIC_LOOP){
            prevDsgLoop=1;
            lastLoopIter=welt_c_sca_static_loop_get_current_iteration((
                welt_c_sca_static_loop_context_type*)nextDsg);
        }
        else{
            prevDsgLoop=0;

        }
        nextDsg=array[welt_c_fifo_unit_size_get_index(
            welt_c_dsg_actor_get_last_fifo(nextDsg))];
    }
    return true;
}

bool welt_c_util_dsg_optimised_scheduler_with_cluster(
    welt_c_graph_context_type*context){


    welt_c_dsg_actor_context_type* nextDsg;
    bool prevDsgLoop;welt_c_dsg_actor_context_type** array =
        welt_c_dsg_jitter_sch1_graph_get_sink_array(context);
    int * type =welt_c_dsg_jitter_sch1_graph_get_type_array(context);
    welt_c_dsg_jitter_sch1_graph_context_type *dsgContext;
    int value = 1,i;
    int lastLoopIter;
    nextDsg=(welt_c_dsg_actor_context_type *)
        (context->actors[DSG_ACTOR_STARTLOOP]);
    prevDsgLoop=0;
    while(1){

        if (nextDsg->index == DSG_ACTOR_RDVL){
            nextDsg->invoke(context->actors[DSG_ACTOR_RDVL]);
            nextDsg->invoke(context->actors[DSG_ACTOR_RSTR]);
            nextDsg->invoke(context->actors[DSG_ACTOR_RFSM]);
            nextDsg->invoke(context->actors[DSG_ACTOR_RTRT]);
            nextDsg->invoke(context->actors[DSG_ACTOR_RRE]);
            nextDsg->invoke(context->actors[DSG_ACTOR_RRRE]);
            nextDsg->invoke(context->actors[DSG_ACTOR_RLFT]);
            nextDsg = array[welt_c_fifo_unit_size_get_index(
                welt_c_dsg_actor_get_last_fifo((
                welt_c_dsg_actor_context_type*)
                context->actors[DSG_ACTOR_RLFT]))];

        }
        nextDsg->invoke((welt_c_actor_context_type*)nextDsg);

        if(((nextDsg)->last_fifo)==NULL){
        	break;
        }
        nextDsg=array[welt_c_fifo_unit_size_get_index(
            welt_c_dsg_actor_get_last_fifo(nextDsg))];
    }
    return true;
}

bool welt_c_utl_guarded_execution(welt_c_actor_context_type*context){
    if (context->enable(context)) {
        context->invoke(context);
        return true;
    } else {
        return false;
    }
}
void welt_c_utl_simple_scheduler(welt_c_graph_context_type* context){
    bool progress = false;
    int i = 0;
    int value = 1;
    welt_c_actor_context_type* nextDsg;
    nextDsg=context->actors[DSG_ACTOR_STARTLOOP];
    welt_c_fifo_unit_size_write((welt_c_fifo_unit_size_pointer)
        context->fifos[DSG_FIFO_E15],&value);
    bool holder;
    do {
        progress = 0;
        for (i = 0; i < context->actor_count; i++) {
            progress |=welt_c_utl_guarded_execution(context->actors[i]);
        }
    } while (progress);
}*/
