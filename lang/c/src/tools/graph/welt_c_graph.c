/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2016
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "welt_c_graph.h"
#include "welt_c_util.h"


/*Update actor count*/
void welt_c_graph_add_actor(struct welt_c_graph_context_struct *graph){
    graph->actor_count++;
    return;
} 
        

/*Initialize sink_array and source array*/        
void welt_c_graph_add_connection(
        struct welt_c_graph_context_struct *graph,
        struct welt_c_actor_context_struct *context, int port_index,
        int direction){
    welt_c_fifo_pointer *p;
    int fifo_index;
    p = (welt_c_fifo_pointer *)context->portrefs[port_index];
    fifo_index = (*p)->get_index((*p));
    if(direction == GRAPH_IN_CONN_DIRECTION){
        graph->sink_array[fifo_index] = context;
    }else if(direction == GRAPH_OUT_CONN_DIRECTION){
        graph->source_array[fifo_index] = context;
    }else{
        fprintf(stderr, "direction of the port is neither input nor output\n");
    }    
    return;
}      

/*Update port connection in the actor and then the graph*/
void welt_c_graph_update_connection(
        struct welt_c_graph_context_struct *graph,
        struct welt_c_actor_context_struct *context, int port_index,
        int fifo_index, int direction){
    welt_c_fifo_pointer new_fifo;
    welt_c_fifo_pointer *p;
    int fifo_gr_index;
    new_fifo = graph->fifos[fifo_index];
    welt_c_actor_set_portref(context, port_index, new_fifo);
    p = (welt_c_fifo_pointer *)context->portrefs[port_index];
    fifo_gr_index = (*p)->get_index((*p));
    if(direction == GRAPH_IN_CONN_DIRECTION){
        graph->sink_array[fifo_gr_index] = context;
    }else if(direction == GRAPH_OUT_CONN_DIRECTION){
        graph->source_array[fifo_gr_index] = context;
    }else{
        fprintf(stderr, "direction of the port is neither input nor output\n");
    }  
    return;     
} 


        

void welt_c_graph_set_actor(struct welt_c_graph_context_struct *graph,
        int index, welt_c_actor_context_type *actor_context){
    graph->actors[index] = actor_context;
    return;
}   
        

welt_c_actor_context_type *welt_c_graph_get_actor(
        struct welt_c_graph_context_struct *graph, int index){
    return graph->actors[index];
}       
        

void welt_c_graph_set_fifo(struct welt_c_graph_context_struct *graph,
        int index, welt_c_fifo_pointer fifo){
    graph->fifos[index] = fifo;
    return;
 }         
        

welt_c_fifo_pointer welt_c_graph_get_fifo(
        struct welt_c_graph_context_struct *graph, int index){
    return graph->fifos[index];
}  
        
void welt_c_graph_set_actor_count(
        struct welt_c_graph_context_struct *graph, int count){
    graph->actor_count = count;
    return;
}           

int welt_c_graph_actor_count(struct welt_c_graph_context_struct *graph){
    return graph->actor_count;
}  

  
int welt_c_graph_fifo_count(struct welt_c_graph_context_struct *graph){
    return graph->fifo_count;
} 

void welt_c_graph_set_scheduler(struct welt_c_graph_context_struct *graph,
        welt_c_graph_scheduler_ftype scheduler_ftype){
    graph->scheduler = scheduler_ftype;
    return;
}   

welt_c_graph_scheduler_ftype welt_c_graph_get_scheduler(
        struct welt_c_graph_context_struct *graph){
    return graph->scheduler;
} 


welt_c_actor_context_type **welt_c_graph_get_sink_array(
        struct welt_c_graph_context_struct *graph){
    return graph->sink_array;
}

welt_c_actor_context_type **welt_c_graph_get_source_array(
        struct welt_c_graph_context_struct *graph){
    return graph->source_array;
}

void welt_c_graph_add_fifo(struct welt_c_graph_context_struct *graph){
    graph->fifo_count++;
    return;
}
