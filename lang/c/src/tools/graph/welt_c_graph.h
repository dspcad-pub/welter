#ifndef _welt_c_graph_h
#define _welt_c_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include "welt_c_basic.h"
#include "welt_c_actor.h"
#include "welt_c_fifo.h"
#include "welt_c_graph_def.h"

#define GRAPH_IN_CONN_DIRECTION  0   /* input port of an actor*/
#define GRAPH_OUT_CONN_DIRECTION 1   /* output port of an actor*/


/*****************************************************************************
A pointer to a "welt_c_graph_scheduler", which is a function that execute the
dataflow graph
*****************************************************************************/
typedef void (*welt_c_graph_scheduler_ftype)
        (struct welt_c_graph_context_struct *graph);

/*****************************************************************************
Adds an actor in the graph: update the actor count
*****************************************************************************/
void welt_c_graph_add_actor(struct welt_c_graph_context_struct *graph);
        
/*****************************************************************************
Set connections between actors in the graph: set up the portrefs arrays, 
as well as data structures such as the source and sink tables.  
*****************************************************************************/
void welt_c_graph_add_connection(
        struct welt_c_graph_context_struct *graph,
        struct welt_c_actor_context_struct *context, int port_index,
        int direction);        

/*****************************************************************************
Update connections between actors in the graph: set up the portrefs arrays, 
as well as data structures such as the source and sink tables.  
*****************************************************************************/
void welt_c_graph_update_connection(
    struct welt_c_graph_context_struct *graph,
        struct welt_c_actor_context_struct *context, int port_index,
        int fifo_index, int direction);  

        
/*****************************************************************************
Set a actor in the graph.
*****************************************************************************/
void welt_c_graph_set_actor(struct welt_c_graph_context_struct *graph,
        int index, welt_c_actor_context_type *actor_context);
        
/*****************************************************************************
Get a actor context in the graph.
*****************************************************************************/
welt_c_actor_context_type *welt_c_graph_get_actor(
        struct welt_c_graph_context_struct *graph, int index);

/*****************************************************************************
Add a FIFO in the graph: update the fifo count
*****************************************************************************/
void welt_c_graph_add_fifo(struct welt_c_graph_context_struct *graph);
        
        
/*****************************************************************************
Set a fifo pointer in the graph.
*****************************************************************************/
void welt_c_graph_set_fifo(struct welt_c_graph_context_struct *graph,
        int index, welt_c_fifo_pointer fifo);
        
/*****************************************************************************
Get a fifo pointer in the graph.
*****************************************************************************/
welt_c_fifo_pointer welt_c_graph_get_fifo(
        struct welt_c_graph_context_struct *graph, int index);
      

/*****************************************************************************
Get actor count in the graph.
*****************************************************************************/
int welt_c_graph_actor_count(struct welt_c_graph_context_struct *graph);

/*****************************************************************************
Get fifo count in the graph.
*****************************************************************************/
int welt_c_graph_fifo_count(struct welt_c_graph_context_struct *graph);

 /*****************************************************************************
Set scheduler function pointer in the graph.
*****************************************************************************/
void welt_c_graph_set_scheduler(struct welt_c_graph_context_struct *graph,
        welt_c_graph_scheduler_ftype scheduler_ftype);

/*****************************************************************************
Get scheduler function pointer in the graph.
*****************************************************************************/
welt_c_graph_scheduler_ftype welt_c_graph_get_scheduler(
        struct welt_c_graph_context_struct *graph);

/*****************************************************************************
Get sink array pointer in the graph.
*****************************************************************************/
welt_c_actor_context_type **welt_c_graph_get_sink_array(
        struct welt_c_graph_context_struct *graph);

/*****************************************************************************
Get sink array pointer in the graph.
*****************************************************************************/
welt_c_actor_context_type **welt_c_graph_get_source_array(
        struct welt_c_graph_context_struct *graph);
        
struct welt_c_graph_context_struct {
#include "welt_c_graph_context_type_common.h"
};


#endif
