#ifndef _welt_c_dsg_h
#define _welt_c_dsg_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdarg.h>
#include "welt_c_basic.h"
#include "welt_c_graph_def.h"
#include "welt_c_fifo.h"


#include "welt_c_ref_actor.h"
#include "welt_c_sca_sch_loop.h"
#include "welt_c_sca_static_loop.h"
#include "welt_c_rec_actor.h"
#include "welt_c_snd_actor.h"


#define REF 0
#define SCA_SCH_LOOP 1
#define SCA_STATIC_LOOP 2
#define SND 3
#define REC 4


//struct _welt_c_dsg_context_struct;
typedef struct _welt_c_dsg_context_struct
        welt_c_dsg_context_type;

typedef void (* welt_c_dsg_add_actor_ftype) (welt_c_dsg_actor_context_type *context);

welt_c_dsg_context_type* welt_c_dsg_new(int dsg_count, int thread_count);

void welt_c_dsg_add_actor(int dsg_actor_type, welt_c_graph_context_type* sdsg,
                          welt_c_actor_context_type *actor , int index);

void welt_c_dsg_set_first_actor(welt_c_dsg_context_type* context,
                                welt_c_actor_context_type *actor, int actor_index );

struct _welt_c_dsg_context_struct{
    
    /* sdsg graph */
    struct welt_c_graph_context_struct * sdsg;
    /* the number of dsg actors*/
    int dsg_count;
    int thread_count;
    /* first actors for each sdsg */
    welt_c_actor_context_type ** first_actors;
    int * first_actors_index;
    int * cdsg_actor_num;
    int ind;
    
    /* actor index in sdsg */
    int * actor_to_sdsg;
    /* edge index in sdsg */
    int * edge_to_sdsg;
    
    welt_c_dsg_add_actor_ftype add_actor;
};

#endif
