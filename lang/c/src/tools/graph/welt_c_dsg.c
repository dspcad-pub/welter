/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2016
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "welt_c_actor.h"
#include "welt_c_util.h"
#include "welt_c_dsg.h"

welt_c_dsg_context_type* welt_c_dsg_new(int dsg_count, int thread_count){
    
    welt_c_dsg_context_type * context = NULL;
    context = (welt_c_dsg_context_type *)welt_c_util_malloc
            (sizeof(welt_c_dsg_context_type));
    
    /* initialize the sdsg */
    context -> sdsg = NULL;
    
    /* number of actors in dsg */
    context -> dsg_count = dsg_count;
    /* number of actors in thread */
    context -> thread_count = thread_count;

    /* arrays of first actors of each sdsg */
    context -> first_actors = (welt_c_actor_context_type **)
            welt_c_util_malloc(sizeof(welt_c_actor_context_type *)*thread_count);
    
    context -> first_actors_index = (int*)
            welt_c_util_malloc(sizeof(int)*thread_count);
    
    context -> ind = 0;

    /* adding methods */
    context->add_actor = (welt_c_dsg_add_actor_ftype)welt_c_dsg_add_actor;
    
    return context;

}

void welt_c_dsg_add_actor(int dsg_actor_type, welt_c_graph_context_type* sdsg,
        welt_c_actor_context_type *actor , int index){
    //printf("actor index set\n");

    /* set index for the actor */
    welt_c_actor_set_index(actor,index);
    
    switch (dsg_actor_type){
        case REF :
            welt_c_ref_actor_connect((welt_c_actor_context_type *) actor,(welt_c_graph_context_type *)sdsg);
            break;
        case SCA_SCH_LOOP :
            welt_c_sca_sch_loop_connect((welt_c_actor_context_type *) actor,(welt_c_graph_context_type *)sdsg);
            break;
        case SCA_STATIC_LOOP :
            welt_c_sca_static_loop_connect((welt_c_actor_context_type *) actor,(welt_c_graph_context_type *)sdsg);
            break;
        case SND :
            welt_c_snd_actor_connect((welt_c_actor_context_type *) actor,(welt_c_graph_context_type *)sdsg);
            break;
        case REC :
            welt_c_rec_actor_connect((welt_c_actor_context_type *) actor,(welt_c_graph_context_type *)sdsg);
            break;
    }

}

void welt_c_dsg_set_first_actor(welt_c_dsg_context_type* context,
        welt_c_actor_context_type *actor, int actor_index ){
    context -> first_actors[context->ind] = actor ;
    context -> first_actors_index[context->ind] = actor_index;
    context -> ind ++;
}

/*int welt_c_dsg_get_start_index(welt_c_add_shched2_context_type *graph, int thread_ind, int thread_num){
    return graph->first_actors_index[thread_ind];
}

int welt_c_dsg_get_end_index(welt_c_add_shched2_context_type *graph, int thread_ind, int thread_num){
    if (thread_ind != thread_num -1){
        return graph->first_actors_index[ thread_ind+1 ] - 1;
    }else{
        return graph->dsg_count -1;
    }
    
}*/
