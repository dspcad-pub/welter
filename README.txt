-------------------------------------------------------------------------
@ddblock_begin copyright
----------------------------------------------------------------------------
Copyright (c) 1997-2020
Maryland DSPCAD Research Group, The University of Maryland at College Park 
All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
---------------------------------------------------------------------------
@ddblock_end copyright
-------------------------------------------------------------------------

Welterweight dataflow.

This DSPCAD Framework (DFW) package provides significant extensions to LIDE
that provide additional features for dataflow actor and graph design. The
features trade-off some amount of "lightweight" structure for additional
convenience and capabilities in actor/graph design.

Two key types of extensions are support for passive-active flowgraph (PAFG)
modeling, and more extensive support for actor, edge and graph implementation 
in C++. More information about PAFG modeling can be found in the following
publication:

[Lee 2020] Y. Lee, Y. Liu, K. Desnos, L. Barford, and S. S. Bhattacharyya.
Passive-active flowgraphs for efficient modeling and design of signal
processing systems. Journal of Signal Processing Systems, 92(10):1133-1151,
October 2020.

The welter package depends on the following packages: DICE, LIDE.

The welter package is a plug-in to the LIDE package. Once DICE and LIDE
are started up in one's Bash environment, welter can be started up
by running:

dxloadplugin lide welter

The command name prefix for this package is welter.

For more information about installing and starting up DFW packages, see:

[1] https://code.umd.edu/dspcad-pub/dspcadwiki/-/wikis/software/DSPCAD-Framework-Base-Packages

[2] https://code.umd.edu/dspcad-pub/dspcadwiki/-/wikis/software/DSPCAD-Framework-Plug-In-Packages

The repositories for downloading DICE and LIDE are at:

[3] https://code.umd.edu/dspcad-pub/dice.git

[4] https://code.umd.edu/dspcad-pub/lide.git


Release created on Fri Feb 11 18:09:15 EST 2022
